<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr. Prakruthi. J</h1></div><br>
             <div class="row">
                 <div class="col-md-3">  <img src="images/doctor/prakruthi.jpg" class="card-img" alt="..." height="230"> </div>
                 <div class="col-md-9">

                     <p>
                         <strong>MD in General Medicine from Kempegowda Institute of Medical Sciences Bangalore, Karnataka and DNB in General Medicine from New Delhi, India. </strong>
Dr. Prakruthi. J, is currently a Fellowship Student in Immunology & Rheumatology at the ChanRe Rheumatology & Immunology Center & Research. . She has completed her MD in General Medicine from Kempegowda Institute of Medical Sciences Bangalore, Karnataka and DNB in General Medicine from New Delhi, India. She has undergone Post Graduate program in Cardiology and Rheumatology in Johns Hopkins University school of Medicine , Baltimore. 
                     </p>
                     <p>
                        She has also done BLS and ACLS Provider and Instructor Course from American Heart Association and also pursuing the online course on Rheumatology – EULAR 11 th Online Course on Rheumatology.
                     </p>
                     <p>
                       She has worked as Senior Resident in Tata Memorial Hospital, Mumbai for 1 year followed by that worked as Assistant Professor in Dept. of General Medicine in Yenepoya Medical College, Magalore for 2.5 years.
                     </p>
                     <p>Her areas of special interests: Rheumatology, ACLS and BLS, Simulation teaching, Infectious Diseases and Interpretation of ECGs.</p>
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>