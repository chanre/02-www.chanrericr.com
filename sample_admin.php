<?php 

session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:admin.php');
}

else{
  if(isset($_GET['pid']))
{
  $id=intval($_GET['pid']);
  $query1=mysqli_query($con,"update samplecollection set Is_Active=1 where id='$id'");
}
?>


<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style>
th{
	background-color:#53a6d5;
	color:white;
}
</style>
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <header id="top" class="top_navbar">
            <div class="container">
                <nav class="navbar  navbar-expand-lg navbar-light ">
                    <a class="navbar-brand" href="/admindashboard.php"><img src="images/chanrelogo.png" alt="" width="250" /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon">
                        <i class="fa fa-bars"></i>
                      </span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                          <ul class="navbar-nav"> 
                            <li class="nav-item">
                                <a class="nav-link" href="admindashboard.php">Main Dashboard</a>
                            </li>
                           
                             <li class="nav-item">
                                <a class="nav-link" href="sample-collected.php">Sample Collected</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="pddsraadmin.php">PDDSRA</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="logout.php">Logout</a>
                            </li>
                        </ul>
                  
                    </div>
                </nav>
            </div>
        </header>

        <div class="container">
            <div class="row">
                <div class="col-md-10">
                     <table class="table table-bordered">
    <thead>
      <tr>
		    <th>Sl no</th>
        <th>Name</th>
        <th>Contact</th>
        <th>Age</th>
        <th>Address</th>
        <th>Doctor</th>
        <th>state</th>
        <th>City</th>
        <th>PIN</th>
        <th>Test</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        <?php $query=mysqli_query($con,"select * from samplecollection Where Is_Active = '0' ");
        while ($rows=mysqli_fetch_array($query)) {
         ?>
      <tr>
		    <td><?php echo htmlentities($rows['id']); ?></td>
        <td><?php echo htmlentities($rows['name']); ?></td>
    		<td><?php echo htmlentities($rows['contact']); ?></td>
    		<td><?php echo htmlentities($rows['age']); ?></td>
        <td><?php echo htmlentities($rows['address']); ?></td>
        <td><?php echo htmlentities($rows['doctor']); ?></td>
        <td><?php echo htmlentities($rows['state']); ?></td>
        <td><?php echo htmlentities($rows['city']); ?></td>
        <td><?php echo htmlentities($rows['pin']); ?></td>
        <td><?php echo htmlentities($rows['test']); ?></td>
        
          <td><a href="sample_admin.php?pid=<?php echo htmlentities($rows['id']);?>&&action=del" onclick="return confirm('Are You sure the appointment is not confirmed?')">Collected</a></td>
      </tr>
  <?php } ?>
    </tbody>
  </table>
                </div>
            </div>
        </div>
         
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>
<?php } ?>