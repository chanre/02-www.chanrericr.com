<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- <link rel="stylesheet" type="text/css" href="css/mystyle.css"> -->

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

   <style type="text/css">
      label{padding-left: 2px;}
      input[type='text']{width: 100%;}
      input[type='date']{width: 100%;}
      textarea{width: 100%;}
      select{width: 100%;}
/*table tr td{padding: 2px;}*/
.myrow{
    border-radius: 10px;



}
h3{
    font-size: 20px;
    font-weight: bold;
    background-color: #23232378;
    color: white;
    text-align: center;
    line-height: 55px;
    border-radius: 10px;
    padding-left: 10px; 
}

table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}
th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}



</style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
        <div class="container-fluid">
            <div class="row myrow">
				
               <div class="col-md-2">
			   <?php include('layout/leftnav.php');?>
			   </div>
                <div class="col-md-10">
				<?php $id=intval($_GET['pid']);
							$query= mysqli_query($con,"Select * from radmdata where id='$id'");
								while ($rows=mysqli_fetch_array($query)) {
							 ?>	
                    <form method="post">
                       
                        <h3>Hydroxychloroquine treatment in RA patients with diabetes</h3>
                        <br>

                        <div class="row">
                        <div class="col-md-2"> <label>Pateint Id</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['patientid']);?></div>
                        <div class="col-md-2"> <label>Date</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['pdate']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>Name</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['pname']);?></div>
                        <div class="col-md-2"> <label>Date of Birth</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['dob']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"> <label>Gender</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['gender']);?></div>
                        <div class="col-md-2"><label>Address</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['address']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"> <label>Contact</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['contact']);?></div>
                        <div class="col-md-2"><label>Email</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['email']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>Annual Income</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['income']);?></div>
                        <div class="col-md-2"><label>Height</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['height']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>Weight</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['weight']);?></div>
                        <div class="col-md-2"><label>BMI</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['bmi']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>smoker</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['smoker']);?></div>
                        <div class="col-md-2"><label>Alcohol</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['alcohol']);?></div>
                        </div>
                        <hr>
                        <h3>Clinical History</h2>
                        <hr>
                        <div class="row">
                        <div class="col-md-2"><label>Total Duration of Illness</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['illness']);?></div>
                        <div class="col-md-2"><label>EMS (Mts)</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['ems']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>VAS (pain):</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['vas']);?></div>
                        <div class="col-md-2">Patient global (RA): </label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['pgra']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>Patient global (General): </label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['pgg']);?></div>
                        <div class="col-md-2">Physician’s global </label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['phg']);?></div>
                        </div>
                        <hr>
                        <h3>Functional class</h2>
                        <hr>
                        <div class="row">
                        <div class="col-md-2"><label>SJC </label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['sjc']);?></div>
                        <div class="col-md-2">TJC</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['tjc']);?></div>
                        </div>
                          <div class="row">
                        <div class="col-md-2"><label>Hb</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['hb']);?></div>
                        <div class="col-md-2">ESR</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['esr']);?></div>
                        </div>
                         <div class="row">
                        <div class="col-md-2"><label>CRP</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['crp']);?></div>
                        <div class="col-md-2">CDAI</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['cdai']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>DAS</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['das']);?></div>
                        </div>
                        <hr>
                        <h3>Co-morbidities: </h2>
                        <hr>
                        <div class="row">
                        <div class="col-md-2"><label>IHD</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['ihd']);?></div>
                        <div class="col-md-2">DM</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['dm']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>HT</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['ht']);?></div>
                        <div class="col-md-2">Lipids</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['lipids']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>Gout or hyperuricemia</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['goh']);?></div>
                        <div class="col-md-2">Major infections in past needing hospitalization</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['minh']);?></div>
                        </div>
                        <div class="row">
                        <div class="col-md-2"><label>Herpes</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['herpes']);?></div>
                        <div class="col-md-2">CA or malignancy</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['cam']);?></div>
                        </div>
                        <hr>
                        <h3>Drug treatments for RA </h2>
                        <hr>

                        <div class="row">
                            <div style="overflow-x:auto;">
                              <table>
                                <tr>
                                  <th>Drug DMARD NSAID  Biologics</th>
                                  <th>Start</th>
                                  <th>Stop</th>
                                  <th>Total Dose</th>
                                  <th>Reason For Stopping</th>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['dd1']);?></td>
                                  <td><?php echo htmlentities($rows['ddst1']);?></td>
                                  <td><?php echo htmlentities($rows['ddsp1']);?></td>
                                  <td><?php echo htmlentities($rows['ddtd1']);?></td>
                                  <td><?php echo htmlentities($rows['ddrs1']);?></td>
                                </tr>
                                 <tr>
                                    <td><?php echo htmlentities($rows['dd2']);?></td>
                                  <td><?php echo htmlentities($rows['ddst2']);?></td>
                                  <td><?php echo htmlentities($rows['ddsp2']);?></td>
                                  <td><?php echo htmlentities($rows['ddtd2']);?></td>
                                  <td><?php echo htmlentities($rows['ddrs2']);?></td>
                                </tr>
                                 <tr>
                                    <td><?php echo htmlentities($rows['dd3']);?></td>
                                  <td><?php echo htmlentities($rows['ddst3']);?></td>
                                  <td><?php echo htmlentities($rows['ddsp3']);?></td>
                                  <td><?php echo htmlentities($rows['ddtd3']);?></td>
                                  <td><?php echo htmlentities($rows['ddrs3']);?></td>
                                </tr>
                                 <tr>
                                  <td><?php echo htmlentities($rows['dd4']);?></td>
                                  <td><?php echo htmlentities($rows['ddst4']);?></td>
                                  <td><?php echo htmlentities($rows['ddsp4']);?></td>
                                  <td><?php echo htmlentities($rows['ddtd4']);?></td>
                                  <td><?php echo htmlentities($rows['ddrs4']);?></td>
                                </tr>
                                 <tr>
                                    <td><?php echo htmlentities($rows['dd5']);?></td>
                                  <td><?php echo htmlentities($rows['ddst5']);?></td>
                                  <td><?php echo htmlentities($rows['ddsp5']);?></td>
                                  <td><?php echo htmlentities($rows['ddtd5']);?></td>
                                  <td><?php echo htmlentities($rows['ddrs5']);?></td>
                                </tr>
                                 <tr>
                                    <td><?php echo htmlentities($rows['dd6']);?></td>
                                  <td><?php echo htmlentities($rows['ddst6']);?></td>
                                  <td><?php echo htmlentities($rows['ddsp6']);?></td>
                                  <td><?php echo htmlentities($rows['ddtd6']);?></td>
                                  <td><?php echo htmlentities($rows['ddrs6']);?></td>
                                </tr>
                               
                              </table>
                            </div>
                           
                        </div> <hr>
                        <h3>Details of Diabetes</h2>
                        <hr>
                        <div class="row">
                        <div class="col-md-2"><label>Duration of Diabetes</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['duration']);?></div>
                        <div class="col-md-2">Type</label></div>
                        <div class="col-md-3"><?php echo htmlentities($rows['type']);?></div>
                        </div>
                        <hr>
                        <h3>Any complications related to DM</h2>
                        <hr>
                        <div class="row">
                         <div style="overflow-x:auto;">
                              <table>
                                <tr>
                                  <th>Complications</th>
                                  <th>Since when</th>
                                  <th>Management</th>
                                 
                                </tr>
                                <tr>
                                  <td>Retinopathy</td>
                                  <td><?php echo htmlentities($rows['rtsw']);?></td>
                                  <td><?php echo htmlentities($rows['rtmt']);?></td>                                 
                                </tr>
                                <tr>
                                  <td>Nephropathy</td>
                                  <td><?php echo htmlentities($rows['nesw']);?></td>
                                  <td><?php echo htmlentities($rows['nemt']);?></td>                                  
                                </tr>
                                <tr>
                                  <td>Neuropathy</td>
                                  <td><?php echo htmlentities($rows['nusw']);?></td>
                                  <td><?php echo htmlentities($rows['numt']);?></td>
                                </tr>
                                <tr>
                                  <td>Angiopathy</td>
                                  <td><?php echo htmlentities($rows['answ']);?></td>
                                  <td><?php echo htmlentities($rows['anmt']);?></td>
                                </tr>
                                <tr>
                                  <td>Micro angiopathy</td>
                                  <td><?php echo htmlentities($rows['misw']);?></td>
                                  <td><?php echo htmlentities($rows['mimt']);?></td>
                                </tr>
                                <tr>
                                  <td>Macroangiopathy</td>
                                  <td><?php echo htmlentities($rows['masw']);?></td>
                                  <td><?php echo htmlentities($rows['mamt']);?></td>
                                </tr>
                            <tr>
                                  <td>Cardiovascular</td>
                                  <td><?php echo htmlentities($rows['casw']);?></td>
                                  <td><?php echo htmlentities($rows['camt']);?></td>
                                </tr>
                              </table>
                            </div>
                        </div>
                        <hr>
                        <h3>Drug treatment of DM</h2>
                        <hr>
                         <div class="row">
                            <div style="overflow-x:auto;">
                              <table>
                                <tr>
                                  <th>Drug</th>
                                  <th>Start</th>
                                  <th>Stop</th>
                                  <th>Total Dose</th>
                                  <th>Reason For Stopping</th>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['drug1']);?></td>
                                  <td><?php echo htmlentities($rows['start1']);?></td>
                                  <td><?php echo htmlentities($rows['stop2']);?></td>
                                  <td><?php echo htmlentities($rows['total1']);?></td>
                                  <td><?php echo htmlentities($rows['reason1']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['drug2']);?></td>
                                  <td><?php echo htmlentities($rows['start2']);?></td>
                                  <td><?php echo htmlentities($rows['stop2']);?></td>
                                  <td><?php echo htmlentities($rows['total2']);?></td>
                                  <td><?php echo htmlentities($rows['reason2']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['drug3']);?></td>
                                  <td><?php echo htmlentities($rows['start3']);?></td>
                                  <td><?php echo htmlentities($rows['stop3']);?></td>
                                  <td><?php echo htmlentities($rows['total3']);?></td>
                                  <td><?php echo htmlentities($rows['reason3']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['drug4']);?></td>
                                  <td><?php echo htmlentities($rows['start4']);?></td>
                                  <td><?php echo htmlentities($rows['stop4']);?></td>
                                  <td><?php echo htmlentities($rows['total4']);?></td>
                                  <td><?php echo htmlentities($rows['reason4']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['drug5']);?></td>
                                  <td><?php echo htmlentities($rows['start5']);?></td>
                                  <td><?php echo htmlentities($rows['stop5']);?></td>
                                  <td><?php echo htmlentities($rows['total5']);?></td>
                                  <td><?php echo htmlentities($rows['reason5']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['drug6']);?></td>
                                  <td><?php echo htmlentities($rows['start6']);?></td>
                                  <td><?php echo htmlentities($rows['stop6']);?></td>
                                  <td><?php echo htmlentities($rows['total6']);?></td>
                                  <td><?php echo htmlentities($rows['reason6']);?></td>
                                </tr>
                               
                              </table>
                            </div>
                        </div>
                          <div class="row">
                            <div style="overflow-x:auto;">
                              <table>
                                <tr>
                                  <th>Insulin</th>
                                  <th>Start</th>
                                  <th>Stop</th>
                                  <th>Total Dose</th>
                                  <th>Reason For Stopping</th>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['insulin1']);?></td>
                                  <td><?php echo htmlentities($rows['instart1']);?></td>
                                  <td><?php echo htmlentities($rows['instop1']);?></td>
                                  <td><?php echo htmlentities($rows['intotal1']);?></td>
                                  <td><?php echo htmlentities($rows['inreason1']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['insulin2']);?></td>
                                  <td><?php echo htmlentities($rows['instart2']);?></td>
                                  <td><?php echo htmlentities($rows['instop2']);?></td>
                                  <td><?php echo htmlentities($rows['intotal2']);?></td>
                                  <td><?php echo htmlentities($rows['inreason2']);?></td>
                                </tr>
                                <tr>
                                  <td><?php echo htmlentities($rows['insulin3']);?></td>
                                  <td><?php echo htmlentities($rows['instart3']);?></td>
                                  <td><?php echo htmlentities($rows['instop3']);?></td>
                                  <td><?php echo htmlentities($rows['intotal3']);?></td>
                                  <td><?php echo htmlentities($rows['inreason3']);?></td>
                                </tr>
                                
                              </table>
                            </div>
                        </div>
                        <hr>
                        <h3>Investigations</h2>
                        <hr>
                             <div class="row">
                            <div style="overflow-x:auto;">
                              <table>
                                <tr>
                                  <th>investigations</th>
                                  <th>Results </th>
                                </tr>
                                <tr>
                                  <td>CBC-(Hb, TC, DC, Platelet count, ESR)</td>
                                  <td><?php echo htmlentities($rows['cbc']);?></td>
                                </tr>
                                <tr>
                                  <td>CRP</td>
                                  <td><?php echo htmlentities($rows['invcrp']);?></td> 
                                </tr>
                                <tr>
                                  <td>ALT</td>
                                  <td><?php echo htmlentities($rows['alt']);?></td>
                                </tr>  
                                <tr>
                                  <td>AST</td>
                                  <td><?php echo htmlentities($rows['ast']);?></td>
                                </tr>
                                <tr>
                                  <td>RBS</td>
                                  <td><?php echo htmlentities($rows['rbs']);?></td> 
                                </tr>
                                <tr>
                                  <td>Urine</td>
                                  <td><?php echo htmlentities($rows['urine']);?></td>
                                </tr>  
                                <tr>
                                  <td>Creatinine</td>
                                  <td><?php echo htmlentities($rows['creatinine']);?></td>
                                </tr>
                                <tr>
                                  <td>HBsAg</td>
                                  <td><?php echo htmlentities($rows['hbsag']);?></td> 
                                </tr>
                                <tr>
                                  <td>HCV</td>
                                  <td><?php echo htmlentities($rows['hcv']);?></td>
                                </tr>  
                                <tr>
                                  <td>HIV</td>
                                  <td><?php echo htmlentities($rows['hiv']);?></td>
                                </tr>
                                <tr>
                                  <td>RF titer, methodology</td>
                                  <td><?php echo htmlentities($rows['rf']);?></td> 
                                </tr>
                                <tr>
                                  <td>ANA methodology</td>
                                  <td><?php echo htmlentities($rows['anamethod']);?></td>
                                </tr>                          
                                <tr>
                                  <td>ANA profile or ENA blot</td>
                                  <td><?php echo htmlentities($rows['anaprofile']);?></td>
                                </tr>
                                <tr>
                                  <td>Anti CCP antibody, which generation ELISA,</td>
                                  <td><?php echo htmlentities($rows['anticcp']);?></td> 
                                </tr>
                                <tr>
                                  <td>Quantiferon Gold for TB</td>
                                  <td><?php echo htmlentities($rows['quantgold']);?></td> 
                                </tr>

                              </table>
                            </div>
                        </div>
                        <hr>
                        <h3>Ophthalmology examination</h2>
                        <hr>
                        <div class="row">
                            <div style="overflow-x:auto;">
                              <table>
                                <tr>
                                  <th>Examination</th>
                                  <th>Rt Eye</th>
                                  <th>Lt Eye</th>
                                  <th>Comments</th>
                                </tr>
                                <tr>
                                  <td>Clinical examination</td>
                                  <td><?php echo htmlentities($rows['cert']);?></td>
                                  <td><?php echo htmlentities($rows['celt']);?></td>
                                  <td><?php echo htmlentities($rows['cecom']);?></td>
                                </tr>
                                <tr>
                                  <td>corrected snellens visual acuity</td>
                                  <td><?php echo htmlentities($rows['csvrt']);?></td>
                                  <td><?php echo htmlentities($rows['csvlt']);?></td>
                                  <td><?php echo htmlentities($rows['csvcom']);?></td>
                                </tr>
                                <tr>
                                  <td>Intraocular pressure (measurement by rebound tonometry) </td>
                                  <td><?php echo htmlentities($rows['intrart']);?></td>
                                  <td><?php echo htmlentities($rows['intralt']);?></td>
                                  <td><?php echo htmlentities($rows['intracom']);?></td>
                                </tr> 
                                <tr>
                                  <td>Colour vision check with ischiara chart.</td>
                                  <td><?php echo htmlentities($rows['cvcrt']);?></td>
                                  <td><?php echo htmlentities($rows['cvclt']);?></td>
                                  <td><?php echo htmlentities($rows['cvccom']);?></td>
                                </tr> 
                                <tr>
                                  <td>Anterior segment examination</td>
                                  <td><?php echo htmlentities($rows['asrt']);?></td>
                                  <td><?php echo htmlentities($rows['aslt']);?></td>
                                  <td><?php echo htmlentities($rows['ascom']);?></td>
                                </tr> 
                                <tr>
                                  <td>dilated fundus examination by a retina specialist (HCQ toxicity is graded according to American academy of ophthalmology guidelines.)</td>
                                  <td><?php echo htmlentities($rows['dfrt']);?></td>
                                  <td><?php echo htmlentities($rows['dflt']);?></td>
                                  <td><?php echo htmlentities($rows['dfcom']);?></td>
                                </tr>
                                <tr>
                                  <td>Optical coherence tomography (OCT) scans of macula of both eyes using SD-OCT</td>
                                  <td><?php echo htmlentities($rows['ocrt']);?></td>
                                  <td><?php echo htmlentities($rows['oclt']);?></td>
                                  <td><?php echo htmlentities($rows['occom']);?></td>
                                </tr>  
                                <tr>
                                  <td>visual fields assessment with 24-2 remidio C3FA field analyser</td>
                                  <td><?php echo htmlentities($rows['vfrt']);?></td>
                                  <td><?php echo htmlentities($rows['vflt']);?></td>
                                  <td><?php echo htmlentities($rows['vfcom']);?></td>
                                </tr> 
                                 <tr>
                                  <td>Macula centred colour fundus photos of both eyes using Remidio FOPNM-10</td>
                                  <td><?php echo htmlentities($rows['mcrt']);?></td>
                                  <td><?php echo htmlentities($rows['mclt']);?></td>
                                  <td><?php echo htmlentities($rows['mccom']);?></td>
                                </tr>                             
                              </table>
                            </div>
                        </div>
                        <div class="row">
                            <div align="center" style="margin-left: auto;margin-right: auto;margin-bottom: 30px;"> <br>
                             
                              <br> 
                            </div>
                           
                        </div>                        
                    </form>
								<?php } ?>
                </div>
              
            </div>
        </div>

        <?php include('layout/footer.php'); ?>
    
    </div>

    <script src="js/jquery.min.js"></script>
    

    <script src="js/popper.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>
<?php } ?>