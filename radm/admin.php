<?php
 session_start();
//Database Configuration File
include('config.php');
//error_reporting(0);
if(isset($_POST['login']))
  {
    $uname=$_POST['username'];
    $password=$_POST['password'];
$sql =mysqli_query($con,"SELECT name,email,password FROM admintable WHERE (name='$uname' || email='$uname') and Is_Active = 2 ");
 $num=mysqli_fetch_array($sql);
if($num>0)
{
$hashpassword=$num['password']; 

if (password_verify($password, $hashpassword)) {
$_SESSION['login']=$_POST['username'];
    echo "<script type='text/javascript'> document.location = 'admindashboard.php'; </script>";
  } else {
echo "<script>alert('Wrong Password');</script>";
 
  }
}

else{
echo "<script>alert('User not registered with us');</script>";
  }
 
}
?>


<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <link rel="stylesheet" href="css/responsive.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
       <?php include ('layout/header.php'); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="login">
                    <form align="center" method="post">
                        <h3>Login Form</h3>
                        <hr>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" class="form-control" name="username" aria-describedby="emailHelp" placeholder="Enter your username">
                        
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                      </div>
                      
                     
                     <button class="btn btn-lg btn-primary btn-block"  type="submit" name="login">Login</button> 
                      <div class="form-group">
                     
                      </div>

                    </form>
                </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <?php include('layout/footer.php'); ?>
    
    </div>

    <script src="js/jquery.min.js"></script>
    

    <script src="js/popper.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>