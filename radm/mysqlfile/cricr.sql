-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2020 at 05:54 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cricr`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(14) NOT NULL,
  `name` varchar(80) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `email` varchar(90) NOT NULL,
  `department` varchar(100) NOT NULL,
  `doctor` varchar(100) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `symptoms` varchar(255) NOT NULL,
  `rdoctor` varchar(55) NOT NULL,
  `cdate` varchar(100) NOT NULL,
  `ctime` varchar(100) NOT NULL,
  `registerno` varchar(233) NOT NULL,
  `Is_Active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `name`, `contact`, `email`, `department`, `doctor`, `gender`, `symptoms`, `rdoctor`, `cdate`, `ctime`, `registerno`, `Is_Active`) VALUES
(1, 'shamim', '2147483647', 'shamim@gmail.com', 'Rheumatology', 'Dr.Beenish Nazir', 'Select Gender', '', '', '2020-03-13', '10 : 44 AM', '12345678', 0),
(2, 'shamim sheikh', '2147483647', 'xyz@gmail.com', 'Rheumatology', 'Dr. Ram Krishna Giri', 'male', 'Cough', '', '2020-03-28', '10 : 46 AM', '', 0),
(3, 'shamim sheikh', '2147483647', 'xyz@gmail.com', 'Rheumatology', 'Dr. Ram Krishna Giri', 'male', 'Cough', 'Dr. Reddy', '2020-03-28', '10 : 46 AM', '', 0),
(4, 'Rahul', '2147483647', 'abc@gmail.com', 'Paediatric', 'Dr.Chandrika S Bhat', 'male', 'Feeling cold', 'Dr Varun', '2020-03-28', '3 : 50 AM', '', 0),
(5, 'Gaurav', '1234567890', 'chanre@gmail.com', 'Rheumatology', 'Dr. Ram Krishna Giri', 'male', 'Hello world', 'Dr prasad', '2020-07-16', '10 : 54 AM', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `id` int(11) NOT NULL,
  `AdminUserName` varchar(255) NOT NULL,
  `AdminPassword` varchar(255) NOT NULL,
  `AdminEmailId` varchar(255) NOT NULL,
  `Is_Active` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdationDate` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`id`, `AdminUserName`, `AdminPassword`, `AdminEmailId`, `Is_Active`, `CreationDate`, `UpdationDate`) VALUES
(1, 'admin', '$2y$12$i4LMfeFPQpGSNPTaccIkKuwxAkJ4PhBr3JND7FpXwWFjRvchQn17C', 'chanreadmin@gmail.com', 1, '2018-05-27 17:51:00', '2020-02-25 06:39:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
