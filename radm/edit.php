<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!-- <link rel="stylesheet" type="text/css" href="css/mystyle.css"> -->

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

   <style type="text/css">
      label{padding-left: 2px;}
      input[type='text']{width: 100%;}
      input[type='date']{width: 100%;}
      textarea{width: 100%;}
      select{width: 100%;}
/*table tr td{padding: 2px;}*/
.myrow{
    border-radius: 10px;



}
h3{
    font-size: 20px;
    font-weight: bold;
    background-color: #23232378;
    color: white;
    text-align: center;
    line-height: 55px;
    border-radius: 10px;
    padding-left: 10px; 
}

table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}
th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}



</style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
        <div class="container-fluid">
            <div class="row myrow">
				
               <div class="col-md-2">
			   <?php include('layout/leftnav.php');?>
			   </div>
                <div class="col-md-10">
                    <form method="post">
					<h3>Hydroxychloroquine treatment in RA patients with diabetes</h3>
					<table border="2">
					<tr>
					<th>Name</th>
					<th>Age</th>
					<th>Gender</th>
					<th>Address</th>
					<th>Action</th>
					</tr>
					<?php 
					$fetch=mysqli_query($con,"SELECT * FROM radmdata");
					while($rows=mysqli_fetch_array($fetch))
					{
					?>
					<tr>
					<td><?php echo htmlentities($rows['pname']); ?></td>
					<td><?php echo htmlentities($rows['dob']); ?></td>
					<td><?php echo htmlentities($rows['gender']); ?></td>
					<td><?php echo htmlentities($rows['address']); ?></td>
					<td><a href="editdetails.php?pid=<?php echo htmlentities($rows['id']);?>" class="btn btn-success">Edit Details</a></td>
					</tr>
					<?php } ?> 
					</table>
                        <br>
                        
                    </form>
                </div>
              
            </div>
        </div>

        <?php include('layout/footer.php'); ?>
    
    </div>

    <script src="js/jquery.min.js"></script>
    

    <script src="js/popper.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>
<?php } ?>