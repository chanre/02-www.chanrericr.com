<?php 
include('config.php');	
if(isset($_POST['submit']))
{
$name=$_POST['name'];
$email=$_POST['email'];
$institute=$_POST['institute'];
$address=$_POST['address'];	
$password = $_POST['password'];
$status=1;
$password=password_hash($password, PASSWORD_DEFAULT);
$check=mysqli_query($con,"select * from admintable where email ='$email' ");
     $checkrows=mysqli_num_rows($check);
     if($checkrows>0) {
     echo "<script>alert('Email you have used is already exist in our database...');</script>"; 
     } else {
    $query=mysqli_query($con,"Insert into admintable(name,email,institute,address,password,Is_Active) 
	values('$name','$email','$institute','$address','$password','$status')");
	 }
	if($query)
{
echo "<script> alert('Your request is under review!!')</script>";
}
else{
echo "<script> alert('Your request can not processed.. Please try again!!')</script>";
} 


}
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/mystyle.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="login">
                    <form align="center" method="post">
                        <h3>Registration Form</h3>
                        <hr>
                                <div class="form-group">
                        <label for="">User Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter your User name..">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email..">
                        
                      </div>
                      <div class="form-group">
                        <label for="">Institute</label>
                        <input type="text" name="institute" class="form-control" id="" placeholder="Enter your institute..">
                      </div>
                      <div class="form-group">
                        <label for="">Address</label>
                        <input type="text" name="address" class="form-control" id="" placeholder="Enter your address..">
                      </div>
                      <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" id="" placeholder="Enter Your Password..">
                      </div>
                     <!-- <div class="form-group">
                        <label for="">Confirm Password</label>
                        <input type="password" class="form-control" name="conpassword" id="" placeholder="Password">
                      </div> -->
                      <button type="submit" name="submit" class="btn btn-primary">Register</button>
                      <div class="form-group">
                       <a href="index.php" class="click">Click Here to Login</a>
                      </div>
                    </form>
                </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <?php include('layout/footer.php'); ?>
    
    </div>

    <script src="js/jquery.min.js"></script>
    

    <script src="js/popper.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script src="js/custom.js"></script>

</body>

</html>