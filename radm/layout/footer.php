  <br>
  <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="form-box">
                            
							  <div class="social-icons">
                                <ul>
                                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                               
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 col-xl-3">
                        <div class="single-widget single-widget-margin-top">
                            <div class="widget-title">
                                <h2>Site Links</h2>
                            </div>
                            <div class="footer">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="doctor.php">Doctor</a></li>
                                    <li><a href="about_us.php">About Us</a></li>
                                    <li><a href="https://www.research-assist.com/">Reasearch</a></li>
                                    <li><a href="">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="single-widget">
                            <div class="widget-title">
                                <h2>Quick Links</h2>
                            </div>
                            <div class="footer">
                                <ul>
                                    <li><a href="https://mychanreclinic.com/home">My ChanRe Clinic</a></li>
                                    <li><a href="https://www.chanrediagnostic.com/">ChanRe Diagnostic Laboratory</a></li>
                                    <li><a href="https://www.research-assist.com/">Research</a></li>
                                    <li><a href="https://www.chanrejournals.com/">ChanRe Journals</a></li>
                                    <li><a href="https://www.chanrebookshop.com/">ChanRe Book Shop</a></li>
                                  

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-3">
                        <div class="single-widget">
                            <div class="widget-title">
                                <h2>Contact</h2>
                            </div>
                            <div class="footer">
                                <p><span>Address :</span><br> No. 65 (414), 20th Main,West of Chord road,1st Block, Rajajinagar, Bangalore-10<br><br>
                                </p>
                                <p><span>Phone :</span>  +91 - 080 - 42516699
                                </p>
                                <p><span>Email :</span>
                                    <a href=""> info@chanrericr.com</a>
                                </p>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <p>Copyright &#169; 2020 <a href="javascript:;">ChanRe Rheumatology and Immunology Center and Research</a>. All Rights Reserved.</p>
            </div>
        </div>