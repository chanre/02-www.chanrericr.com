<div class="container">
        <br>
        <div class="row ">
          
        <div class="col-md-8">
         <h1 class="Rheumatology">Rheumatology & Autoimmune Diseases</h1>  
          <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/Chandrashekara.jpg" class="card-img" alt="...">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr.Chandrashekara .S</h5>
                  <p class="card-text">MD, DNB, DM</p>   
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-chandrashekhera.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/beenish.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr.Beenish Nazir</h5>
                  <p class="card-text">MBBS,DNB,PGDR</p>
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-beenish-nazir.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/RKGiri.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Ram Krishna Giri</h5>
                  <p class="card-text">MD (General Medicine)</p>
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-ram-krishna-giri.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
          <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/veena.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Veena Ramachandran,</h5>
                  <p class="card-text">MD (General Medicine)</p>
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-veena-ramachandran.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
             <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/prakruthi.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Prakruthi. J,</h5>
                  <p class="card-text">MD, DNB</p>
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-prakruthi.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/shruti.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Shruti Vidyadhar Paramshetti</h5>
                  <p class="card-text">MD (General Medicine)</p> 
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-shruti.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4"></div>
       
        </div>
      </div>


        <div class="container">
        <br>
        <div class="row ">
        <div class="col-md-8"> 
          <div class="card mb-3" >
            <h1 class="Rheumatology">Paediatric Rheumatology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/chandrika.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr.Chandrika S Bhat</h5>
                  <p class="card-text">MBBS,MD Fellow (PAEDIATRIC RHEUMATOLOGY (U.K))</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-chandrika.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
          <div class="card mb-3" >
            <h1 class="Rheumatology">Allergy & Clinical Immunology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/smitha.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Smitha J N Singh</h5>
                  <p class="card-text">MD (Physiology)</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-smitha-singh.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
           <div class="card mb-3" >
            <h1 class="Rheumatology">General Surgery</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Manojith S S</h5>
                  <p class="card-text">MBBS, MS (General Surgeon), FSGE (Surgical Gastroenterology)</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
          <div class="card mb-3" >
          
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Nagendra K</h5>
                  <p class="card-text">MBBS, MS </p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
           
             <div class="card mb-3" >
            <h1 class="Rheumatology">Dental</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/dilip.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Dilip Bharadwaj</h5>
                  <p class="card-text">BDS</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-dilip.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
           <div class="card mb-3" >
            <h1 class="Rheumatology">Cardiology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/vikram.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Vikram Kolhari</h5>
                  <p class="card-text">MD, DM</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-vikram.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
           <div class="card mb-3" >
          
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/nagesh.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Nagesh M B</h5>
                  <p class="card-text">MBBS, DIP CARD </p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-nagesh.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            
            <div class="card mb-3" >
            <h1 class="Rheumatology">Radiology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Shivanand N D</h5>
                  <p class="card-text">MBBS</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-shivanand.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            <div class="card mb-3" >
            <h1 class="Rheumatology">Urology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/nagarajaiah.jpg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Nagarajaiah Narayanaswamy</h5>
                  <p class="card-text">MS (General Surgery), MCH (Urology).</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-nagarajaiah.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            <div class="card mb-3" >
            <h1 class="Rheumatology">Dermatologist</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/sunil.png" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Sunil Kumar S</h5>
                  <p class="card-text">MBBS, MD - Dermatology</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-sunil.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
<!-- =================================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Chronic Pain Management</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/sasi.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Mr. Sasikumar. M</h5>
                  <p class="card-text">MPT(Ortho), PG. D.Acu, F.ACC.A, CDNT<br></p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-shasikumar.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
          <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                 <img src="images/doctor/rajkannan.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Rajkannan. P</h5>
                  <p class="card-text">MPT, CDNT, D.(FM), Dip. (Acu)</p>  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-rajkannan.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
          <!--=============================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Joint Preservation & Restoration</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/prabhu.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. C. B. Prabhu (Knee & Large Joints)</h5>
                  <p class="card-text">D’Ortho, DNB, FAO, AOAA.</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-prabhu.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>

             <!--=============================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Ophthalmology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Usharani</h5>
                  <p class="card-text">D’Ortho, DNB, FAO, AOAA</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
      <!--=============================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Orthopedics</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/venu.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Venu Madhav</h5>
                  <p class="card-text">MS Ortho, Fellow-Joint Replacement Surgery</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-veenu-madhab.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
                <!--=============================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Neurosurgery</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                 <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Karthik Malepathi</h5>
                  <p class="card-text">MCH (Neurosurgery)</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
<!--=============================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Diabetology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                   <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Radha Rangarajan</h5>
                  <p class="card-text">MBBS, PGDHSC (Diab)</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
          <!--=============================================================== -->
          <div class="card mb-3" >
            <h1 class="Rheumatology">Gastroenterology</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                   <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. E R Siddeshi</h5>
                  <p class="card-text">MD, DM, PGI Chd FACG (USA) PGCC, Dich, Care (AUS)</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div>
            <div class="card mb-3" >
            <h1 class="Rheumatology">Reproductive immunology & High risk pregnancy</h1>
            <div class="row no-gutters">
              <div class="col-md-4">
                  <img src="images/doctor/nandyala.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Nandyala Sundari</h5>
                  <p class="card-text">MBBS, DNB, PGDMLE</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-nandyala.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
		
          </div>

		<div class="card mb-3">
			<div class="row no-gutters">
              <div class="col-md-4">
                  <img src="images/doctor/dr-chaitra.jpg" class="card-img-top" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Chaitra S Nirantara</h5>
                  <p class="card-text">MBBS, MS OBG, DNB, PGDMLE</p>
                  
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-chaitra.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
		</div>

        </div>

        <div class="col-md-4">
      

        </div>

        </div>
      </div>



   
   




       