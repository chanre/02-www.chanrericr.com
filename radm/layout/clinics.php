  <div class="container">
        <br>
        <div class="myheader"><h1 align="center">Our Clinics</h1> <p align="center">Here is our advanced clinics</p></div>
           <div class="row">
               <div class="col-md-4">
                   <div class="card">
                    <div class="card-header">SLE/Lupus Clinic</div>
                    <div class="card-body"><p>Lupus Clinic focuses on the evaluation and management of moderate to severe forms of systemic lupus erythematosus (SLE). Rheumatologists work closely with Nephrologists and other specialists to collaboratively manage all aspects of this complex systemic disease. <br><br></p></div> 
                    <div class="card-footer">Wednesdays(3pm-5pm) <a href="" class="btn btn-success btn-sm">Know More</a></div>
                  </div>
               </div>
               <div class="col-md-4">
                   <div class="card">
                    <div class="card-header">ILD Clinic</div>
                    <div class="card-body"><p>Interstitial Lung Disease (ILD) Clinic is devoted to manage patients with idiopathic pulmonary fibrosis and other forms of interstitial lung diseases, such as autoimmune-associated ILD, bronchiolitis obliterans organizing pneumonia, hypersensitivity pneumonitis, and lymphocytic interstitial pneumonitis. <br></p></div> 
                    <div class="card-footer">Wednesday & Friday(4pm–6pm) <a href="" class="btn btn-success btn-sm">Know More</a></div>
                  </div>
               </div>
               <div class="col-md-4">
                   <div class="card">
                    <div class="card-header">Allergy Clinic</div>
                    <div class="card-body"><p>Allergy Clinic is dedicated to diagnose and treat various allergic conditions caused by hypersensitivity of the immune system to environmental allergens and other allergic reactions. Our clinic emphasize on treating all types of allergies and immune-related diseases.<br><br></p></div> 
                    <div class="card-footer">Monday to Saturday(10am-4pm) <a href="" class="btn btn-success btn-sm">Know More</a></div>
                  </div>
               </div>
           </div>
           <br>

              <div class="row">
               <div class="col-md-4">
                   <div class="card">
                    <div class="card-header">Chronic Pain Clinic</div>
                    <div class="card-body"><p>Chronic Pain Clinic deals with the diagnosis and management of patients with chronic pain. Chronic pain is any pain lasting more than 12 weeks. We offer state-of-the-art techniques such as Dry Needling, Trigger Point Management, and Cognitive Behavioral Therapy.</p></div> 
                    <div class="card-footer">Monday to Saturday(9am-6pm) <a href="" class="btn btn-success btn-sm">Know More</a></div>
                  </div>
               </div>
               <div class="col-md-4">
                   <div class="card">
                    <div class="card-header">Renal Clinic</div>
                    <div class="card-body"><p>Renal Clinic is created to address the kidney related issues by assessing the current status of the Kidney function and later management of Kidney. <br><br><br></p></div> 
                    <div class="card-footer">Fridays(5pm-7pm) <a href="" class="btn btn-success btn-sm">Know More</a></div>
                  </div>
               </div>
               <div class="col-md-4">
                   <div class="card">
                    <div class="card-header">Allergy Clinic</div>
                    <div class="card-body"><p>Allergy Clinic is dedicated to diagnose and treat various allergic conditions caused by hypersensitivity of the immune system to environmental allergens and other allergic reactions. Our clinic emphasize on treating all types of allergies and immune-related diseases.</p></div> 
                    <div class="card-footer">Monday to Saturday(10am-4pm) <a href="" class="btn btn-success btn-sm">Know More</a></div>
                  </div>
               </div>
           </div>
           <br>
       </div>