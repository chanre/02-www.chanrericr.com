<?php 
include('config.php');

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
  <style type="text/css">
  .card{
  	width: 100%;
  	border-radius: 16px;
background: #7ac6ff;

             margin: 5px;
  }
  .lightbox-gallery {
    background-image: linear-gradient(#4A148C, #E53935);
    background-repeat: no-repeat;
    color: #000;
    overflow-x: hidden

}
.lightbox-gallery p {
    color: #000;
}
.lightbox-gallery h2 {
    font-weight: bold;
    margin-bottom: 40px;
    padding-top: 40px;
    color: #fff;
}
@media (max-width:767px) {
    .lightbox-gallery h2 {
        margin-bottom: 25px;
        padding-top: 25px;
        font-size: 24px
    }
}
.lightbox-gallery .intro {
    font-size: 16px;
    max-width: 500px;
    margin: 0 auto 40px
}
.lightbox-gallery .intro p {
    margin-bottom: 0
}

.lightbox-gallery .photos {
    padding-bottom: 20px
}
.lightbox-gallery .item {
    padding-bottom: 30px
}  
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  height: 300px;
}
/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}
/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}
/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}
/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 70%;
  border-left: none;
  height: 300px;
}
.carousel-item img{height: 20%;width: 100%;}
  </style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('../layout/header.php'); ?>
          <div class="container">
    <h3 align="center"></h3>
  <hr>
  <div class="row">
    <div class="col-md-2 mb-3">
        <ul class="nav nav-pills flex-column" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Rheumatology</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Immunology</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Patient awareness program</a>
  </li>
</ul>
    </div>
    <!-- /.col-md-4 -->
        <div class="col-md-10">
      <div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
   <div class="lightbox-gallery">
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Rheumatology Videos</h2>
        </div>
        <div class="row photos">
        	<?php $query=mysqli_query($con,"select * from tblposts");
        	while($rows=mysqli_fetch_array($query)){
        	 ?>
        	
          <div class="card" style="width: 18rem;">
		  <a href="videofile.php?vid=<?php echo htmlentities($rows['id']);?>"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo htmlentities($rows['title']); ?></h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <div class="" align="right">
		    	<a href="videofile.php?vid=<?php echo htmlentities($rows['id']);?>" class="btn btn-success btn-lg btn-round">View</a>
		    </div>
		   </div>
		  </div>
		<?php } ?>
        </div>
    </div>
</div>
  </div>
  <div class="tab-pane fade lightbox-gallery" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="intro">
      <h2 class="text-center">Immunology</h2>
    </div>
    <div class="container" ><br>
            <div class="row">
                 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
            </div>
   </div>
  
   
  </div>
  <div class="tab-pane fade lightbox-gallery" id="contact" role="tabpanel" aria-labelledby="contact-tab">
  <div class="intro">
      <h2 class="text-center">Patient Awareness Program</h2>
    </div>
    <p></p>
    <div class="container" ><br>
            <div class="row">
                 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
				 <div class="card" style="width: 18rem;">
					  <a href="videofile.php"><img class="card-img-top" src="img/play.jpg" alt="Card image cap"></a>
					  <div class="card-body">
					    <h5 class="card-title">Rheumatology</h5>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <div class="" align="right">
					    	<a href="videofile.php" class="btn btn-success btn-lg btn-round">View</a>
					    </div>
					  </div>
				 </div>
            </div>
   </div>
  
  </div>
</div>
    </div>
    <!-- /.col-md-8 -->
  </div>
  
  
  
</div>
          <br>
          
        <?php include('../layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>

</body>

</html>