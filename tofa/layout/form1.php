<?php 
$fileid = intval($_GET['pid']);
$myquery = mysqli_query($con,"SELECT DISTINCT * FROM tofaeval INNER JOIN tofaadverse
  ON tofaeval.fileno = tofaadverse.fileno INNER JOIN tofaneoplasm ON tofaadverse.fileno = tofaneoplasm.fileno 
  WHERE tofaadverse.fileno = '$fileid' ");
while($rows = mysqli_fetch_array($myquery))
{

  ?>

<form method="post">
    <div class="form-row">
        <div class="col-md-3 mb-3">
            <label for="validationCustom01">Name</label>
            <input type="text" class="form-control" name="pname" placeholder="Please enter name" 
            value="<?php echo htmlentities($rows['pname']);?>">
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationCustom02">File no</label>
            <input type="text" class="form-control" name="fileno" placeholder="File No"
            value="<?php echo htmlentities($rows['fileno']);?>" 
            >
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationCustom02">Diagnosis</label>
            <select class="form-control" name="diagnosis">
                <option value="">Choose an option</option>
                <option value="RA" <?php if($rows['diagnosis'] == 'RA') { ?> selected="selected"<?php } ?>>RA</option>
                <option value="SLE"<?php if($rows['diagnosis'] == 'SLE') { ?> selected="selected"<?php } ?>>SLE</option>
                <option value="AS" <?php if($rows['diagnosis'] == 'AS') { ?> selected="selected"<?php } ?>>AS</option>
                <option value="PsA"<?php if($rows['diagnosis'] == 'PsA') { ?> selected="selected"<?php } ?>>PsA</option>
                <option value="Others"<?php if($rows['diagnosis'] == 'Others') { ?> selected="selected"<?php } ?>>Others</option>
            </select>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>

        <div class="col-md-3 mb-3">
            <label for="validationCustom02">Other</label>
            <input type="text" value="<?php echo htmlentities($rows['other_diagnosis']);?>" class="form-control" name="other_diagnosis">
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
    </div>
    <div class="form-row">

        <div class="col-md-3 mb-3">
            <label for="validationCustom04">Age</label>
            <input type="text" value="<?php echo htmlentities($rows['age']);?>" class="form-control" name="age" id="validationCustom04" placeholder="Age">
            <div class="invalid-feedback">
                Please provide age
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationCustom02">Dosage</label>
            <select class="form-control" name="dosage">
                <option value="">Choose an option</option>
                <option value="5mg OD" <?php if($rows['dosage'] == '5mg OD') { ?> selected="selected"<?php } ?>>5 mg OD</option>
                <option value="5mg BD" <?php if($rows['dosage'] == '5mg BD') { ?> selected="selected"<?php } ?>>5 mg BD</option>
                <option value="10mg OD"<?php if($rows['dosage'] == '10mg OD') { ?> selected="selected"<?php } ?> >10 mg OD</option>
                <option value="10mg BD"<?php if($rows['dosage'] == '10mg BD') { ?> selected="selected"<?php } ?>>10 mg BD</option>
                <option value="11mg OD"<?php if($rows['dosage'] == '11mg OD') { ?> selected="selected"<?php } ?>>11 mg OD</option>
                <option value="11mg BD"<?php if($rows['dosage'] == '11mg BD') { ?> selected="selected"<?php } ?>>11 mg BD</option>
            </select>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <input type="text" class="form-control" name="username" value="<?php echo $_SESSION['login']; ?>" hidden>
            <div class="valid-feedback">
                Looks good!
            </div>
        </div>
    </div>
    <hr>
    <table>
        <tr>
            <th  colspan="3">Concomitant use of medication</th>
        </tr>
        <tr>
            <th>Name of the medicine</th>
            <th>Treatment initiation</th>
            <th>Treatment termination</th>
        </tr>
        <tr>
            <td><input type="text" value="<?php echo htmlentities($rows['med1']);?>" class="form-control" name="med1"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med1_init']);?>" class="form-control" name="med1_init"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med1_end']);?>" class="form-control" name="med1_end"></td>
        </tr>
        <tr>
            <td><input type="text" value="<?php echo htmlentities($rows['med2']);?>" class="form-control" name="med2"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med2_init']);?>" class="form-control" name="med2_init"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med2_end']);?>" class="form-control" name="med2_end"></td>
        </tr>

        <tr>
            <td><input type="text" value="<?php echo htmlentities($rows['med3']);?>" class="form-control" name="med3"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med3_init']);?>" class="form-control" name="med3_init"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med3_end']);?>" class="form-control" name="med3_end"></td>
        </tr>
        <tr>
            <td><input type="text" value="<?php echo htmlentities($rows['med4']);?>" class="form-control" name="med4"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med4_init']);?>" class="form-control" name="med4_init"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med4_end']);?>" class="form-control" name="med4_end"></td>
        </tr>
        <tr>
            <td><input type="text" value="<?php echo htmlentities($rows['med5']);?>" class="form-control" name="med5"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med5_init']);?>" class="form-control" name="med5_init"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['med5_end']);?>" class="form-control" name="med5_end"></td>
        </tr>
    </table>
    <hr>
    <!-- Script to hide and show evaluation of commorbidities -->
    <script>
function myFunction() {
  var a= document.getElementById('disable').value;
  if(a == 'No'){
    document.getElementById("asplenia").disabled = true;
    document.getElementById("aspdur").disabled = true;
    document.getElementById("asptreat").disabled = true;
    document.getElementById("chd").disabled = true;
    document.getElementById("chddur").disabled = true;
    document.getElementById("chdtreat").disabled = true;
    document.getElementById("cancer").disabled = true;
    document.getElementById("cancerdur").disabled = true;
    document.getElementById("cancertreat").disabled = true;
    document.getElementById("ckd").disabled = true;
    document.getElementById("ckddur").disabled = true;
    document.getElementById("ckdtreat").disabled = true;
    document.getElementById("cld").disabled = true;
    document.getElementById("clddur").disabled = true;
    document.getElementById("cldtreat").disabled = true;
    document.getElementById("cnd").disabled = true;
    document.getElementById("cnddur").disabled = true;
    document.getElementById("cndtreat").disabled = true;
    document.getElementById("hiv").disabled = true;
    document.getElementById("hivdur").disabled = true;
    document.getElementById("hivtreat").disabled = true;
    document.getElementById("diabetes").disabled = true;
    document.getElementById("diabetes_dur").disabled = true;   
    document.getElementById("diabetes_treat").disabled = true;
    document.getElementById("hyper").disabled = true;
    document.getElementById("hyperdur").disabled = true;
    document.getElementById("hypertreat").disabled = true;
    document.getElementById("immuno").disabled = true;
    document.getElementById("imunodur").disabled = true;
    document.getElementById("imunotreat").disabled = true;
    document.getElementById("mhc").disabled = true;
    document.getElementById("mhcdur").disabled = true;
    document.getElementById("mhctreat").disabled = true;
    document.getElementById("tb").disabled = true;
    document.getElementById("tbdur").disabled = true;
    document.getElementById("tbtreat").disabled = true;
    document.getElementById("ard").disabled = true;
    document.getElementById("arddur").disabled = true;
    document.getElementById("ardtreat").disabled = true;
    document.getElementById("obesity").disabled = true;
    document.getElementById("obesitydur").disabled = true;
    document.getElementById("obesitytreat").disabled = true;
    document.getElementById("clg").disabled = true;
    document.getElementById("clgdur").disabled = true;
    document.getElementById("clgtreat").disabled = true;
    document.getElementById("aother").disabled = true;
    document.getElementById("aotherdur").disabled = true;
    document.getElementById("aothertreat").disabled = true;
  }
  else{
    
    document.getElementById("asplenia").disabled = false;
    document.getElementById("aspdur").disabled = false;
    document.getElementById("asptreat").disabled = false;
    document.getElementById("chd").disabled = false;
    document.getElementById("chddur").disabled = false;
    document.getElementById("chdtreat").disabled = false;
    document.getElementById("cancer").disabled = false;
    document.getElementById("cancerdur").disabled = false;
    document.getElementById("cancertreat").disabled = false;
    document.getElementById("ckd").disabled = false;
    document.getElementById("ckddur").disabled = false;
    document.getElementById("ckdtreat").disabled = false;
    document.getElementById("cld").disabled = false;
    document.getElementById("clddur").disabled = false;
    document.getElementById("cldtreat").disabled = false;
    document.getElementById("cnd").disabled = false;
    document.getElementById("cnddur").disabled = false;
    document.getElementById("cndtreat").disabled = false;
    document.getElementById("hiv").disabled = false;
    document.getElementById("hivdur").disabled = false;
    document.getElementById("hivtreat").disabled = false;
    document.getElementById("diabetes").disabled = false;
    document.getElementById("diabetes_dur").disabled = false;
    document.getElementById("diabetes_treat").disabled = false;
    document.getElementById("hyper").disabled = false;
    document.getElementById("hyperdur").disabled = false;
    document.getElementById("hypertreat").disabled = false;
    document.getElementById("immuno").disabled = false;
    document.getElementById("imunodur").disabled = false;
    document.getElementById("imunotreat").disabled = false;
    document.getElementById("mhc").disabled = false;
    document.getElementById("mhcdur").disabled = false;
    document.getElementById("mhctreat").disabled = false;
    document.getElementById("tb").disabled = false;
    document.getElementById("tbdur").disabled = false;
    document.getElementById("tbtreat").disabled = false;
    document.getElementById("ard").disabled = false;
    document.getElementById("arddur").disabled = false;
    document.getElementById("ardtreat").disabled = false;
    document.getElementById("obesity").disabled = false;
    document.getElementById("obesitydur").disabled = false;
    document.getElementById("obesitytreat").disabled = false;
    document.getElementById("clg").disabled = false;
    document.getElementById("clgdur").disabled = false;
    document.getElementById("clgtreat").disabled = false;
    document.getElementById("aother").disabled = false;
    document.getElementById("aotherdur").disabled = false;
    document.getElementById("aothertreat").disabled = false;

    
   
  }
  
}
    
</script>


    <!-- Script to hide and show evaluation of commorbidities -->
    <table>
        <tr>
            <th >Evaluation of comorbidities</th>
            <td>
                <select class="form-control" id="disable" name="disable" onchange="myFunction()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <th>Comorbidities</th>
            <th></th>
            <th>Duration of illness</th>
            <th>Treatment undertaken</th>
        </tr>

        <tr>
            <td>Asplenia</td>
            <td>
                <select class="form-control" name="asplenia" id="asplenia">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['asplenia'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['asplenia'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['aspdur']);?>" type="text" class="form-control" name="aspdur" id="aspdur"></td>
            <td><input value="<?php echo htmlentities($rows['asptreat']);?>" type="text" class="form-control" name="asptreat" id="asptreat"></td>
        </tr>
        <tr>
            <td>Chronic heart disease (not hypertension)</td>
            <td>
                <select class="form-control" name="chd" id="chd">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['chd'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['chd'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['chddur']);?>" type="text" class="form-control"  name="chddur" id="chddur" ></td>
            <td><input value="<?php echo htmlentities($rows['chdtreat']);?>" type="text" class="form-control" name="chdtreat" id="chdtreat"></td>
        </tr>
        <tr>
            <td>Cancer</td>
            <td>
                <select class="form-control" name="cancer" id="cancer">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['cancer'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['cancer'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['cancerdur']);?>" type="text" class="form-control" name="cancerdur" id="cancerdur"></td>
            <td><input value="<?php echo htmlentities($rows['cancertreat']);?>" type="text" class="form-control" name="cancertreat" id="cancertreat"></td>
        </tr>
        <tr>
            <td>Chronic kidney disease</td>
            <td>
                <select class="form-control" name="ckd" id="ckd">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['ckd'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['ckd'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['ckddur']);?>" type="text" class="form-control" name="ckddur" id="ckddur"></td>
            <td><input value="<?php echo htmlentities($rows['ckdtreat']);?>" type="text" class="form-control" name="ckdtreat" id="ckdtreat"></td>
        </tr>
        <tr>
            <td>Chronic liver disease</td>
            <td>
                <select class="form-control" name="cld" id="cld">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['cld'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['cld'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['clddur']);?>" type="text" class="form-control" name="clddur" id="clddur"></td>
            <td><input value="<?php echo htmlentities($rows['cldtreat']);?>" type="text" class="form-control" name="cldtreat" id="cldtreat"></td>
        </tr>
        <tr>
            <td>Chronic neurologic disorder</td>
            <td>
                <select class="form-control" name="cnd" id="cnd">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['cnd'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['cnd'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['cnddur']);?>" type="text" class="form-control" name="cnddur" id="cnddur"></td>
            <td><input value="<?php echo htmlentities($rows['cndtreat']);?>" type="text" class="form-control" name="cndtreat" id="cndtreat"></td>
        </tr>
        <tr>
            <td>HIV</td>
            <td>
                <select class="form-control" name="hiv" id="hiv">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['hiv'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['hiv'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['hivdur']);?>" type="text" class="form-control" name="hivdur" id="hivdur"></td>
            <td><input value="<?php echo htmlentities($rows['hivtreat']);?>" type="text" class="form-control" name="hivtreat" id="hivtreat"></td>
        </tr>
        <tr>
            <td>Diabetes</td>
            <td>
                <select class="form-control" name="diabetes" id="diabetes">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['diabetes'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['diabetes'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['diabetes_dur']);?>" type="text" class="form-control" name="diabetes_dur" id="diabetes_dur"></td>
            <td><input value="<?php echo htmlentities($rows['diabetes_treat']);?>" type="text" class="form-control" name="diabetes_treat" id="diabetes_treat"></td>
        </tr>
        <tr>
            <td>Hypertension</td>
            <td>
                <select class="form-control" name="hyper" id="hyper"> 
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['hyper'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['hyper'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['hyperdur']);?>" type="text" class="form-control" name="hyperdur" id="hyperdur"></td>
            <td><input value="<?php echo htmlentities($rows['hypertreat']);?>" type="text" class="form-control" name="hypertreat" id="hypertreat"></td>
        </tr>
        <tr>
            <td>Immunodeficiency</td>
            <td>
                <select class="form-control" name="immuno" id="immuno">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['immuno'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['immuno'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['imunodur']);?>" type="text" class="form-control" name="imunodur" id="imunodur"></td>
            <td><input value="<?php echo htmlentities($rows['imunotreat']);?>" type="text" class="form-control" name="imunotreat" id="imunotreat"></td>
        </tr>
        <tr>
            <td>Mental health conditions</td>
            <td>
                <select class="form-control" name="mhc" id="mhc">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['mhc'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['mhc'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['mhcdur']);?>" type="text" class="form-control" name="mhcdur" id="mhcdur"></td>
            <td><input value="<?php echo htmlentities($rows['mhctreat']);?>" type="text" class="form-control" name="mhctreat" id="mhctreat"></td>
        </tr>
        <tr>
            <td>Tuberculosis</td>
            <td>
                <select class="form-control" name="tb" id="tb">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['tb'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['tb'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['tbdur']);?>" type="text" class="form-control" name="tbdur" id="tbdur"></td>
            <td><input value="<?php echo htmlentities($rows['tbtreat']);?>" type="text" class="form-control" name="tbtreat" id="tbtreat"></td>
        </tr>
        <tr>
            <td>Autoimmune Rheumatic Diseases</td>
            <td>
                <select class="form-control" name="ard" id="ard">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['ard'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['ard'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['arddur']);?>" type="text" class="form-control" name="arddur" id="arddur"></td>
            <td><input value="<?php echo htmlentities($rows['ardtreat']);?>" type="text" class="form-control" name="ardtreat" id="ardtreat"></td>
        </tr>
        <tr>
            <td>Obesity</td>
            <td>
                <select class="form-control" name="obesity" id="obesity">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['obesity'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['obesity'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['obesitydur']);?>" type="text" class="form-control" name="obesitydur" id="obesitydur"></td>
            <td><input value="<?php echo htmlentities($rows['obesitytreat']);?>" type="text" class="form-control" name="obesitytreat" id="obesitytreat"></td>
        </tr>
        <tr>
            <td>Chronic lung disease</td>
            <td>
                <select class="form-control" name="clg" id="clg">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['clg'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['clg'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['clgdur']);?>" type="text" class="form-control" name="clgdur" id="clgdur"></td>
            <td><input value="<?php echo htmlentities($rows['clgtreat']);?>" type="text" class="form-control" name="clgtreat" id="clgtreat"></td>
        </tr>
        <tr>
            <td>Any Other</td>
            <td>
                <select class="form-control" name="aother" id="aother">
                    <option value="">Choose an option</option>
                    <option value="Yes" <?php if($rows['aother'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                    <option value="No" <?php if($rows['aother'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                </select>
            </td>
            <td><input value="<?php echo htmlentities($rows['aotherdur']);?>" type="text" class="form-control" name="aotherdur" id="aotherdur"></td>
            <td><input value="<?php echo htmlentities($rows['aothertreat']);?>" type="text" class="form-control" name="aothertreat" id="aothertreat"></td>
        </tr>
        
    </table>
   <br>

<!-- Adverse Event -->

<script>
function adverseEvent() {
  var a= document.getElementById('adverse').value;
  if(a == 'No'){
    document.getElementById("pneudays").disabled = true;
    document.getElementById("pneutreat").disabled = true;
    document.getElementById("pneuoutcome").disabled = true;
    document.getElementById("pneudiscont").disabled = true;
    document.getElementById("infdays").disabled = true;
    document.getElementById("inftreat").disabled = true;
    document.getElementById("infoutcome").disabled = true;
    document.getElementById("infdiscont").disabled = true;
    document.getElementById("sindays").disabled = true;
    document.getElementById("sintreat").disabled = true;
    document.getElementById("sinoutcome").disabled = true;
    document.getElementById("sindiscont").disabled = true;
    document.getElementById("brondays").disabled = true;
    document.getElementById("brontreat").disabled = true;
    document.getElementById("bronoutcome").disabled = true;
    document.getElementById("brondiscont").disabled = true;
    document.getElementById("nasdays").disabled = true;
    document.getElementById("nastreat").disabled = true;
    document.getElementById("nasoutcome").disabled = true;
    document.getElementById("nasdiscont").disabled = true;
    document.getElementById("phardays").disabled = true;
    document.getElementById("phartreat").disabled = true;
    document.getElementById("pharoutcome").disabled = true;
    document.getElementById("phardiscont").disabled = true;
    document.getElementById("diverdays").disabled = true;
    document.getElementById("divertreat").disabled = true;
    document.getElementById("diveroutcome").disabled = true;
    document.getElementById("diverdiscont").disabled = true;
    document.getElementById("pyedays").disabled = true;
    document.getElementById("pyetreat").disabled = true;
    document.getElementById("pyeoutcome").disabled = true;
    document.getElementById("pyediscont").disabled = true;
    document.getElementById("celldays").disabled = true;
    document.getElementById("celltreat").disabled = true;
    document.getElementById("celloutcome").disabled = true;
    document.getElementById("celldiscont").disabled = true;
    document.getElementById("herpdays").disabled = true;
    document.getElementById("herptreat").disabled = true;
    document.getElementById("herpoutcome").disabled = true;
    document.getElementById("herpdiscont").disabled = true;
    document.getElementById("gasdays").disabled = true;
    document.getElementById("gastreat").disabled = true;
    document.getElementById("gasoutcome").disabled = true;
    document.getElementById("gasdiscont").disabled = true;
    document.getElementById("vidays").disabled = true;
    document.getElementById("vitreat").disabled = true;
    document.getElementById("vioutcome").disabled = true;
    document.getElementById("vidiscont").disabled = true;
    document.getElementById("sepdays").disabled = true;
    document.getElementById("septreat").disabled = true;
    document.getElementById("sepoutcome").disabled = true;
    document.getElementById("sepdiscont").disabled = true;
    document.getElementById("urodays").disabled = true;
    document.getElementById("urotreat").disabled = true;
    document.getElementById("urooutcome").disabled = true;
    document.getElementById("urodiscont").disabled = true;
    document.getElementById("necdays").disabled = true;
    document.getElementById("nectreat").disabled = true;
    document.getElementById("necoutcome").disabled = true;
    document.getElementById("necdiscont").disabled = true;
    document.getElementById("bactdays").disabled = true;
    document.getElementById("bacttreat").disabled = true;
    document.getElementById("bactoutcome").disabled = true;
    document.getElementById("bactdiscont").disabled = true;
    document.getElementById("stapdays").disabled = true;
    document.getElementById("staptreat").disabled = true;
    document.getElementById("stapoutcome").disabled = true;
    document.getElementById("stapdiscont").disabled = true;
    document.getElementById("jirodays").disabled = true;
    document.getElementById("jirotreat").disabled = true;
    document.getElementById("jirooutcome").disabled = true;
    document.getElementById("jirodiscont").disabled = true;
    document.getElementById("pneumocodays").disabled = true;
    document.getElementById("pneumocotreat").disabled = true;
    document.getElementById("pneumocooutcome").disabled = true;
    document.getElementById("pneumocodiscont").disabled = true;
    document.getElementById("pneubactdays").disabled = true;
    document.getElementById("pneubacttreat").disabled = true;
    document.getElementById("pneubactoutcome").disabled = true;
    document.getElementById("pneubactdiscont").disabled = true;
    document.getElementById("encedays").disabled = true;
    document.getElementById("encetreat").disabled = true;
    document.getElementById("enceoutcome").disabled = true;
    document.getElementById("encediscont").disabled = true;
    document.getElementById("arthdays").disabled = true;
    document.getElementById("arthtreat").disabled = true;
    document.getElementById("arthoutcome").disabled = true;
    document.getElementById("arthdiscont").disabled = true;

  }
  else{
    
    document.getElementById("pneudays").disabled = false;
    document.getElementById("pneutreat").disabled = false;
    document.getElementById("pneuoutcome").disabled = false;
    document.getElementById("pneudiscont").disabled = false;
    document.getElementById("infdays").disabled = false;
    document.getElementById("inftreat").disabled = false;
    document.getElementById("infoutcome").disabled = false;
    document.getElementById("infdiscont").disabled = false;
    document.getElementById("sindays").disabled = false;
    document.getElementById("sintreat").disabled = false;
    document.getElementById("sinoutcome").disabled = false;
    document.getElementById("sindiscont").disabled = false;
    document.getElementById("brondays").disabled = false;
    document.getElementById("brontreat").disabled = false;
    document.getElementById("bronoutcome").disabled = false;
    document.getElementById("brondiscont").disabled = false;
    document.getElementById("nasdays").disabled = false;
    document.getElementById("nastreat").disabled = false;
    document.getElementById("nasoutcome").disabled = false;
    document.getElementById("nasdiscont").disabled = false;
    document.getElementById("phardays").disabled = false;
    document.getElementById("phartreat").disabled = false;
    document.getElementById("pharoutcome").disabled = false;
    document.getElementById("phardiscont").disabled = false;
    document.getElementById("diverdays").disabled = false;
    document.getElementById("divertreat").disabled = false;
    document.getElementById("diveroutcome").disabled = false;
    document.getElementById("diverdiscont").disabled = false;
    document.getElementById("pyedays").disabled = false;
    document.getElementById("pyetreat").disabled = false;
    document.getElementById("pyeoutcome").disabled = false;
    document.getElementById("pyediscont").disabled = false;
    document.getElementById("celldays").disabled = false;
    document.getElementById("celltreat").disabled = false;
    document.getElementById("celloutcome").disabled = false;
    document.getElementById("celldiscont").disabled = false;
    document.getElementById("herpdays").disabled = false;
    document.getElementById("herptreat").disabled = false;
    document.getElementById("herpoutcome").disabled = false;
    document.getElementById("herpdiscont").disabled = false;
    document.getElementById("gasdays").disabled = false;
    document.getElementById("gastreat").disabled = false;
    document.getElementById("gasoutcome").disabled = false;
    document.getElementById("gasdiscont").disabled = false;
    document.getElementById("vidays").disabled = false;
    document.getElementById("vitreat").disabled = false;
    document.getElementById("vioutcome").disabled = false;
    document.getElementById("vidiscont").disabled = false;
    document.getElementById("sepdays").disabled = false;
    document.getElementById("septreat").disabled = false;
    document.getElementById("sepoutcome").disabled = false;
    document.getElementById("sepdiscont").disabled = false;
    document.getElementById("urodays").disabled = false;
    document.getElementById("urotreat").disabled = false;
    document.getElementById("urooutcome").disabled = false;
    document.getElementById("urodiscont").disabled = false;
    document.getElementById("necdays").disabled = false;
    document.getElementById("nectreat").disabled = false;
    document.getElementById("necoutcome").disabled = false;
    document.getElementById("necdiscont").disabled = false;
    document.getElementById("bactdays").disabled = false;
    document.getElementById("bacttreat").disabled = false;
    document.getElementById("bactoutcome").disabled = false;
    document.getElementById("bactdiscont").disabled = false;
    document.getElementById("stapdays").disabled = false;
    document.getElementById("staptreat").disabled = false;
    document.getElementById("stapoutcome").disabled = false;
    document.getElementById("stapdiscont").disabled = false;
    document.getElementById("jirodays").disabled = false;
    document.getElementById("jirotreat").disabled = false;
    document.getElementById("jirooutcome").disabled = false;
    document.getElementById("jirodiscont").disabled = false;
    document.getElementById("pneumocodays").disabled = false;
    document.getElementById("pneumocotreat").disabled = false;
    document.getElementById("pneumocooutcome").disabled = false;
    document.getElementById("pneumocodiscont").disabled = false;
    document.getElementById("pneubactdays").disabled = false;
    document.getElementById("pneubacttreat").disabled = false;
    document.getElementById("pneubactoutcome").disabled = false;
    document.getElementById("pneubactdiscont").disabled = false;
    document.getElementById("encedays").disabled = false;
    document.getElementById("encetreat").disabled = false;
    document.getElementById("enceoutcome").disabled = false;
    document.getElementById("encediscont").disabled = false;
    document.getElementById("arthdays").disabled = false;
    document.getElementById("arthtreat").disabled = false;
    document.getElementById("arthoutcome").disabled = false;
    document.getElementById("arthdiscont").disabled = false;
   
  }
  
}
    
</script>






    <table>
        <tr>
            <th  colspan="5">
                <h6>Examination of the patient-reported adverse drug reactions and the treatment outcomes</h6> 
            </th>
        </tr>
        <tr>
            <td ><h6>Adverse events</h6></td>
            <td >
                <select class="form-control" onchange="adverseEvent()" name="adverse" id="adverse">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
    </tr>
        <tr >
            <th></th>
            <th>Days after taking the drug</th>
            <th>Treatment given</th>
            <th>Outcomes</th>
            <th>Treatment discontinuation</th>
        </tr>

        <tr >
            <th colspan="5">Infections and infestations </th>
        </tr>
        <tr>
            <td>Pneumonia </td>
            <td><input type="text" value="<?php echo htmlentities($rows['pneudays']);?>" class="form-control" name="pneudays" id="pneudays"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['pneutreat']);?>" class="form-control" name="pneutreat" id="pneutreat"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['pneuoutcome']);?>" class="form-control" name="pneuoutcome" id="pneuoutcome"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['pneudiscont']);?>" class="form-control" name="pneudiscont" id="pneudiscont"></td>
        </tr>
        <tr>
            <td>Influenza</td>
            <td><input type="text" value="<?php echo htmlentities($rows['infdays']);?>" class="form-control" name="infdays" id="infdays"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['inftreat']);?>" class="form-control" name="inftreat" id="inftreat"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['infoutcome']);?>" class="form-control" name="infoutcome" id="infoutcome"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['infdiscont']);?>" class="form-control" name="infdiscont" id="infdiscont"></td>
        </tr>
        <tr>
            <td>Sinusitis</td>
            <td><input type="text" value="<?php echo htmlentities($rows['sindays']);?>" class="form-control" name="sindays" id="sindays"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['sintreat']);?>" class="form-control" name="sintreat" id="sintreat"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['sinoutcome']);?>" class="form-control" name="sinoutcome" id="sinoutcome"></td>
            <td><input type="text" value="<?php echo htmlentities($rows['sindiscont']);?>" class="form-control" name="sindiscont" id="sindiscont"></td>
        </tr>
        <tr>
            <td>Bronchitis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['brondays']);?>" name="brondays" id="brondays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['brontreat']);?>" name="brontreat" id="brontreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['bronoutcome']);?>" name="bronoutcome" id="bronoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['brondiscont']);?>" name="brondiscont" id="brondiscont"></td>
        </tr>
        <tr>
            <td>Nasopharyngitis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nasdays']);?>" name="nasdays" id="nasdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nastreat']);?>" name="nastreat" id="nastreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nasoutcome']);?>" name="nasoutcome" id="nasoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nasdiscont']);?>" name="nasdiscont" id="nasdiscont"></td>
        </tr>
        <tr>
            <td>Pharyngitis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['phardays']);?>" name="phardays" id="phardays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['phartreat']);?>" name="phartreat" id="phartreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pharoutcome']);?>" name="pharoutcome" id="pharoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['phardiscont']);?>" name="phardiscont" id="phardiscont"></td>
        </tr>
        <tr>
            <td>Diverticulitis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diverdays']);?>" name="diverdays" id="diverdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['divertreat']);?>" name="divertreat" id="divertreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diveroutcome']);?>" name="diveroutcome" id="diveroutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diverdiscont']);?>" name="diverdiscont" id="diverdiscont"></td>
        </tr>
        <tr>
            <td>Pyelonephritis </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pyedays']);?>" name="pyedays" id="pyedays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pyetreat']);?>" name="pyetreat" id="pyetreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pyeoutcome']);?>" name="pyeoutcome" id="pyeoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pyediscont']);?>" name="pyediscont" id="pyediscont" ></td>
        </tr>
        <tr>
            <td>Cellulitis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['celldays']);?>" name="celldays" id="celldays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['celltreat']);?>" name="celltreat" id="celltreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['celloutcome']);?>" name="celloutcome" id="celloutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['celldiscont']);?>" name="celldiscont" id="celldiscont"></td>
        </tr>
        <tr>
            <td>Herpes simplex</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['herpdays']);?>" name="herpdays" id="herpdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['herptreat']);?>" name="herptreat" id="herptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['herpoutcome']);?>" name="herpoutcome" id="herpoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['herpdiscont']);?>" name="herpdiscont" id="herpdiscont"></td>
        </tr>
        <tr>
            <td>Gastroenteritis viral</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gasdays']);?>" name="gasdays" id="gasdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gastreat']);?>" name="gastreat" id="gastreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gasoutcome']);?>" name="gasoutcome" id="gasoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gasdiscont']);?>" name="gasdiscont" id="gasdiscont"></td>
        </tr>
        <tr>
            <td>Viral infection </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vidays']);?>" name="vidays" id="vidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vitreat']);?>" name="vitreat" id="vitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vioutcome']);?>" name="vioutcome" id="vioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vidiscont']);?>" name="vidiscont" id="vidiscont"></td>
        </tr>
        <tr>
            <td>Sepsis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sepdays']);?>" name="sepdays" id="sepdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['septreat']);?>" name="septreat" id="septreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sepoutcome']);?>" name="sepoutcome" id="sepoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sepdiscont']);?>" name="sepdiscont" id="sepdiscont"></td>
        </tr>
        <tr>
            <td>Urosepsis </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urodays']);?>" name="urodays" id="urodays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urotreat']);?>" name="urotreat" id="urotreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urooutcome']);?>" name="urooutcome" id="urooutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urodiscont']);?>" name="urodiscont" id="urodiscont"></td>
        </tr>
        <tr>
            <td>Necrotizing fasciitis </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['necdays']);?>" name="necdays" id="necdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nectreat']);?>" name="nectreat" id="nectreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['necoutcome']);?>" name="necoutcome" id="necoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['necdiscont']);?>" name="necdiscont" id="necdiscont"></td>
        </tr>
        <tr>
            <td>Bacteremia</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['bactdays']);?>" name="bactdays"  id="bactdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['bacttreat']);?>" name="bacttreat" id="bacttreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['bactoutcome']);?>" name="bactoutcome" id="bactoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['bactdiscont']);?>" name="bactdiscont" id="bactdiscont"></td>
        </tr>
        <tr>
            <td>Staphylococcal bacteremia</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['stapdays']);?>" name="stapdays" id="stapdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['staptreat']);?>" name="staptreat" id="staptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['stapoutcome']);?>" name="stapoutcome" id="stapoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['stapdiscont']);?>" name="stapdiscont" id="stapdiscont"></td>
        </tr>

        <tr>
            <td>Pneumocystis jirovecii pneumonia</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['jirodays']);?>" name="jirodays" id="jirodays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['jirotreat']);?>" name="jirotreat" id="jirotreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['jirooutcome']);?>" name="jirooutcome" id="jirooutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['jirodiscont']);?>" name="jirodiscont" id="jirodiscont"></td>
        </tr>
        <tr>
            <td>Pneumonia pneumococcal</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneumocodays']);?>" name="pneumocodays" id="pneumocodays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneumocotreat']);?>" name="pneumocotreat" id="pneumocotreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneumocooutcome']);?>" name="pneumocooutcome" id="pneumocooutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneumocodiscont']);?>" name="pneumocodiscont" id="pneumocodiscont"></td>
        </tr>
        <tr>
            <td>Pneumonia bacterial</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneubactdays']);?>" name="pneubactdays" id="pneubactdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneubacttreat']);?>" name="pneubacttreat" id="pneubacttreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneubactoutcome']);?>" name="pneubactoutcome" id="pneubactoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pneubactdiscont']);?>" name="pneubactdiscont" id="pneubactdiscont"></td>
        </tr>
        <tr>
            <td>Encephalitis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['encedays']);?>" name="encedays" id="encedays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['encetreat']);?>" name="encetreat" id="encetreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['enceoutcome']);?>" name="enceoutcome" id="enceoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['encediscont']);?>" name="encediscont" id="encediscont"></td>
        </tr>
        <tr>
            <td>Arthritis bacterial</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['arthdays']);?>" name="arthdays" id="arthdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['arthtreat']);?>" name="arthtreat" id="arthtreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['arthoutcome']);?>" name="arthoutcome" id="arthoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['arthdiscont']);?>" name="arthdiscont" id="arthdiscont"></td>
        </tr>
        <tr> 
            <td ><h6>Infection on special interest</h6></td>
            <td >
                <select class="form-control" name="infection" id="infection" onchange="infectionEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Herpes zoster</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hzodays']);?>" name="hzodays" id="hzodays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hzotreat']);?>" name="hzotreat" id="hzotreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hzooutcome']);?>" name="hzooutcome" id="hzooutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hzodiscont']);?>" name="hzodiscont" id="hzodiscont"></td>
        </tr>
        <tr>
            <td>Tuberculosis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tuberculosisdays']);?>" name="tuberculosisdays" id="tuberculosisdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tuberculosistreat']);?>" name="tuberculosistreat" id="tuberculosistreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tuberculosisoutcome']);?>" name="tuberculosisoutcome" id="tuberculosisoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tuberculosisdiscont']);?>" name="tuberculosisdiscont" id="tuberculosisdiscont"></td>
        </tr>
        <tr>
            <td>Disseminated TB </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dtbdays']);?>" name="dtbdays" id="dtbdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dtbtreat']);?>" name="dtbtreat" id="dtbtreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dtboutcome']);?>" name="dtboutcome" id="dtboutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dtbdiscont']);?>" name="dtbdiscont" id="dtbdiscont"></td>
        </tr>
        <tr>
            <td>Atypical mycobacterial infection </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['amidays']);?>" name="amidays" id="amidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['amitreat']);?>" name="amitreat" id="amitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['amioutcome']);?>" name="amioutcome" id="amioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['amidiscont']);?>" name="amidiscont" id="amidiscont"></td>
        </tr>
        <tr>
            <td>Cytomegalovirus infection </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['cytodays']);?>" name="cytodays" id="cytodays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['cytotreat']);?>" name="cytotreat" id="cytotreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['cytooutcome']);?>" name="cytooutcome" id="cytooutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['cytodiscont']);?>" name="cytodiscont" id="cytodiscont"></td>
        </tr>
        <tr>
            <td>Tuberculosis of central nervous system</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tcsdays']);?>" name="tcsdays" id="tcsdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tcstreat']);?>" name="tcstreat" id="tcstreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tcsoutcome']);?>" name="tcsoutcome" id="tcsoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['tcsdiscont']);?>" name="tcsdiscont" id="tcsdiscont"></td>
        </tr>
        <tr>
            <td>Meningitis cryptococcal</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['mcrypdays']);?>" name="mcrypdays" id="mcrypdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['mcryptreat']);?>" name="mcryptreat" id="mcryptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['mcrypoutcome']);?>" name="mcrypoutcome" id="mcrypoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['mcrypdiscont']);?>" name="mcrypdiscont" id="mcrypdiscont"></td>
        </tr>
        <tr>
            <td>Mycobacterium avium complex infection</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['macidays']);?>" name="macidays" id="macidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['macitreat']);?>" name="macitreat" id="macitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['macioutcome']);?>" name="macioutcome" id="macioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['macidiscont']);?>" name="macidiscont" id="macidiscont"></td>
        </tr>
        <tr>
            <td ><h6>Neoplasms benign, malignant and unspecified (incl cysts and polyps)</h6></td>
            <td >
                <select class="form-control" name="neoplasm" id="neoplasm" onchange="neplasmEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Non-melanoma skin cancers</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nmscdays']);?>" name="nmscdays" id="nmscdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nmsctreat']);?>" name="nmsctreat" id="nmsctreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nmsctreat']);?>" name="nmsctreat" id="nmscoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nmscdiscont']);?>" name="nmscdiscont" id="nmscdiscont"></td>
        </tr>
        <tr>
            <td>Melanoma</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['meladays']);?>" name="meladays" id="meladays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['melatreat']);?>" name="melatreat" id="melatreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['melaoutcome']);?>" name="melaoutcome" id="melaoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['meladiscont']);?>" name="meladiscont" id="meladiscont"></td>
        </tr>
        <tr>
            <td>Other malignancy</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['omdays']);?>" name="omdays" id="omdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['omtreat']);?>" name="omtreat" id="omtreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['omoutcome']);?>" name="omoutcome" id="omoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['omdiscont']);?>" name="omdiscont" id="omdiscont"></td>
        </tr>
        <tr>
            <td ><h6>Blood and lymphatic system disorders</h6></td>
            <td >
                <select class="form-control" name="lymphatic" id="lymphatic" onchange="lymphaticEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Anemia</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['anemiadays']);?>" name="anemiadays" id="anemiadays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['anemiatreat']);?>" name="anemiatreat" id="anemiatreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['anemiaoutcome']);?>" name="anemiaoutcome" id="anemiaoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['anemiadiscont']);?>" name="anemiadiscont" id="anemiadiscont"></td>
        </tr>
        <tr>
            <td>Leukopenia</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['leukdays']);?>" name="leukdays" id="leukdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['leuktreat']);?>" name="leuktreat" id="leuktreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['leukoutcome']);?>" name="leukoutcome" id="leukoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['leukdiscont']);?>" name="leukdiscont" id="leukdiscont"></td>
        </tr>
        <tr>
            <td>Lymphopenia </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lympdays']);?>" name="lympdays" id="lympdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lymptreat']);?>" name="lymptreat" id="lymptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lympoutcome']);?>" name="lympoutcome" id="lympoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lympdiscont']);?>" name="lympdiscont" id="lympdiscont"></td>
        </tr>
        <tr>
            <td>Neutropenia </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['neutrodays']);?>" name="neutrodays" id="neutrodays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['neutrotreat']);?>" name="neutrotreat" id="neutrotreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['neutrooutcome']);?>" name="neutrooutcome" id="neutrooutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['neutrodiscont']);?>" name="neutrodiscont" id="neutrodiscont"></td>
        </tr>
        <tr>
            <td ><h6>Drug hypersensitivity</h6></td>
            <td >
                <select class="form-control" name="hypersensitivity" id="hypersensitivity" onchange="hypersensitivityEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Angioedema  </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['angidays']);?>" name="angidays" id="angidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['angitreat']);?>" name="angitreat" id="angitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['angioutcome']);?>" name="angioutcome" id="angioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['angidiscont']);?>" name="angidiscont" id="angidiscont"></td>
        </tr>
        <tr>
            <td>Urticaria </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urtidays']);?>" name="urtidays" id="urtidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urtitreat']);?>" name="urtitreat" id="urtitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urtioutcome']);?>" name="urtioutcome" id="urtioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['urtidiscont']);?>" name="urtidiscont" id="urtidiscont"></td>
        </tr>
        <tr>
            <td ><h6>Psychiatric disorders</h6></td>
            <td >
                <select class="form-control" name="psychiatric" id="psychiatric" onchange="psychiatricEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Insomnia   </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['insdays']);?>" name="insdays" id="insdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['instreat']);?>" name="instreat" id="instreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['insoutcome']);?>" name="insoutcome" id="insoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['insdiscont']);?>" name="insdiscont" id="insdiscont"></td>
        </tr>
        <tr>
            <td>Psychosis    </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['psydays']);?>" name="psydays" id="psydays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['psytreat']);?>" name="psytreat" id="psytreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['psyoutcome']);?>" name="psyoutcome" id="psyoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['psydiscont']);?>" name="psydiscont" id="psydiscont"></td>
        </tr>
        <tr>
            <td ><h6>Nervous system disorders</h6></td>
            <td >
                <select class="form-control" name="nsd" id="nsd" onchange="nsdEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Headache</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['headachedays']);?>" name="headachedays" id="headachedays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['headachetreat']);?>" name="headachetreat" id="headachetreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['headacheoutcome']);?>" name="headacheoutcome" id="headacheoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['headachediscont']);?>" name="headachediscont" id="headachediscont"></td>
        </tr>
        <tr>
            <td>Paraesthesia </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pardays']);?>" name="pardays" id="pardays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['partreat']);?>" name="partreat" id="partreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['paroutcome']);?>" name="paroutcome" id="paroutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['pardiscont']);?>" name="pardiscont" id="pardiscont"></td>
        </tr>
        <tr>
            <td ><h6>Vascular disorders</h6></td>
            <td >
                <select class="form-control" name="vascular" id="vascular" onchange="vascularEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Hypertension</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hypdays']);?>" name="hypdays" id="hypdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hyptreat']);?>" name="hyptreat" id="hyptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hypoutcome']);?>" name="hypoutcome" id="hypoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hypdiscont']);?>" name="hypdiscont" id="hypdiscont"></td>
        </tr>
        <tr>
            <td>Venous thromboembolism </td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vendays']);?>" name="vendays" id="vendays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['ventreat']);?>" name="ventreat" id="ventreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['venoutcome']);?>" name="venoutcome" id="venoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vendiscont']);?>" name="vendiscont" id="vendiscont"></td>
        </tr>
        <tr>
            <td ><h6>Respiratory, thoracic and mediastinal disorders (without infection)</h6></td>
            <td >
                <select class="form-control" name="respiratory" id="respiratory" onchange="respiratoryEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Cough</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['coughdays']);?>" name="coughdays" id="coughdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['coughtreat']);?>" name="coughtreat" id="coughtreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['coughtcome']);?>" name="coughtcome" id="coughtcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['coughdiscont']);?>" name="coughdiscont" id="coughdiscont"></td>
        </tr>
        <tr>
            <td>Dyspnea</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dysdays']);?>" name="dysdays" id="dysdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dystreat']);?>" name="dystreat" id="dystreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dysoutcome']);?>" name="dysoutcome" id="dysoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dysdiscont']);?>" name="dysdiscont" id="dysdiscont"></td>
        </tr>
        <tr>
            <td>Sinus congestion</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sinusdays']);?>" name="sinusdays" id="sinusdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sinustreat']);?>" name="sinustreat" id="sinustreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sinusoutcome']);?>" name="sinusoutcome" id="sinusoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['sinusdiscont']);?>" name="sinusdiscont" id="sinusdiscont"></td>
        </tr>
        <tr>
            <td><h6>Gastrointestinal disorders</h6></td>
            <td >
                <select class="form-control" name="gastrointestinal" id="gastrointestinal" onchange="gastrointestinalEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Abdominal pain</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['abpdays']);?>" name="abpdays" id="abpdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['abptreat']);?>" name="abptreat" id="abptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['abpoutcome']);?>" name="abpoutcome" id="abpoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['abpdiscont']);?>" name="abpdiscont" id="abpdiscont"></td>
        </tr>
        <tr>
            <td>Vomiting</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vomidays']);?>" name="vomidays" id="vomidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vomitreat']);?>" name="vomitreat" id="vomitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vomioutcome']);?>" name="vomioutcome" id="vomioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['vomidiscont']);?>" name="vomidiscont" id="vomidiscont"></td>
        </tr>
        <tr>
            <td>Diarrhea</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diadays']);?>" name="diadays" id="diadays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diatreat']);?>" name="diatreat" id="diatreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diaoutcome']);?>" name="diaoutcome" id="diaoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['diadiscont']);?>" name="diadiscont" id="diadiscont"></td>
        </tr>
        <tr>
            <td>Nausea</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['naudays']);?>" name="naudays" id="naudays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nautreat']);?>" name="nautreat" id="nautreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['nauoutcome']);?>" name="nauoutcome" id="nauoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['naudiscont']);?>" name="naudiscont" id="naudiscont"></td>
        </tr>
        <tr>
            <td>Gastritis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gastrdays']);?>" name="gastrdays" id="gastrdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gastrtreat']);?>" name="gastrtreat" id="gastrtreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gastroutcome']);?>" name="gastroutcome" id="gastroutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['gastrdiscont']);?>" name="gastrdiscont" id="gastrdiscont"></td>
        </tr>
        <tr>
            <td>Dyspepsia</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dyspdays']);?>" name="dyspdays" id="dyspdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dysptreat']);?>" name="dysptreat" id="dysptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dyspoutcome']);?>" name="dyspoutcome" id="dyspoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['dyspdiscont']);?>" name="dyspdiscont" id="dyspdiscont"></td>
        </tr>
        <tr>
            <td ><h6>Hepatobiliary disorders</h6></td>
            <td >
                <select class="form-control" name="hepatobiliary" id="hepatobiliary" onchange="hepatobiliaryEvent()">
                    <option value="">Choose an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Hepatic steatosis</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hepdays']);?>" name="hepdays"  id="hepdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['heptreat']);?>" name="heptreat"  id="heptreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hepoutcome']);?>" name="hepoutcome" id="hepoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['hepdiscont']);?>" name="hepdiscont" id="hepdiscont"></td>
        </tr>
        <tr>
            <td>Increase in hepatic enzymes</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iihedays']);?>" name="iihedays" id="iihedays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iihetreat']);?>" name="iihetreat" id="iihetreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iiheoutcome']);?>" name="iiheoutcome" id="iiheoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iihediscont']);?>" name="iihediscont" id="iihediscont"></td>
        </tr>
        <tr>
            <td>Increase in transaminases</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iitdays']);?>" name="iitdays"  id="iitdays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iittreat']);?>" name="iittreat" id="iittreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iitoutcome']);?>" name="iitoutcome" id="iitoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['iitdiscont']);?>" name="iitdiscont" id="iitdiscont"></td>
        </tr>
        <tr>
            <td>Liver function test abnormal</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lftadays']);?>" name="lftadays" id="lftadays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lftatreat']);?>" name="lftatreat" id="lftatreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lftaoutcome']);?>" name="lftaoutcome" id="lftaoutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['lftadiscont']);?>" name="lftadiscont" id="lftadiscont"></td>
        </tr>
        <tr>
            <td>Gamma glutamyl-transferase increased</td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['ggtidays']);?>" name="ggtidays" id="ggtidays"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['ggtitreat']);?>" name="ggtitreat" id="ggtitreat"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['ggtioutcome']);?>" name="ggtioutcome" id="ggtioutcome"></td>
            <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['ggtidiscont']);?>" name="ggtidiscont" id="ggtidiscont"></td>
        </tr>
        
        
    </table>

    <button class="btn btn-primary" type="submit" name="submit">Submit</button>
</form>
<?php } ?>

<script>
function hepatobiliaryEvent(){
  var a= document.getElementById('hepatobiliary').value;
  if(a == 'No'){
    document.getElementById("hepdays").disabled = true;
    document.getElementById("heptreat").disabled = true;
    document.getElementById("hepoutcome").disabled = true;
    document.getElementById("hepdiscont").disabled = true;
    document.getElementById("iihedays").disabled = true;
    document.getElementById("iihetreat").disabled = true;
    document.getElementById("iiheoutcome").disabled = true;
    document.getElementById("iihediscont").disabled = true;
    document.getElementById("iitdays").disabled = true;
    document.getElementById("iittreat").disabled = true;
    document.getElementById("iitoutcome").disabled = true;
    document.getElementById("iitdiscont").disabled = true;
    document.getElementById("lftadays").disabled = true;
    document.getElementById("lftatreat").disabled = true;
    document.getElementById("lftaoutcome").disabled = true;
    document.getElementById("lftadiscont").disabled = true;
    document.getElementById("ggtidays").disabled = true;
    document.getElementById("ggtitreat").disabled = true;
    document.getElementById("ggtioutcome").disabled = true;
    document.getElementById("ggtidiscont").disabled = true;

  }
  else{

    document.getElementById("hepdays").disabled = false;
    document.getElementById("heptreat").disabled = false;
    document.getElementById("hepoutcome").disabled = false;
    document.getElementById("hepdiscont").disabled = false;
    document.getElementById("iihedays").disabled = false;
    document.getElementById("iihetreat").disabled = false;
    document.getElementById("iiheoutcome").disabled = false;
    document.getElementById("iihediscont").disabled = false;
    document.getElementById("iitdays").disabled = false;
    document.getElementById("iittreat").disabled = false;
    document.getElementById("iitoutcome").disabled = false;
    document.getElementById("iitdiscont").disabled = false;
    document.getElementById("lftadays").disabled = false;
    document.getElementById("lftatreat").disabled = false;
    document.getElementById("lftaoutcome").disabled = false;
    document.getElementById("lftadiscont").disabled = false;
    document.getElementById("ggtidays").disabled = false;
    document.getElementById("ggtitreat").disabled = false;
    document.getElementById("ggtioutcome").disabled = false;
    document.getElementById("ggtidiscont").disabled = false;
  
  }
  
}
    
</script>



<script>
function gastrointestinalEvent(){
  var a= document.getElementById('gastrointestinal').value;
  if(a == 'No'){
    document.getElementById("abpdays").disabled = true;
    document.getElementById("abptreat").disabled = true; 
    document.getElementById("abpoutcome").disabled = true;
    document.getElementById("abpdiscont").disabled = true;
    document.getElementById("vomidays").disabled = true;
    document.getElementById("vomitreat").disabled = true;
    document.getElementById("vomioutcome").disabled = true;
    document.getElementById("vomidiscont").disabled = true;
    document.getElementById("diadays").disabled = true;
    document.getElementById("diatreat").disabled = true;
    document.getElementById("diaoutcome").disabled = true;
    document.getElementById("diadiscont").disabled = true;
    document.getElementById("naudays").disabled = true;
    document.getElementById("nautreat").disabled = true;
    document.getElementById("nauoutcome").disabled = true;
    document.getElementById("naudiscont").disabled = true;
    document.getElementById("gastrdays").disabled = true;
    document.getElementById("gastrtreat").disabled = true;
    document.getElementById("gastroutcome").disabled = true;
    document.getElementById("gastrdiscont").disabled = true;
    document.getElementById("dyspdays").disabled = true;
    document.getElementById("dysptreat").disabled = true;
    document.getElementById("dyspoutcome").disabled = true;
    document.getElementById("dyspdiscont").disabled = true;
  }
  else{
    document.getElementById("abpdays").disabled = false;
    document.getElementById("abptreat").disabled = false; 
    document.getElementById("abpoutcome").disabled = false;
    document.getElementById("abpdiscont").disabled = false;
    document.getElementById("vomidays").disabled = false;
    document.getElementById("vomitreat").disabled = false;
    document.getElementById("vomioutcome").disabled = false;
    document.getElementById("vomidiscont").disabled = false;
    document.getElementById("diadays").disabled = false;
    document.getElementById("diatreat").disabled = false;
    document.getElementById("diaoutcome").disabled = false;
    document.getElementById("diadiscont").disabled = false;
    document.getElementById("naudays").disabled = false;
    document.getElementById("nautreat").disabled = false;
    document.getElementById("nauoutcome").disabled = false;
    document.getElementById("naudiscont").disabled = false;
    document.getElementById("gastrdays").disabled = false;
    document.getElementById("gastrtreat").disabled = false;
    document.getElementById("gastroutcome").disabled = false;
    document.getElementById("gastrdiscont").disabled = false;
    document.getElementById("dyspdays").disabled = false;
    document.getElementById("dysptreat").disabled = false;
    document.getElementById("dyspoutcome").disabled = false;
    document.getElementById("dyspdiscont").disabled = false;
  
  }
  
}
    
</script>

<script>
function respiratoryEvent(){
  var a= document.getElementById('respiratory').value;
  if(a == 'No'){
    document.getElementById("coughdays").disabled = true;
    document.getElementById("coughtreat").disabled = true;
    document.getElementById("coughtcome").disabled = true;
    document.getElementById("coughdiscont").disabled = true;
    document.getElementById("dysdays").disabled = true;
    document.getElementById("dystreat").disabled = true;
    document.getElementById("dysoutcome").disabled = true;
    document.getElementById("dysdiscont").disabled = true;
    document.getElementById("sinusdays").disabled = true;
    document.getElementById("sinustreat").disabled = true;
    document.getElementById("sinusoutcome").disabled = true;
    document.getElementById("sinusdiscont").disabled = true; 
  }
  else{
    document.getElementById("coughdays").disabled = false;
    document.getElementById("coughtreat").disabled = false;
    document.getElementById("coughtcome").disabled = false;
    document.getElementById("coughdiscont").disabled = false;
    document.getElementById("dysdays").disabled = false;
    document.getElementById("dystreat").disabled = false;
    document.getElementById("dysoutcome").disabled = false;
    document.getElementById("dysdiscont").disabled = false;
    document.getElementById("sinusdays").disabled = false;
    document.getElementById("sinustreat").disabled = false;
    document.getElementById("sinusoutcome").disabled = false;
    document.getElementById("sinusdiscont").disabled = false;
  
  }
  
}
    
</script>

<script>
function vascularEvent() {
  var a= document.getElementById('vascular').value;
  if(a == 'No'){
    document.getElementById("hypdays").disabled = true;
    document.getElementById("hyptreat").disabled = true;
    document.getElementById("hypoutcome").disabled = true;
    document.getElementById("hypdiscont").disabled = true;
    document.getElementById("vendays").disabled = true;
    document.getElementById("ventreat").disabled = true;
    document.getElementById("venoutcome").disabled = true;
    document.getElementById("vendiscont").disabled = true;
  }
  else{
    document.getElementById("hypdays").disabled = false;
    document.getElementById("hyptreat").disabled = false;
    document.getElementById("hypoutcome").disabled = false;
    document.getElementById("hypdiscont").disabled = false;
    document.getElementById("vendays").disabled = false;
    document.getElementById("ventreat").disabled = false;
    document.getElementById("venoutcome").disabled = false;
    document.getElementById("vendiscont").disabled = false;
  }
  
}
    
</script>


<script>
function nsdEvent() {
  var a= document.getElementById('nsd').value;
  if(a == 'No'){
    document.getElementById("headachedays").disabled = true;
    document.getElementById("headachetreat").disabled = true;
    document.getElementById("headacheoutcome").disabled = true;
    document.getElementById("headachediscont").disabled = true;
    document.getElementById("pardays").disabled = true;
    document.getElementById("partreat").disabled = true;
    document.getElementById("paroutcome").disabled = true;
    document.getElementById("pardiscont").disabled = true;
  }
  else{
    document.getElementById("headachedays").disabled = false;
    document.getElementById("headachetreat").disabled = false;
    document.getElementById("headacheoutcome").disabled = false;
    document.getElementById("headachediscont").disabled = false;
    document.getElementById("pardays").disabled = false;
    document.getElementById("partreat").disabled = false;
    document.getElementById("paroutcome").disabled = false;
    document.getElementById("pardiscont").disabled = false;
  }
  
}
    
</script>

<script>
function psychiatricEvent() {
  var a= document.getElementById('psychiatric').value;
  if(a == 'No'){
    document.getElementById("insdays").disabled = true;
    document.getElementById("instreat").disabled = true;
    document.getElementById("insoutcome").disabled = true;
    document.getElementById("insdiscont").disabled = true;
    document.getElementById("psydays").disabled = true;
    document.getElementById("psytreat").disabled = true;
    document.getElementById("psyoutcome").disabled = true;
    document.getElementById("psydiscont").disabled = true;
  }
  else{
    document.getElementById("insdays").disabled = false;
    document.getElementById("instreat").disabled = false;
    document.getElementById("insoutcome").disabled = false;
    document.getElementById("insdiscont").disabled = false;
    document.getElementById("psydays").disabled = false;
    document.getElementById("psytreat").disabled = false;
    document.getElementById("psyoutcome").disabled = false;
    document.getElementById("psydiscont").disabled = false;
  }
  
}
    
</script>

<script>
function hypersensitivityEvent() {
  var a= document.getElementById('hypersensitivity').value;
  if(a == 'No'){
    document.getElementById("angidays").disabled = true;
    document.getElementById("angitreat").disabled = true;
    document.getElementById("angioutcome").disabled = true;
    document.getElementById("angidiscont").disabled = true;
    document.getElementById("urtidays").disabled = true;
    document.getElementById("urtitreat").disabled = true;
    document.getElementById("urtioutcome").disabled = true;
    document.getElementById("urtidiscont").disabled = true;
    
  }
  else{
    
    document.getElementById("angidays").disabled = false;
    document.getElementById("angitreat").disabled = false;
    document.getElementById("angioutcome").disabled = false;
    document.getElementById("angidiscont").disabled = false;
    document.getElementById("urtidays").disabled = false;
    document.getElementById("urtitreat").disabled = false;
    document.getElementById("urtioutcome").disabled = false;
    document.getElementById("urtidiscont").disabled = false;
    
  }
  
}
    
</script>
<script>
function neplasmEvent() {
  var a= document.getElementById('neoplasm').value;
  if(a == 'No'){
    document.getElementById("nmscdays").disabled = true;
    document.getElementById("nmsctreat").disabled = true;
    document.getElementById("nmscoutcome").disabled = true;
    document.getElementById("nmscdiscont").disabled = true;
    document.getElementById("meladays").disabled = true;
    document.getElementById("melatreat").disabled = true;
    document.getElementById("melaoutcome").disabled = true;
    document.getElementById("meladiscont").disabled = true;
    document.getElementById("omdays").disabled = true;
    document.getElementById("omtreat").disabled = true;
    document.getElementById("omoutcome").disabled = true;
    document.getElementById("omdiscont").disabled = true;
    
   

  }
  else{
    
    document.getElementById("nmscdays").disabled = false;
    document.getElementById("nmsctreat").disabled = false;
    document.getElementById("nmscoutcome").disabled = false;
    document.getElementById("nmscdiscont").disabled = false;
    document.getElementById("meladays").disabled = false;
    document.getElementById("melatreat").disabled = false;
    document.getElementById("melaoutcome").disabled = false;
    document.getElementById("meladiscont").disabled = false;
    document.getElementById("omdays").disabled = false;
    document.getElementById("omtreat").disabled = false;
    document.getElementById("omoutcome").disabled = false;
    document.getElementById("omdiscont").disabled = false;
  }
  
}
    
</script>
<script>
function infectionEvent() {
  var a= document.getElementById('infection').value;
  if(a == 'No'){
    document.getElementById("hzodays").disabled = true;
    document.getElementById("hzotreat").disabled = true;
    document.getElementById("hzooutcome").disabled = true;
    document.getElementById("hzodiscont").disabled = true;
    document.getElementById("tuberculosisdays").disabled = true;
    document.getElementById("tuberculosistreat").disabled = true;
    document.getElementById("tuberculosisoutcome").disabled = true;
    document.getElementById("tuberculosisdiscont").disabled = true;
    document.getElementById("dtbdays").disabled = true;
    document.getElementById("dtbtreat").disabled = true;
    document.getElementById("dtboutcome").disabled = true;
    document.getElementById("dtbdiscont").disabled = true;
    document.getElementById("amidays").disabled = true;
    document.getElementById("amitreat").disabled = true;
    document.getElementById("amioutcome").disabled = true;
    document.getElementById("amidiscont").disabled = true;
    document.getElementById("cytodays").disabled = true;
    document.getElementById("cytotreat").disabled = true;
    document.getElementById("cytooutcome").disabled = true;
    document.getElementById("cytodiscont").disabled = true;
    document.getElementById("tcsdays").disabled = true;
    document.getElementById("tcstreat").disabled = true;
    document.getElementById("tcsoutcome").disabled = true;
    document.getElementById("tcsdiscont").disabled = true;
    document.getElementById("mcrypdays").disabled = true;
    document.getElementById("mcryptreat").disabled = true;
    document.getElementById("mcrypoutcome").disabled = true;
    document.getElementById("mcrypdiscont").disabled = true;
    document.getElementById("macidays").disabled = true;
    document.getElementById("macitreat").disabled = true;
    document.getElementById("macioutcome").disabled = true;
    document.getElementById("macidiscont").disabled = true;

  }
  else{
    
    document.getElementById("hzodays").disabled = false;
    document.getElementById("hzotreat").disabled = false;
    document.getElementById("hzooutcome").disabled = false;
    document.getElementById("hzodiscont").disabled = false;
    document.getElementById("tuberculosisdays").disabled = false;
    document.getElementById("tuberculosistreat").disabled = false;
    document.getElementById("tuberculosisoutcome").disabled = false;
    document.getElementById("tuberculosisdiscont").disabled = false;
    document.getElementById("dtbdays").disabled = false;
    document.getElementById("dtbtreat").disabled = false;
    document.getElementById("dtboutcome").disabled = false;
    document.getElementById("dtbdiscont").disabled = false;
    document.getElementById("amidays").disabled = false;
    document.getElementById("amitreat").disabled = false;
    document.getElementById("amioutcome").disabled = false;
    document.getElementById("amidiscont").disabled = false;
    document.getElementById("cytodays").disabled = false;
    document.getElementById("cytotreat").disabled = false;
    document.getElementById("cytooutcome").disabled = false;
    document.getElementById("cytodiscont").disabled = false;
    document.getElementById("tcsdays").disabled = false;
    document.getElementById("tcstreat").disabled = false;
    document.getElementById("tcsoutcome").disabled = false;
    document.getElementById("tcsdiscont").disabled = false;
    document.getElementById("mcrypdays").disabled = false;
    document.getElementById("mcryptreat").disabled = false;
    document.getElementById("mcrypoutcome").disabled = false;
    document.getElementById("mcrypdiscont").disabled = false;
    document.getElementById("macidays").disabled = false;
    document.getElementById("macitreat").disabled = false;
    document.getElementById("macioutcome").disabled = false;
    document.getElementById("macidiscont").disabled = false;

  }
  
}
    
</script>
<script>
function lymphaticEvent() {
  var a= document.getElementById('lymphatic').value;
  if(a == 'No'){
    document.getElementById("anemiadays").disabled = true;
    document.getElementById("anemiatreat").disabled = true;
    document.getElementById("anemiaoutcome").disabled = true;
    document.getElementById("anemiadiscont").disabled = true;
    document.getElementById("leukdays").disabled = true;
    document.getElementById("leuktreat").disabled = true;
    document.getElementById("leukoutcome").disabled = true;
    document.getElementById("leukdiscont").disabled = true;
    document.getElementById("lympdays").disabled = true;
    document.getElementById("lymptreat").disabled = true;
    document.getElementById("lympoutcome").disabled = true;
    document.getElementById("lympdiscont").disabled = true;
    document.getElementById("neutrodays").disabled = true;
    document.getElementById("neutrotreat").disabled = true;
    document.getElementById("neutrooutcome").disabled = true;
    document.getElementById("neutrodiscont").disabled = true;

    
  }
  else{
    
    document.getElementById("anemiadays").disabled = false;
    document.getElementById("anemiatreat").disabled = false;
    document.getElementById("anemiaoutcome").disabled = false;
    document.getElementById("anemiadiscont").disabled = false;
    document.getElementById("leukdays").disabled = false;
    document.getElementById("leuktreat").disabled = false;
    document.getElementById("leukoutcome").disabled = false;
    document.getElementById("leukdiscont").disabled = false;
    document.getElementById("lympdays").disabled = false;
    document.getElementById("lymptreat").disabled = false;
    document.getElementById("lympoutcome").disabled = false;
    document.getElementById("lympdiscont").disabled = false;
    document.getElementById("neutrodays").disabled = false;
    document.getElementById("neutrotreat").disabled = false;
    document.getElementById("neutrooutcome").disabled = false;
    document.getElementById("neutrodiscont").disabled = false;
  }
  
}
    
</script>