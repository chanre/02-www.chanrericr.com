<form  method="post">
             <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Name</label>
                   <input type="text" class="form-control" name="name"  placeholder="Please enter name"  >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Mrd no</label>
                   <input type="text" class="form-control" name="mrd_no"  placeholder="MRD No"  >
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Date</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                       <input type="date" class="form-control" name="date"  placeholder="Username" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please choose a date</div>
                        </div>
                </div>
                   <div class="col-md-3 mb-3">
                   <!-- <label for="validationCustom01">User Name</label> -->
                   <input type="text" class="form-control" name="username" value="<?php echo $_SESSION['login']; ?>"  hidden >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              </div>
                                  <div class="form-row">
                                      <div class="col-md-6 mb-3">
                                          <label for="validationCustom03">Gender</label>
                                          <select class="form-control"  name="gender">
                                            <option>Select an option</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                          </select>
                                          <div class="invalid-feedback">
                                              Please choose an option.
                                          </div>
                                      </div>
                                      <div class="col-md-3 mb-3">
                                          <label for="validationCustom04">Age</label>
                                          <input type="text" class="form-control" name="age" id="validationCustom04" placeholder="Age" >
                                          <div class="invalid-feedback">
                                              Please provide age
                                          </div>
                                      </div>
                                      <div class="col-md-3 mb-3">
                                          <label for="validationCustom05">Occupation</label>
                                          <input type="text" name="occupation" class="form-control" id="validationCustom05" placeholder="Occupation" >
                                          <div class="invalid-feedback">
                                              Please enter occupation.
                                          </div>
                                      </div>
                                  </div>
                                  <div class="form-row">
                                      <div class="col-md-3 mb-3">
                                          <label for="validationCustom04">PIN Code</label>
                                          <input type="text" class="form-control" name="pin" id="validationCustom04" placeholder="PIN code" >
                                          <div class="invalid-feedback">
                                              Please provide PIN code
                                          </div>
                                      </div>
                                      <div class="col-md-3 mb-3">
                                          <label for="validationCustom05">Town</label>
                                          <input type="text" name="town" class="form-control" id="validationCustom05" placeholder="Town" >
                                          <div class="invalid-feedback">
                                              Please enter town.
                                          </div>
                                      </div>
                                  </div>

              <div class="form-row">
                 <div class="col-md-4 mb-3">
                   <label for="validationCustom01">District</label>
                   <input type="text" class="form-control" name="district"  placeholder="District"  >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-4 mb-3">
                   <label for="validationCustom02">Height</label>
                   <input type="text" class="form-control" name="height"  placeholder="Height"  >
                  <div class="valid-feedback">
                      Looks good!
                  </div>
                  <div class="invalid-feedback">Please enter height</div>
               </div>
               <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Weight</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                       <input type="text" class="form-control" name="weight"  placeholder="Weight" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter weight</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-4 mb-3">
                   <label for="validationCustom01">Highest Level of education completed?</label>
                   <select class="form-control" name="education" >
                    <option value="">Choose an option</option>
                     <option value="No schooling">No schooling or never completed any grade</option>
                     <option value="Elementary school">Elementary school</option>
                     <option value="Vocational school">Vocational school</option>
                     <option value="Secondary school">Secondary school </option>
                     <option value="University">University</option>
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-4 mb-3">
                   <label for="validationCustom02">Smoking</label>
                   <select class="form-control" name="smoking" >
                    <option value="">Choose an option</option>
                     <option value="Current">Current</option>
                     <option value="Former">Former</option>
                     <option value="Never">Never</option>
                     <option value="Unknown">Unknown </option>
                   </select>
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
               <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Substance Abuse</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                       <select class="form-control" name="substance" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>
                     <option value="Unknown">Unknown </option>
                     
                   </select>
                       <div class="invalid-feedback">Please choose a date</div>
                        </div>
                </div>
              </div>


               <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">If Yes?</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="substancey" >
                    <option value="">Choose an option</option>
                     <option value="Alcohol">Alcohol</option>
                     <option value="Drug">Drug</option>
                     <option value="Other">Other</option>
                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              </div>
              <hr>
<h6>2.  PRE-EXISTING CONDITIONS IN THE YEAR PRIOR TO YOUR ACUTE ILLNESS OF  COVID-19</h6>
                <p>In the year prior to the acute illness of COVID-19, has the participant been diagnosed with any of the following conditions?</p>
                <hr>
               <div class="form-row">

                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Asplenia</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="asplenia" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="aspduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Cancer</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="cancer" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <input type="text" class="form-control" name="cancerduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic heart disease (not hypertension)</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="chd" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="chdd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Chronic kidney disease</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="ckd" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                    </div>
                   <input type="text" class="form-control" name="ckdd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>
              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic liver disease</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="cld" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="cldd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Chronic lung disease</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="clud" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                    </div>
                   <input type="text" class="form-control" name="cludd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic neurological disorder</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="cnd" >
                    <option value="">Choose an option</option>
                      <option value="Dementia">Dementia</option>
                     <option value="Stroke">Stroke</option>
                     <option value="Multiple Sclerosis">Multiple Sclerosis</option>  
                     <option value="Parkinsons's disease">Parkinsons's disease</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="cndd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
             <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Diabetes</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="diabetics" >
                    <option value="">Choose an option</option>
                      <option value="Yes">Yes</option>
                     <option value="No">No</option>
                                          
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="diabeticsdur"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               
              </div>

 <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">HIV</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="hiv" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="hivd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If yes, was on ART?</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="art" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
               
              </div>
<!--  
 <div class="form-row">
                 <div class="col-md-3 mb-3">
                   
                   <select class="form-control" name="regimen2" >
                    <option value="">Choose an option</option>
                     <option value="Protease inhibitor-based ART">Protease inhibitor-based ART </option>
                     <option value="NNRTI Based ART">NNRTI Based ART</option> 
                     <option value="Integrase inhibitor-based ART">Integrase inhibitor-based ART</option>                    
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Last viral load test</label>
                   <input type="text" class="form-control" name="viral"  placeholder="Last viral load test" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Last CD4 cell count</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend">@</span>
                    </div>
                   <input type="text" class="form-control" name="cd4"  placeholder="Last CD4 cell count" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>-->
<div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Hypertension:</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="hypertension" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="hyperduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If Yes, participant receive medication?</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="hyparticipant" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                    </div>
                   <input type="text" class="form-control" name="hypduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>


              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Immunodeficiency:</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="immunodeficiency" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="immunoduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Mental health conditions</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="mh" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                    </div>
                   <input type="text" class="form-control" name="mhd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">If yes specify.</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="mspecify" >
                    <option value="">Choose an option</option>
                     <option value="Psychosis">Psychosis</option>
                     <option value="Depression">Depression</option> 
                     <option value="anxiety">Anxiety</option>                    
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
             
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Obesity(BMI>30)</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="obesity" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
               <!--     <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                   <span class="input-group-text" id="inputGroupPrepend">@</span> 
                    </div>
                   <input type="text" class="form-control" name="obesityduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>-->
              </div>

               <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Tuberculosis</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="tb" >
                    <option value="">Choose an option</option>
                     <option value="Active">Active</option>
                     <option value="Previous">Previous</option>  
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="tbd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
              </div>

 <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Autoimmune Rheumatic Diseases (AIRD)</label>
                   <!-- <input type="text" class="form-control" name="district"  placeholder="District"  > -->
                   <select class="form-control" name="aird" >
                    <option value="">Choose an option</option>
                     <option value="Rheumatoid Arthritis">Rheumatoid Arthritis</option>
                     <option value="Ankylosing Spondylitis">Ankylosing Spondylitis</option>
                     <option value="SLE">SLE</option>
                     <option value="Psoriatic Arthritis">Psoriatic Arthritis</option>
                     <option value="Sjogren’s Syndrome">Sjogren’s Syndrome</option>
                     <option value="Scleroderma">Scleroderma</option>
                     <option value="Reactive Arthritis">Reactive Arthritis</option>
                     <option value="Vasculitis">Vasculitis</option>
                     <option value="Dermatomyositis & Polymyositis">Dermatomyositis & Polymyositis</option>
                     <option value="Others">Others</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="airdd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Any Other</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                           </div>
                   <select class="form-control" name="airother" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If yes, Specify</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      <!-- <span class="input-group-text" id="inputGroupPrepend">@</span> -->
                    </div>
                   <input type="text" class="form-control" name="airspecify"  placeholder="specify" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please specify</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-12 mb-12">
                  
                      <section class="card">
                          <header class="card-header">
                              Medication
                          </header>
                          <div class="card-body">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                          <th>Drug</th>
                                         
                                          <th>Current Drug</th>
                                        
                                          
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>Anti Diabetic</td>
                                          <td><input type="text" class="form-control" name="antidiabetic"></td>
                                      </tr>
                                      <tr>
                                          <td>Anti-Hypertensive</td>
                                          <td><input type="text" class="form-control" name="antihypertensive"></td>
                                      </tr>
                                       
                                      <tr>
                                          <td>Anti Epileptic</td>
                                          <td><input type="text" class="form-control" name="epileptic"></td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </section>
                  
                  </div>               
              </div>


              <div class="form-row">
                 <div class="col-md-12 mb-12">
                      <section class="card">
                          <header class="card-header">
                              <!-- Medication -->
                          </header>
                          <div class="card-body">
                              <div class="table-responsive">
                                  <table class="table table-bordered">
                                      <thead>
                                      <tr>
                                          <th>WHO Clinical Classification</th>
                                          <th>Based on available clinical records</th>
                                          <th>Based on self-report, if clinical records are not available</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>Mild</td>
                                          <td><select class="form-control" name="mild[]" multiple id="langOpt1">
                                            
                                            <option value="No Hypoxia">No Hypoxia</option>
                                            <option value="Pneumonia">Pneumonia</option>
                                          </select></td>
                                          <td rowspan="2">Didn't recieve oxygen</td>
                                      </tr>
                                      <tr>
                                        <td>Moderate</td>
                                          <td><select class="form-control" name="moderate[]" multiple id="langOpt2">
                                           
                                            <option value="Clinical signs of non-severe pneumonia">Clinical signs of non-severe pneumonia </option>
                                            <option value="SpO2>90% on room air">SpO2>90% on room air</option>
                                          </select></td>
                                         
                                      </tr>
                                      <tr>
                                          <td>Severe</td>
                                          <!-- Clinical signs of severe pneumonia -->
                                           <td> <select class="form-control" name="severe[]" multiple id="langOpt3">
                                            
                                              <option value="SpO2 <90% on room air">SpO2 <90% on room air</option>
                                              <option value="RR > 30 breaths/min">RR > 30 breaths/min</option>
                                            </select></td>
                                            <td>Received oxygen
(or told you they needed it, but it was not available)
</td>
                                      </tr>
                                       <tr>
                                          <td>Critical</td>
                                          <td><select class="form-control" name="critical[]" multiple id="langOpt4">
                                           
                                            <option value="ARDS">ARDS</option>
                                            <option value="sepsis/septic shock">sepsis/septic shock</option>
                                            <option value="pulmonary embolism">pulmonary embolism</option>
                                            <option value="acute coronary syndrome">acute coronary syndrome</option>
                                            <option value="acute stroke">acute stroke</option>
                                            <option value="Multi-Inflammatory Syndrome in Children and adolescents temporally related to COVID-19">Multi-Inflammatory Syndrome in Children and adolescents temporally related to COVID-19</option>
                                          </select></td>
                                          <td>Received invasive ventilation </td>
                                      </tr>
                                      
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </section>
                  
                  </div>               
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Vaccination for covid</label>
                   <select class="form-control" name="vaccination" >
                    <option value="">Choose an option</option>
                     <option value="Single">Single</option>
                     <option value="Double">Double</option> 
                     <option value="Not Done">Not Done</option>                    
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Covid-19 Tested</label>
                   <select class="form-control" name="tested" >
                    <option value="">Choose an option</option>
                     <option value="RAT">RAT</option>
                     <option value="RTPCR">RTPCR</option>
                     <option value="No">No</option> 
                                       
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Other</label>
                   <input type="text" class="form-control" name="covidduration"  placeholder="Method" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Date</label>
                   <input type="date" class="form-control" name="cdate"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>



              <div class="form-row">
                 <div class="col-md-12 mb-12">
                  
                      <section class="card">
                         
                          <div class="card-body">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                        <th></th>
                                          <th>No. of days from first symptom</th>
                                          <th>Total Number of days</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          
                                          <td>URTI symptoms</td>
                                          <td><input type="text" class="form-control" name="urtisym"></td>
                                          <td><input type="text" class="form-control" name="urtidays"></td>
                                      </tr>
                                      <tr>
                                         <td>Loss of Taste/Smell</td>
                                          <td><input type="text" class="form-control" name="lotsym"></td>
                                          <td><input type="text" class="form-control" name="lotdays"></td>
                                      </tr>
                                      <tr>
                                         
                                          <td>LRTI</td>
                                          <td><input type="text" class="form-control" name="lrtisym"></td> 
                                          <td><input type="text" class="form-control" name="lrtidays"></td>
                                      </tr>
                                       
                                      <tr>
                                         
                                          <td>Oxygen Requirement</td>
                                          <td><input type="text" class="form-control" name="orsym"></td>
                                          <td><input type="text" class="form-control" name="ordays"></td>
                                      </tr>
                                    
                                   
                                      
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </section>
                  
                  </div>               
              </div>

               <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Hospitalization</label>
                   <select class="form-control" name="hospitalization" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>            
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Regular Ward</label>
                   <select class="form-control" name="rward" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option> 
                                       
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02"></label>
                   <input type="text" class="form-control" name="hdur"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Mortality</label>
                  <select class="form-control" name="mortality" >
                    <option value="">Choose an option</option>

                     <option value="Yes">Yes</option>
                     <option value="No">No</option>            
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">ICU Admission</label>
                   <select class="form-control" name="icuadm" >
                    <option value="">Choose an option</option>
                     <option value="invasive ventilator">Invasive Ventilator</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>            
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>

                  <div class="col-md-3 mb-3">
                   <label for="validationCustom02"></label>
                   <input type="text" class="form-control" name="icudays"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>

              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Under Oxygen</label>
                   <select class="form-control" name="underoxygen" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option> 
                                       
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02"></label>
                   <input type="text" class="form-control" name="uod"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>
<div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Under High Oxygen</label>
                   <select class="form-control" name="uho" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>            
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>

                  <div class="col-md-3 mb-3">
                   <label for="validationCustom02"></label>
                   <input type="text" class="form-control" name="uhod"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>

            
              
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">TOTAL number of hospitalization days</label>
                   <input type="text" class="form-control" name="tnhd"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>



              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Did the patient receive treatment for COVID-19</label>
                   <select class="form-control" name="treatment" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>            
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Antibiotic received</label>
                   <select class="form-control" name="antibiotic" >
                    <option value="">Choose an option</option>
                     <option value="Macrolides (e.g. Azithromycin, clarithromycin)">Macrolides (e.g. Azithromycin, clarithromycin)</option>
                     <option value="Fluoroquinolones (e.g. ciprofloxacin, levofloxacin">Fluoroquinolones (e.g. ciprofloxacin, levofloxacin</option>  
                     <option value="3rd and 4rd generation Cephalosporins (e.g. ceftriaxone, cefotaxime, ceftazidime, cefepime)">3rd and 4rd generation Cephalosporins (e.g. ceftriaxone, cefotaxime, ceftazidime, cefepime)</option> 
                     <option value="Carbapenems (e.g imipenem, meropenem)">Carbapenems (e.g imipenem, meropenem)</option>    
                     <option value="Piperacillin + Tazobactam">Piperacillin + Tazobactam</option>  
                     <option value="Amoxicillin-clavulanate">Amoxicillin-clavulanate</option>
                     <option value="Cotrimoxazole">Cotrimoxazole</option>  
                     <option value="No">No</option> 
                     <option value="unknown">Unknown</option> 
                     <option value="Other Antibiotics">Other Antibiotics</option>          
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
                  <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days):</label>
                   <input type="text" class="form-control" name="tdays"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>

            
              
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="tdosage"  placeholder="No of dosage" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

               <div class="form-row">
                 
                <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Antithrombotic/anticoagulation drugs received</label>
                   <select class="form-control" name="anticoagulation" >
                    <option value="">Choose an option</option>
                     <option value="Low molecular weight heparin">Low molecular weight heparin</option>
                     <option value="Warfarin">Warfarin</option> 
                     <option value="Direct oral anticoagulant">Direct oral anticoagulant</option> 
                     <option value="No">No</option> 
                     <option value="unknown">Unknown</option>           
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
                 
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days): </label>
                   <input type="text" class="form-control" name="anticoagulationdays"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dose</label>
                   <select class="form-control" name="anticoagulationdose" >
                    <option value="">Choose an option</option>
                     <option value="Preventive dose">Preventive dose</option>
                     <option value="Therapeutic dose">Therapeutic dose</option> 
                               
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

              <div class="form-row">
                
                
                  <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Antiviral Drugs Received?</label>
                    <select class="form-control" name="antiviral" >
                    <option value="">Choose an option</option>
                     
                     <option value="No">No</option> 
                      <option value="Lopinavir/Ritonavir">Lopinavir/Ritonavir</option>
                     <option value="Darunavir +/- cobicistat">Darunavir +/- cobicistat</option> 
                     <option value="Remdesivir">Remdesivir</option> 
                     <option value="Favipiravir">Favipiravir</option>
                     <option value="Acyclovir/Ganciclovir">Acyclovir/Ganciclovir</option>
                     <option value="Oseltamivir">Oseltamivir</option>
                     <option value="Ivermectin">Ivermectin</option> 
                     <option value="other">Other</option>    
                     <option value="unknown">Unknown</option>           
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days):</label>
                   <input type="text" class="form-control" name="antiviraldays"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="antiviraldosage"  placeholder="Dosage" aria-describedby="inputGroupPrepend" >
                   
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
              </div>

<div class="form-row">
                
                  <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Chloroquine/Hydroxychloroquine</label>
                    <select class="form-control" name="hydroxy" >
                    <option value="">Choose an option</option>
                     <option value="No">No</option>
                      <option value="Malaria prophylaxis">Malaria prophylaxis</option>
                     <option value="COVID-19 prophylaxis">COVID-19 prophylaxis</option> 
                     <option value="COVID-19 treatment">COVID-19 treatment</option>  
                     <option value="unknown">Unknown</option>           
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days):</label>
                   <input type="text" class="form-control" name="hydroxydays"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="hydroxydosage"  placeholder="Dosage" aria-describedby="inputGroupPrepend" >
                   
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

<div class="form-row">
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Kinase Inhibitors received?</label>
                    <select class="form-control" name="kinase" >
                    <option value="">Choose an option</option>
                     
                     <option value="No">No</option> 
                     <option value="Tofacitinib">Tofacitinib</option>
                     <option value="Baricitinib">Baricitinib</option>         
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>

                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days)</label>
                   <input type="text" class="form-control" name="kinased"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="kinasedose"  placeholder="Dosage" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Any Adverse Event</label>
                   <input type="text" class="form-control" name="kinevent"  placeholder="" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

<div class="form-row">
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Plasma Therapy</label>
                    <select class="form-control" name="plasma" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>         
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days)</label>
                   <input type="text" class="form-control" name="plasmad"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="plasmadose"  placeholder="Dosage" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Any Adverse Event</label>
                   <input type="text" class="form-control" name="anyevent"  placeholder="" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

<div class="form-row">
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Il-6 Antagonists</label>
                    <select class="form-control" name="antagonists" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>         
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days)</label>
                   <input type="text" class="form-control" name="antagonistdays"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="antagonistdosage"  placeholder="Dosage" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Any Adverse Event</label>
                   <input type="text" class="form-control" name="antagonistevent"  placeholder="No of days" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

              <div class="form-row">
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Interferon</label>
                    <select class="form-control" name="interferon" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Eculizumab</label>
                  <select class="form-control" name="eculizumab" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Other agents</label>
                   <select class="form-control" name="otheragent" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">If yes, specify?</label>
                   <input type="text" class="form-control" name="otheryes"   aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>


              <div class="form-row">
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days)</label>
                    <input type="text" class="form-control" name="oaduration"   aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                  <input type="text" class="form-control" name="oadosage"   aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Steroids received</label>
                   <select class="form-control" name="steroid" >
                    <option value="">Choose an option</option>
                    
                     <option value="No">No</option> 
                     <option value="Dexamethasone">Dexamethasone</option>
                     <option value="Prednisone">Prednisone</option> 
                     <option value="Hydrocortisone">Hydrocortisone</option> 
                     <option value="Methylprednisolone">Methylprednisolone</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Other</label>
                    <input type="text" class="form-control" name="routeother"   aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

              <div class="form-row">
                
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration of treatment (days): </label>
                  <input type="text" class="form-control" name="sttreatment"   aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <input type="text" class="form-control" name="stdosage"   aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Route</label>
                   <select class="form-control" name="route" >
                    <option value="">Choose an option</option>
                     <option value="Oral">Oral</option>
                     <option value="Intravenous">Intravenous</option> 
                     <option value="Inhaled">Inhaled</option>     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
              </div>


              <div class="form-row">
                <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Did the participant perform any radiographic examination?</label>
                    <select class="form-control" name="radiographic" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">CT Scan Chest </label>
                  <select class="form-control" name="ctscan" >
                    <option value="">Choose an option</option>
                      <option value="Normal">Normal </option>
                     <option value="Abnormal, likely unrelated to COVID-19">Abnormal, likely unrelated to COVID-19 </option> 
                     <option value="Abnormal, likely related to COVID-19">Abnormal, likely related to COVID-19 </option> 
                     <option value="Abnormal, but  unknown if related to COVID-19">Abnormal, but  unknown if related to COVID-19</option>    
                     <option value="Not Done">Not Done</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Echocardiogram</label>
                   <select class="form-control" name="echocard" >
                    <option value="">Choose an option</option>
                      <option value="Normal">Normal </option>
                     <option value="Abnormal, likely unrelated to COVID-19">Abnormal, likely unrelated to COVID-19 </option> 
                     <option value="Abnormal, likely related to COVID-19">Abnormal, likely related to COVID-19 </option> 
                     <option value="Abnormal, but  unknown if related to COVID-19">Abnormal, but  unknown if related to COVID-19</option>    
                     <option value="Not Done">Not Done</option> 
                         
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>

               <div class="form-row">
              
             
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">X-ray Chest </label>
                  <select class="form-control" name="xray" >
                    <option value="">Choose an option</option>
                     <option value="Normal">Normal </option>
                     <option value="Abnormal, likely unrelated to COVID-19">Abnormal, likely unrelated to COVID-19 </option> 
                     <option value="Abnormal, likely related to COVID-19">Abnormal, likely related to COVID-19 </option> 
                     <option value="Abnormal, but  unknown if related to COVID-19">Abnormal, but  unknown if related to COVID-19</option>    
                     <option value="Not Done">Not Done</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Was a blood test done? </label>
                  <select class="form-control" name="bloodtest" >
                    <option value="">Choose an option</option>
                     <option value="Done">Done</option>
                     <option value="Not Done">Not Done</option> 
                     <option value="unknown">Unknown</option>        
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              </div>


               <div class="form-row">
                 <div class="col-md-12 mb-12">
                  
                      <section class="card">
                         
                          <div class="card-body">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-striped">
                                     
                                      <tbody>
                                      <tr>
                                          <td>Albumin</td>
                                          <td><select class="form-control" name="albumin">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="albuminval"></td>
                                      </tr>
                                      <tr>
                                         <td>ALT/SGPT</td>
                                          <td><select class="form-control" name="alt">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="altval"></td>
                                      </tr>
                                      <tr>
                                          <td>Antithyroglobulin</td>
                                          <td><select class="form-control" name="agl">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td> 
                                          <td><input type="text" class="form-control" name="aglv"></td>
                                      </tr>
                                       
                                      <tr>
                                          <td>AST/SGOT</td>
                                          <td><select class="form-control" name="ast">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="astval"></td>
                                      </tr>
                                       <tr>
                                          <td>Creatine Kinase MM</td>
                                          <td><select class="form-control" name="creatinine">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="creatinineval"></td>
                                      </tr>
                                       <tr>
                                          <td>Creatinine</td>
                                          <td><select class="form-control" name="crea">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="creaval"></td>
                                      </tr>
                                       <tr>
                                          <td>C-reactive protein (CRP):</td>
                                          <td><select class="form-control" name="crp">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="crpval"></td>
                                      </tr> 
                                      <tr>
                                          <td>D-Dimer</td>
                                          <td><select class="form-control" name="dim">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="dimval"></td>
                                      </tr>
                                       <tr>
                                          <td>Fasting Blood Glucose</td>
                                          <td><select class="form-control" name="glucose">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="glucoseval"></td>
                                      </tr>
                                       <tr>
                                          <td>Ferritin</td>
                                          <td><select class="form-control" name="ferritin">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="ferritinval"></td>
                                      </tr>
                                       <tr>
                                          <td>Fibrinogen</td>
                                          <td><select class="form-control" name="fibrinogen">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="fibrinogenval"></td>
                                      </tr>
                                       <tr>
                                          <td>Globular Filtration Rate</td>
                                          <td><select class="form-control" name="filtration">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="filtrationval"></td>
                                      </tr>
                                       <tr>
                                          <td>LDH</td>
                                          <td><select class="form-control" name="ldh">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="ldhval"></td>
                                      </tr>
                                       <tr>
                                          <td>Lymphocytes</td>
                                          <td><select class="form-control" name="lympho">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="lymphoval"></td>
                                      </tr>
                                       <tr>
                                          <td>Thyroid peroxidase antibodies</td>
                                          <td><select class="form-control" name="thyroid">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="thyroidval"></td>
                                      </tr>
                                       <tr>
                                          <td>Troponin</td>
                                          <td><select class="form-control" name="troponin">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="troponinval"></td>
                                      </tr>
                                       <tr>
                                          <td>TSH</td>
                                          <td><select class="form-control" name="tsh">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="tshval"></td>
                                      </tr>
                                       <tr>
                                          <td>Urea (BUN):</td>
                                          <td><select class="form-control" name="urea">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="ureaval"></td>
                                      </tr>
                                      <tr>
                                          <td>Serum IL6</td>
                                          <td><select class="form-control" name="serum">
                                            <option value="">Choose an option</option>
                                            <option value="Done">Done</option>
                                            <option value="Not Done">Not Done</option>
                                          </select></td>
                                          <td><input type="text" class="form-control" name="serumval"></td>
                                      </tr>
                                      <tr>
                                          <td>Others Specify</td>
                                          <td><input type="text" class="form-control" name="otspec"></td>
                                          <td><input type="text" class="form-control" name="otspecval"></td>
                                      </tr>                                    
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </section>
                  
                  </div>               
              </div>


                                  <div class="form-group">
                                      <div class="form-check">
                                          <input class="form-control" type="text" name="feedback"  >
                                       
                                      </div>
                                  </div>
                                  <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                              </form>
                          
                             