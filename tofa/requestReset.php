<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use \PHPMailer\PHPMailer\PHPMailer;
use \PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require 'config.php';

if (isset($_POST['email'])) {

    $emailTo = $_POST['email']; 
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    $code = uniqid(true); // true for more uniqueness 
    $query = mysqli_query($con,"INSERT INTO resetpasswords (code, email) VALUES('$code','$emailTo')"); 
    if (!$query) {
       exit('Error'); 
    }
    try {
        //Server settings
        $mail->SMTPDebug = 0;     // Enable verbose debug output, 1 for produciton , 2,3 for debuging in devlopment 
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'webdesigner@chanrejournals.com';        // SMTP username
        $mail->Password = '*meghalaya2';                          // SMTP password
        // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        // $mail->Port = 587;   // for tls                                 // TCP port to connect to
        $mail->Port = 465;

        //Recipients
        $mail->setFrom('email@gmail.com', 'Password Recovery'); // from who? 
        $mail->addAddress($emailTo, 'Joe User');     // Add a recipient

        $mail->addReplyTo('no-replay@example.com', 'No Replay');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Content
        // this give you the exact link of you site in the right page 
        // if you are in actual web server, instead of http://" . $_SERVER['HTTP_HOST'] write your link 
        

        $url = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']). "/resetPassword.php?code=$code"; 
		$mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Your password reset link';
        $mail->Body    = "<h1> You have requested password reset </h1>
                         Click <a href='$url'>Change Password</a> to do so";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        // to solve a problem 
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );


        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }

    exit(); // to stop user from submitting more than once 
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
       .slider {
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
      background-image: linear-gradient(to right, #ffc107 , #dc3545);
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}
a{  width:100%;

text-align:center;
    display:inline-block;
    position:relative;
    padding:10px 30px;
    text-decoration:none;
    text-transform: uppercase;
    font-weight:500;
    letter-spacing:2px;

    box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}



#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}

#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
    font-family:times;
}
img{
    
    width:98%;
}
form{
   
    align-content: center;
    
    background: #99c79b;
    padding: 20px;
    margin-top: 20px;
    color: white;
}
input[type='submit']{
    
box-shadow:  5px 5px 10px #82808e,
             -5px -5px 10px #ffffff;
}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
     <div id="navbar">
  <!-- <a class="active" href="/multiapp.php">Chanre</a> -->
  
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form method="post">
                <h3 align="center">Please enter your registered email.</h3>
                <div class="form-group">
                   <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="off"> 
                </div>
                <div class="form-group" align="center">
                  <input type="submit" name="submit" class="btn btn-primary btn-round" value="Reset Email">
                </div>
            
                
            </form>

        </div>
        <div class="col-md-3"></div>
    </div>
</div>



</body>
</html>