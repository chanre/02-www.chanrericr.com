<?php
 session_start();
//Database Configuration File
include('config.php');
//error_reporting(0);
if(isset($_POST['login']))
  {
    // Getting username/ email and password
    $uname=$_POST['username'];
    $password=$_POST['password'];
    // Fetch data from database on the basis of username/email and password
$sql =mysqli_query($con,"SELECT AdminUserName,AdminEmailId,AdminPassword FROM tbladmin WHERE (AdminUserName='$uname') and Is_Active=2 ");
 $num=mysqli_fetch_array($sql);
if($num>0)
{
$hashpassword=$num['AdminPassword']; // Hashed password fething from database
//verifying Password
if (password_verify($password, $hashpassword)) {
$_SESSION['login']=$_POST['username'];

    echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
  } else {
echo "<script>alert('Wrong Password');</script>";
 
  }
}
//if username or email not found in database
else{
echo "<script>alert('Wait for approval from admin');</script>";
  }
 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.html">
    <title></title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
</head>
<body class="login-body">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <form class="form-signin" method="post">
                    <h2 class="form-signin-heading">sign in now</h2>
                    <div class="login-wrap">
                        <input class="form-control" type="text" required="" name="username" placeholder="Username"
                            autocomplete="off">
                        <input class="form-control" type="password" name="password" required="" placeholder="Password"
                            autocomplete="off">
                        <button class="btn btn-lg btn-login btn-block" type="submit" name="login">Sign in</button>
                        <div class="registration">
                            Don't have an account yet?
                            <a class="" href="registration.php">
                                Create an account
                            </a>
                        </div>
                        <div class="" align="center">

                            <a class="" href="admin.php">
                                Admin Login
                            </a>| <a href="requestReset.php">Forgot Password</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3">
                
            </div>

        </div>
        <div class="row">

        </div>
    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>


</body>

</html>