<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require 'config.php';

if (isset($_POST['submit'])) {

    $name=$_POST['name'];
    $email = $_POST['email'];
    $message=$_POST['message'];
    $subject=$_POST['subject'];

    $mail = new PHPMailer(true);// Passing `true` enables exceptions
    // $code = uniqid(true); // true for more uniqueness 
    // $query = mysqli_query($con,"INSERT INTO resetpasswords (code, email) VALUES('$code','$emailTo')"); 
   
    try {
        //Server settings
        $mail->SMTPDebug = 0;     // Enable verbose debug output, 1 for produciton , 2,3 for debuging in devlopment 
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'webdesigner@chanrejournals.com';        // SMTP username
        $mail->Password = '*meghalaya2';                          // SMTP password
        // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        // $mail->Port = 587;   // for tls                                 // TCP port to connect to
        $mail->Port = 465;

        //Recipients
        $mail->setFrom($email, $name); // from who? 
        $mail->addAddress('webdesigner@chanrejournals.com', 'User');     // Add a recipient

        $mail->addReplyTo($email, 'Reply');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Content
        // this give you the exact link of you site in the right page 
        // if you are in actual web server, instead of http://" . $_SERVER['HTTP_HOST'] write your link 
        

        // $url = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']). "/resetPassword.php?code=$code"; 
    $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = "$message";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        // to solve a problem 
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );


        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }

    exit(); // to stop user from submitting more than once 
}

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
       <div class="container">
           <div class="row">
              
               <div class="col-md-8"><br>
                <h1 align="center">Contact Us</h1>
                   <form method="POST">
                       <div class="form-group">
                           <label>Name</label>
                           <input type="text" name="name" class="form-control" placeholder="Enter your name .....">
                       </div>
                       <div class="form-group">
                           <label>Email</label>
                           <input type="text" name="email" class="form-control" placeholder="Enter your email .....">
                       </div>
                       <div class="form-group">
                           <label>Subject</label>
                           <input type="text" name="subject" class="form-control" placeholder="Enter your subject .....">
                       </div>
                       <div class="form-group">
                           <label>Message</label>
                           <textarea name="message" class="form-control" placeholder="Enter your message ....."></textarea>
                       </div>
                       <div class="form-group" align="center">
                        
                           <button type="submit" name="submit" class="btn btn-primary">submit</button>
                       </div>
                   </form>
               </div>
               <div class="col-md-4"><br>
                   <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15550.220323318843!2d77.55110821681919!3d13.000284361207601!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3dbc8be6ee1f%3A0x99d77f3579e9ed2e!2sChanre%20Rheumatology%20%26%20Immunology%20Center%20%26%20Research!5e0!3m2!1sen!2sin!4v1583737117397!5m2!1sen!2sin" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
               </div>
           </div>
       </div>

          
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
<script>
function myFunction() {
  alert('Hello');
}
</script>
    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>