<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>DR Chandrashekara S- ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr. Dhanashree Gavali</h1></div><br>
             <div class="row">
                 <div class="col-md-3"> <img src="images/doctor/dhanashree.jpg" class="card-img" alt="..."></div>
                 <div class="col-md-9">

                  
                        
                     <p>
                     Dr. Dhanashree Gavali is currently a Post MD Fellowship student in Rheumatology &amp; Immunology
at ChanRe Rheumatology &amp; Immunology Center &amp; Research. She completed her MBBS from
Government Medical College, Miraj(Maharashtra) and her Post Graduate (DNB) in General
medicine from New Delhi.</p><p> She has worked as a Consulting Physician and Intesivist with
varied approach in critical cases in Pune, Aurangabad (Maharashtra) . 
Her areas of interest include Rheumatology and Immunology, Joint and soft tissue diseases,
Connective tissue diseases, Palliative Care ,Rehabilitation,and Geriatrics and associated
research. </p><p>
She has been working on different research projects since 6 years and also has presented
research articles and presentations on National Platform .                     </p>
                                    </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>


    <script src="js/bootstrap.min.js"></script>

    
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>