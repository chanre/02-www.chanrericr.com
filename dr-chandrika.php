<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr.Chandrika S Bhat</h1></div><br>
             <div class="row">
                 <div class="col-md-3">  <img src="images/doctor/chandrika.jpg" class="card-img" alt="..." height="230"> </div>
                 <div class="col-md-9">

                     <p>
                         <strong>Completed her graduation from Kasturba Medical College, Mangalore and post graduation in Pediatrics from Grant Medical College, Mumbai</strong>
Dr.Chandrika Bhat completed her graduation from Kasturba Medical College, Mangalore and post graduation in Pediatrics from Grant Medical College, Mumbai. She has worked as a medical officer in KEM hospital in Mumbai, and also worked as Pediatrics consultant in Columbia Asia hospital, Hebbal and St. Johns Hospital. Subsequently, she completed her fellowship training from the prestigious Bristol Royal hospital for children in UK which is a center of excellence for management of pediatric uveitis.
                     </p>
                     <p>
                         Dr Bhat has experience in managing children with various rheumatologic diseases and is trained in intra-articular corticosteroid joint injections including ultrasound guided injections. She is also adept in pain management having gained experience from Bath Centre for Pain Services, UK. Apart from authoring various chapters in reputed textbooks and publishing articles in international journals of Paediatric Rheumatology, she is also a reviewer for prestigious journals. Her special interests include inflammatory arthritis, uveitis, vasculitis and auto-inflammatory diseases.
                     </p>
                     
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>