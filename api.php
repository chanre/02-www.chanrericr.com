<?php

$api_url = 'https://jsonplaceholder.typicode.com/posts';

// Read JSON file
$json_data = file_get_contents($api_url);

// Decode JSON data into PHP array
$response_data = json_decode($json_data);

// All user data exists in 'data' object
$user_data = $response_data->$json_data;

// Cut long data into small & select only first 10 records
$user_data = array_slice($user_data, 0, 9);

// Print data if need to debug
//print_r($user_data);

// Traverse array and display user data
foreach ($user_data as $user) {
	echo "name: ".$user->title;
	echo "<br />";
	echo "name: ".$user->id;
	echo "<br /> <br />";
}

?>