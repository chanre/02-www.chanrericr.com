<?php 
include ('config.php');

if(isset($_POST['submit']))
{
$name= $_POST['name'];
$fileno=$_POST['fileno'];
$diagnosis=$_POST['diagnosis'];
$gender=$_POST['gender'];
$age=$_POST['age'];
$a = $_POST['a'];
$b = $_POST['b'];
$c = $_POST['c'];
$d = $_POST['d'];
$e = $_POST['e'];
$f = $_POST['f'];
$g = $_POST['g'];
$h = $_POST['h'];
$i = $_POST['i'];
$j = $_POST['j'];
$k = $_POST['k'];
$l = $_POST['l'];
$m = $_POST['m'];
$mex = $_POST['mex'];
$feedback= $_POST['feedback'];
$query=mysqli_query($con,"Insert into pcs (name,fileno,diagnosis,age,gender,a,b,c,d,e,f,g,h,i,j,k,l,m,total,feedback)
values('$name','$fileno','$diagnosis','$age','$gender','$a','$b','$c','$d','$e','$f','$g','$h','$i','$j','$k','$l','$m','$mex','$feedback')
	");

if($query)
	{echo "<script>alert('Successfully saved..');</script>";
}
else{echo "<script>alert('Unable to save the data..Try again');</script>";
}

}


 ?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="shortcut icon" href="images1/Refined/chanre1.png" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
<style>
form{width:100%;
	border-radius: 0px;
background: #e0e0e0;
padding: 10px;
/*box-shadow:  5px 5px 10px #5a5a5a,-5px -5px 10px #ffffff;*/
}
table{padding:20px;}
table tr td{padding:10px;}
select{
	width:100%;
	/*border-radius:10px;*/
}
</style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
       <div class="container">
           <div class="row">
              <div class="col-md-2"></div>
               <div class="col-md-7"><br>
                
                   <form method="post"><h2 align="center">Pain Catastrophizing Scale</h2>
				   <table align="center">
				   <tr> 
				   <td>Name:</td>
				   <td><input type="text" name="name"></td>
				   </tr>
				    <tr> 
				   <td>File No:</td>
				   <td><input type="text" name="fileno"></td>
				   </tr>

				      <tr> 
				   <td>Diagnosis:</td>
				   <td><select id="diagnosis" name="diagnosis"> 
				   	<option>Select option..</option>
				   	<option value="Ankylosing spondylitis">Ankylosing spondylitis</option>
				   	<option value="Rheumatoids Arithritis">Rheumatoids Arithritis</option>
				   	<option value="psoriatic arthritis">psoriatic arthritis</option>
				   	<option value="Osteo Arithritis">Osteo Arithritis</option>
				   	<option value="Scleroderma">Scleroderma</option>
				   	<option value="Fibromyalgia">Fibromyalgia</option>
				   </select></td>
				   </tr>

				    <tr> 
				   <td>Age:</td>
				   <td><input type="text" name="age"></td>
				   </tr>
				    <tr> 
				   <td>Gender:</td>
				   <td><input type="text" name="gender"></td>
				   </tr>
				   </table>
				   <table border="1">
				   <tr>
				   <td>I worry all the time about whether the pain will end</td>
				   <td><select name="a" id="a" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I feel I can’t go on</td>
				   <td><select name="b" id="b" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>It’s terrible and I think it’s never going to get any better</td>
				   <td><select name="c" id="c" onchange="pcs()">
				       <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>It’s awful and I feel that it overwhelms me</td>
				   <td><select name="d" id="d" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I feel I can’t stand it anymore</td>
				   <td><select name="e" id="e" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I become afraid that the pain will get worse</td>
				   <td><select name="f" id="f" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I keep thinking of other painful events</td>
				   <td><select name="g" id="g" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I anxiously want the pain to go away</td>
				   <td><select name="h" id="h" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I can’t seem to keep it out of my mind</td>
				   <td><select name="i" id="i" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I keep thinking about how much it hurts</td>
				   <td><select name="j" id="j" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>I keep thinking about how badly I want the pain to stop</td>
				   <td><select name="k" id="k" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   <tr>
				   <td>There’s nothing I can do to reduce the intensity of the pain</td>
				   <td><select name="l" id="l" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   
				   <tr>
				   <td>I wonder whether something serious may happen</td>
				   <td><select name="m" id="m" onchange="pcs()">
				   <option value="">Select the below option</option>
					   <option value="0">Not at all</option>
					   <option value="1">To aslight degree</option>
					   <option value="2">To a moderate degree</option>
					   <option value="3">To a great degree</option>
					   <option value="4">All the time</option>
					   </select></td>
				   </tr>
				   </table>
				   <div align="center">
				   <br>
				   <button align="center" onclick="pcs()" type="submit"  name="submit" class="btn btn-primary">Submit</button><br><br>
				   <input type="number" id="mex" name="mex" readonly> &nbsp;
<br>
				   <br>
				   <textarea rows="5" name="feedback" class="form-control" placeholder="Enter your text.."></textarea><br>
				   <br>
				   </div>
				   
                   </form>
				   <br>
               </div>
               <div class="col-md-2"><br>
                   
               </div>
           </div>
       </div>
        <?php include('layout/footer.php'); ?>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>
	<script>
	function pcs(){
		const a=Number(document.getElementById("a").value);
		const b=Number(document.getElementById("b").value);
		const c=Number(document.getElementById("c").value);
		const d=Number(document.getElementById("d").value);
		const e=Number(document.getElementById("e").value);
		const f=Number(document.getElementById("f").value);
		const g=Number(document.getElementById("g").value);
		const h=Number(document.getElementById("h").value);
		const i=Number(document.getElementById("i").value);
		const j=Number(document.getElementById("j").value);
		const k=Number(document.getElementById("k").value);
		const l=Number(document.getElementById("l").value);
		const m=Number(document.getElementById("m").value);
		
		
		  const n = a+b+c+d+e+f+g+h+i+j+k+l+m;
  
  document.getElementById("mex").value= n.toFixed(0);
	}
	</script>

</body>

</html>