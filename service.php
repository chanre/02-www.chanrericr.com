<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="shortcut icon" href="images1/Refined/chanre1.png" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
        .card-header{background-color:#4f89d0;height: 60px;}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
        
       <div class="container">
        <br>
                <h1 class="Rheumatology" align="center">Our Services</h1>
           <div class="row">
               <div class="col-md-8">
                   
                <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn active" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Rheumatology & Immunology
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="row">
        <div class="col-md-10">  
          <div class="card-body">
        <ul style="text-decoration: none;" class="offset-2">
                        <li>Rheumatologists treat arthritis</li>
                        <li>Autoimmune diseases</li>
                        <li>Pain disorders affecting joints</li>
                        <li>Osteoporosis</li>
                      </ul>
                  <p>There are more than 200 types of these diseases, including rheumatoid arthritis, osteoarthritis, gout, lupus, back pain, osteoporosis, and tendinitis</p>
                   <div>
                <h3 class="card-title">Degenerative arthropathies</h3>
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Osteoarthritis</li>
                      </ul>
               </div>
               <div>
                <h3 class="card-title">Inflammatory arthropathies</h3>
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Rheumatoid arthritis</li>
                        <li>Spondyloarthropathies
                          <ul>
                            <li>Ankylosing spondylitis</li>
                            <li>Reactive arthritis (reactive arthropathy)</li>
                            <li>Psoriatic arthropathy</li>
                            <li>Enteropathic arthropathy</li>
                          </ul>
                        </li>
                        <li>Juvenile Idiopathic Arthritis (JIA)</li>
                        <li>Crystal arthropathies: gout, pseudogout</li>
                        <li>Septic arthritis</li>
                      </ul>
               </div>
               <div>
                <h3 class="card-title">Systemic conditions and connective tissue diseases</h3>
                      <ul class="offset-2" style="text-decoration: none;">
                          <li>Lupus</li>
                          <li>Ehlers-Danlos syndrome</li>
                          <li>Sjögren's syndrome</li>
                          <li>Scleroderma (systemic sclerosis)</li>
                          <li>Polymyositis</li>
                          <li>Dermatomyositis</li>
                          <li>Polymyalgia rheumatica</li>
                          <li>Mixed connective tissue disease</li>
                          <li>Relapsing polychondritis</li>
                          <li>Adult-onset Still's disease</li>
                          <li>Sarcoidosis</li>
                          <li>Fibromyalgia</li>
                          <li>Myofascial pain syndrome</li>
                          <li>Vasculitis-
                          <ul class="offset-1">
                              <li>Urticarial Vasculitis</li>
                              <li>Microscopic polyangiitis</li>
                              <li>Eosinophilic granulomatosis with polyangiitis</li>
                              <li>Granulomatosis with polyangiitis</li>
                              <li>Polyarteritis nodosa</li>
                              <li>Henoch–Schönlein purpura</li>
                              <li>Serum sickness</li>
                              <li>Giant cell arteritis, Temporal arteritis</li>
                              <li>Takayasu's arteritis</li>
                              <li>Behçet's disease</li>
                              <li>Kawasaki disease (mucocutaneous lymph node syndrome)</li>
                              <li>Thromboangiitis obliterans</li>
                          </ul>
                          </li>

                          <li>Hereditary periodic fever syndromes</li>
                        </ul>
               </div>
               <div>
                <h3 class="card-title">Soft tissue rheumatism</h3>
                <p>Local diseases and lesions affecting the joints and structures around the joints including tendons, ligaments capsules, bursae, stress fractures, muscles, nerve entrapment, vascular lesions, and ganglia. For example:</p>
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Low back pain</li>
                        <li>Tennis elbow</li>
                        <li>Golfer's elbow</li>
                        <li>Olecranon bursitis</li>
                        <li>Planter fasciitis</li>
                      </ul>
                      <h4>Other Immunodeficiency Disorder</h4>
               </div>
      </div>
      
  </div>
  
      </div>
      
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
          Allergy & Clinical Immunology 
        </button>
      </h2>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="row">
          <div class="col-md-10">
              <div class="card-body">
                <p>An hypersensitive disorder of the immune system It happens when your body Encounters allergen either by </p>
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Inhalation</li>
                        <li>Injection</li>
                        <li>Ingestion</li>
                        <li>Skin Contact</li>
                      </ul>
                      <h4>Common types of allergens</h4>
                      <p>Food- Nuts, Seafood, Eggs, Peas, Beans, Milk
                        <ul class="offset-2">
                          <li>Plant pollens: Parthenium, Rye, Timothy Grass</li>
                          <li>Insect products: Bee venom, ant venom, dust mites, cockroach calyx</li>
                          <li>Omgs: penicillin, Sulphonamides, Analgesics, local anaesthetics Animal hair, Dander</li>
                        </ul>
                      </p>

                      <div>
                <h4 class="card-title">Allergy has multi organ involvement with the following symptoms</h4>
                <p>An hypersensitive disorder of the immune system It happens when your body Encounters allergen either by </p>
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Eyes: Itchy Eyes, Redness, Watering</li>
                        <li>Nose: Running Nose, Recurrent Sneezing, Nasal Block, Headache</li>
                        <li>Throat: Itchy throat, Irritation, Repeated Sore Throat</li>
                        <li>Ears: Itching</li>
                        <li>Lungs: Cough, Wheezing</li>
                        <li>Skin: Rashes, Itching</li>
                        <li>Gastrointestinal Tract: Crampy Abdominal Pain, Bloating Sensation, Unexplainable Diarrhoea</li>
                      </ul>
                      <p>Diagnosis is possible with the help of examination and investigation</p>
                     
               </div>
        
              </div>
          </div>
          
      </div>
    </div>
  </div>





  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Reproductive Immunology, High -Risk Pregnancy & Infertility 
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
     <div class="row">
         <div class="col-md-10"><div class="card-body">
        <ul class="offset-2" style="text-decoration: none;">
                        <li>Failure of ovulation – polycystic ovarian syndrome  (PCOS), Primary / Secondary ovarian failure</li>
                        <li>Problems with menstrual cycle</li>
                        <li>Structural problems usually involve the presence of abnormal tissue in fallopian tubes or uterus</li>
                        <li>Endometriosis</li>
                        <li>Uterine fibroids</li>
                        <li>Polyps</li>
                        <li>Gastrointestinal Tract: Crampy Abdominal Pain, Bloating Sensation, Unexplainable Diarrhoea</li>
                        <li>Scarring in the uterus from previous injuries, infections or surgery</li>
                        <li>An unusual shaped uterus</li>
                        <li>Infections</li>
                        <li>Pelvic inflammatory disease </li>
                        <li>Sexually transmitted infections (STIs)</li>
                        <li>Implantation failure –IUI / IVF Failure</li>
                        <li>Autoimmune disorders</li>
                        <li>Previous Anomalies</li>
                        <li>SLE , APLA</li>
                        <li>Thrombophilia</li>
                        <li>Associated Autoimmune Conditions like Rheumatoid Arthritis , Allergies</li>
                      </ul>
      </div></div>
      
     </div>
      
    </div>
  </div>



  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Physiotherapy & Rehabilitation
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
     <div class="row">
         <div class="col-md-6">
             <div class="card-body">
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Musculoskeletal Rehabilitation & Physiotherapy</li>
                        <li>Soft tissue & Chronic Pain Management (Dry Needling, Trigger point Management)</li>
                        <li>Walking and Gait Rehabilitation</li>
                        <li>Quality of Life and Activities of Daily Improvement Clinics</li>
                        <li>Occupational Therapy & Rehabilitation</li>
                        <li>Orthotics & Prosthetics</li>
                      </ul>
      </div>
         </div>
         <div class="col-md-4" align="right"><img src="images/serv3.png"></div>
     </div>
      
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingThre">
      <h2 class="mb-0">
        <button class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="false" aria-controls="collapseThree">
          Joint Preservation and Restoration
        </button>
      </h2>
    </div>
    <div id="collapseThree1" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
     <div class="row">
         <div class="col-md-6">
             <div class="card-body">
        This is an integrated consultation system,providing orthopedic expertise for preservation and restoration of joints.
      </div>
         </div>
         <div class="col-md-4" align="right"><img src="images/serv3.png"></div>
     </div>
      
    </div>
  </div>


<div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapseThree">
          Integrated Pain Management (IPM) Center
        </button>
      </h2>
    </div>
    <div id="collapsefour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="row">
          <div class="col-md-6"><div class="card-body">
        Pain is the most common and challenging symptom for all musculoskeletal disorders.
      </div></div>
       <div class="col-md-4" align="right"><img src="images/serv4.png"></div>
      </div>
    </div>
  </div>


  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
          Physiotherapy & Rehabilitation
        </button>
      </h2>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="row">
          <div class="col-md-6">
           <div class="card-body">
        Our Physiotherapy & Rehabilitation center provides a full range of services to help patients prevent injury and disease, recover function and strength, and reduce pain
      </div>
          </div>
           <div class="col-md-4" align="right"><img src="images/serv5.png"></div>
      </div>
    </div>
  </div>

</div>

               </div>
               <div class="col-md-4">
                 <div>
                <h5 class="card-header">Inpatient Services</h5>
                <!-- <h5 class="offset-1">Counselling</h5> -->
                <!-- <p></p> -->
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>24X7 hospital service</li>
                        <li>Inpatient Step1 ICU</li>
                        <li>Minor procedure Unit</li>

                      </ul>
                     
               </div>


                <div>
               <h5 class="card-header">Food & Nutrition</h5>
                
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>Diet Counselling</li>
                        <li>Therapeutic Nutrition</li>
                        <li>Lifestyle Management</li>
                      </ul> 
               </div>
               <div>
               <h5 class="card-header">Supporting Services</h5>
                
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>X-ray</li>
                        <li>Nerve Conduction Studies  </li>
                        <li>PFT</li>
                        <li>ECG</li>
                        <li>Ultrasound & Doppler</li>
                        <li>2D Echo</li>
                      </ul> 
               </div>

               <div>
               <h5 class="card-header">24 hours in service</h5>
                
                      <ul class="offset-2" style="text-decoration: none;">
                        <li>24X7 Laboratory</li>
                        <li>24X7 Pharmacy</li>
                        <li>Home Blood Sample Collection</li>
                      </ul> 
               </div>

               </div>
           </div>
       </div>

        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>