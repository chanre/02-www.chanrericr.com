<?php 

session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{




 ?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Form Component</title>
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
     <?php include('layout/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
 <?php include('layout/aside.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              
              
              
              <div class="row">
                  <div class="col-lg-12">
                      
                      <section class="card">
                          <div class="card-header">Data Insertion Form </div>
                          <div class="card-body">
                             <?php include('layout/form1.php') ?>

                          </div>


                                  <div class="form-group">
                                      <div class="form-check">
                                          <input class="form-control" value="<?php echo htmlentities($rows['feedback']);?>" type="text" name="feedback"  >
                                       
                                      </div>
                                  </div>
                                  <!-- <button class="btn btn-primary" type="submit" name="submit">Submit</button> -->
                              </form>
                      
                             

                              <script>
                                  // Example starter JavaScript for disabling form submissions if there are invalid fields
                                  (function() {
                                      'use strict';
                                      window.addEventListener('load', function() {
                                          // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                          var forms = document.getElementsByClassName('needs-validation');
                                          // Loop over them and prevent submission
                                          var validation = Array.prototype.filter.call(forms, function(form) {
                                              form.addEventListener('submit', function(event) {
                                                  if (form.checkValidity() === false) {
                                                      event.preventDefault();
                                                      event.stopPropagation();
                                                  }
                                                  form.classList.add('was-validated');
                                              }, false);
                                          });
                                      }, false);
                                  })();
                              </script>
                          </div>
                      </section>

                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!-- Right Slidebar start -->
    
      <!-- Right Slidebar end -->

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2021 &copy; By ChanRe.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

    <script src="js/jquery-ui.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>
  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>

  <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

  <script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script src="js/respond.min.js" ></script>
<!-- <script src="js/jquery-3.2.1.min.js"></script> -->
  <!--right slidebar-->
  <script src="js/slidebars.min.js"></script>
<script src="js/jquery.multiselect.js"></script>

  <!--common script for all pages-->
  <script src="js/common-scripts.js"></script>

  <!--script for this page-->
  <script src="js/form-component.js"></script>
     <script type="text/javascript">
                              

$('#langOpt1').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,

    selectAll: true
});


$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt4').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});



$('#langOpt5').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,

    selectAll: true
});


$('#langOpt6').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt7').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt8').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});

$('#langOpt9').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,

    selectAll: true
});


$('#langOpt10').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt11').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt12').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt13').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
</script>
  </body>
</html>
<?php } ?>