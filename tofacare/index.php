<?php
 session_start();
//Database Configuration File
include('config.php');
//error_reporting(0);
if(isset($_POST['login']))
  {
 
    // Getting username/ email and password
     $uname=$_POST['username'];
    $password=$_POST['password'];
    // Fetch data from database on the basis of username/email and password
$sql =mysqli_query($con,"SELECT AdminUserName,AdminEmailId,AdminPassword FROM tbladmin WHERE (AdminUserName='$uname') and Is_Active=2 ");
 $num=mysqli_fetch_array($sql);
if($num>0)
{
$hashpassword=$num['AdminPassword']; // Hashed password fething from database
//verifying Password
if (password_verify($password, $hashpassword)) {
$_SESSION['login']=$_POST['username'];

    echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
  } else {
echo "<script>alert('Wrong Password');</script>";
 
  }
}
//if username or email not found in database
else{
echo "<script>alert('Wait for approval from admin');</script>";
  }
 
}
?>

<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.html">

    <title></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />


</head>

  <body class="login-body">

    <div class="container">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4">
      <form class="form-signin" method="post">
        <h2 align="center">Tofacare</h2>
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <!-- <input type="text" class="form-control" placeholder="User ID" autofocus> -->
            <input class="form-control" type="text" required="" name="username" placeholder="Username" autocomplete="off">
            <input class="form-control" type="password" name="password" required="" placeholder="Password" autocomplete="off">
            <!-- <input type="password" class="form-control" placeholder="Password"> -->
         
            <button class="btn btn-lg btn-login btn-block" type="submit" name="login">Sign in</button>
           
            <div class="registration">
                Don't have an account yet?
                <a class="" href="registration.php">
                    Create an account
                </a>
            </div>
            <div class="" align="center">
               
                <a class="" href="admin.php">
                    Admin Login
                </a>| <a href="requestReset.php">Forgot Password</a>
            </div>

        </div>

          <!-- Modal -->
          
          <!-- modal -->

      </form></div>
      <div class="col-md-6">
        <div style="padding: 10px;margin-top: 80px;">
       <p class="form-signin-heading">The study is intended to collect the details of adverse events experienced by patients prescribed Tofacitinib. Tofacitinib is a JAK inhibitor with a significant potential of adverse events. The drug being a potent immune-suppressive, infection and predisposition to malignancy is a concern. Tofacitinib was earlier because of economical reason was less prescribed. The prescription number has increased significantly with availability of generics leading to increased concern on its adverse events. We donot have a real-time data of adverse events of this drug with refinance to India. Your data will help us to understand the safety and to formulate guidelines to improvise its utility.</p>
       <p>The study has been cleared by central ethical committee for collecting data.</p>
       <p>
         
Data privacy is completely maintained the name of the patients and other demographics are not visible to central data monitors and only the person who has entered data using his login credential can see.
       </p>
       <p>Please join and contribute. Any one who has entered more than ten will be acknowledge as authors. </p>

      <ol>
        <li>You install the application. Since it is a private application and the security alert may come
          sometime. Please click install anyway. The source is from the database and will be sent by
          email only by your known person.</li>
          <li>Register to the application Tofacare if you are willing to participate in the study.</li>
          <li>The application works only in Android phone and those who are using Iphone You need to work through your browser and the same can be accessed by laptop or any
          other system- https://chanrericr.com/tofacare/index.php</li>
          <li>Once you register wait for activation by admin. Normally it may take few hours to 24 hours.</li>
          <li>Some time e-mail verification or request may come to verify credentials.</li>
      </ol>
     </div>
      </div>

     </div>
     <div class="row">

</div>
    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>


  </body>
</html>
