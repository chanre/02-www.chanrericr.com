<form  method="post">
             <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Name</label>
                   <input type="text" class="form-control" name="name"  placeholder="Please enter name"  >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">File no</label>
                   <input type="text" class="form-control" name="fileno"  placeholder="File No"  >
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Diagnosis</label>
                   <select class="form-control" name="diagnosis" >
                    <option value="">Choose an option</option>
                     <option value="RA">RA</option>
                     <option value="SLE">SLE</option>
                     <option value="AS">AS</option>
                     <option value="PsA">PsA</option>
                     <option value="Others">Others</option>                     
                   </select>
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
             
                   <div class="col-md-3 mb-3">
                   <input type="text" class="form-control" name="username" value="<?php echo $_SESSION['login']; ?>"  hidden >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              </div>
                    <div class="form-row">           
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom04">Age</label>
                        <input type="text" class="form-control" name="age" id="validationCustom04" placeholder="Age" >
                        <div class="invalid-feedback">
                          Please provide age
                        </div>
                   </div>
                   <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <select class="form-control" name="dosage" >
                    <option value="">Choose an option</option>
                     <option value="5mg">5mg</option>
                     <option value="10mg">10mg</option>
                     <option value="15mg">15mg</option>
                     <option value="20mg">20mg</option>                    
                   </select>
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                        <label for="validationCustom04">Duration</label>
                        <input type="text" class="form-control" name="dose_duration" id="validationCustom04" placeholder="Dose Duration" >
                        <div class="invalid-feedback">
                          Please provide age
                        </div>
                   </div>
                                     
                                  </div>
                                
              
              <hr>


              <script>
function myFunction() {
  var a= document.getElementById('disable').value;
  if(a == 'No'){
    document.getElementById("asplenia").disabled = true;
    document.getElementById("aspduration").disabled = true;
    document.getElementById("cancer").disabled = true;
    document.getElementById("cancerduration").disabled = true;
    document.getElementById("chd").disabled = true;
    document.getElementById("chdd").disabled = true;
    document.getElementById("ckd").disabled = true;
    document.getElementById("ckdd").disabled = true;
    document.getElementById("cld").disabled = true;
    document.getElementById("cldd").disabled = true;
    document.getElementById("clud").disabled = true;
    document.getElementById("cludd").disabled = true;
    document.getElementById("cnd").disabled = true;
    document.getElementById("cndd").disabled = true;
    document.getElementById("hiv").disabled = true;
    document.getElementById("hivd").disabled = true;
    document.getElementById("art").disabled = true;
    document.getElementById("hypertension").disabled = true;
    document.getElementById("hyperduration").disabled = true;
    document.getElementById("hyparticipant").disabled = true;
    document.getElementById("hypduration").disabled = true;
    document.getElementById("immunodeficiency").disabled = true;
    document.getElementById("immunoduration").disabled = true;
    document.getElementById("mh").disabled = true;
    document.getElementById("mhd").disabled = true;
    document.getElementById("mspecify").disabled = true;
    document.getElementById("tb").disabled = true;
    document.getElementById("tbd").disabled = true;
    document.getElementById("aird").disabled = true;
    document.getElementById("airdd").disabled = true;
    document.getElementById("airother").disabled = true;
    document.getElementById("airspecify").disabled = true;
    document.getElementById("obesity").disabled = true;
    document.getElementById("diabetics").disabled = true;
    document.getElementById("diabeticsdur").disabled = true;
  }
  else{
    document.getElementById("asplenia").disabled = false;
    document.getElementById("aspduration").disabled = false;
    document.getElementById("cancer").disabled = false;
    document.getElementById("cancerduration").disabled = false;
    document.getElementById("chd").disabled = false;
    document.getElementById("chdd").disabled = false;
    document.getElementById("ckd").disabled = false;
    document.getElementById("ckdd").disabled = false;
    document.getElementById("cld").disabled = false;
    document.getElementById("cldd").disabled = false;
    document.getElementById("clud").disabled = false;
    document.getElementById("cludd").disabled = false;
    document.getElementById("cnd").disabled = false;
    document.getElementById("cndd").disabled = false;
    document.getElementById("hiv").disabled = false;
    document.getElementById("hivd").disabled = false;
    document.getElementById("art").disabled = false;
    document.getElementById("hypertension").disabled = false;
    document.getElementById("hyperduration").disabled = false;
    document.getElementById("hyparticipant").disabled = false;
    document.getElementById("hypduration").disabled = false;
    document.getElementById("immunodeficiency").disabled = false;
    document.getElementById("immunoduration").disabled = false;
    document.getElementById("mh").disabled = false;
    document.getElementById("mhd").disabled = false;
    document.getElementById("mspecify").disabled = false;
    document.getElementById("tb").disabled = false;
    document.getElementById("tbd").disabled = false;
    document.getElementById("aird").disabled = false;
    document.getElementById("airdd").disabled = false;
    document.getElementById("airother").disabled = false;
    document.getElementById("airspecify").disabled = false;
    document.getElementById("obesity").disabled = false;
    document.getElementById("diabetics").disabled = false;
    document.getElementById("diabeticsdur").disabled = false;
  }
  
}
</script>
<h6>PRE-EXISTING CONDITIONS</h6>
               
                <hr>
                <div class="form-row">
                  <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Presence of any comorbidity</label>
                   <select class="form-control" name="comorbidity" id="disable" onchange="myFunction()">
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
                </div>
               <div class="form-row">

                 <div class="col-md-3 mb-3">

                   <label for="validationCustom01">Asplenia</label>
                   <select class="form-control" name="asplenia" id="asplenia" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="aspduration" id="aspduration" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Cancer</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="cancer" id="cancer" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <input type="text" class="form-control" name="cancerduration" id="cancerduration" placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic heart disease (not hypertension)</label>
                   <select class="form-control" name="chd" id="chd" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="chdd" id="chdd" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Chronic kidney disease</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="ckd" id="ckd" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="ckdd" id="ckdd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>
              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic liver disease</label>
                   <select class="form-control" name="cld" id="cld" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="cldd" id="cldd" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Chronic lung disease</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="clud" id="clud" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="cludd" id="cludd" placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic neurological disorder</label>
                   <select class="form-control" name="cnd" id="cnd">
                    <option value="">Choose an option</option>
                      <option value="Dementia">Dementia</option>
                     <option value="Stroke">Stroke</option>
                     <option value="Multiple Sclerosis">Multiple Sclerosis</option>  
                     <option value="Parkinsons's disease">Parkinsons's disease</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="cndd" id="cndd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
             <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Diabetes</label>
                   <select class="form-control" name="diabetics" id="diabetics" >
                    <option value="">Choose an option</option>
                      <option value="Yes">Yes</option>
                     <option value="No">No</option>
                                          
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="diabeticsdur" id="diabeticsdur" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               
              </div>

 <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">HIV</label>
                   <select class="form-control" name="hiv" id="hiv">
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="hivd" id="hivd" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If yes, was on ART?</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="art" id="art">
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
               
              </div>

<div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Hypertension:</label>
                   <select class="form-control" name="hypertension" id="hypertension">
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="hyperduration" id="hyperduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If Yes, participant receive medication?</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="hyparticipant" id="hyparticipant" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="hypduration" id="hypduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>


              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Immunodeficiency:</label>
                   <select class="form-control" name="immunodeficiency" id="immunodeficiency" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="immunoduration" id="immunoduration" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Mental health conditions</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="mh" id="mh" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="mhd" id="mhd" placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">If yes specify.</label>
                   <select class="form-control" name="mspecify" id="mspecify">
                    <option value="">Choose an option</option>
                     <option value="Psychosis">Psychosis</option>
                     <option value="Depression">Depression</option> 
                     <option value="anxiety">Anxiety</option>                    
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
             
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Obesity(BMI>30)</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="obesity" id="obesity">
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
               <!-- <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                   <span class="input-group-text" id="inputGroupPrepend">@</span> 
                    </div>
                   <input type="text" class="form-control" name="obesityduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>-->
              </div>

               <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Tuberculosis</label>
                   <select class="form-control" name="tb" id="tb" >
                    <option value="">Choose an option</option>
                     <option value="Active">Active</option>
                     <option value="Previous">Previous</option>  
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="tbd" id="tbd" placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
              </div>

             <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Autoimmune Rheumatic Diseases (AIRD)</label>
                   <select class="form-control" name="aird" id="aird">
                    <option value="">Choose an option</option>
                     <option value="Rheumatoid Arthritis">Rheumatoid Arthritis</option>
                     <option value="Ankylosing Spondylitis">Ankylosing Spondylitis</option>
                     <option value="SLE">SLE</option>
                     <option value="Psoriatic Arthritis">Psoriatic Arthritis</option>
                     <option value="Sjogren’s Syndrome">Sjogren’s Syndrome</option>
                     <option value="Scleroderma">Scleroderma</option>
                     <option value="Reactive Arthritis">Reactive Arthritis</option>
                     <option value="Vasculitis">Vasculitis</option>
                     <option value="Dermatomyositis & Polymyositis">Dermatomyositis & Polymyositis</option>
                     <option value="Others">Others</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="airdd" id="airdd"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Any Other</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="airother" id="airother">
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If yes, Specify</label>
                    <div class="input-group">
                       <div class="input-group-prepend">
                        </div>
                       <input type="text" class="form-control" name="airspecify" id="airspecify"  placeholder="specify" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please specify</div>
                    </div>
                </div>
              </div>

  <h6><label>Screening Test</label></h6>
              <div class="form-row">

                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">TB</label>
                   <select class="form-control" name="tbst" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">Not Done</option>
                                         
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">TB Report</label>
                   <select class="form-control" name="tbr" >
                    <option value="">Choose an option</option>
                     <option value="Positive">Positive</option>
                     <option value="Negative">Negative</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">HbSag</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                   <select class="form-control" name="hbst" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">Not Done</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                 <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">HbSag Report</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                   <select class="form-control" name="hbr" >
                    <option value="">Choose an option</option>
                     <option value="Positive">Positive</option>
                     <option value="Negative">Negative</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">HIV</label>
                   <select class="form-control" name="hivst" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">Not Done</option>
                                         
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">HIV Report</label>
                   <select class="form-control" name="hivr" >
                    <option value="">Choose an option</option>
                     <option value="Positive">Positive</option>
                     <option value="Negative">Negative</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Hepatitis C</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="hcst" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">Not Done</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                 <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Hepatitis C Report</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="hcr" >
                    <option value="">Choose an option</option>
                     <option value="Positive">Positive</option>
                     <option value="Negative">Negative</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                
              </div>


              <div class="form-row">
                 
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Quantiferom</label>
                   <select class="form-control" name="quantiferom" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Monto</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="monto" >
                    <option value="">Choose an option</option>
                     <option value="Yes">Yes</option>
                     <option value="No">No</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                
              </div>





              <div class="form-row">
                 <div class="col-md-12 mb-12">
                      <section class="card">
                          <header class="card-header">
                              Medication
                          </header>
                          <div class="card-body">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                          <th>Drug</th>
                                          <th>Current Drug</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>Anti Diabetic</td>
                                          <td><input type="text" class="form-control" name="antidiabetic"></td>
                                      </tr>
                                      <tr>
                                          <td>Anti-Hypertensive</td>
                                          <td><input type="text" class="form-control" name="antihypertensive"></td>
                                      </tr>
                                      <tr>
                                          <td>Anti Epileptic</td>
                                          <td><input type="text" class="form-control" name="epileptic"></td>
                                      </tr>
                                      <tr>
                                          <td>DMARDS</td>
                                          <td><input type="text" class="form-control" name="dmards"></td>
                                      </tr>
                                      <tr>
                                          <td>Steroids</td>
                                          <td><input type="text" class="form-control" name="steroids"></td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </section>
                  
                  </div>               
              </div>

               <div class="form-row">

                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Infections and infestations</label>
                    <select class="form-control" name="infections[]" multiple id="langOpt1">
                    <option value="">Choose an option</option>
                     <option value="Pneumonia">Pneumonia</option>
                     <option value="Influenza">Influenza</option>
                     <option value="Urinary tract infection">Urinary tract infection</option>
                     <option value="Sinusitis">Sinusitis</option>
                     <option value="Bronchitis">Bronchitis</option>
                     <option value="Nasopharyngitis">Nasopharyngitis</option>
                     <option value="Pharyngitis">Pharyngitis</option>                    
                     <option value="Diverticulitis">Diverticulitis</option>
                     <option value="Pyelonephritis">Pyelonephritis</option>
                     <option value="Cellulitis">Cellulitis</option>
                     <option value="Herpes simplex">Herpes simplex</option>
                     <option value="Gastroenteritis viral">Gastroenteritis viral</option>
                     <option value="Viral infection">Viral infection</option>
                     <option value="Sepsis">Sepsis</option>
                     <option value="Urosepsis">Urosepsis</option>
                     <option value="Necrotizing fasciitis">Necrotizing fasciitis</option>
                     <option value="Bacteremia">Bacteremia</option>
                     <option value="Staphylococcal bacteremia">Staphylococcal bacteremia</option>
                     <option value="Pneumocystis jirovecii pneumonia">Pneumocystis jirovecii pneumonia</option>
                     <option value="Pneumonia pneumococcal">Pneumonia pneumococcal</option>
                     <option value="Pneumonia bacterial">Pneumonia bacterial</option>
                     <option value="Encephalitis">Encephalitis</option>
                     <option value="Arthritis bacterial">Arthritis bacterial</option>
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Infection on special interest</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="infectionsi[]" multiple id="langOpt2">
                    <option value="">Choose an option</option>
                     <option value="Herpes zoster">Herpes zoster</option>
                     <option value="Tuberculosis">Tuberculosis</option>
                     <option value="Disseminated TB">Disseminated TB</option>
                     <option value="Atypical mycobacterial infection">Atypical mycobacterial infection</option> 
                     <option value="Cytomegalovirus infection">Cytomegalovirus infection</option> 
                     <option value="Tuberculosis of central nervous system">Tuberculosis of central nervous system</option> 
                     <option value="Meningitis cryptococcal">Meningitis cryptococcal</option>
                     <option value="Mycobacterium avium complex infection">Mycobacterium avium complex infection</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>

               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Blood and lymphatic system disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="blsd[]" multiple id="langOpt3">
                    <option value="">Choose an option</option>
                     <option value="Anemia">Anemia</option>
                     <option value="Leukopenia">Leukopenia</option>
                     <option value="Lymphopenia">Lymphopenia</option>
                     <option value="Neutropenia">Neutropenia</option>
                    </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Drug hypersensitivity</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="drughypers[]" multiple id="langOpt4">
                    <option value="">Choose an option</option>
                     <option value="Angioedema">Angioedema</option>
                     <option value="Urticaria">Urticaria</option>
                    </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
              <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Neoplasms(Malignancy)</label>
                   <select class="form-control" name="neoplasm[]" multiple id="langOpt5">
                    <option value="">Choose an option</option>
                     <option value="Non-melanoma skin cancers">Non-melanoma skin cancers</option>
                     <option value="Malignant Melanoma">Malignant Melanoma</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
              </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Metabolism and nutrition disorders</label>
                   <select class="form-control" name="metabolism[]" multiple id="langOpt6">
                    <option value="">Choose an option</option>
                     <option value="Dyslipidemia">Dyslipidemia</option>
                     <option value="Hyperlipidemia">Hyperlipidemia</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Psychiatric disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="psychiatric[]" multiple id="langOpt7">
                    <option value="">Choose an option</option>
                     <option value="Insomnia">Insomnia</option>
                     <option value="Psychosis">Psychosis</option>
                     <option value="Depression">Depression</option> 
                     <option value="Neurosis">Neurosis</option>                    
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Nervous system disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                  <select class="form-control" name="nsd[]" multiple id="langOpt8">
                    <option value="Headache">Headache</option>
                    <option value="Paraesthesia">Paraesthesia</option>
                  </select>
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>


              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Vascular disorders</label>
                   <select class="form-control" name="vsd[]" multiple id="langOpt9">
                    <option value="">Choose an option</option>
                     <option value="Hypertension">Hypertension</option>
                     <option value="Venous thromboembolism">Venous thromboembolism</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Respiratory, thoracic and mediastinal disorders (without infection)</label>
                   <select class="form-control" name="rtmd[]" multiple id="langOpt10" >
                    <option value="">Choose an option</option>
                     <option value="Cough">Cough</option>
                     <option value="Dyspnea">Dyspnea</option>
                     <option value="Sinus congestion">Sinus congestion</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Gastrointestinal disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="gd[]" multiple id="langOpt11">
                    <option value="">Choose an option</option>
                     <option value="Abdominal pain">Abdominal pain</option>
                     <option value="Vomiting">Vomiting</option>
                     <option value="Diarrhea">Diarrhea</option> 
                     <option value="Nausea">Nausea</option>
                     <option value="Gastritis">Gastritis</option>
                     <option value="Dyspepsia">Dyspepsia</option>                    
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Hepatobiliary disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                  <select class="form-control" name="hepatobiliary[]" multiple id="langOpt12">
                    <option value="Hepatic steatosis">Hepatic steatosis</option>
                    <option value="Increase in hepatic enzymes">Increase in hepatic enzymes</option>
                    <option value="Increase in transaminases ">Increase in transaminases </option>
                    <option value="Liver function test abnormal">Liver function test abnormal</option>
                    <option value="Gamma glutamyl-transferase increased">Gamma glutamyl-transferase increased</option>
                  </select>
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Skin and subcutaneous tissue disorders</label>
                   <select class="form-control" name="sstd[]" multiple id="langOpt13">
                    <option value="">Choose an option</option>
                     <option value="Rash">Rash</option>
                     <option value="Erythema">Erythema</option>
                     <option value="Pruritus">Pruritus</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
           
              </div>

          <button class="btn btn-primary" type="submit" name="submit">Submit</button>
      </form>
                          
                             