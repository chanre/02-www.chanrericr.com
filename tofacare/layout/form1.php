<?php 
$rid=intval($_GET['pid']);
$query=mysqli_query($con,"Select * from tofacare where id='$rid'");

while ($rows=mysqli_fetch_array($query)) {

 ?>
<form  method="post">
             <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Name</label>
                   <input type="text" class="form-control" name="name"  value="<?php echo htmlentities($rows['name']); ?>" >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">File no</label>
                   <input type="text" class="form-control" name="fileno"  value="<?php echo htmlentities($rows['fileno']); ?>"  >
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Diagnosis</label>
                   <select class="form-control" name="diagnosis" >
                    <option value="">Choose an option</option>
                     <option value="RA" <?php if($rows['diagnosis'] == 'RA') { ?> selected="selected"<?php } ?>>RA</option>
                     <option value="SLE" <?php if($rows['diagnosis'] == 'SLE') { ?> selected="selected"<?php } ?>>SLE</option>
                     <option value="AS" <?php if($rows['diagnosis'] == 'AS') { ?> selected="selected"<?php } ?>>AS</option>
                     <option value="PsA" <?php if($rows['diagnosis'] == 'PsA') { ?> selected="selected"<?php } ?> >PsA</option>
                     <option value="Others" <?php if($rows['diagnosis'] == 'Others') { ?> selected="selected"<?php } ?>>Others</option>                     
                   </select>
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
             
                   <div class="col-md-3 mb-3">
                   <input type="text" class="form-control" name="username" value="<?php echo $_SESSION['login']; ?>"  hidden >
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              </div>
                    <div class="form-row">           
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom04">Age</label>
                        <input type="text" class="form-control" name="age" id="validationCustom04" value="<?php echo htmlentities($rows['age']); ?>" >
                        <div class="invalid-feedback">
                          Please provide age
                        </div>
                   </div>
                   <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Dosage</label>
                   <select class="form-control" name="dosage" >
                    <option value="">Choose an option</option>
                     <option value="5mg" <?php if($rows['dosage'] == '5mg') { ?> selected="selected"<?php } ?>>5mg</option>
                     <option value="10mg" <?php if($rows['dosage'] == '10mg') { ?> selected="selected"<?php } ?> >10mg</option>
                     <option value="15mg" <?php if($rows['dosage'] == '15mg') { ?> selected="selected"<?php } ?>>15mg</option>
                     <option value="20mg" <?php if($rows['dosage'] == '20mg') { ?> selected="selected"<?php } ?>>20mg</option>                    
                   </select>
                  <div class="valid-feedback">
                      Looks good!
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                        <label for="validationCustom04">Age</label>
                        <input type="text" class="form-control" name="dose_duration" id="validationCustom04" value="<?php echo htmlentities($rows['dose_duration']); ?>" >
                        <div class="invalid-feedback">
                          Please provide age
                        </div>
                   </div>
                                     
                                  </div>
                                
              
              <hr>


              <script>
function myFunction() {
  var a= document.getElementById('disable').value;
  if(a == 'No'){
    document.getElementById("asplenia").disabled = true;
    document.getElementById("aspduration").disabled = true;
    document.getElementById("cancer").disabled = true;
    document.getElementById("cancerduration").disabled = true;
    document.getElementById("chd").disabled = true;
    document.getElementById("chdd").disabled = true;
    document.getElementById("ckd").disabled = true;
    document.getElementById("ckdd").disabled = true;
    document.getElementById("cld").disabled = true;
    document.getElementById("cldd").disabled = true;
    document.getElementById("clud").disabled = true;
    document.getElementById("cludd").disabled = true;
    document.getElementById("cnd").disabled = true;
    document.getElementById("cndd").disabled = true;
    document.getElementById("hiv").disabled = true;
    document.getElementById("hivd").disabled = true;
    document.getElementById("art").disabled = true;
    document.getElementById("hypertension").disabled = true;
    document.getElementById("hyperduration").disabled = true;
    document.getElementById("hyparticipant").disabled = true;
    document.getElementById("hypduration").disabled = true;
    document.getElementById("immunodeficiency").disabled = true;
    document.getElementById("immunoduration").disabled = true;
    document.getElementById("mh").disabled = true;
    document.getElementById("mhd").disabled = true;
    document.getElementById("mspecify").disabled = true;
    document.getElementById("tb").disabled = true;
    document.getElementById("tbd").disabled = true;
    document.getElementById("aird").disabled = true;
    document.getElementById("airdd").disabled = true;
    document.getElementById("airother").disabled = true;
    document.getElementById("airspecify").disabled = true;
    document.getElementById("obesity").disabled = true;
    document.getElementById("diabetics").disabled = true;
    document.getElementById("diabeticsdur").disabled = true;
  }
  else{
    document.getElementById("asplenia").disabled = false;
    document.getElementById("aspduration").disabled = false;
    document.getElementById("cancer").disabled = false;
    document.getElementById("cancerduration").disabled = false;
    document.getElementById("chd").disabled = false;
    document.getElementById("chdd").disabled = false;
    document.getElementById("ckd").disabled = false;
    document.getElementById("ckdd").disabled = false;
    document.getElementById("cld").disabled = false;
    document.getElementById("cldd").disabled = false;
    document.getElementById("clud").disabled = false;
    document.getElementById("cludd").disabled = false;
    document.getElementById("cnd").disabled = false;
    document.getElementById("cndd").disabled = false;
    document.getElementById("hiv").disabled = false;
    document.getElementById("hivd").disabled = false;
    document.getElementById("art").disabled = false;
    document.getElementById("hypertension").disabled = false;
    document.getElementById("hyperduration").disabled = false;
    document.getElementById("hyparticipant").disabled = false;
    document.getElementById("hypduration").disabled = false;
    document.getElementById("immunodeficiency").disabled = false;
    document.getElementById("immunoduration").disabled = false;
    document.getElementById("mh").disabled = false;
    document.getElementById("mhd").disabled = false;
    document.getElementById("mspecify").disabled = false;
    document.getElementById("tb").disabled = false;
    document.getElementById("tbd").disabled = false;
    document.getElementById("aird").disabled = false;
    document.getElementById("airdd").disabled = false;
    document.getElementById("airother").disabled = false;
    document.getElementById("airspecify").disabled = false;
    document.getElementById("obesity").disabled = false;
    document.getElementById("diabetics").disabled = false;
    document.getElementById("diabeticsdur").disabled = false;
  }
  
}
</script>
<h6>PRE-EXISTING CONDITIONS</h6>
                <p>In the year prior to the acute illness of COVID-19, has the participant been diagnosed with any of the following conditions?</p>
                <hr>
                <div class="form-row">
                  <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Presence of any comorbidity</label>
                   <select class="form-control" name="comorbidity" id="disable" onchange="myFunction()">
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['comorbidity'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['comorbidity'] == 'No') { ?> selected="selected"<?php } ?> >No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
                </div>
               <div class="form-row">

                 <div class="col-md-3 mb-3">

                   <label for="validationCustom01">Asplenia</label>
                   <select class="form-control" name="asplenia" id="asplenia" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['asplenia'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['asplenia'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="aspduration" id="aspduration" value="<?php echo htmlentities($rows['aspduration']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Cancer</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="cancer" id="cancer" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['cancer'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['cancer'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <input type="text" class="form-control" name="cancerduration" id="cancerduration" value="<?php echo htmlentities($rows['cancerduration']); ?>" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic heart disease (not hypertension)</label>
                   <select class="form-control" name="chd" id="chd" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['chd'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['chd'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control"value="<?php echo htmlentities($rows['chdd']); ?>" name="chdd" id="chdd"  aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Chronic kidney disease</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="ckd" id="ckd" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['ckd'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['ckd'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="ckdd" id="ckdd"  value="<?php echo htmlentities($rows['ckdd']); ?>" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>
              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic liver disease</label>
                   <select class="form-control" name="cld" id="cld" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['cld'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['cld'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="cldd" id="cldd" value="<?php echo htmlentities($rows['cldd']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Chronic lung disease</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="clud" id="clud" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['clud'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['clud'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="cludd" id="cludd" value="<?php echo htmlentities($rows['cludd']); ?>" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Chronic neurological disorder</label>
                   <select class="form-control" name="cnd" id="cnd">
                    <option value="">Choose an option</option>
                      <option value="Dementia" <?php if($rows['cnd'] == 'Dementia') { ?> selected="selected"<?php } ?>>Dementia</option>
                     <option value="Stroke" <?php if($rows['cnd'] == 'Stroke') { ?> selected="selected"<?php } ?>>Stroke</option>
                     <option value="Multiple Sclerosis" <?php if($rows['cnd'] == 'Multiple Sclerosis') { ?> selected="selected"<?php } ?>>Multiple Sclerosis</option>  
                     <option value="Parkinsons disease" <?php if($rows['cnd'] == 'Parkinsons disease') { ?> selected="selected"<?php } ?>>Parkinsons's disease</option>
                     <option value="No" <?php if($rows['cnd'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="cndd" id="cndd" value="<?php echo htmlentities($rows['cndd']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
             <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Diabetes</label>
                   <select class="form-control" name="diabetics" id="diabetics" >
                    <option value="">Choose an option</option>
                      <option value="Yes" <?php if($rows['diabetics'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['diabetics'] == 'No') { ?> selected="selected"<?php } ?>>No</option>
                                          
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="diabeticsdur" id="diabeticsdur" value="<?php echo htmlentities($rows['diabeticsdur']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               
              </div>

 <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">HIV</label>
                   <select class="form-control" name="hiv" id="hiv">
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['hiv'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['hiv'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="hivd" id="hivd" value="<?php echo htmlentities($rows['hivd']); ?>"aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If yes, was on ART?</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="art" id="art">
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['art'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['art'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
               
              </div>

<div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Hypertension:</label>
                   <select class="form-control" name="hypertension" id="hypertension">
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['hypertension'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['hypertension'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="hyperduration" id="hyperduration"  value="<?php echo htmlentities($rows['hyperduration']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If Yes, participant receive medication?</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="hyparticipant" id="hyparticipant" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['hyparticipant'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['hyparticipant'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="hypduration" id="hypduration"  value="<?php echo htmlentities($rows['hypduration']); ?>" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>


              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Immunodeficiency:</label>
                   <select class="form-control" name="immunodeficiency" id="immunodeficiency" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['immunodeficiency'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['immunodeficiency'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="immunoduration" id="immunoduration" value="<?php echo htmlentities($rows['immunoduration']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Mental health conditions</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="mh" id="mh" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['mh'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['mh'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <input type="text" class="form-control" name="mhd" id="mhd" value="<?php echo htmlentities($rows['mhd']); ?>" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">If yes specify.</label>
                   <select class="form-control" name="mspecify" id="mspecify">
                    <option value="">Choose an option</option>
                     <option value="Psychosis" <?php if($rows['mspecify'] == 'Psychosis') { ?> selected="selected"<?php } ?>>Psychosis</option>
                     <option value="Depression" <?php if($rows['mspecify'] == 'Depression') { ?> selected="selected"<?php } ?>>Depression</option> 
                     <option value="anxiety" <?php if($rows['mspecify'] == 'anxiety') { ?> selected="selected"<?php } ?>>Anxiety</option>                    
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
             
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Obesity(BMI>30)</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="obesity" id="obesity">
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['obesity'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['obesity'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
               <!-- <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Duration</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                   <span class="input-group-text" id="inputGroupPrepend">@</span> 
                    </div>
                   <input type="text" class="form-control" name="obesityduration"  placeholder="duration" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>-->
              </div>

               <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Tuberculosis</label>
                   <select class="form-control" name="tb" id="tb" >
                    <option value="">Choose an option</option>
                     <option value="Active" <?php if($rows['tb'] == 'Active') { ?> selected="selected"<?php } ?>>Active</option>
                     <option value="Previous" <?php if($rows['tb'] == 'Previous') { ?> selected="selected"<?php } ?>>Previous</option>  
                     <option value="No" <?php if($rows['tb'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="tbd" id="tbd" value="<?php echo htmlentities($rows['tbd']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
              
              </div>

             <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Autoimmune Rheumatic Diseases (AIRD)</label>
                   <select class="form-control" name="aird" id="aird">
                    <option value="">Choose an option</option>
                     <option value="Rheumatoid Arthritis" <?php if($rows['aird'] == 'Rheumatoid Arthritis') { ?> selected="selected"<?php } ?> >Rheumatoid Arthritis</option>
                     <option value="Ankylosing Spondylitis" <?php if($rows['aird'] == 'Ankylosing Spondylitis') { ?> selected="selected"<?php } ?>>Ankylosing Spondylitis</option>
                     <option value="SLE" <?php if($rows['aird'] == 'SLE') { ?> selected="selected"<?php } ?>>SLE</option>
                     <option value="Psoriatic Arthritis" <?php if($rows['aird'] == 'Psoriatic Arthritis') { ?> selected="selected"<?php } ?>>Psoriatic Arthritis</option>
                     <option value="Sjogrens Syndrome" <?php if($rows['aird'] == 'Sjogrens Syndrome') { ?> selected="selected"<?php } ?>>Sjogren’s Syndrome</option>
                     <option value="Scleroderma" <?php if($rows['aird'] == 'Scleroderma') { ?> selected="selected"<?php } ?>>Scleroderma</option>
                     <option value="Reactive Arthritis" <?php if($rows['aird'] == 'Reactive Arthritis') { ?> selected="selected"<?php } ?> >Reactive Arthritis</option>
                     <option value="Vasculitis" <?php if($rows['aird'] == 'Vasculitis') { ?> selected="selected"<?php } ?> >Vasculitis</option>
                     <option value="Dermatomyositis & Polymyositis" <?php if($rows['aird'] == 'Dermatomyositis & Polymyositis') { ?> selected="selected"<?php } ?> >Dermatomyositis & Polymyositis</option>
                     <option value="Others" <?php if($rows['aird'] == 'Others') { ?> selected="selected"<?php } ?> >Others</option>
                     <option value="No" <?php if($rows['aird'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Duration</label>
                   <input type="text" class="form-control" name="airdd" id="airdd"  value="<?php echo htmlentities($rows['airdd']); ?>" aria-describedby="inputGroupPrepend" >
                  <div class="invalid-feedback"> 
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Any Other</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="airother" id="airother">
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['airother'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['airother'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">If yes, Specify</label>
                    <div class="input-group">
                       <div class="input-group-prepend">
                        </div>
                       <input type="text" class="form-control" name="airspecify" id="airspecify"  value="<?php echo htmlentities($rows['airspecify']); ?>" aria-describedby="inputGroupPrepend" >
                       <div class="invalid-feedback">Please specify</div>
                    </div>
                </div>
              </div>

  <h6><label>Screening Test</label></h6>
              <div class="form-row">

                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">TB</label>
                   <select class="form-control" name="tbst" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['tbst'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['tbst'] == 'No') { ?> selected="selected"<?php } ?>>Not Done</option>
                                         
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">TB Report</label>
                   <select class="form-control" name="tbr" >
                    <option value="">Choose an option</option>
                     <option value="Positive" <?php if($rows['tbr'] == 'Positive') { ?> selected="selected"<?php } ?>>Positive</option>
                     <option value="Negative" <?php if($rows['tbr'] == 'Negative  ') { ?> selected="selected"<?php } ?>>Negative</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">HbSag</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                       
                    </div>
                   <select class="form-control" name="hbst" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['hbst'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['hbst'] == 'No') { ?> selected="selected"<?php } ?>>Not Done</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                 <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">HbSag Report</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                   <select class="form-control" name="hbr" >
                    <option value="">Choose an option</option>
                     <option value="Positive" <?php if($rows['hbr'] == 'Positive') { ?> selected="selected"<?php } ?>>Positive</option>
                     <option value="Negative" <?php if($rows['hbr'] == 'Negative') { ?> selected="selected"<?php } ?>>Negative</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">HIV</label>
                   <select class="form-control" name="hivst" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['hivst'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['hivst'] == 'No') { ?> selected="selected"<?php } ?>>Not Done</option>
                                         
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">HIV Report</label>
                   <select class="form-control" name="hivr" >
                    <option value="">Choose an option</option>
                     <option value="Positive" <?php if($rows['hivr'] == 'Positive') { ?> selected="selected"<?php } ?>>Positive</option>
                     <option value="Negative" <?php if($rows['hivr'] == 'Negative') { ?> selected="selected"<?php } ?>>Negative</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Hepatitis C</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="hcst" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['hcst'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['hcst'] == 'No') { ?> selected="selected"<?php } ?>>Not Done</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                 <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Hepatitis C Report</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="hcr" >
                    <option value="">Choose an option</option>
                     <option value="Positive" <?php if($rows['hcr'] == 'Positive') { ?> selected="selected"<?php } ?>>Positive</option>
                     <option value="Negative" <?php if($rows['hcr'] == 'Negative') { ?> selected="selected"<?php } ?>>Negative</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                
              </div>


              <div class="form-row">
                 
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Quantiferom</label>
                   <select class="form-control" name="quantiferom" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['quantiferom'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['quantiferom'] == 'No') { ?> selected="selected"<?php } ?>>No</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Monto</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="monto" >
                    <option value="">Choose an option</option>
                     <option value="Yes" <?php if($rows['monto'] == 'Yes') { ?> selected="selected"<?php } ?>>Yes</option>
                     <option value="No" <?php if($rows['monto'] == 'No') { ?> selected="selected"<?php } ?>>No</option>           
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                
              </div>





              <div class="form-row">
                 <div class="col-md-12 mb-12">
                      <section class="card">
                          <header class="card-header">
                              Medication
                          </header>
                          <div class="card-body">
                              <div class="table-responsive">
                                  <table class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                          <th>Drug</th>
                                          <th>Current Drug</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>Anti Diabetic</td>
                                          <td><input type="text" class="form-control" value="<?php echo htmlentities($rows['antidiabetic']); ?>" name="antidiabetic"></td>
                                      </tr>
                                      <tr>
                                          <td>Anti-Hypertensive</td>
                                          <td><input type="text" class="form-control" name="antihypertensive" value="<?php echo htmlentities($rows['antihypertensive']); ?>"></td>
                                      </tr>
                                      <tr>
                                          <td>Anti Epileptic</td>
                                          <td><input type="text" class="form-control" name="epileptic" value="<?php echo htmlentities($rows['epileptic']); ?>"></td>
                                      </tr>
                                      <tr>
                                          <td>DMARDS</td>
                                          <td><input type="text" class="form-control" name="dmards" value="<?php echo htmlentities($rows['dmards']); ?>"></td>
                                      </tr>
                                      <tr>
                                          <td>Steroids</td>
                                          <td><input type="text" class="form-control" name="steroids" value="<?php echo htmlentities($rows['steroids']); ?>"></td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </section>
                  
                  </div>               
              </div>

               <div class="form-row">

                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Infections and infestations</label>
                    <select class="form-control" name="infections[]" multiple id="langOpt1">
                    <option value="">Choose an option</option>
                     <option value="Pneumonia" <?php if($rows['infections'] == 'Pneumonia') { ?> selected="selected"<?php } ?>>Pneumonia</option>
                     <option value="Influenza" <?php if($rows['infections'] == 'Influenza') { ?> selected="selected"<?php } ?>>Influenza</option>
                     <option value="Urinary tract infection" <?php if($rows['infections'] == 'Urinary tract infection') { ?> selected="selected"<?php } ?>>Urinary tract infection</option>
                     <option value="Sinusitis"<?php if($rows['infections'] == 'Sinusitis') { ?> selected="selected"<?php } ?>>Sinusitis</option>
                     <option value="Bronchitis"<?php if($rows['infections'] == 'Bronchitis') { ?> selected="selected"<?php } ?>>Bronchitis</option>
                     <option value="Nasopharyngitis" <?php if($rows['infections'] == 'Nasopharyngitis') { ?> selected="selected"<?php } ?>>Nasopharyngitis</option>
                     <option value="Pharyngitis" <?php if($rows['infections'] == 'Pharyngitis') { ?> selected="selected"<?php } ?>>Pharyngitis</option>                    
                     <option value="Diverticulitis" <?php if($rows['infections'] == 'Diverticulitis') { ?> selected="selected"<?php } ?>>Diverticulitis</option>
                     <option value="Pyelonephritis" <?php if($rows['infections'] == 'Pyelonephritis') { ?> selected="selected"<?php } ?>>Pyelonephritis</option>
                     <option value="Cellulitis" <?php if($rows['infections'] == 'Cellulitis') { ?> selected="selected"<?php } ?>>Cellulitis</option>
                     <option value="Herpes simplex" <?php if($rows['infections'] == 'Herpes simplex') { ?> selected="selected"<?php } ?>>Herpes simplex</option>
                     <option value="Gastroenteritis viral" <?php if($rows['infections'] == 'Gastroenteritis viral') { ?> selected="selected"<?php } ?>>Gastroenteritis viral</option>
                     <option value="Viral infection" <?php if($rows['infections'] == 'Viral infection') { ?> selected="selected"<?php } ?> >Viral infection</option>
                     <option value="Sepsis" <?php if($rows['infections'] == 'Sepsis') { ?> selected="selected"<?php } ?> >Sepsis</option>
                     <option value="Urosepsis" <?php if($rows['infections'] == 'Urosepsis') { ?> selected="selected"<?php } ?>>Urosepsis</option>
                     <option value="Necrotizing fasciitis" <?php if($rows['infections'] == 'Necrotizing fasciitis') { ?> selected="selected"<?php } ?> >Necrotizing fasciitis</option>
                     <option value="Bacteremia" <?php if($rows['infections'] == 'Bacteremia') { ?> selected="selected"<?php } ?>>Bacteremia</option>
                     <option value="Staphylococcal bacteremia" <?php if($rows['infections'] == 'Staphylococcal bacteremia') { ?> selected="selected"<?php } ?>>Staphylococcal bacteremia</option>
                     <option value="Pneumocystis jirovecii pneumonia" <?php if($rows['infections'] == 'Pneumocystis jirovecii pneumonia') { ?> selected="selected"<?php } ?>>Pneumocystis jirovecii pneumonia</option>
                     <option value="Pneumonia pneumococcal" <?php if($rows['infections'] == 'Pneumonia pneumococcal') { ?> selected="selected"<?php } ?>>Pneumonia pneumococcal</option>
                     <option value="Pneumonia bacterial" <?php if($rows['infections'] == 'Pneumonia bacterial') { ?> selected="selected"<?php } ?>>Pneumonia bacterial</option>
                     <option value="Encephalitis" <?php if($rows['infections'] == 'Encephalitis') { ?> selected="selected"<?php } ?>>Encephalitis</option>
                     <option value="Arthritis bacterial" <?php if($rows['infections'] == 'Arthritis bacterial') { ?> selected="selected"<?php } ?>>Arthritis bacterial</option>
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Infection on special interest</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="infectionsi[]" multiple id="langOpt2">
                    <option value="">Choose an option</option>
                     <option value="Herpes zoster"  <?php if($rows['infectionsi'] == 'Herpes zoster') { ?> selected="selected"<?php } ?>>Herpes zoster</option>
                     <option value="Tuberculosis" <?php if($rows['infectionsi'] == 'Tuberculosis') { ?> selected="selected"<?php } ?>>Tuberculosis</option>
                     <option value="Disseminated TB" <?php if($rows['infectionsi'] == 'Disseminated TB') { ?> selected="selected"<?php } ?>>Disseminated TB</option>
                     <option value="Atypical mycobacterial infection" <?php if($rows['infectionsi'] == 'Atypical mycobacterial infection') { ?> selected="selected"<?php } ?>>Atypical mycobacterial infection</option> 
                     <option value="Cytomegalovirus infection" <?php if($rows['infectionsi'] == 'Cytomegalovirus infection') { ?> selected="selected"<?php } ?>>Cytomegalovirus infection</option> 
                     <option value="Tuberculosis of central nervous system" <?php if($rows['infectionsi'] == 'Tuberculosis of central nervous system') { ?> selected="selected"<?php } ?>>Tuberculosis of central nervous system</option> 
                     <option value="Meningitis cryptococcal" <?php if($rows['infectionsi'] == 'Meningitis cryptococcal') { ?> selected="selected"<?php } ?>>Meningitis cryptococcal</option>
                     <option value="Mycobacterium avium complex infection" <?php if($rows['infectionsi'] == 'Mycobacterium avium complex infection') { ?> selected="selected"<?php } ?>>Mycobacterium avium complex infection</option>                     
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>

               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Blood and lymphatic system disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                           </div>
                   <select class="form-control" name="blsd[]" multiple id="langOpt3">
                    <option value="">Choose an option</option>
                     <option value="Anemia" <?php if($rows['blsd'] == 'Anemia') { ?> selected="selected"<?php } ?>>Anemia</option>
                     <option value="Leukopenia" <?php if($rows['blsd'] == 'Leukopenia') { ?> selected="selected"<?php } ?>>Leukopenia</option>
                     <option value="Lymphopenia" <?php if($rows['blsd'] == 'Lymphopenia') { ?> selected="selected"<?php } ?>>Lymphopenia</option>
                     <option value="Neutropenia" <?php if($rows['blsd'] == 'Neutropenia') { ?> selected="selected"<?php } ?>>Neutropenia</option>
                    </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Drug hypersensitivity</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="drughypers[]" multiple id="langOpt4">
                    <option value="">Choose an option</option>
                     <option value="Angioedema" <?php if($rows['drughypers'] == 'Angioedema') { ?> selected="selected"<?php } ?>>Angioedema</option>
                     <option value="Urticaria" <?php if($rows['drughypers'] == 'Urticaria') { ?> selected="selected"<?php } ?>>Urticaria</option>
                    </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
              <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Neoplasms(Malignancy)</label>
                   <select class="form-control" name="neoplasm[]" multiple id="langOpt5">
                    <option value="">Choose an option</option>
                     <option value="Non-melanoma skin cancers" <?php if($rows['neoplasm'] == 'Non-melanoma skin cancers') { ?> selected="selected"<?php } ?>>Non-melanoma skin cancers</option>
                     <option value="Malignant Melanoma" <?php if($rows['neoplasm'] == 'Malignant Melanoma') { ?> selected="selected"<?php } ?> >Malignant Melanoma</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
              </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Metabolism and nutrition disorders</label>
                   <select class="form-control" name="metabolism[]" multiple id="langOpt6">
                    <option value="">Choose an option</option>
                     <option value="Dyslipidemia" <?php if($rows['metabolism'] == 'Dyslipidemia') { ?> selected="selected"<?php } ?>>Dyslipidemia</option>
                     <option value="Hyperlipidemia" <?php if($rows['metabolism'] == 'Hyperlipidemia') { ?> selected="selected"<?php } ?>>Hyperlipidemia</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Psychiatric disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="psychiatric[]" multiple id="langOpt7">
                    <option value="">Choose an option</option>
                     <option value="Insomnia" <?php if($rows['psychiatric'] == 'Insomnia') { ?> selected="selected"<?php } ?>>Insomnia</option>
                     <option value="Psychosis" <?php if($rows['psychiatric'] == 'Psychosis') { ?> selected="selected"<?php } ?>>Psychosis</option>
                     <option value="Depression" <?php if($rows['psychiatric'] == 'Depression') { ?> selected="selected"<?php } ?>>Depression</option> 
                     <option value="Neurosis" <?php if($rows['psychiatric'] == 'Neurosis') { ?> selected="selected"<?php } ?>>Neurosis</option>                    
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Nervous system disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                  <select class="form-control" name="nsd[]" multiple id="langOpt8">
                    <option value="Headache" <?php if($rows['nsd'] == 'Headache') { ?> selected="selected"<?php } ?>>Headache</option>
                    <option value="Paraesthesia" <?php if($rows['nsd'] == 'Paraesthesia') { ?> selected="selected"<?php } ?>>Paraesthesia</option>
                  </select>
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>


              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Vascular disorders</label>
                   <select class="form-control" name="vsd[]" multiple id="langOpt9">
                    <option value="">Choose an option</option>
                     <option value="Hypertension" <?php if( $rows['vsd'] == 'Hypertension') { ?> selected="selected"<?php } ?>>Hypertension</option>
                     <option value="Venous thromboembolism" <?php if($rows['vsd'] == 'Venous thromboembolism') { ?> selected="selected"<?php } ?>>Venous thromboembolism</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
              <div class="col-md-3 mb-3">
                   <label for="validationCustom02">Respiratory, thoracic and mediastinal disorders (without infection)</label>
                   <select class="form-control" name="rtmd[]" multiple id="langOpt10" >
                    <option value="">Choose an option</option>
                     <option value="Cough" <?php if( $rows['rtmd'] == 'Cough') { ?> selected="selected"<?php } ?>>Cough</option>
                     <option value="Dyspnea" <?php if( $rows['rtmd'] == 'Dyspnea') { ?> selected="selected"<?php } ?>>Dyspnea</option>
                     <option value="Sinus congestion" <?php if( $rows['rtmd'] == 'Sinus congestion') { ?> selected="selected"<?php } ?>>Sinus congestion</option>                     
                   </select>
                  <div class="invalid-feedback">
                     Please enter duration.
                  </div>
               </div>
               <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Gastrointestinal disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                    </div>
                   <select class="form-control" name="gd[]" multiple id="langOpt11">
                    <option value="">Choose an option</option>
                     <option value="Abdominal pain" <?php if( $rows['gd'] == 'Abdominal pain') { ?> selected="selected"<?php } ?>>Abdominal pain</option>
                     <option value="Vomiting" <?php if( $rows['gd'] == 'Vomiting') { ?> selected="selected"<?php } ?>>Vomiting</option>
                     <option value="Diarrhea" <?php if( $rows['gd'] == 'Diarrhea') { ?> selected="selected"<?php } ?>>Diarrhea</option> 
                     <option value="Nausea" <?php if( $rows['gd'] == 'Nausea') { ?> selected="selected"<?php } ?>>Nausea</option>
                     <option value="Gastritis" <?php if( $rows['gd'] == 'Gastritis') { ?> selected="selected"<?php } ?>>Gastritis</option>
                     <option value="Dyspepsia" <?php if( $rows['gd'] == 'Dyspepsia') { ?> selected="selected"<?php } ?>>Dyspepsia</option>                    
                   </select>
                       <div class="invalid-feedback">Please choose an option</div>
                        </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustomUsername">Hepatobiliary disorders</label>
                    <div class="input-group">
                    <div class="input-group-prepend">
                      
                    </div>
                  <select class="form-control" name="hepatobiliary[]" multiple id="langOpt12">
                    <option value="Hepatic steatosis" <?php if( $rows['hepatobiliary'] == 'Hepatic steatosis') { ?> selected="selected"<?php } ?>>Hepatic steatosis</option>
                    <option value="Increase in hepatic enzymes" <?php if( $rows['hepatobiliary'] == 'Increase in hepatic enzymes') { ?> selected="selected"<?php } ?>>Increase in hepatic enzymes</option>
                    <option value="Increase in transaminases " <?php if( $rows['hepatobiliary'] == 'Increase in transaminases') { ?> selected="selected"<?php } ?> >Increase in transaminases </option>
                    <option value="Liver function test abnormal" <?php if( $rows['hepatobiliary'] == 'Liver function test abnormal') { ?> selected="selected"<?php } ?>>Liver function test abnormal</option>
                    <option value="Gamma glutamyl-transferase increased" <?php if( $rows['hepatobiliary'] == 'Gamma glutamyl-transferase increased') { ?> selected="selected"<?php } ?>>Gamma glutamyl-transferase increased</option>
                  </select>
                       <div class="invalid-feedback">Please enter duration</div>
                        </div>
                </div>
              </div>

              <div class="form-row">
                 <div class="col-md-3 mb-3">
                   <label for="validationCustom01">Skin and subcutaneous tissue disorders</label>
                   <select class="form-control" name="sstd[]" multiple id="langOpt13">
                    <option value="">Choose an option</option>
                     <option value="Rash" <?php if( $rows['sstd'] == 'Rash') { ?> selected="selected"<?php } ?>>Rash</option>
                     <option value="Erythema" <?php if( $rows['sstd'] == 'Erythema') { ?> selected="selected"<?php } ?>>Erythema</option>
                     <option value="Pruritus" <?php if( $rows['sstd'] == 'Pruritus') { ?> selected="selected"<?php } ?>>Pruritus</option>                     
                   </select>
                  <div class="valid-feedback">
                   Looks good!
                  </div>
                  </div>
           
              </div>

          <button class="btn btn-primary" type="submit" name="submit">Submit</button>
      </form>
                          
                             
                          <?php } ?>
                             