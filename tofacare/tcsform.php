<?php 

session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
 $name = $_POST['name'];
 $fileno = $_POST['fileno'];
 $diagnosis = $_POST['diagnosis'];
 $username = $_POST['username'];
 $age = $_POST['age'];
 $dosage = $_POST['dosage'];
 $asplenia = $_POST['asplenia'];
 $cancer=$_POST['cancer'];
 $cancerduration=$_POST['cancerduration'];
 $aspduration = $_POST['aspduration'];
 $chd = $_POST['chd'];
 $chdd = $_POST['chdd'];
 $ckd = $_POST['ckd'];
 $ckdd =$_POST['ckdd'];
 $cld = $_POST['cld'];
 $cldd =$_POST['cldd'];
 $clud = $_POST['clud'];
 $cludd = $_POST['cludd'];
 $cnd = $_POST['cnd'];
 $cndd = $_POST['cndd'];
 $diabetics = $_POST['diabetics'];
 $diabeticsdur = $_POST['diabeticsdur'];
 $hiv = $_POST['hiv'];
 $hivd = $_POST['hivd'];
 $art = $_POST['art'];
 $hypertension = $_POST['hypertension'];
 $hyperduration = $_POST['hyperduration'];
 $hyparticipant = $_POST['hyparticipant'];
 $hypduration = $_POST['hypduration'];
 $immunodeficiency = $_POST['immunodeficiency'];
 $immunoduration = $_POST['immunoduration'];
 $mh = $_POST['mh'];
 $mhd = $_POST['mhd'];
 $mspecify = $_POST['mspecify'];
 $obesity = $_POST['obesity'];
 $tb = $_POST['tb'];
 $tbd = $_POST['tbd'];
 $aird = $_POST['aird'];
 $airdd = $_POST['airdd'];
 $airother = $_POST['airother'];
 $airspecify = $_POST['airspecify'];
 $tbst = $_POST['tbst'];
 $tbr = $_POST['tbr'];
 $hbst = $_POST['hbst'];
 $hbr = $_POST['hbr'];
 $hivst = $_POST['hivst'];
 $hivr = $_POST['hivr'];
 $hcst = $_POST['hcst'];
 $hcr = $_POST['hcr'];
 $quantiferom = $_POST['quantiferom'];
 $monto = $_POST['monto'];
 $antidiabetic = $_POST['antidiabetic'];
 $antihypertensive = $_POST['antihypertensive'];
 $epileptic = $_POST['epileptic'];
 $dmards = $_POST['dmards'];
 $steroids = $_POST['steroids'];
 $infections = implode(', ', $_POST['infections']);
 $infectionsi = implode(', ', $_POST['infectionsi']);
 $blsd = implode(', ', $_POST['blsd']);
 $drughypers = implode(', ', $_POST['drughypers']);
 $neoplasm = implode(', ', $_POST['neoplasm']);
 $metabolism = implode(', ', $_POST['metabolism']);
 $psychiatric = implode(', ', $_POST['psychiatric']);
 $nsd = implode(', ', $_POST['nsd']);
 $vsd = implode(', ', $_POST['vsd']);
 $rtmd = implode(', ', $_POST['rtmd']);
 $gd = implode(', ', $_POST['gd']);
 $hepatobiliary = implode(', ', $_POST['hepatobiliary']);
 $sstd = implode(', ', $_POST['sstd']);
 $dose_duration=$_POST['dose_duration'];

 $query = mysqli_query($con,"insert into  tofacare (name, fileno, diagnosis, username, age, dosage, asplenia, aspduration, cancer, cancerduration, chd, chdd, ckd, ckdd, cld, cldd, clud, cludd, cnd, cndd, diabetics, diabeticsdur, hiv, hivd, art, hypertension, hyperduration, hyparticipant, hypduration, immunodeficiency, immunoduration, mh, mhd, mspecify, obesity, tb, tbd, aird, airdd, airother, airspecify, tbst, tbr, hbst, hbr, hivst, hivr, hcst, hcr, quantiferom, monto, antidiabetic, antihypertensive, epileptic, dmards, steroids, infections, infectionsi, blsd, drughypers, neoplasm, metabolism, psychiatric, nsd, vsd, rtmd, gd, hepatobiliary, sstd, dose_duration)

  values('$name','$fileno','$diagnosis','$username','$age','$dosage','$asplenia','$aspduration','$cancer','$cancerduration','$chd','$chdd','$ckd','$ckdd','$cld','$cldd','$clud','$cludd','$cnd','$cndd','$diabetics','$diabeticsdur','$hiv','$hivd','$art','$hypertension','$hyperduration','$hyparticipant','$hypduration','$immunodeficiency','$immunoduration','$mh','$mhd','$mspecify','$obesity','$tb','$tbd','$aird','$airdd','$airother','$airspecify','$tbst','$tbr','$hbst','$hbr','$hivst','$hivr','$hcst','$hcr','$quantiferom','$monto','$antidiabetic','$antihypertensive','$epileptic','$dmards','$steroids','$infections','$infectionsi','$blsd','$drughypers','$neoplasm','$metabolism','$psychiatric','$nsd','$vsd','$rtmd','$gd','$hepatobiliary','$sstd','$dose_duration')
  ");

if($query)
{
 echo "<script>alert('Successfully entered the data...');</script>"; 
}
else{
   echo "<script>alert('Failed..');</script>"; 
}

}

 ?>
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Form Component</title>
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
   

  </head>

  <body>

  <section id="container" class="">
      <!--header start-->
 <?php include('layout/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
 <?php include('layout/aside.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              
              
              
              <div class="row">
                  <div class="col-lg-12">
                      
                      <section class="card">
                          <div class="card-header">Data Insertion Form </div>
                          <div class="card-body">
                              <?php include('layout/tofaform.php') ?>

                              <script>
                                  // Example starter JavaScript for disabling form submissions if there are invalid fields
                                  (function() {
                                      'use strict';
                                      window.addEventListener('load', function() {
                                          // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                          var forms = document.getElementsByClassName('needs-validation');
                                          // Loop over them and prevent submission
                                          var validation = Array.prototype.filter.call(forms, function(form) {
                                              form.addEventListener('submit', function(event) {
                                                  if (form.checkValidity() === false) {
                                                      event.preventDefault();
                                                      event.stopPropagation();
                                                  }
                                                  form.classList.add('was-validated');
                                              }, false);
                                          });
                                      }, false);
                                  })();
                              </script>
                          </div>
                      </section>

                  </div>
              </div>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!-- Right Slidebar start -->
    
      <!-- Right Slidebar end -->

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              <?php echo date("Y"); ?> &copy; By ChanRe.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>

    <script src="js/jquery-ui.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>

  <!--custom switch-->
  <script src="js/bootstrap-switch.js"></script>
  <!--custom tagsinput-->
  <script src="js/jquery.tagsinput.js"></script>
  <!--custom checkbox & radio-->
  <script type="text/javascript" src="js/ga.js"></script>

  <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script>

  <script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
  <script src="js/respond.min.js" ></script>
<!-- <script src="js/jquery-3.2.1.min.js"></script> -->
  <!--right slidebar-->
  <script src="js/slidebars.min.js"></script>
<script src="js/jquery.multiselect.js"></script>

  <!--common script for all pages-->
  <script src="js/common-scripts.js"></script>

  <!--script for this page-->
  <script src="js/form-component.js"></script>
    <script type="text/javascript">
                              

$('#langOpt1').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,

    selectAll: true
});


$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt4').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});



$('#langOpt5').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,

    selectAll: true
});


$('#langOpt6').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt7').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt8').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});

$('#langOpt9').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,

    selectAll: true
});


$('#langOpt10').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt11').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt12').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
$('#langOpt13').multiselect({
    columns: 1,
    placeholder: 'Choose an option',
    search: true,
   
    selectAll: true
});
</script>
  </body>
</html>
<?php } ?>