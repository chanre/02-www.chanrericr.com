<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.html');
}
else{
if(isset($_POST['submit']))
{
//Current Password hashing 
$password=$_POST['password'];
$options = ['cost' => 12];
$hashedpass=password_hash($password, PASSWORD_BCRYPT, $options);
$adminid=$_SESSION['login'];
// new password hashing 
$newpassword=$_POST['newpassword'];
$newhashedpass=password_hash($newpassword, PASSWORD_BCRYPT, $options);

date_default_timezone_set('Asia/Kolkata');// change according timezone
$currentTime = date( 'd-m-Y h:i:s A', time () );
$sql=mysqli_query($con,"SELECT AdminPassword FROM  tbladmin where AdminUserName='$adminid' || AdminEmailId='$adminid'");
$num=mysqli_fetch_array($sql);
if($num>0)
{
 $dbpassword=$num['AdminPassword'];

if (password_verify($password, $dbpassword)) {

 $con=mysqli_query($con,"update tbladmin set AdminPassword='$newhashedpass', updationDate='$currentTime' where AdminUserName='$adminid'");
$msg="Password Changed Successfully !!";
}
}
else
{
$error="Old Password not match !!";
}
}


?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style>
th{
	background-color:#53a6d5;
	color:white;
}
form{
	width:100%;
	margin-top: 100px;
	margin-bottom: 100px;
	background-color:#99d6c5;
	font-weight:500;
	font-size:11px;
	letter-spacing:2px;
	border-radius: 40px;
	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
.form-design{
	padding:20px;
}
input[type=password],[type=submit]{
	margin-bottom:10px;
	border-radius: 40px;
	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);

}
body{
	background-color:#99d6c5;
}
h1{padding:20px;
	font-family:times;
	color:white;
}
</style>
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <header id="top" class="top_navbar">
            <div class="container">
                <nav class="navbar  navbar-expand-lg navbar-light ">
                    <a class="navbar-brand" href="/admindashboard.php"><img src="images/chanrelogo.png" alt="" width="250" /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon">
                        <i class="fa fa-bars"></i>
                      </span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                          <ul class="navbar-nav"> 
                            <li class="nav-item">
                                <a class="nav-link" href="admindashboard.php">Main Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="confirmed.php">Confirmed</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="cancelled.php">Cancelled</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="pddsraadmin.php">PDDSRA</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="logout.php">Logout</a>
                            </li>
                        </ul>
                  
                    </div>
                </nav>
            </div>
        </header>

        <div class="container-fluid">
            <div class="row">
			<div class="col-md-4"></div>
                <div class="col-md-4">
                     <form class="form-signin" method="post">
					 <h1 align="center">Change Password</h1>
					 <hr>
					 <div class="form-design">
				<input type="password" name="password" id="" class="form-control floatlabel" placeholder="Please enter your password" required>
				<input type="password" name="newpassword" id="password" class="form-control floatlabel" placeholder="New password" required>
				<input type="password" name="confirmpassword" id="confirm_password" class="form-control floatlabel" placeholder="Confirm password" required>
                <div id="remember" class="checkbox"></div>
				<button type="submit" class="btn btn-primary btn-custom btn-block  waves-effect waves-light btn-md" name="submit">
                                                    Submit
                                                </button>
				</div>
                
            </form>
                </div>
				<div class="col-md-4"></div>
            </div>
        </div>
        <?php include('layout/footer.php'); ?>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>
<?php } ?>