<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
        
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
          <div class="container"><br>
            <div class="row">
                <div class="col-md-8 offset-2">


  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
    <img class="card-img-top" src="events/4.jpg" alt="Card image cap">
  </button>

  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Visit Youtube</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
           <img class="card-img-top" src="events/4.jpg" alt="Card image cap">
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">View Video</button>
        </div>
        
      </div>
    </div>
  </div>
                  <div class="card">
                       <img class="card-img-top" src="events/1.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div> 
                  <br>
                  <div class="card">
                       <img class="card-img-top" src="events/2.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div>
                  <br>
                   <div class="card">
                       <img class="card-img-top" src="events/3.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div> 
                  <br>
                  <div class="card">
                       <img class="card-img-top" src="events/4.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div>
                  <br>
                   <div class="card">
                       <img class="card-img-top" src="events/5.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div> 
                  <br>
                  <div class="card">
                       <img class="card-img-top" src="events/6.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div>
                  <br>
                   <div class="card">
                       <img class="card-img-top" src="events/7.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div> 
                  <br>
                  <div class="card">
                       <img class="card-img-top" src="events/8.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div>
                  <br>
                   <div class="card">
                       <img class="card-img-top" src="events/9.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div> 
                  <br>
                  <div class="card">
                       <img class="card-img-top" src="events/10.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div>
                  <br>
                   <div class="card">
                       <img class="card-img-top" src="events/11.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div> 
                  <br>
                  <div class="card">
                       <img class="card-img-top" src="events/12.jpg" alt="Card image cap">
                       <p class="card-text">Patient awareness program</p>
                  </div>
                  <br>
                </div>
            </div>
          </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>