<?php 

// include('config.php');
include('config2.php')



?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
    <link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
    <link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

   <style type="text/css">
       body{
        background-color: #fdfdfd;;
       }
       
a{  

  /*width:100%;*/

  text-align:center;
  display:inline-block;
  position:relative;
  padding:10px 10px;
  text-decoration:none;
  text-transform: uppercase;
  font-weight:500;
  
  
  /*box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);*/
}
.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}



#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
  position:fixed;
  width:100%;
  margin-bottom:20px;
}

#navbar a {
  width: 100%;
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  
  text-decoration: none;
 
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
  font-family:times;
}
img{
  
  width:98%;
}
table{width: 100%;margin-right: auto;margin-left: auto;}
table tr td{
  width: 50%;
  border: 1px solid gray;
  margin-left: auto;
  margin-right: auto;
  font-size: 14px;
  text-align: center;
  overflow: hidden;
  text-align: left;
}



    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
  <div id="navbar">
  <a class="active" href="multiapp.php">Chanre</a>
 
</div>

    <!-- Begin wrapper -->
    <div class="container">
      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
          <br><br><br>
          <table class="table-responsive">
            <tr>
              <td><a href="android.php" >Appointment Booking</a></td>
              <td><a href="diagrampatient.php" >PDDSRA</a></td>
            </tr>
            <tr>
              <td> <a href="sample_collection.php" >Sample Collection</a></td>
              <td><a href="symptom-checker.php">Symptom Checker</a></td>
            </tr>
          </table>
          
          
         
       
          <hr>
          
            

            <br><br><br><br>
        </div>
        <div class="col-md-4">
     

         
        </div>
      </div>
    </div>

    <!-- End wrapper -->
    <style type="text/css">#mobile-footer {
    position: fixed;
    bottom: 0;
    height: 50px;
    width: 100%;
    color: #f2f2f2;
    @media(min-width: 768px) {
        display: none;
    }
}
#mobile-menu {

    background: hsl(122deg 39% 49%);
}
#mobile-footer-container {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    overflow: hidden;
}
.mobile-link {
    padding-top: 0.75em;
    padding-bottom: 0.75em;
}
.mobile-link a {
    font-size: 16px;
    color: #fff;
    text-decoration: none;
}
#mobile-footer-close {
    position: relative;
}
#mobile-footer-btn {
    position: absolute;
    bottom: 25px;
    right: 5px;
    width: 30px;
    height: 30px;
    background-color: #959192;
    border: none;
    border-radius: 50%;
    overflow: hidden;
    text-indent: 100%;
    color: transparent;
    white-space: nowrap;
    cursor: pointer;
    &:focus {
        outline: 0;
    }
}
.mobile-btn-close span,
.mobile-btn-close span::before {
    content: '';
    position: absolute;
    width: 5px;
    height: 18px;
    top: calc(50% - 9px);
    right: calc(50% + -2.5px);
    background-color: #fff;
    transform: rotate(-90deg);
    transition: 0.3s ease-out;
}
.mobile-btn-close {
    transition: 1s ease-out;
    &::focus {
        transition: 1s ease-out;
    }
}
.mobile-btn-close {
    span {
        transform: rotate(45deg);
        &::before {
            content: '';
            transform: rotate(-90deg);
        }
    }
}
.is-rotating {
    transform: rotate(135deg);
}
.is-rotating-back {
    transform: rotate(-90deg);
}
.mobile-menu-hide {
    animation: hideFooter 10s forwards;
}
.mobile-menu-show {
    animation: showFooter 1s forwards;
}
@keyframes hideFooter {
    0% {
        transform: translateY(0);
        opacity: 1;
    }
    100% {
        transform: translateY(1000px);
        opacity: 0;
    }
}
@keyframes showFooter {
    0% {
        transform: translateY(300px);
        opacity: 0;
    }
    100% {
        transform: translateY(0);
        opacity: 1;
    }
}


</style>
    <footer id="mobile-footer">
  <div id="mobile-menu">
    <div id="mobile-footer-container">
      <div class="mobile-link">
        <a href="symptom-checker.php" class="text-center"><i class="fa fa-medkit fa-24x"></i></a>
      </div>
      <div class="mobile-link">
        <a href="diagrampatient.php" class="text-center"><i class="fa fa-user-md fa-10x"></i></a>
      </div>
      <div class="mobile-link">
        <a href="android.php" class="text-center"><i class="fa fa-plus-square"></i></a>
      </div>
    </div>
  </div>
  <!--  
  <div id="mobile-footer-close">
    <button id="mobile-footer-btn">
      <div class="mobile-btn-close is-rotating-back">
        <span></span>
      </div>
    </button>
  </div>-->
</footer>
    
<script type="text/javascript">
  (function($) {
  $(function() {
    // Store menu container
    var mobileMenu = '#mobile-menu';
    // Store Trigger
    var mobileBtn = '#mobile-footer-btn';

    var rotation = '.mobile-btn-close';

    $(mobileBtn).on("click", function(e) {
      e.stopPropagation();
      if ($(mobileMenu).hasClass('mobile-menu-hide') || $(rotation).hasClass('is-rotating')) {
        $(mobileMenu).removeClass("mobile-menu-hide").addClass("mobile-menu-show");
        $(rotation).removeClass("is-rotating").addClass("is-rotating-back");
      } else {
        $(mobileMenu).removeClass("mobile-menu-show").addClass("mobile-menu-hide");
        $(rotation).removeClass('is-rotating-back').addClass('is-rotating');
      }
    });
  });
})(jQuery);
</script>
    <!-- jquery latest version -->
    <script src="jquery.min.js"></script>
<script src="jquery.multiselect.js"></script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/wickedpicker.js"></script>
           

</body>

</html>
