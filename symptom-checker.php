<!DOCTYPE html>
<html>
<head>
	<title>Symptom checker</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
 
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
     <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}

#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
  font-family:times;
}
img{
  
  width:98%;
}
form{
  width:100%;
  display:inline-block;
  
  padding:10px 30px;
  text-decoration:none;
  margin-top: 20px; 
  font-weight:500;
 
  
  box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
a{  width:100%;
  display:inline-block;
  position:relative;
  padding:10px 30px;
  text-decoration:none;
  text-transform: uppercase;
  font-weight:500;
  letter-spacing:2px;
  
  /*box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);*/
}
.qheader{
  font-weight: bold;
  margin: 5px;
  font-size: 16px;
}

    </style>

</head>
<body>
  <div id="navbar">
  <a class="active" href="multiapp.php">Chanre</a>
 
</div>
<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <form name="form1" onsubmit="return validate(this)"> 
<div class="qheader"> 1) Do you feel persistent pain in one or more of your joints?
</div> 
<div class="qselections"> 
<input name="Yn" onclick="show1();" type="radio" value="1" />No 
<input name="Yn" onclick="show2();" type="radio" value="1" />Yes 
<div class="collapse" id="div1"> if yes how many?
<br /> 
<input class="answer1" name="question1" onclick="show7();" type="checkbox" value="1" />1 
<input class="answer1" name="question1" onclick="show8();" type="checkbox" value="2" />2-5 
<input class="answer1" name="question1" onclick="show8();" type="checkbox" value="3" />5
<br /> 
<div id="sides"> 
<input class="answer1a" name="question1" type="checkbox" value="0" />One side 
<input class="answer1a" name="question1" type="checkbox" value="4" />Both side
</div> 
<br /> a) Do you have swelling in the above mentioned painful joints?
<br /> 
<input name="question1" type="radio" value="1" />No 
<input name="question1" type="radio" value="1" />Yes
<br /> 
<br /> b)Since how long have you been experiencing these problems?
<br /> 
<input class="answer11" name="question1" type="checkbox" value="0" /> Few months 
<input class="answer11" name="question1" type="checkbox" value="1" /> 1 yr 
<input class="answer11" name="question1" type="checkbox" value="2" /> 2-5 yrs 
<input class="answer11" name="question1" type="checkbox" value="3" /> &gt;5 yrs
<br /> 
<br /> c) Whether the pain is due to a recent injury occurred while playing, at work or at home ?
<br /> 
<input name="qu1" type="radio" value="1" />No 
<input name="qu1" type="radio" value="-2" />Yes
<br /> &nbsp;
</div> 
</div> 
<div class="qheader"> 2) Do you have persistent low back pain or buttock pain for &gt; 3 months, which is obstracting you sleep?
</div> 
<input class="answer2 open_options" name="question2" type="checkbox" value="1" /> Low back pain 
<input class="answer2 open_options" name="question2" type="checkbox" value="1" /> Buttock Pain 
<input class="answer2 open_options" name="question2" type="checkbox" value="2" /> Both 
<input class="answer2 close_options" name="question2" type="checkbox" value="1" /> No
<br /> 
<div id="options"> 
<input class="answer2a" name="question2" type="checkbox" value="1" /> Pain after waking up
<br /> 
<input class="answer2a" name="question2" type="checkbox" value="0" /> After excess of straining &gt; 30 min
<br /> 
<input class="answer2b" name="question2" type="checkbox" value="1" /> After prolonged sitting
<br /> 
<input class="answer2b" name="question2" type="checkbox" value="0" /> Dose it impair your daily activities
</div> 
<div class="qheader"> a) Do you have early morning stiffness which improves or worsen with activity?
</div> 
<div class="qselections"> 
<input name="question2" onclick="show5();" type="radio" value="0" />No 
<input name="question2" onclick="show6();" type="radio" value="1" />Yes 
<div id="div2"> 
<input id="answer2aa" name="question2" type="checkbox" value="1" /> stiffness &gt; 30 min
<br /> 
<input id="answer2aa" name="question2" type="checkbox" value="0" /> stiffness &lt; 30 min
<br /> 
<input id="answer2ab" name="question2" type="checkbox" value="1" /> improves with activity
<br /> 
<input id="answer2ab" name="question2" type="checkbox" value="0" /> worsen with activity
</div> 
</div> 
<br /> 
<div class="qheader"> 3) Do you have deformity in your hands, feet or back?
</div> 
<div class="qselections"> 
<input name="question3" onclick="show4();" type="radio" value="3" />Yes 
<input name="question3" onclick="show3();" type="radio" value="0" />No 
<div class="collapse" id="div3"> 
<input class="question3" name="question3" type="checkbox" value="0" /> Deformity in hands
<br /> 
<input class="question3" name="question3" type="checkbox" value="0" /> Deformity in legs
<br /> 
<input class="question3" name="question3" type="checkbox" value="0" /> Deformity in back
</div> 
</div> 
<div class="qheader"> a) Are you experiencing any sound (crepitus) on moving the joint?
</div> 
<div class="qselections"> 
<input name="YorN3a" type="radio" value="2" /> Yes 
<input name="YorN3a" type="radio" value="0" /> No
</div> 
<div class="qheader"> b) Do you experiance excessive fatigue or feel abnormally feverish
</div> 
<div class="qselections"> 
<input name="YorN3b" type="radio" value="2" /> Yes 
<input name="YorN3b" type="radio" value="0" />No
</div> 
<br /> 
<div class="qheader"> 4 a) Do you have any of the following symptoms?
</div> 
<input id="answer4a" name="question4" type="checkbox" value="1" /> Generalized body pain
<br /> 
<input id="answer4a" name="question4" type="checkbox" value="4" /> Difficulty in using upper or lower limb due to muscle weakness
<br /> 
<input id="answer4a" name="question4" type="checkbox" value="1" /> Red skin rash with raised borders over your face, scalp, neck or behind ears
<br /> 
<input id="answer4a" name="question4" type="checkbox" value="1" /> Flat facial skin rash over bridge of your nose, across cheeks which is non-itchy and scarring
<br /> 
<input id="answer4a" name="question4" type="checkbox" value="1" /> Sleep disturbance
<br /> 
<input id="answer4an" name="question4" type="checkbox" value="1" /> None
<br /> 
<div class="qheader"> b) Do you have any problems below
</div> 
<div class="qselections"> 
<input id="answer4b" name="question4" type="checkbox" value="1" /> Unusually / excessively sensitive to sunlight
<br /> 
<input id="answer4b" name="question4" type="checkbox" value="1" /> Ulcers of mouth, nose or throat
<br /> 
<input id="answer4b" name="question4" type="checkbox" value="1" /> Having excessive hair loss in last three months
<br /> 
<input id="answer4b" name="question4" type="checkbox" value="3" /> Does your fingers and toes react to cold, turning blue
<br /> 
<input id="answer4bn" name="question4" type="checkbox" value="0" /> None
</div> 
<div class="qheader"> c)Have you experienced?
</div> 
<input id="answer4c" name="question4" type="checkbox" value="2" /> Irrational or disturbed thought process
<br /> 
<input id="answer4c" name="question4" type="checkbox" value="1" /> Seizures
<br /> 
<input id="answer4c" name="question4" type="checkbox" value="1" /> Personality changes
<br /> 
<input id="answer4c" name="question4" type="checkbox" value="1" /> Anxiety
<br /> 
<input id="answer4cn" name="question4" type="checkbox" value="0" /> None
<br /> 
<div class="qheader"> d) Do you have inflammation of kidney or abnormal urine analysis showing protein/cellular caste in the urine?
</div> 
<input name="Yn4d" type="radio" value="3" />Yes 
<input name="Yn4d" type="radio" value="0" />No
<br /> 
<div class="qheader"> e) Whether the clinical/ findings show any of the following?
</div> 
<input class="4e" name="Yn4e" type="checkbox" value="4" /> Abnormal blood counts (leucopenia, thrombocytopenia, anemia)
<br /> 
<input class="4e" name="Yn4e" type="checkbox" value="6" /> Blood test positive for ds DNA, ANA, anti-Sm, anticardiolipin antibody, lupus anticoagulant, RO52, SSA or SSB
<br /> 
<input class="4en" name="Yn4e" type="checkbox" value="0" /> None
<br /> 
<div class="qheader"> 5) Do you have swelling in the glands around your face or neck?
</div> 
<div class="qselections"> 
<input name="question5" type="radio" value="3" />Yes 
<input name="question5" type="radio" value="1" />No
<br /> a) Do you have dental problems (i.e. dental decay, oral disease)
</div> 
<div class="qselections"> 
<input name="YorN5a" type="radio" value="2" /> Yes 
<input name="YorN5a" type="radio" value="0" /> No
</div> 
<div class="qheader"> b) Have you experienced any of the following in the last three months?
</div> 
<input id="answer5" name="question5" type="checkbox" value="2" /> Gritting sensation in the eye
<br /> 
<input id="answer5" name="question5" type="checkbox" value="1" /> Redness of the eye
<br /> 
<input id="answer5" name="question5" type="checkbox" value="2" /> Use eye drops for dry eyes
<br /> 
<input id="answer5n" name="question5" type="checkbox" value="1" /> None
<br /> 
<div class="qheader"> 
<br /> 6) Do you have one or frequent episodes of sudden onset of severe pain in big toe?
</div> 
<input name="YorN6a" type="radio" value="3" />Yes 
<input name="YorN6a" type="radio" value="0" />No
<br /> 
<div class="qheader"> a) Whether the uric acid value is
</div> 
<input name="YorN6b" type="radio" value="2" /> &gt;8 
<input name="YorN6b" type="radio" value="0" /> &lt;8
<br /> 
<br /> 
<div id="submit" style="text-align: center"> 
<input class="btn btn-primary" type="submit" value="Submit" />
</div> 
<h5> &nbsp;
</h5> 
<ul> Disclaimer: 
<li> This questionnaire is intended for informational purposes only and does not substitute medical advice or consultation.
</li> 
<li> Always consult your physician or other qualified health provider with any questions you may have regarding a medical condition.
</li> 
</ul> 
</form> 

    </div>
    <div class="col-md-2"></div>
  </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" rel="stylesheet" /> <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script></p> <script>/*<![CDATA[*/$(document).ready(function(){$("input.answer1").on("change",function(){$("input.answer1").not(this).prop("checked",false)});$("input.answer1a").on("change",function(){$("input.answer1a").not(this).prop("checked",false)});$("input.answer11").on("change",function(){$("input.answer11").not(this).prop("checked",false)});$("input.answer2").on("change",function(){$("input.answer2").not(this).prop("checked",false)});$("input.answer2a").on("change",function(){$("input.answer2a").not(this).prop("checked",false)});$("input#answer2aa").on("change",function(){$("input#answer2aa").not(this).prop("checked",false)});$("input#answer2ab").on("change",function(){$("input#answer2ab").not(this).prop("checked",false)});$("input#answer4n").on("change",function(){$("input#answer4").not(this).prop("checked",false)});$("input#answer4").on("change",function(){$("input#answer4n").not(this).prop("checked",false)});$("input#answer4an").on("change",function(){$("input#answer4a").not(this).prop("checked",false)});$("input#answer4a").on("change",function(){$("input#answer4an").not(this).prop("checked",false)});$("input#answer4bn").on("change",function(){$("input#answer4b").not(this).prop("checked",false)});$("input#answer4b").on("change",function(){$("input#answer4bn").not(this).prop("checked",false)});$("input#answer4cn").on("change",function(){$("input#answer4c").not(this).prop("checked",false)});$("input#answer4c").on("change",function(){$("input#answer4cn").not(this).prop("checked",false)});$("input.4en").on("change",function(){$("input.4e").not(this).prop("checked",false)});$("input.4e").on("change",function(){$("input.4en").not(this).prop("checked",false)});$("input#answer5n").on("change",function(){$("input#answer5").not(this).prop("checked",false)});$("input#answer5").on("change",function(){$("input#answer5n").not(this).prop("checked",false)});$(function(){$(".close_options").on("click",function(){document.getElementById("options").style.display="none";$(".answer2a, .answer2b").each(function(){this.checked=false})})});$(function(){$(".open_options").on("click",function(){document.getElementById("options").style.display="block"})})});function show1(){document.getElementById("div1").style.display="none";$('input[name="question1"]').each(function(){this.checked=false})}function show2(){document.getElementById("div1").style.display="block"}function show5(){document.getElementById("div2").style.display="none";$("#answer2aa,#answer2ab").each(function(){this.checked=false})}function show6(){document.getElementById("div2").style.display="block"}function show3(){document.getElementById("div3").style.display="none";$(".question3").each(function(){this.checked=false})}function show4(){document.getElementById("div3").style.display="block"}function show7(){document.getElementById("sides").style.display="none";$(".answer1a").each(function(){this.checked=false})}function show8(){document.getElementById("sides").style.display="block"}function validate(g){var f=0;for(var e=0;e<document.form1.question1.length;e++){if(document.form1.question1[e].checked){f+=parseInt(document.form1.question1[e].value)}}f+=parseInt(+(document.form1.Yn.value));f+=parseInt(+(document.form1.qu1.value));var d=0;for(var e=0;e<document.form1.question2.length;e++){if(document.form1.question2[e].checked){d+=parseInt(document.form1.question2[e].value)}}var c=0;for(var e=0;e<document.form1.question3.length;e++){if(document.form1.question3[e].checked){c+=parseInt(document.form1.question3[e].value)}}c+=parseInt(+(document.form1.YorN3a.value));c+=parseInt(+(document.form1.YorN3b.value));var b=0;for(var e=0;e<document.form1.question4.length;e++){if(document.form1.question4[e].checked){b+=parseInt(document.form1.question4[e].value)}}b+=parseInt(+(document.form1.Yn4d.value));b+=parseInt(+(document.form1.Yn4e.value));var a=0;for(var e=0;e<document.form1.question5.length;e++){if(document.form1.question5[e].checked){a+=parseInt(document.form1.question5[e].value)}}a+=parseInt(+(document.form1.YorN6a.value));a+=parseInt(+(document.form1.YorN6b.value));console.log(f);console.log(d);console.log(c);console.log(b);console.log(a);if(f==""){alert("select Answers to question 1")}else{if(d==""){alert("select Answers of question 2")}else{if(b==""){alert("select Answers of quesions number 4")}else{if(a==""){alert("select answers of question 5")}else{if(d>5||f>=6||c>=5||b>=7||a>=6){$.alert({title:"",content:"You require consultation with a rheumatologist.",type:"red",typeAnimated:true,useBootstrap:false,})}else{if(d==3||d==4||f>=4||f==4||c>=3||b>=5||a>=3){$.alert({title:"You may require consultation with a rheumatologist or assessment.",content:"You may probably have a rheumatologic disease to be confirmed with rheumatologist",type:"blue",typeAnimated:true,useBootstrap:false,})}else{$.alert({title:"Consult your family physician for further assessment",content:"You may not have rheumatologic disease kindly contact your physician for further assistance.",type:"green",typeAnimated:true,useBootstrap:false,})}}}}}}return false};/*]]>*/</script>

 <style type="text/css">#mobile-footer {
    position: fixed;
    bottom: 0;
height: 50px;
    width: 100%;
    color: #f2f2f2;
    @media(min-width: 768px) {
        display: none;
    }
}
#mobile-menu {
    background: hsl(122deg 39% 49%);
}
#mobile-footer-container {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    overflow: hidden;
}
.mobile-link {
    padding-top: 0.75em;
    padding-bottom: 0.75em;
}
.mobile-link a {
    font-size: 16px;
    color: #fff;
    text-decoration: none;
}
#mobile-footer-close {
    position: relative;
}
#mobile-footer-btn {
    position: absolute;
    bottom: 25px;
    right: 5px;
    width: 30px;
    height: 30px;
    background-color: #959192;
    border: none;
    border-radius: 50%;
    overflow: hidden;
    text-indent: 100%;
    color: transparent;
    white-space: nowrap;
    cursor: pointer;
    &:focus {
        outline: 0;
    }
}
.mobile-btn-close span,
.mobile-btn-close span::before {
    content: '';
    position: absolute;
    width: 5px;
    height: 18px;
    top: calc(50% - 9px);
    right: calc(50% + -2.5px);
    background-color: #fff;
    transform: rotate(-90deg);
    transition: 0.3s ease-out;
}
.mobile-btn-close {
    transition: 1s ease-out;
    &::focus {
        transition: 1s ease-out;
    }
}
.mobile-btn-close {
    span {
        transform: rotate(45deg);
        &::before {
            content: '';
            transform: rotate(-90deg);
        }
    }
}
.is-rotating {
    transform: rotate(135deg);
}
.is-rotating-back {
    transform: rotate(-90deg);
}
.mobile-menu-hide {
    animation: hideFooter 10s forwards;
}
.mobile-menu-show {
    animation: showFooter 1s forwards;
}
@keyframes hideFooter {
    0% {
        transform: translateY(0);
        opacity: 1;
    }
    100% {
        transform: translateY(1000px);
        opacity: 0;
    }
}
@keyframes showFooter {
    0% {
        transform: translateY(300px);
        opacity: 0;
    }
    100% {
        transform: translateY(0);
        opacity: 1;
    }
}


</style>
<br><br>
    <footer id="mobile-footer">
  <div id="mobile-menu">
    <div id="mobile-footer-container">
      <div class="mobile-link">
        <a href="symptom-checker.php" class="text-center"><i class="fa fa-medkit fa-24x"></i></a>
      </div>
      <div class="mobile-link">
        <a href="diagrampatient.php" class="text-center"><i class="fa fa-user-md fa-10x"></i></a>
      </div>
      <div class="mobile-link">
        <a href="android.php" class="text-center"><i class="fa fa-plus-square"></i></a>
      </div>
    </div>
  </div>
  <!--  
  <div id="mobile-footer-close">
    <button id="mobile-footer-btn">
      <div class="mobile-btn-close is-rotating-back">
        <span></span>
      </div>
    </button>
  </div>-->
</footer>
    
<script type="text/javascript">
  (function($) {
  $(function() {
    // Store menu container
    var mobileMenu = '#mobile-menu';
    // Store Trigger
    var mobileBtn = '#mobile-footer-btn';

    var rotation = '.mobile-btn-close';

    $(mobileBtn).on("click", function(e) {
      e.stopPropagation();
      if ($(mobileMenu).hasClass('mobile-menu-hide') || $(rotation).hasClass('is-rotating')) {
        $(mobileMenu).removeClass("mobile-menu-hide").addClass("mobile-menu-show");
        $(rotation).removeClass("is-rotating").addClass("is-rotating-back");
      } else {
        $(mobileMenu).removeClass("mobile-menu-show").addClass("mobile-menu-hide");
        $(rotation).removeClass('is-rotating-back').addClass('is-rotating');
      }
    });
  });
})(jQuery);
</script>


</body>
</html>