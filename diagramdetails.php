<?php 

include ('config.php');

 ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
       .slider {
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
      background-image: linear-gradient(to right, #ffc107 , #dc3545);
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}



#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}

#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
	font-family:times;
}
img{
	
	width:98%;
}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
  <div id="navbar">
  <a class="active" href="javascript:void(0)">Chanre</a>
  <a href="logout_diagram_admin.php">Logout</a>
</div>

    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       

       <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
				<?php $pid=intval($_GET['nid']);
			$query= mysqli_query($con,"Select * from questionaire where pid='$pid'");
				while ($rows=mysqli_fetch_array($query)) {
			 ?><br> <br><br>
			<table border="3">
		
			<tr>
			<td>Age</td>
			<td><?php echo htmlentities($rows['age']); ?></td>
			</tr>
			<tr>
			<td>Pain as per Scale</td>
			<td><?php echo htmlentities($rows['demo']); ?></td>
			</tr>
			<tr>
			<td>Patient Id</td>
			<td><?php echo htmlentities($rows['pid']); ?></td>
			</tr>
			<tr>
			<td>Assesment</td>
			<td><?php echo htmlentities($rows['assesment']); ?></td>
			</tr>
			<tr>
			<td>Date</td>
			<td><?php echo htmlentities($rows['date']); ?></td>
			</tr>
			<tr>
			<td>How was your exercise this week compared to preceding week?</td>
			<td><?php echo htmlentities($rows['exercise']); ?></td>
			</tr>
			<tr>
			<td>Overall how you feel today compared to last week?</td>
			<td><?php echo htmlentities($rows['feel']); ?></td>
			</tr>
			<tr>
			<td>If you experienced stiffness , how was it?</td>
			<td><?php echo htmlentities($rows['stiffness']); ?></td>
			</tr>
			<tr>
			<td>How was your self care activities this week compared to preceding week?</td>
			<td><?php echo htmlentities($rows['activity']); ?></td>
			</tr>
			<tr>
			<td>Please Select the area where you had pain past week?</td>
			<td><?php echo htmlentities($rows['pain']); ?></td>
			</tr>
			<tr>
			<td>Please Select the area where you had pain past week?</td>
			<td><?php echo htmlentities($rows['swelling']); ?></td>
			</tr>
				<tr>
			<td>Do you have any problem with the medicine?</td>
			<td><?php echo htmlentities($rows['problem_med']); ?></td>
			</tr>
			<tr>
			<td>To much of gastritis stomach irritation</td>
			<td><?php echo htmlentities($rows['tomuch']); ?></td>
			</tr>
			<tr>
			<td>Skin irritation</td>
			<td><?php echo htmlentities($rows['skin_irritation']); ?></td>
			</tr>
			<tr>
			<td>Rashes over the body</td>
			<td><?php echo htmlentities($rows['rashes']); ?></td>
			</tr>
			<tr>
			<td>Urine problem</td>
			<td><?php echo htmlentities($rows['urine']); ?></td>
			</tr>
			<tr>
			<td>Other </td>
			<td><?php echo htmlentities($rows['others']); ?></td>
			</tr>
			<tr>
			<td colspan="2"  style="background-color:orange;"><label for="exampleFormControlSelect2"><h4>Test Result</h4></label></td>
			</tr>
			<tr>
			<td>Hb%</td>
			<td><?php echo htmlentities($rows['hb']); ?></td>
			</tr>
			<tr>
			<td>TC</td>
			<td><?php echo htmlentities($rows['tc']); ?></td>
			</tr>
			<tr>
			<td>N</td>
			<td><?php echo htmlentities($rows['n']); ?></td>
			</tr>
			<tr>
			<td>L</td>
			<td><?php echo htmlentities($rows['l']); ?></td>
			</tr>
			<tr>
			<td>M</td>
			<td><?php echo htmlentities($rows['m']); ?></td>
			</tr>
			<tr>
			<td>B</td>
			<td><?php echo htmlentities($rows['b']); ?></td>
			</tr>
			<tr>
			<td>E</td>
			<td><?php echo htmlentities($rows['e']); ?></td>
			</tr>
			<tr>
			<td>Platelets</td>
			<td><?php echo htmlentities($rows['platelet']); ?></td>
			</tr>
			<tr>
			<td>ESR</td>
			<td><?php echo htmlentities($rows['esr']); ?></td>
			</tr>
			<tr>
			<td>Creatinine</td>
			<td><?php echo htmlentities($rows['creatinine']); ?></td>
			</tr>
			<tr>
			<td>SGPT</td>
			<td><?php echo htmlentities($rows['sgpt']); ?></td>
			</tr>
			<tr>
			<td>SGOT</td>
			<td><?php echo htmlentities($rows['sgot']); ?></td>
			</tr>
			<tr>
			<td>Other</td>
			<td><?php echo htmlentities($rows['other']); ?></td>
			</tr>
			</table>
                  
				<?php } ?>
				<br>
				<a href="https://www.chanrericr.com/diagram1.php" class="btn btn-success">Back</a>
<br><br>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
            </div>
        </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="jquery.min.js"></script>
<script src="jquery.multiselect.js"></script>
<script>
$('#langOpt').multiselect({
    columns: 1,
    placeholder: 'Select'
});

$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Select',
    search: true
});

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select where you had pain past week',
    search: true,
    selectAll: true
});
$('#langOpt4').multiselect({
    columns: 1,
    placeholder: 'Select where you had swelling past week',
    search: true,
    selectAll: true
});
$('#langOptgroup').multiselect({
    columns: 4,
    placeholder: 'Select',
    search: true,
    selectAll: true
});
</script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
            <script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>

 <script type="text/javascript"> window.onload = function() {
  // provs is an object but you can think of it as a lookup table
  var provs = {
        'Select':[''],
        'Shoulder': ['Right','Left','Both'],
        'Elbow': ['Right','Left','Both'],
        'Wrist':['Right','Left','Both'],
        'Hand':['Right','Left','Both'],
        'Finger':['Right','Left','Both'],
        'Knee':['Right','Left','Both'],
        'Ankle':['Right','Left','Both'],
        'Toes':['Right','Left','Both'],
         
      },
      // just grab references to the two drop-downs
      prov_select = document.querySelector('#prov'),
      town_select = document.querySelector('#town');

  // populate the provinces drop-down
  setOptions(prov_select, Object.keys(provs));
  // populate the town drop-down
  setOptions(town_select, provs[prov_select.value]);
  
  // attach a change event listener to the provinces drop-down
  prov_select.addEventListener('change', function() {
    // get the towns in the selected province
    setOptions(town_select, provs[prov_select.value]);
  });
    
  function setOptions(dropDown, options) {
    // clear out any existing values
    dropDown.innerHTML = '';
    // insert the new options into the drop-down
    options.forEach(function(value) {
      dropDown.innerHTML += '<option name="' + value + '">' + value + '</option>';
    });
  }  
};</script>

</body>

</html>