<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Not all ANA positivity is lupus</title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
	<meta name="descriptions" content="In the current era of information being made accessible at fingertips, health awareness has increased tremendously. "/>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
<style>
img{
	width:100%;
	margin-left:auto;
	margin-right:auto;
}
</style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
       <?php include ('layout/header.php'); ?>
       <div class="container">
        <br>
        <div class="myheader"><h4 align="center">Not all ANA positivity is lupus</h4></div>
           <div class="row">
		   <div class="col-md-2"></div>
              <div class="col-md-8"><br>
			  <p>In the current era of information being made accessible at fingertips, health awareness has increased tremendously. This has become a double-edged sword. A lot of individuals believe that every headache could be a sign of brain tumour. Similarly, in autoimmune diseases, a positive antinuclear antibody (ANA) test is always speculated as  lupus by patients.</p>  
			  <p>ANA test measures the presence of autoantibodies against nuclear antigens. Antigen is a whole or part of protein that is capable of eliciting an immune response. It can originate from an external source ( e.g. bacteria, virus or parasites) or internally within the body. Certain proteins present in our body are sometimes mistakenly recognised by our immune system as potentially dangerous and mounts an immune response to these proteins/antigens. 
			  </p>
			  <br>
			  <div>
			  <a href=""><img src="images/ana.png" alt="Ana Positivity"></a>
			  </div><br>
			  <p>There are certain types of white blood cells (WBC) called B cells, which produce antibodies against such antigens. Such antibodies  produced against self-antigens are called autoantibodies. An ANA test measures the level of these antibodies and it is performed by several methods. The most popular method used is immunofluorescence (IF) and it is very simple, sensitive and specific. It is performed in dilutions and generally reported as 1:40, 1:80, 1:160, and 1:640. It can also identify the pattern of immunofluorescence produced which is reported as speckled, homogenous etc.</p>
			  <p>The production of autoantibodies is occurring regularly in almost all individuals. However,this process is kept under check by the constant surveillance of regulatory immune cells by the body and hence the presence of small amounts of autoantibodies is considered as normal.</p>
			  <p>It should not be the cause of alarm, unless accompanied by symptoms. ANA is positive in almost 5% of normal population. The chance of ANA test being positive increases gradually with advancement of age and it could be upto 37% in healthy individuals who are > 65 years of age. </p>
			  <p>Presence of larger amounts of ANA could be suggestive of an autoimmune disease, not essentially lupus. Other conditions that can have a positive ANA include mixed connective tissue disorder, Sjogren’s syndrome, scleroderma, myositis (polymyositis/dermatomyositis), and undifferentiated connective tissue disorder. It can also be positive in certain organ specific autoimmune conditions like Hashimotos thyroiditis (a thyroid disease), myasthenia gravis (a muscle disease),autoimmune hepatitis etc. Certain medications can also induce production of autoantibodies like sulfadiazine, procainamide, isoniazid, methyldopa, quinidine, minocycline etc. Rarely, certain infections or cancer types can also produce ANA positivity. </p>
			  <p>Presence of ANA does not translate to having an autoimmune disease. It is just one piece of the jigsaw puzzle that your doctor is trying to put in place. The diagnosis of an autoimmune condition is based on many factors including the symptoms, signs observed, and other blood investigations, apart from positive ANA. ANA test certainly helps in ruling out many conditions. </p>
			  <p>Presence of ANA should not be the cause of panic, if the test turns out to be positive. In fact, it is not included in the routine health check-ups offered by many laboratories. Your clinician does not ask for an ANA test, unless there are certain pointers towards an autoimmune condition. This test is mostly used to confirm the possibility of an autoimmune cause.</p>
			  <p>Feel free to discuss with your healthcare provider about any concerns you might have before taking the test.</p>
			
			  </div>
           </div>
           <br>

  
           <br>
       </div>
        <?php include('layout/footer.php'); ?>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>