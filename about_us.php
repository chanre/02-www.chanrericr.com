<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
     <style type="text/css">
         p{
                font-family: times;
                font-weight:;
                font-size: 18px;

         }
         .image{
            padding: 10px;
            overflow: hidden;
         }
     </style>



</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
    <?php include ('layout/header.php'); ?>
    <div class="container">
        <br>
        <h1 class="Rheumatology" align="center">CRICR</h1>
        <div class="row">
            
            <div class="col-md-10">

                <p class="about">
                <strong>ChanRe Rheumatology and Immunology Center and Research (CRICR)</strong> was established on 12th December 2002 by the renowned rheumatologist Dr. Chandrashekara S. 
                It is a one-of-its-kind hospital in India, dedicated for the management of patients suffering from rheumatic diseases (musculoskeletal) and other immunological diseases. 
                This is a tertiary reference facility providing complete care under one roof for the patients suffering from arthritis and other immunological diseases such as immunodeficiency disorders, allergic disorders, reproductive immunological disorders, immunohematological disorders etc. 
                </p>
                <p class="about">
                    Over the years, CRICR has developed as a multispecialty center with modern and improved infrastructure including the amenities to accommodate more associated specialties and sub-specialties for the benefit of patients with a vision to deliver the best. Our facility is fully equipped with modern technologies, and it is aimed at providing a comprehensive specialty approach in the field of Immunology & Rheumatology with in-patient, emergency services, out-patient services, emergency ventilator support, minor procedure rooms, and cutting-edge research wing 
                </p>
                <p class="about">
                    <b>Specialists & Consultants</b> <br>The center offers patient-centric services through the following departments: 
                    <ul class="about">
                        <li class="about">Clinical immunology </li>
                        <li class="about">Allergy</li>
                        <li class="about">Immunodeficiency</li>
                        <li class="about">Reproductive immunology</li>
                        <li class="about"> Integrated soft tissue and chronic pain management</li>
                        <li class="about">Joint preservation and restoration</li>
                        <li class="about">Physiotherapy</li>

                    </ul>
                       </p>
                       <p class="about">
                       The existing departments also provides counselling on orthotics, diet, nutrition, obesity and lifestyle. Diverse investigational facilities available include X-ray, soft tissue ultrasound, pulmonary function test (PFT), nerve conduction studies (NCS), Doppler and home blood sample collection. At CRICR, the service of a qualified resident inhouse doctor is available for 24/7 hours. The patient care and emergency services are supported by experienced receptionists, nursing, paramedics, housekeeping and security personnel. 
             
                       </p>
                <p class="about">
                Over the years, CRICR has also developed into a reputed academic institute with the introduction of basic research, clinical trials, and fellowship and training programs in Rheumatology & Immunology, with the publication of a few Journals and Department of Research Assist and other supportive educational initiatives. 
                </p>

                     <p class="about">
                     The institute has recently expanded its Post MD Fellowship in Immunology & Rheumatology programme to a full time 2 years course. Faculties and students have published several papers. Some of the scientific projects conducted by the fellowship students of the institute in the past 12 years have been translated into several publications in national and international journals. 
                </p>
                <p class="about">
                Institute has published 7 volumes of physician reference books on ‘Managing Rheumatoid Arthritis: Question & Answer’. These books have undergone reprints and widely distributed receiving critical review from physicians referring to them. The institute has also published other reference books related to Rheumatology and different branches of medicine authored by our consultants. 
                </p>
                <p class="about">CRICR is continuously involved in the Society of Inflammation Research, a national level platform for inflammation scientists and practicing clinicians for the development of Inflammation Research. </p>
                <p class="about">Recently, CRICR has initiated an interactive patient and care-givers awareness sessions involving more than 25 people at each session. Such monthly awareness discussions are conducted by our team of experts including consultants, physiotherapists, psychologist and dietician.</p>
                <p class="about">We are also conducting the patients’ awareness programmes as internet-based weekly virtual sessions through YouTube / Facebook for wider dissemination of knowledge relating to immunological diseases among public and patient groups. </p>
                <p class="about">As a stepping stone towards better standardization, the institute has started the process of accreditation to NABH by standardizing all the protocols.</p>
            </div>
            <!-- <div class="col-md-4"> 
                <div class="image"> <img src="images/about.jpg" width="300"></div>
               
            </div>-->
        </div>
    </div>



    <?php include ('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>