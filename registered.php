<?php 

include ('config.php');

if(isset($_POST['submit']))
{
$appoint=$_POST['appoint'];
$name = $_POST['name'];
$contact = $_POST['contact'];
$email = $_POST['email'];
$department = $_POST['department'];
$doctor = $_POST['doctor'];
$gender = $_POST['gender'];
$registerno = $_POST['registerno'];
$cdate=$_POST['cdate'];
$ctime=$_POST['ctime'];
$mydate = date('d/m/y');

$status=0;

$query = mysqli_query($con,"insert into test (appoint,name,contact,email,department,doctor,gender,cdate,ctime,registerno,Is_Active,date)
 values('$appoint','$name','$contact','$email','$department','$doctor','$gender','$cdate','$ctime','$registerno','$status','$mydate')");
if($query){
echo "<script> alert('Thank you for choosing chanre..We will get back to you in a while!!')</script>";

}
else
{
echo "<script> alert('Request could not proccess.. Please Try again!!')</script>";
}

}
 ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
        h4{
            background-color: #32b58b;
            line-height: 40px;
            color: white;
            font-weight: 800;
            font-family: times;
        }
        .myform{
            background-image: url(images/b1.jpg);
        }
        .myform1{
            background-color: #135c853b;
            color: white;
        }
        .gaps{margin-top: 10px;font-family: times;}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body onload="generateCaptcha()">
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>

       <div class="container-fluid myform">
            <div class="row">
                <div class="col-md-4"></div> <br>
                <div class="col-md-4 myform1"><br>
              
                 <form  method="POST">
				 <h2 style="font-family:times;text-align:center;background-color:#4281cd;line-height:50px;">Appointment Request</h2>
				 <span>New Patient ?<a href="appointment1.php"> Click Here</a></span>
				 <div class="gaps">
                      <select class="form-control" name="appoint" required>
                          <option>Select Appointment type</option>
                          <option value="in_person">In Person</option>
                          <option value="video_consultancy">Video Consultancy</option>
						  
                      </select>
                 </div>
            <div class="left-agileits-w3layouts same">
                <div class="gaps">
                    <input type="text" name="name" pattern="[a-z A-Z]+ [a-z A-Z]+" placeholder="Enter Name." required="" class="form-control" />
                </div>  
                <div class="gaps">  
                    <input type="text" name="contact" pattern="[1-9]{1}[0-9]{9}" title="Enter 10 digit mobile number" placeholder="Enter contact.." required="" class="form-control"/>
                </div>
                <div class="gaps">
                    <input type="email" name="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Enter Your Email .." required="" class="form-control" />
                </div>
                <div class="gaps">
                    <select id="prov" name="department" class="form-control"></select>
                </div>
                <div class="gaps">
                    <select id="town" name="doctor" class="form-control"></select>
                </div>
                
                <!-- ============================================================================================================ -->
                 
                 <!-- ================================================ -->

                  <div class="gaps">
                        <input placeholder="Enter Registration no/File no/Patient id.." pattern="[0-9]{5}" type="text" name="registerno" class="form-control">    
                 </div>

                 
           
<!-- ======================================================================================================================================================= -->
                 <div class="gaps">
                        <input placeholder="Select Date.."  name="cdate" type="date"  class="form-control" required>
                </div>
               
         
                 <div class="gaps">
                      <input placeholder="Select time.."  type="time" id="timepicker" name="ctime" class="form-control" required>    
                 </div>
				 
				   <div class="gaps">
                      <input type="text" id="mainCaptcha"readonly="readonly" style="background-image: linear-gradient(#00adff6b, #3ee6df36); text-align: center;font-size: 18px;" />
                                
                                    <img src="img/refresh1.png" onclick="generateCaptcha();">
                                 <span id="error" style="color:red"></span>
                                 <span id="success" style="color:green"></span> 
                                 <input type="text" id="txtInput" placeholder="Enter the captcha here..." /> 
                                        <span id="error" style="color:red"></span></td>
                                         <span id="success" style="color:green"></span>   
                 </div>

                  <div class="gaps" align="center">
                    <input type="submit" name="submit" value="Submit Request" class="btn btn-primary">   
                 </div>
            </div>
                 
            </form>
               <br>
    <!-- -============================Registered=============================================== -->
             <script type="text/javascript"> 
  var provs = {
       'Select Department':[''],
       'Rheumatology & Immunology': ['Select Doctor','Dr.Chandrashekara .S', 'Dr.Beenish Nazir','Dr. Shruthi Desai','Dr.Dhanashree','Dr. Yogitha'],
        'Allergy & Clinical Immunology':['Select Doctor','Dr. Smitha J N Singh',''],
        'Reproductive immunology & High-risk pregnancy':['Select Doctor','Dr.Chandrashekara .S','Dr. Chaitra S Niranthara'],
        'Paediatric Rheumatology':['Select Doctor','Dr.Chandrashekara .S'],
        'Physiotherapy and Chronic pain management':['Select Doctor','Mr. Sasikumar. M','Mr. Rajkannan. P','Ms.Yogarani'],
        'Ophthalmology':['Select Doctor','Dr. Usharani',''],
        'Radiology':['Select Doctor','Dr. Shivanand N D','Dr.Shivakumar S M'],
        'Cardiology':['Select Doctor','Dr. Anand Kumar M','Dr. Nagesh M B'],
        'Nephrology':['Select Doctor','Dr. Vinod Nagesh'],
        'Pulmonology':['Select Doctor','Dr. Ramesh R','Dr.Arjun A S'],
        'Vascular Surgeon':['Select Doctor','Dr. Chandrashekar A R'],
        'ENT':['Select Doctor','Dr. Tejmurthy B V'],
        'Psychiatrist':['Select Doctor','Dr. Chandrashekar M','Dr. Sushma','Dr.Adarsh B'],
        'Diabetology':['Select Doctor','Dr. Radha Rangarajan',''],
        'Gastroenterology':['Select Doctor','Dr. Abhijith B R',''],
        'General Surgery':['Select Doctor','Dr. Manojith S S','Dr. Nagendra K'],
        'Dental':['Select Doctor','Dr. Dilip Bharadwaj',''],
        'Dermatologist':['Select Doctor','Dr. Sushmitha E S',''],
        'Urology':['Select Doctor','Dr. Nagarajaiah Narayanaswamy',''],
        'Orthopaedics':['Select Doctor','Dr. Balasubramanyam','Dr. Venu Madhav','Dr.C.B Prabhu','Dr. Darshan Kumar  A Jain','Dr. Kodlady Surendar Shetty'],

                // 'Orthopedics':['Dr. Venu Madhav',''],
        
      },
      prov_select = document.querySelector('#prov'),
      town_select = document.querySelector('#town');


  setOptions(prov_select, Object.keys(provs));
  setOptions(town_select, provs[prov_select.value]);
  prov_select.addEventListener('change', function() {
    setOptions(town_select, provs[prov_select.value]);
  });
    
  function setOptions(dropDown, options) {

    dropDown.innerHTML = '';

    options.forEach(function(value) {
      dropDown.innerHTML += '<option name="' + value + '">' + value + '</option>';
    });
  }  
</script>

<!-- =================================================================================== -->
                </div>

                <div class="col-md-4">
						<br><br>
				<h2 style="color:white">Instructions:</h2>
				<p style="color:white">
				
1. This application will assist you in submitting the request for appointment.<br>
2. Please select the doctor, department, and your preferred timings and date
for consultation.<br>
3. The appointment date of consultation is provided based on the availability
and consultation timings. Preferred date and time is a guideline to help
the front office and they will try to accommodate as far as possible.<br>
4. Once the appointment request is submitted, ChanRe’s front office team
will contact you within 24 hours, and based on the doctor&#39;s availability,
the appointment will be confirmed.<br>
5. While filling the details, please ensure that the mobile number and the
email address are entered correctly.<br><br>

<b>Note: Online video consultation facilities are available only to the registered
patients.</b>
				</p>
				<p><a href="appointment.apk" class="btn btn-primary" download>Download Android Application</a></p>
				</div>
            </div>
        </div>

     
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->
<script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
            <script type="text/javascript">
                $('.timepicker').wickedpicker({twentyFour: false});
            </script>
        <!-- Calendar -->
                <link rel="stylesheet" href="css/jquery-ui.css" />
                <script src="js/jquery-ui.js"></script>
                  <script>
                          $(function() {
                            $( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
                          });
                  </script>
                  <script>
    function showDiv(prefix,chooser) 
    {
            for(var i=0;i<chooser.options.length;i++) 
            {
                var div = document.getElementById(prefix+chooser.options[i].value);
                div.style.display = 'none';
            }

            var selectedOption = (chooser.options[chooser.selectedIndex].value);

            if(selectedOption == "1")
            {
                displayDiv(prefix,"1");
            }
            if(selectedOption == "2")
            {
                displayDiv(prefix,"2");
            }
    }

    function displayDiv(prefix,suffix) 
    {
            var div = document.getElementById(prefix+suffix);
            div.style.display = 'block';
    }
</script>
<script type="text/javascript">
    function generateCaptcha()
         {
             var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
             var i;
             for (i=0;i<5;i++){
               var a = alpha[Math.floor(Math.random() * alpha.length)];
               var b = alpha[Math.floor(Math.random() * alpha.length)];
               var c = alpha[Math.floor(Math.random() * alpha.length)];
               var d = alpha[Math.floor(Math.random() * alpha.length)];
               var e = alpha[Math.floor(Math.random() * alpha.length)];
              }
            var code = a + '' + b + '' + '' + c + '' + d +''+e;
            document.getElementById("mainCaptcha").value = code
          }
          function CheckValidCaptcha(){
             
              var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
              var string2 = removeSpaces(document.getElementById('txtInput').value);
              if (string1 == string2){
         document.getElementById('success').innerHTML = "Captcha is validated Successfully";
                return true;
              }
              else{       
         document.getElementById('error').innerHTML = "Please enter a valid captcha."; 
                return false;
         
              }
          }
          function removeSpaces(string){
            return string.split(' ').join('');
          }
          
</script>
</body>

</html>