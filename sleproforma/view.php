<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$name=$_SESSION['login'];
 ?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>SLE PROFORMA PATIENT DATA COLLECTION FORM</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">

    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
      table{text-align: center;width: 100%;}
      h5{
        color: black;
        font-weight: bold;
        font-family: Tahoma;
        border: 1px solid gray;
        width: 100%;
        padding: 5px;
      }
    </style>

  </head>

  <body class="light-sidebar-nav">

  <section id="container">
      <!--header start-->
      <?php include('layout/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include ('layout/aside.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
       <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="card">
                          <header class="card-header">
                              SLE PROFORMA PATIENT DATA COLLECTION
                          </header>
                          <div class="card-body">
                              <form method="POST">
                                 <table border="2">
								 <tr>
								 <th>Mrd No</th>
								 <th>SLE No</th>
								 <th>Age</th>
								 <th>Patient ID</th>
								 <th>Action</th>
								 </tr>
								 <?php $query1=mysqli_query($con,"Select * from sledai where username='$name'");
								 while($rows = mysqli_fetch_array($query1))
								 {
								 ?>
								 <tr>
								 <td><?php echo htmlentities ($rows['mrdno']);?></td>
								 <td><?php echo htmlentities ($rows['sleno']);?></td>
								 <td><?php echo htmlentities ($rows['age']);?></td>
								 <td><?php echo htmlentities ($rows['patientid']);?></td>
								 <td><a href="viewdata.php?nid=<?php echo htmlentities($rows['id']);?>" class="btn btn-primary">View </a>|<a href="editdata.php?pid=<?php echo htmlentities($rows['id']);?>" class="btn btn-danger">Edit</a></td>
								 </tr>
								 <?php } ?>
								 </table>
                              </form>

                          </div>
                      </section>

                  </div>
              </div>
            
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->

      <!-- Right Slidebar start -->
     
      <!-- Right Slidebar end -->

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2020 &copy; ChanRe.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/owl.carousel.js" ></script>
    <script src="js/jquery.customselect.min.js" ></script>
    <script src="js/respond.min.js" ></script>

    <!--right slidebar-->
    <script src="js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/count.js"></script>
    <script type="text/javascript">
      function mycal() {
  const se = Number(document.getElementById("se").value);
  const ps = Number(document.getElementById("ps").value);
  const or = Number(document.getElementById("or").value);
  const vd = Number(document.getElementById("vd").value);
  const cn = Number(document.getElementById("cn").value);

  const lh = Number(document.getElementById("lh").value);
  const cva = Number(document.getElementById("cva").value);
  const vas = Number(document.getElementById("vas").value);
  const ar = Number(document.getElementById("ar").value);
  const my = Number(document.getElementById("my").value);
  const uc = Number(document.getElementById("uc").value);
  const he = Number(document.getElementById("he").value);
  const pro = Number(document.getElementById("pro").value);
  const py = Number(document.getElementById("py").value);
  const rash = Number(document.getElementById("rash").value);
  const alo = Number(document.getElementById("alo").value);
  const muco = Number(document.getElementById("muco").value);
  const ple = Number(document.getElementById("ple").value);
  const pe = Number(document.getElementById("pe").value);
  const lc = Number(document.getElementById("lc").value);
  const idb = Number(document.getElementById("idb").value);
  const thro = Number(document.getElementById("thro").value);
  const leu = Number(document.getElementById("leu").value);
   
  
  const m = se + ps + or + vd+ cn+lh+cva+vas+ar+my+uc+he+pro+py+rash+alo+muco+ple+pe+lc+idb+thro+leu;
  
  document.getElementById("ss").value= m.toFixed(0);
  }
  
  function slic()
  {
const oc=Number(document.getElementById("oc").value);
const rtc=Number(document.getElementById("rtc").value);
const coi=Number(document.getElementById("coi").value);
const ser=Number(document.getElementById("ser").value);
const cea=Number(document.getElementById("cea").value);
const cran=Number(document.getElementById("cran").value);
const trm= Number(document.getElementById("trm").value);
const esm= Number(document.getElementById("esm").value);
const prh= Number(document.getElementById("prh").value);
const ens= Number(document.getElementById("ens").value);
const puh= Number(document.getElementById("puh").value);
const puf= Number(document.getElementById("puf").value);
const shl= Number(document.getElementById("shl").value);
const plf= Number(document.getElementById("plf").value);
const pui= Number(document.getElementById("pui").value);
const ano= Number(document.getElementById("ano").value);
const myi= Number(document.getElementById("myi").value);
const cad= Number(document.getElementById("cad").value);
const vad= Number(document.getElementById("vad").value);
const pef= Number(document.getElementById("pef").value);
const pev= Number(document.getElementById("pev").value);
const mit= Number(document.getElementById("mit").value);
const sit= Number(document.getElementById("sit").value);
const vet= Number(document.getElementById("vet").value);
const inr= Number(document.getElementById("inr").value);
const mei= Number(document.getElementById("mei").value);
const chp= Number(document.getElementById("chp").value);
const sto= Number(document.getElementById("sto").value);
const mua= Number(document.getElementById("mua").value);
const deo= Number(document.getElementById("deo").value);
const osw= Number(document.getElementById("osw").value);
const avn= Number(document.getElementById("avn").value);
const ost= Number(document.getElementById("ost").value);
const scc= Number(document.getElementById("scc").value);
const exc= Number(document.getElementById("exc").value);
const sku= Number(document.getElementById("sku").value);
const prg= Number(document.getElementById("prg").value);
const reg= Number(document.getElementById("reg").value);
const exd = Number(document.getElementById("exd").value);

 const k = oc+rtc+coi+ser+cea+cran+trm+esm+prh+ens+puh+puf+shl+plf+pui+ano+myi+cad+vad+pef+pev+mit+sit+vet+inr+mei+chp+sto+mua+deo+osw+avn+ost+scc+exc+sku+prg+reg+exd;
  
  document.getElementById("res").value= k.toFixed(0);
  }
function sledai()
{
  const neud = Number(document.getElementById("neud").value);
const renald = Number(document.getElementById("renald").value);
const ugtf = Number(document.getElementById("ugtf").value);
const haemo = Number(document.getElementById("haemo").value);
const thromb = Number(document.getElementById("thromb").value);
const promuscle = Number(document.getElementById("promuscle").value);
const arth = Number(document.getElementById("arth").value);
const mucoc = Number(document.getElementById("mucoc").value);
const sero = Number(document.getElementById("sero").value);
const feverfa = Number(document.getElementById("feverfa").value);
const leuk = Number(document.getElementById("leuk").value);

const l = neud+renald+ugtf+haemo+thromb+promuscle+arth+mucoc+sero+feverfa+leuk;
  
  document.getElementById("mex").value= l.toFixed(0);

}


    </script>
 

  </body>
</html>
<?php } ?>