<?php
 session_start();
//Database Configuration File
include('config.php');
//error_reporting(0);
if(isset($_POST['login']))
  {
    $uname=$_POST['username'];
    $password=$_POST['password'];
$sql =mysqli_query($con,"SELECT name,email,password FROM admintable WHERE (name='$uname' || email='$uname') and Is_Active = 1");
 $num=mysqli_fetch_array($sql);
if($num>0)
{
$hashpassword=$num['password']; 

if (password_verify($password, $hashpassword)) {
$_SESSION['login']=$_POST['username'];
    echo "<script type='text/javascript'> document.location = 'dashboard.php'; </script>";
  } else {
echo "<script>alert('Wrong Password');</script>";
 
  }
}

else{
echo "<script>alert('User not registered with us');</script>";
  }
 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="CRICR">
    <meta name="keyword" content="">
    <link rel="shortcut icon" href="img/favicon.html">
    <title>SLE Proforma - Patient Data Collection Sheet</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />


</head>

  <body class="login-body">

    <div class="container">

      <form class="form-signin" method="POST">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" name="username" placeholder="User ID" autofocus>
            <input type="password" class="form-control" name="password" placeholder="Password">
           
            <button class="btn btn-lg btn-login btn-block" type="submit" name="login">Sign in</button>
            
            <div class="registration">
                Don't have an account yet?
                <a class="" href="registration.php">
                    Create an account
                </a>
            </div>
            <div class="registration">
               
                <a class="" href="requestReset.php">
                    Forgot Password
                </a>
            </div>
        </div>
          <!-- Modal -->
         
      </form>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>


  </body>
</html>
