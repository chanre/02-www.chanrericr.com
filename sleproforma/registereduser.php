<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
 if(GET['nid'])
{
	$id=intval($_GET['nid']);
	$query=mysqli_query($con,"delete from  admintable  where id='$id'");
	
}
if($_GET['pid'])
{
	$id=intval($_GET['pid']);
	$query=mysqli_query($con,"Update admintable set Is_Active='1' where id='$id'");

}
 ?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>SLE PROFORMA PATIENT DATA COLLECTION FORM</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">

    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
      table{text-align: center;width: 100%;}
      h5{
        color: black;
        font-weight: bold;
        font-family: Tahoma;
        border: 1px solid gray;
        width: 100%;
        padding: 5px;
      }
    </style>

  </head>

  <body class="light-sidebar-nav">

  <section id="container">
      <!--header start-->
      <?php include('layout/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include ('layout/aside.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
       <section id="main-content">
          <section class="wrapper">
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="card">
                          <header class="card-header">
                              SLE PROFORMA PATIENT DATA COLLECTION
                          </header>
                          <div class="card-body">
                              <form method="POST">
                                 <table border="2">
								 <tr>
								 <th>Name</th>
								 <th>Email</th>
								 <th>Institute</th>
								 <th>Address</th>
		
								 </tr>
								 <?php $query1=mysqli_query($con,"Select * from admintable where Is_Active=1");
								 while($rows = mysqli_fetch_array($query1))
								 {
								 ?>
								 <tr>
								 <td><?php echo htmlentities ($rows['name']);?></td>
								 <td><?php echo htmlentities ($rows['email']);?></td>
								 <td><?php echo htmlentities ($rows['institute']);?></td>
								 <td><?php echo htmlentities ($rows['address']);?></td>
								 
								 </tr>
								 <?php } ?>
								 </table>
                              </form>

                          </div>
                      </section>

                  </div>
              </div>
            
              <!-- page end-->
          </section>
      </section>
  

      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2020 &copy; ChanRe.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/owl.carousel.js" ></script>
    <script src="js/jquery.customselect.min.js" ></script>
    <script src="js/respond.min.js" ></script>

    <!--right slidebar-->
    <script src="js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/count.js"></script>
    
 

  </body>
</html>
<?php } ?>
