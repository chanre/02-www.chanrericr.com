<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
	$name= $_SESSION['login'];
    ?>
<!DOCTYPE html>
<html lang="en">
  

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">

    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!-- Custom styles for this template -->

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
<style>
table{
	width:100%;
}
</style>
  </head>

  <body class="light-sidebar-nav">

  <section id="container">
      <!--header start-->
      <?php include('layout/header.php'); ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include ('layout/aside.php'); ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
              <div class="row state-overview">
                  <div class="col-lg-3 col-sm-6">
                      <section class="card">
                          <div class="symbol terques">
                              <i class="fa fa-user"></i>
                          </div>
                          <div class="value">
						  <?php $query=mysqli_query($con,"select * from admintable ");
							$countcat=mysqli_num_rows($query);
							?>
                              <h1 class="">
                                 <?php echo htmlentities($countcat);?>
                              </h1>
                              <p>Users</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="card">
                          <div class="symbol red">
                              <i class="fa fa-tags"></i>
                          </div>
                          <div class="value">
						  <?php $query=mysqli_query($con,"select * from sledai where username='$name'");
							$count=mysqli_num_rows($query);
							?>
                              <h1 class="">
                                  <?php echo htmlentities($count);?>
                              </h1>
                              <p>SLEDAI</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="card">
                          <div class="symbol yellow">
                              <i class="fa fa-tags"></i>
                          </div>
                          <div class="value">
								 <?php $query1=mysqli_query($con,"select * from sledata1 where username='$name'");
									$count1=mysqli_num_rows($query);
									?>
                              <h1 class="">
                                  <?php echo htmlentities($count1);?>
                              </h1>
                              <p>SLE DATA</p>
                          </div>
                      </section>
                  </div>
                  <div class="col-lg-3 col-sm-6">
                      <section class="card">
                          <div class="symbol blue">
                              <i class="fa fa-bar-chart-o"></i>
                          </div>
                          <div class="value">
						   <?php $query2=mysqli_query($con,"select * from slemed where username='$name'");
									$count2=mysqli_num_rows($query2);
									?>
                              <h1 class=" ">
                                  <?php echo htmlentities($count2);?>
                              </h1>
                              <p>SLE Medication</p>
                          </div>
                      </section>
                  </div>
              </div>
              <!--state overview end-->

              <div class="row">
                  <div class="col-lg-8">
                      <!--custom chart start-->
                      <div class="border-head">
                          <h3>SLE Data</h3>
                      </div>
                      <div class="custom-bar-chart">
					  <h4>Important information</h4>
					  <ol>
					  <li>MRD No, Age, Patient id, SLE no are mandatory so you must enter all those before entering any data.</li>
					  </ol>
                            <!-- <table>
								 <tr>
								 <th>Patient ID</th>
								 <th>Mrd No</th>
								 <th>Age</th>
								 <th>Action</th>
								 </tr>
			<?php $fetch = mysqli_query($con,"Select * from sledata1 where username='$name'");
			while($rows = mysqli_fetch_array($fetch))
			{
			?>
								 <tr>
								 <td><?php echo htmlentities($rows['patientid']);?></td>
								 <td><?php echo htmlentities($rows['mrdno']);?></td>
								 <td><?php echo htmlentities($rows['age']);?></td>
								 <td><a href="#" class="btn btn-danger">View</a></td>
								  
								 </tr>
								
								
			<?php } ?>
								 </table> -->
                      </div>
                      <!--custom chart end-->
                  </div>
                  <div class="col-lg-4">
                      <!--new earning start-->
                      <div class="card terques-chart">
					  <div class="chart-tittle">
                              <h4>SLEDAI</h4>
                          </div>
                          <div class="card-body chart-texture">
                              <div class="chart">
                                 <table>
								 <tr>
								 <th>Patient ID</th>
								 <th>Mrd No</th>
								 <th>Age</th>
								 <!--<th>Action</th> -->
								 </tr>
			<?php $fetch = mysqli_query($con,"Select * from sledai where username='$name'");
			while($rows = mysqli_fetch_array($fetch))
			{
			?>
								 <tr>
								 <td><?php echo htmlentities($rows['patientid']);?></td>
								 <td><?php echo htmlentities($rows['mrdno']);?></td>
								 <td><?php echo htmlentities($rows['age']);?></td>
								 <!--<td><a href="editdata.php?pid=<?php echo htmlentities($rows['id']);?>" class="btn btn-danger">View</a></td>
								 --> 
								 </tr>
								
								
			<?php } ?>
								 </table>
                             </div>
                          </div>
                          
                      </div>
                
                  </div>
              </div>
           
              
             
      </section>
      <!--main content end-->

      <!-- Right Slidebar start -->
       <!-- Right Slidebar end -->

      <!--footer start
      <footer class="site-footer">
          <div class="text-center">
              2020 &copy; by Chanre Healthcare.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
    footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/owl.carousel.js" ></script>
    <script src="js/jquery.customSelect.min.js" ></script>
    <script src="js/respond.min.js" ></script>

    <!--right slidebar-->
    <script src="js/slidebars.min.js"></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

    <!--script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/count.js"></script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

      $(window).on("resize",function(){
          var owl = $("#owl-demo").data("owlCarousel");
          owl.reinit();
      });

  </script>

  </body>

</html>
<?php } ?>