<?php 
include('config.php');  
if(isset($_POST['submit']))
{
$name=$_POST['name'];
$email=$_POST['email'];
$institute=$_POST['institute'];
$address=$_POST['address']; 
$password = $_POST['password'];
$status=2;
$password=password_hash($password, PASSWORD_DEFAULT);
$check=mysqli_query($con,"select * from admintable where email ='$email' ");
     $checkrows=mysqli_num_rows($check);
     if($checkrows>0) {
     echo "<script>alert('Email you have used is already exist in our database...');</script>"; 
     } else {
    $query=mysqli_query($con,
      "Insert into admintable(name,email,institute,address,password,Is_Active) values('$name','$email','$institute','$address','$password','$status')");
     }
    if($query)
{
echo "<script> alert('Your request is succcessful!!')</script>";
}
else{
echo "<script> alert('Your request can not processed.. Please try again!!')</script>";
} 


}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Registration Form</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />


</head>
 
  <body class="login-body">

    <div class="container">

      <form class="form-signin" method="POST">
        <h2 class="form-signin-heading">registration now</h2>
        <div class="login-wrap">
           
                      
                                <div class="form-group">
                        <label for="">User Name</label>
                        <input type="text" class="form-control" name="name" Title="Space not allowed" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" placeholder="Enter your User name..">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email..">
                        
                      </div>
                      <div class="form-group">
                        <label for="">Institute</label>
                        <input type="text" name="institute" class="form-control" id="" placeholder="Enter your institute..">
                      </div>
                      <div class="form-group">
                        <label for="">Address</label>
                        <input type="text" name="address" class="form-control" id="" placeholder="Enter your address..">
                      </div>
                      <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" id="" placeholder="Enter Your Password..">
                      </div>
            <button class="btn btn-lg btn-login btn-block" type="submit" name="submit">Submit</button>

            <div class="registration">
                Already Registered.
                <a class="" href="index.php">
                    Login
                </a>
            </div>

        </div>

      </form>

    </div>


  </body>

</html>
