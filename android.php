<?php 

include ('config.php');

if(isset($_POST['submit']))
{
$appoint = $_POST['appoint'];
$name = $_POST['name'];
$contact = $_POST['contact'];
$email = $_POST['email'];
$department = $_POST['department'];
$doctor = $_POST['doctor'];
$gender = $_POST['gender'];
$symptoms = $_POST['symptoms'];
$rdoctor=$_POST['rdoctor'];
$cdate=$_POST['cdate'];
$ctime=$_POST['ctime'];
$status=0;
$mydate = date('d/m/y');


$query = mysqli_query($con,"insert into test (appoint,name,contact,email,department,doctor,gender,symptoms,rdoctor,cdate,ctime,Is_Active,date)
 values('$appoint','$name','$contact','$email','$department','$doctor','$gender','$symptoms','$rdoctor','$cdate','$ctime','$status','$mydate')");
if($query){
$msg="Thank you for choosing chanre..We will get back to you in a while!!";

}
else
{
$msg="Couldn't process your request!!! Can you please try again..";
}

}
 ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
<!-- Custom Theme files -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
        h4{
            background-color: #32b58b;
            line-height: 40px;
            color: white;
            font-weight: 800;
            font-family: times;
        }
        .myform{
            background-image: url(images/b1.jpg);
        }
        .myform1{
            background-color: #135c853b;
            color: white;
        }
        .gaps{margin-top: 10px;font-family: times;}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body onload="generateCaptcha()">
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
        <header id="top" class="top_navbar">
            <div class="container">
                <nav class="navbar  navbar-expand-lg navbar-light ">
                    <a class="navbar-brand" href="/multiapp.php"><img src="images/chanrelogo.png" alt="" width="250" /></a>
                    
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                           
                        </ul>
                      
                    </div>
                </nav>
            </div>
        </header>
		
		<style>
		p{
			text-align:justify;
		}
		h6{
			color:green;
		}
		</style>

       <div class="container-fluid myform">
            <div class="row">
                <div class="col-md-4"></div> <br>
                <div class="col-md-4 myform1"><br>
				<h2 style="font-family:times;text-align:center;background-color:#4281cd;line-height:50px;">Appointment Request</h2>
				<h6><?php echo "$msg";?></h6>
                 <form  method="POST" onsubmit="CheckValidCaptcha()">
				 
				 <span>Are You already registered? <a href="aregistered.php">Click Here</a></span>
            <div class="left-agileits-w3layouts same">
				 <div class="gaps">
                      <select class="form-control" name="appoint" required>
                          <option>Select Appointment type</option>
                          <option value="in_person">In Person</option>
                          <option value="video_consultancy">Video Consultancy</option>
						  
                      </select>
                 </div>
                <div class="gaps">
                    <input type="text" name="name" pattern="[a-z A-Z]+ [a-z A-Z]+" placeholder="Enter Name.." required="" class="form-control" />
                </div>  
                <div class="gaps">  
                    <input type="text" name="contact" pattern="[1-9]{1}[0-9]{9}" title="Enter 10 digit mobile number" placeholder="Enter contact.." required="" class="form-control"/>
                </div>
                <div class="gaps">
                    <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Enter Your Email .." required="" class="form-control" />
                </div>
                <div class="gaps">
                    <select id="prov" name="department" class="form-control"></select>
                </div>
                <div class="gaps">
                    <select id="town" name="doctor" class="form-control"></select>
                </div>
                
                <!-- ============================================================================================================ -->
             
                      <div class="gaps">
                      <select class="form-control" name="gender">
                          <option>Select Gender</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                      </select>
                 </div>
                  
                <div class="gaps">
                        <textarea name="symptoms" placeholder="Please Write Symptoms.."  class="form-control" required></textarea>
                </div>
                <div class="gaps">
                        <input type="text" name="rdoctor" pattern="[A-Za-z]+"  title="No numbers or special character allowed." placeholder="Name of Refered Doctor" class="form-control">
                </div>
                
    
                 
             
                 <!-- ================================================ -->
                
<!-- ======================================================================================================================================================= -->
                 <div class="gaps">
                        <input placeholder="Select Date.."  name="cdate" type="date"  class="form-control" required>
                </div>
               
         
                 <div class="gaps">
                     <input placeholder="Select time.."  type="time" id="timepicker" name="ctime" class="form-control" required> 
                 </div>
				  <div class="gaps">
                      <input type="text" id="mainCaptcha"  readonly="readonly" style="background-image: linear-gradient(#00adff6b, #3ee6df36); text-align: center;font-size: 18px;" />
                                
                                    <img src="img/refresh1.png" onclick="generateCaptcha();">
                                 <span id="error" style="color:red"></span>
                                 <span id="success" style="color:green"></span> <br>
                                 <input type="text" id="txtInput" placeholder="Enter the captcha here..." /> 
                                        <span id="error" style="color:red"></span></td>
                                         <span id="success" style="color:green"></span>   
                 </div>

                  <div class="gaps" align="center">
                    <input type="submit" name="submit" onclick="CheckValidCaptcha()" value="Submit Request" class="btn btn-primary">   
                 </div>
            </div>
                 
            </form>
               <br>
    <!-- -============================Registered=============================================== -->
             <script type="text/javascript"> 

  var provs = {
        'Select Department':[''],
        'Rheumatology': ['Dr.Chandrashekara .S', 'Dr.Beenish Nazir','Dr. Ram Krishna Giri','Dr. Veena Ramachandran','Dr. Prakruthi. J','Dr. Shruti Vidyadhar Paramshetti'],
        'Paediatric': ['Dr.Chandrika S Bhat'],
        'Allergy':['Dr. Smitha J N Singh',''],
        'Orthopedics':['Dr. Venu Madhav',''],
        'Cardiology':['Dr. Vikram Kolhari','Dr. Nagesh M B'],
        'Radiology':['Dr. Shivanand N D',''],
        'Diabetology':['Dr. Radha Rangarajan',''],
        'Urology':['Dr. Nagarajaiah Narayanaswamy',''],
        'High Risk Pregnancy':['Dr. Nandyala Sundari',''],
        'Chronic Pain Management':['',''],
        'Joint Preservation & Restoration':['',''],
        'Ophthalmology':['Dr. Usharani',''],
        'General Surgery':['Dr. Manojith S S','Dr. Nagendra K'],
        'Dental':['Dr. Dilip Bharadwaj',''],
        'ENT':['',''],
        'Neurosurgery':['Dr. Karthik Malepathi',''],
        'Gastroenterology':['Dr. E R Siddeshi',''],
        'Dermatologist':['Dr. Sunil Kumar S',''],

      },
      // just grab references to the two drop-downs
      prov_select = document.querySelector('#prov'),
      town_select = document.querySelector('#town');

  // populate the provinces drop-down
  setOptions(prov_select, Object.keys(provs));
  // populate the town drop-down
  setOptions(town_select, provs[prov_select.value]);
  
  // attach a change event listener to the provinces drop-down
  prov_select.addEventListener('change', function() {
    // get the towns in the selected province
    setOptions(town_select, provs[prov_select.value]);
  });
    
  function setOptions(dropDown, options) {
    // clear out any existing values
    dropDown.innerHTML = '';
    // insert the new options into the drop-down
    options.forEach(function(value) {
      dropDown.innerHTML += '<option name="' + value + '">' + value + '</option>';
    });
  }  
</script>

<script type="text/javascript">
    function generateCaptcha()
         {
             var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
             var i;
             for (i=0;i<5;i++){
               var a = alpha[Math.floor(Math.random() * alpha.length)];
               var b = alpha[Math.floor(Math.random() * alpha.length)];
               var c = alpha[Math.floor(Math.random() * alpha.length)];
               var d = alpha[Math.floor(Math.random() * alpha.length)];
               var e = alpha[Math.floor(Math.random() * alpha.length)];
              }
            var code = a + '' + b + '' + '' + c + '' + d +''+e;
            document.getElementById("mainCaptcha").value = code
          }
          function CheckValidCaptcha(){
             
              var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
              var string2 = removeSpaces(document.getElementById('txtInput').value);
              if (string1 == string2){
         document.getElementById('success').innerHTML = "Captcha is validated Successfully";
                return true;
              }
              else{       
         document.getElementById('error').innerHTML = "Please enter a valid captcha."; 
                return false;
         
              }
          }
          function removeSpaces(string){
            return string.split(' ').join('');
          }
          
</script>

                  <script>
    function showDiv(prefix,chooser) 
    {
            for(var i=0;i<chooser.options.length;i++) 
            {
                var div = document.getElementById(prefix+chooser.options[i].value);
                div.style.display = 'none';
            }

            var selectedOption = (chooser.options[chooser.selectedIndex].value);

            if(selectedOption == "1")
            {
                displayDiv(prefix,"1");
            }
            if(selectedOption == "2")
            {
                displayDiv(prefix,"2");
            }
    }

    function displayDiv(prefix,suffix) 
    {
            var div = document.getElementById(prefix+suffix);
            div.style.display = 'block';
    }
</script>
<!-- =================================================================================== -->
                </div>

                <div class="col-md-4"></div>
            </div>
        </div>

     
         <style type="text/css">#mobile-footer {
    position: fixed;
    bottom: 0;

    width: 100%;
    color: #f2f2f2;
    @media(min-width: 768px) {
        display: none;
    }
}
#mobile-menu {
    background: hsl(122deg 39% 49%);
}
#mobile-footer-container {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    overflow: hidden;
}
.mobile-link {
    padding-top: 0.75em;
    padding-bottom: 0.75em;
}
.mobile-link a {
    font-size: 16px;
    color: #fff;
    text-decoration: none;
}
#mobile-footer-close {
    position: relative;
}
#mobile-footer-btn {
    position: absolute;
    bottom: 25px;
    right: 5px;
    width: 30px;
    height: 30px;
    background-color: #959192;
    border: none;
    border-radius: 50%;
    overflow: hidden;
    text-indent: 100%;
    color: transparent;
    white-space: nowrap;
    cursor: pointer;
    &:focus {
        outline: 0;
    }
}
.mobile-btn-close span,
.mobile-btn-close span::before {
    content: '';
    position: absolute;
    width: 5px;
    height: 18px;
    top: calc(50% - 9px);
    right: calc(50% + -2.5px);
    background-color: #fff;
    transform: rotate(-90deg);
    transition: 0.3s ease-out;
}
.mobile-btn-close {
    transition: 1s ease-out;
    &::focus {
        transition: 1s ease-out;
    }
}
.mobile-btn-close {
    span {
        transform: rotate(45deg);
        &::before {
            content: '';
            transform: rotate(-90deg);
        }
    }
}
.is-rotating {
    transform: rotate(135deg);
}
.is-rotating-back {
    transform: rotate(-90deg);
}
.mobile-menu-hide {
    animation: hideFooter 10s forwards;
}
.mobile-menu-show {
    animation: showFooter 1s forwards;
}
@keyframes hideFooter {
    0% {
        transform: translateY(0);
        opacity: 1;
    }
    100% {
        transform: translateY(1000px);
        opacity: 0;
    }
}
@keyframes showFooter {
    0% {
        transform: translateY(300px);
        opacity: 0;
    }
    100% {
        transform: translateY(0);
        opacity: 1;
    }
}


</style>
<br><br>
    <footer id="mobile-footer">
  <div id="mobile-menu">
    <div id="mobile-footer-container">
      <div class="mobile-link">
        <a href="symptom-checker.php" class="text-center"><i class="fa fa-medkit fa-24x"></i></a>
      </div>
      <div class="mobile-link">
        <a href="diagrampatient.php" class="text-center"><i class="fa fa-user-md fa-10x"></i></a>
      </div>
      <div class="mobile-link">
        <a href="android.php" class="text-center"><i class="fa fa-plus-square"></i></a>
      </div>
    </div>
  </div>
  <!--  
  <div id="mobile-footer-close">
    <button id="mobile-footer-btn">
      <div class="mobile-btn-close is-rotating-back">
        <span></span>
      </div>
    </button>
  </div>-->
</footer>
    
<script type="text/javascript">
  (function($) {
  $(function() {
    // Store menu container
    var mobileMenu = '#mobile-menu';
    // Store Trigger
    var mobileBtn = '#mobile-footer-btn';

    var rotation = '.mobile-btn-close';

    $(mobileBtn).on("click", function(e) {
      e.stopPropagation();
      if ($(mobileMenu).hasClass('mobile-menu-hide') || $(rotation).hasClass('is-rotating')) {
        $(mobileMenu).removeClass("mobile-menu-hide").addClass("mobile-menu-show");
        $(rotation).removeClass("is-rotating").addClass("is-rotating-back");
      } else {
        $(mobileMenu).removeClass("mobile-menu-show").addClass("mobile-menu-hide");
        $(rotation).removeClass('is-rotating-back').addClass('is-rotating');
      }
    });
  });
})(jQuery);
</script>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->
<script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
            <script type="text/javascript">
                $('.timepicker').wickedpicker({twentyFour: false});
            </script>
        <!-- Calendar -->
                <link rel="stylesheet" href="css/jquery-ui.css" />
                <script src="js/jquery-ui.js"></script>
                  <script>
                          $(function() {
                            $( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
                          });
                  </script>
              


                  <script>
    function showDiv(prefix,chooser) 
    {
            for(var i=0;i<chooser.options.length;i++) 
            {
                var div = document.getElementById(prefix+chooser.options[i].value);
                div.style.display = 'none';
            }

            var selectedOption = (chooser.options[chooser.selectedIndex].value);

            if(selectedOption == "1")
            {
                displayDiv(prefix,"1");
            }
            if(selectedOption == "2")
            {
                displayDiv(prefix,"2");
            }
    }

    function displayDiv(prefix,suffix) 
    {
            var div = document.getElementById(prefix+suffix);
            div.style.display = 'block';
    }
</script>

</body>

</html>