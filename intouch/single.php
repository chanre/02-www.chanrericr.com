<?php include('blogconfig.php') ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">

    <style type="text/css">
        img{width: 80%;overflow: hidden;}
    </style>
    <title>Intouch</title>
</head>

<body class="color-theme-blue push-content-right theme-light">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- sidebar left start -->
       <?php include('layout/leftsidebar.php'); ?>
        <!-- sidebar left ends -->

        <!-- sidebar right start -->
       
        <!-- sidebar right ends -->

        <!-- fullscreen menu start -->
       
        <!-- fullscreen menu ends -->

        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <!-- header -->
            <?php include('layout/header.php'); ?>
            <!-- header end -->
            <div class="page-content">
                <div class="content-sticky-footer">    
                    <div class="col-12 mb-4">
                        <?php 
                        $pid=intval($_GET['nid']);
 $query=mysqli_query($con,"select * from tblposts where id='$pid'");
while ($rows=mysqli_fetch_array($query)) {
                         ?>
                        
                        <div class="card">
                            <div class="card-header"><h6 class="card-title"><?php echo htmlentities($rows['PostTitle']); ?></h6></div>
                            <div class="card-body" >
                           <!-- card text -->
                              <div class="card-text">
                               <?php $pt=$rows['PostDetails']; echo  (substr($pt,0));?>
                              </div>
                            <!-- card text -->
                            </div>
                        </div>
                    <?php } ?>
                    </div>

                    <!-- button -->
                  
                   

                    
    
    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->
   
</body>
</html>
