<?php include('blogconfig.php'); ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">
    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">
    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">
    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">
    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">
    <title>Intouch</title>
</head>
<body class="color-theme-blue push-content-right theme-light">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- sidebar left start -->
       <?php include('layout/leftsidebar.php'); ?>
        <!-- sidebar left ends -->
        <!-- Popup -->
        <!-- <div class="modal fade " id="intro" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        
                        <div data-pagination='{"el": ".swiper-pagination", "hideOnClick": true}' class="swiper-container swiper-init introswiper">
                            
                            <div class="swiper-wrapper">
                                <div class="">
                                    <div class="row h-100">
                                        <div class="col-12 align-self-center text-center p-5">
                                            <img src="img/nopics.jpg" class="text-primary mb-4 w-50">
                                        
                                            <br>
                                            <h4 class="text-center">Welcome</h4>
                                            <p>We are here to assist you. Please enter correct information whenever we ask to get better service.</p>
                                            <br>
                                            <br>
                                            <button type="button" class="btn btn-primary rounded px-4" data-dismiss="modal" aria-label="Close">Got it!</button>
                                        </div>
                                    </div>
                                </div>
                           
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>  -->
        <!-- sidebar right ends -->

        <!-- fullscreen menu start -->
       
        <!-- fullscreen menu ends -->

        <!-- page main start -->
        <div class="page">
            
            <!-- header -->
            <?php include('layout/header.php'); ?>
            <!-- header end -->
            <div class="page-content">
                <div class="content-sticky-footer">    
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <i class="material-icons text-warning icon-weather icon-4x"></i><img src="img/nopics.jpg" class="w-100">
                            </div>
                        </div>
                    </div>

                    <!-- button -->
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <p class="text-uppercase font-weight-bold text-primary">We are here to assist</p>
                                <div class="text-center justify-content-between d-flex">
                                    <div><a href="appointment.php" class="btn btn-danger rounded sq-btn text-white"><i class="material-icons w-25px">touch_app</i></a>
                                        <small>Appointment</small>

                                    </div>
                                    <div>
                                       <a href="sample_collection.php" class="btn btn-primary rounded sq-btn text-white"><span class="material-icons w-25px">opacity</span></a> 
                                       <small>Sample</small>
                                    </div>
                                    <div>
                                        <a href="pddsra.php" class="btn btn-warning rounded sq-btn text-white"><i class="material-icons w-25px">face</i></a>
                                        <small>PDDSRA</small>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                               <div class="e_consult">
                                <a href="https://mychanreclinic.com/#/authentication/signin" class="btn btn-primary rounded btn-block btn-sm text-white"><small>Email Consultation</small><span class="material-icons w-25px">touch_app</span></a> 
                                
                              </div>
                            </div>
                          </div>
                     </div>
                    <!-- <div class="w-100"> 
                        <div class="carosel">
                            <div class="swiper-container swiper-init swipermultiple">
                                <div class="swiper-pagination"></div>
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block bg-white shadow-15">
                                            <p class="text-uppercase font-weight-bold text-primary">Project</p>
                                            <h4 class="title-small-carousel">Completed</h4>
                                            <p>November 2018</p>
                                            <div class="gaugewrap">
                                                <h2 class="title-number-carousel"><span class="text-primary">6</span><small>/8 Project</small></h2>
                                                <div class="progress_profile1 gauge" data-value="0.65" data-size="20" data-thickness="2" data-animation-start-value="0" data-reverse="false"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block bg-white shadow-15">
                                            <p class="text-uppercase font-weight-bold text-primary">Project</p>
                                            <h4 class="title-small-carousel">Overrun</h4>
                                            <p>November 2018</p>
                                            <div class="gaugewrap">
                                                <h2 class="title-number-carousel"><span class="text-danger">2</span><small>/3 Project</small></h2>
                                                <div class="progress_profile2  gauge" data-value="0.65" data-size="20" data-thickness="2" data-animation-start-value="0" data-reverse="false"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block gradient-danger text-white shadow-15">
                                            <p class="text-uppercase font-weight-bold text-white">Project</p>
                                            <h4 class="text-white title-small-carousel">Completed</h4>
                                            <p class="text-white">November 2018</p>
                                            <div class="gaugewrap">
                                                <h2 class="text-white title-number-carousel"><span class="text-white">6</span><small>/8 Project</small></h2>
                                                <div class="progress_profile5 gauge" data-value="0.65" data-size="20" data-thickness="2" data-animation-start-value="0" data-reverse="false"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block gradient-primary text-white shadow-15">
                                            <p class="text-uppercase font-weight-bold text-white">Project</p>
                                            <h4 class="text-white title-small-carousel">Overrun</h4>
                                            <p class="text-white">November 2018</p>
                                            <div class="gaugewrap">
                                                <h2 class="text-white title-number-carousel"><span class="text-white">2</span><small>/3 Project</small></h2>
                                                <div class="progress_profile5  gauge" data-value="0.65" data-size="20" data-thickness="2" data-animation-start-value="0" data-reverse="false"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block gradient-warning text-white shadow-15">
                                            <p class="text-uppercase font-weight-bold text-white">Project</p>
                                            <h4 class="text-white title-small-carousel">Good Job!</h4>
                                            <p class="text-white">November 2018</p>
                                            <div class="gaugewrap">
                                                <h2 class="text-white title-number-carousel"><span class="text-white">6</span><small>/8 Project</small></h2>
                                                <div class="progress_profile5  gauge" data-value="0.65" data-size="20" data-thickness="2" data-animation-start-value="0" data-reverse="false"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block gradient-success text-white shadow-15">
                                            <p class="text-uppercase font-weight-bold text-white">Project</p>
                                            <h4 class="text-white title-small-carousel">Critical</h4>
                                            <p class="text-white">November 2018</p>
                                            <div class="gaugewrap">
                                                <h2 class="text-white title-number-carousel"><span class="text-white">2</span><small>/3 Project</small></h2>
                                                <div class="progress_profile5  gauge" data-value="0.65" data-size="20" data-thickness="2" data-animation-start-value="0" data-reverse="false"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
<!-- Latest post -->



                    <h2 class="block-title">Latest Posts</h2>
                       <?php $query=mysqli_query($con,"select * from tblposts where Is_Active=1");
                    while($rows=mysqli_fetch_array($query))
                    {
                     ?>
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                               <p><?php echo htmlentities($rows['PostTitle']) ?></p>
                                    <div align="right">
                                        <a href="single.php?nid=<?php echo htmlentities($rows['id'])?> <?php echo htmlentities($rows['PostUrl'])?>" class="btn btn-primary rounded btn-sm text-white">Read more</a>
                                    </div> 
                            </div>     
                        </div>
                    </div>
                   <?php } ?>

<!-- Latest Post 
            
                    
                    <div class="col-12 mb-4">
                        <div class="row">
                            <div class="col-6 col-md-4 col-lg-2">
                                <div class="card">
                                    <div class="card-body text-center pb-1">
                                        <div class="row">
                                            <div class="col pr-0 text-left">
                                                <p class="text-uppercase font-weight-bold text-primary">Today</p>
                                            </div>
                                        </div>
                                        <div class="w-100">
                                            <div class="dynamicsparkline my-3"></div>
                                            <h2 class="font-weight-light mb-0">$1560</h2>
                                            <div class="text-success effort-time mt-3 small"> 2hrs <i class="material-icons">arrow_drop_up</i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-2">
                                <div class="card">
                                    <div class="card-body text-center pb-1">
                                        <div class="row">
                                            <div class="col pr-0 text-left">
                                                <p class="text-uppercase font-weight-bold text-primary">Yesterday</p>
                                            </div>
                                        </div>
                                        <div class="w-100">
                                            <div class="dynamicsparkline my-3"></div>
                                            <h2 class="font-weight-light mb-0">$2150</h2>
                                            <div class="text-danger effort-time mt-3 small"> 2hrs <i class="material-icons">arrow_drop_down</i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->





                    <!-- <div class="w-100"> 
                        <div class="carosel">
                            <div class="swiper-container swiper-init swipermultiple">
                                <div class="swiper-pagination"></div>
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="card w-100 mb-0">
                                            <figure class="background rounded">
                                                <img src="img/city.jpg" alt="" class="opacity-full">
                                            </figure>
                                            <div class="card-body text-center pb-1">
                                                <div class="row">
                                                    <div class="col pr-0 text-left">
                                                        <p class="text-uppercase font-weight-bold text-white">Today</p>
                                                    </div>
                                                </div>
                                                <div class="w-100">
                                                    <i class="material-icons icon-3x text-white my-2">mood</i>
                                                    <div class="text-white mt-3">Work Awesomely</div>
                                                    <button class="btn btn-warning rounded btn-sm my-3">Good</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card w-100 mb-0">
                                            <figure class="background rounded">
                                                <img src="img/city2.jpg" alt="" class="opacity-full">
                                            </figure>
                                            <div class="card-body text-center pb-1">
                                                <div class="row">
                                                    <div class="col pr-0 text-left">
                                                        <p class="text-uppercase font-weight-bold text-white">Yesterday</p>
                                                    </div>
                                                </div>
                                                <div class="w-100">
                                                    <i class="material-icons icon-3x text-white my-2">cake</i>
                                                    <div class="text-white  mt-3">That's Beautifully</div>
                                                    <button class="btn btn-success rounded btn-sm my-3">Thank you</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card w-100 mb-0">
                                            <figure class="background rounded">
                                                <img src="img/city.jpg" alt="" class="opacity-full">
                                            </figure>
                                            <div class="card-body text-center pb-1">
                                                <div class="row">
                                                    <div class="col pr-0 text-left">
                                                        <p class="text-uppercase font-weight-bold text-white">Today</p>
                                                    </div>
                                                </div>
                                                <div class="w-100">
                                                    <i class="material-icons icon-3x text-white my-2">mood</i>
                                                    <div class="text-white mt-3">Work Awesomely</div>
                                                    <button class="btn btn-danger rounded btn-sm my-3">Good</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="card w-100 mb-0">
                                            <figure class="background rounded">
                                                <img src="img/city2.jpg" alt="" class="opacity-full">
                                            </figure>
                                            <div class="card-body text-center pb-1">
                                                <div class="row">
                                                    <div class="col pr-0 text-left">
                                                        <p class="text-uppercase font-weight-bold text-white">Yesterday</p>
                                                    </div>
                                                </div>
                                                <div class="w-100">
                                                    <i class="material-icons icon-3x text-white my-2">cake</i>
                                                    <div class="text-white  mt-3">That's Beautifully</div>
                                                    <button class="btn btn-primary rounded btn-sm my-3">Thank you</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <h2 class="block-title">Top On Going Projects</h2>
                    <ul class="list-group mb-4 media-list ">
                        <li class="list-group-item">
                            <a href="#" class="media shadow-15">
                                <div class="media-body">
                                    <h5>Alpha School portal</h5>
                                    <p class="mb-0">Start Date: 28, July 2018</p>
                                    <h2 class="title-number-carousel color-primary"><span class="text-primary">126</span><small>/208 Project</small></h2>
                                </div>
                                <div class="w-auto">
                                    <small class="text-danger effort-time"> 2hrs  <i class="material-icons">arrow_drop_down</i></small>
                                    <div class="dynamicsparkline"></div>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="media shadow-15">
                                <div class="media-body">
                                    <h5>RozR David</h5>
                                    <p class="mb-0">Start Date: 28, July 2018</p>
                                    <h2 class="title-number-carousel color-primary"><span class="text-primary">150</span><small>/218 Project</small></h2>
                                </div>
                                <div class="w-auto">
                                    <small class="text-danger effort-time"> 2hrs  <i class="material-icons">arrow_drop_down</i></small>
                                    <div class="dynamicsparkline"></div>
                                </div>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="media shadow-15">
                                <div class="media-body">
                                    <h5>Plasma Research group</h5>
                                    <p class="mb-0">Start Date: 28, July 2018</p>
                                    <h2 class="title-number-carousel color-primary"><span class="text-primary">100</span><small>/152 Project</small></h2>
                                </div>
                                <div class="w-auto">
                                    <small class="text-success effort-time"> 2hrs  <i class="material-icons">arrow_drop_up</i></small>
                                    <div class="dynamicsparkline"></div>
                                </div>
                            </a>
                        </li>
                    </ul>

                    <div class="col-12 mb-4">
                        <div class="row">
                            <div class="col-6 col-md-4 col-lg-2">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <div class="row">
                                            <div class="col pr-0 text-left">
                                                <p class="text-uppercase font-weight-bold text-primary">Active</p>
                                            </div>
                                            <div class="col-auto pl-0 text-right">
                                                <input type="checkbox" id="switch" class="switch sm">
                                                <label for="switch">Toggle</label>
                                            </div>
                                        </div>
                                        <i class="material-icons text-warning icon-4x my-4">wb_sunny</i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-2">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <div class="row">
                                            <div class="col pr-0 text-left">
                                                <p class="text-uppercase font-weight-bold text-primary">Active</p>
                                            </div>
                                            <div class="col-auto pl-0 text-right">
                                                <input type="checkbox" id="switch2" class="switch sm" checked>
                                                <label for="switch2">Toggle</label>
                                            </div>
                                        </div>
                                        <i class="material-icons text-danger icon-4x my-4">notifications_active</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h2 class="block-title">Trending Project</h2>
                    <div class="row mx-0 mb-4">
                        <div class="col">
                            <div class="card">
                                <div class="card-body">
                                    <a href="#" class="media">
                                        <div class="media-body">
                                            <h5>Karla Sports App </h5>
                                            <p>25 November 2018</p>
                                        </div>
                                        <div class="w-auto h-100">
                                            <span class="text-primary">Completed</span>
                                        </div>
                                    </a>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col">
                                            <i class="material-icons text-warning">star</i>
                                            <span class="post-seconds">4.9</span>
                                        </div>
                                        <div class="col">
                                            <i class="material-icons text-grey">schedule</i>
                                            <span class="post-seconds">254 <span>hrs</span></span>
                                        </div>
                                        <div class="col">
                                            <i class="material-icons text-grey">monetization_on</i>
                                            <span class="post-seconds">4000 <span>$</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div> 
                <div class="footer-wrapper shadow-15">
                    <div class="footer">
                        <div class="row mx-0">
                            <div class="col">
                                Overux
                            </div>
                            <div class="col-7 text-right">
                                <a href="#" class="social"><img src="img/facebook.png" alt=""></a>
                                <a href="#" class="social"><img src="img/googleplus.png" alt=""></a>
                                <a href="#" class="social"><img src="img/linkedin.png" alt=""></a>
                                <a href="#" class="social"><img src="img/twitter.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="footer dark">
                        <div class="row mx-0">
                            <div class="col  text-center">
                                Copyright @2018, Maxartkiller
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <!-- page main ends -->


        <!-- 
        <button class="theme btn btn-info sq-btn rounded float-bottom-right" data-toggle="modal" data-target="#colorscheme"><i class="material-icons">color_lens</i></button>
        <div class="modal fade " id="colorscheme" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content ">
                    <div class="modal-header border-bottom theme-header">
                        <p class="text-uppercase font-weight-bold text-white my-2">Choose your color</p>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="text-center theme-color">
                            <p class="text-uppercase font-weight-bold text-dark text-center">Select Color</p>
                            <button class="m-2 btn btn-danger rounded sq-btn text-white" data-theme="color-theme-red"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <button class="m-2 btn btn-primary rounded sq-btn text-white" data-theme="color-theme-blue"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <button class="m-2 btn btn-warning rounded sq-btn text-white" data-theme="color-theme-yellow"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <button class="m-2 btn btn-success rounded sq-btn text-white" data-theme="color-theme-green"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <br>
                            <button class="m-2 btn btn-pink rounded sq-btn text-white" data-theme="color-theme-pink"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <button class="m-2 btn btn-orange rounded sq-btn text-white" data-theme="color-theme-orange"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <button class="m-2 btn btn-gray rounded sq-btn text-white" data-theme="color-theme-gray"><i class="material-icons w-25px">color_lens_outline</i></button>
                            <button class="m-2 btn btn-black rounded sq-btn text-white" data-theme="color-theme-black"><i class="material-icons w-25px">color_lens_outline</i></button>
                        </div>
                        <hr class="mt-4">
                        <div class="row align-items-center mt-4">
                            <div class=" col-12 w-100">
                                <p class="text-uppercase font-weight-bold text-dark text-center">Layout Mode</p>
                            </div>
                            <div class="col text-right"><i class="material-icons text-warning icon-3x">wb_sunny</i></div>
                            <div class="col-auto text-center">
                                <input type="checkbox" id="theme-dark" class="switch">
                                <label for="theme-dark">Toggle</label>
                            </div>
                            <div class="col text-left"><i class="material-icons text-dark icon-3x">brightness_2</i></div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
       -->


    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->
    <script>
        $(window).on('load', function() {
            /* sparklines */
            $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
                type: 'bar',
                height: '25',
                barSpacing: 2,
                barColor: '#a9d7fe',
                negBarColor: '#ef4055',
                zeroColor: '#ffffff'
            });

            /* gauge chart circular progress */
            $('.progress_profile1').circleProgress({
                fill: '#169cf1',
                lineCap: 'butt'
            });
            $('.progress_profile2').circleProgress({
                fill: '#f4465e',
                lineCap: 'butt'
            });
            $('.progress_profile4').circleProgress({
                fill: '#ffc000',
                lineCap: 'butt'
            });
            $('.progress_profile3').circleProgress({
                fill: '#00c473',
                lineCap: 'butt'
            });
            $('.progress_profile5').circleProgress({
                fill: '#ffffff',
                lineCap: 'butt'
            });

            /*Swiper carousel */
            var mySwiper = new Swiper('.swiper-container', {
                slidesPerView: 2,
                spaceBetween: 0,
                autoplay: {
                    delay: 1500,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
            /* tooltip */
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });

    </script>
     <script>
        'use strict'
        $(document).ready(function() {
            function introcarousel() {
                $('#intro').modal('show');
                setTimeout(function() {
                    /*Swiper carousel */
                    var mySwiper = new Swiper('.swiper-container', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        autoplay: {
                            delay: 2000,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        }
                    });
                }, 500);
            }

            introcarousel();

            $('#introbtn').on('click', function() {
                introcarousel();
                /*  $('#intro').modal('show');
                setTimeout(function() {
                    Swiper carousel 
                    var mySwiper = new Swiper('.swiper-container', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        autoplay: {
                            delay: 2000,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        }
                    });
                }, 500);*/


            });

        });

    </script>
</body>
</html>
