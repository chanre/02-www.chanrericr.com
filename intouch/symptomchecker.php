
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">


    <title>Intouch</title>
</head>

<body class="color-theme-blue push-content-right theme-light">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- sidebar left start -->
       <?php include('layout/leftsidebar.php'); ?>
        <!-- sidebar left ends -->

        <!-- sidebar right start -->
       
        <!-- sidebar right ends -->

        <!-- fullscreen menu start -->
       
        <!-- fullscreen menu ends -->

        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <!-- header -->
            <?php include('layout/header.php'); ?>
            <!-- header end -->
            <div class="page-content">
                <div class="content-sticky-footer">    
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body" >
                               
                                <p class="text-uppercase font-weight-bold text-center text-primary">Symptom Checker</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body" >
                               <form name="form1" onsubmit="return validate(this)"> 
                                <h6 class="qheader"> 1) Do you feel persistent pain in one or more of your joints?
                                </h6> 
                                <div class="qselections"> 
                                <input name="Yn" onclick="show1();" type="radio" value="1" />No 
                                <input name="Yn" onclick="show2();" type="radio" value="1" />Yes 
                                <div class="collapse" id="div1"> if yes how many?
                                <br /> 
                                <input class="answer1" name="question1" onclick="show7();" type="checkbox" value="1" />1 
                                <input class="answer1" name="question1" onclick="show8();" type="checkbox" value="2" />2-5 
                                <input class="answer1" name="question1" onclick="show8();" type="checkbox" value="3" />5
                                <br /> 
                                <div id="sides"> 
                                <input class="answer1a" name="question1" type="checkbox" value="0" />One side 
                                <input class="answer1a" name="question1" type="checkbox" value="4" />Both side
                                </div> 
                                <br /> <h6>a) Do you have swelling in the above mentioned painful joints?</h6>
                                <br /> 
                                <input name="question1" type="radio" value="1" />No 
                                <input name="question1" type="radio" value="1" />Yes
                                <br /> 
                                <br /> <h6>b)Since how long have you been experiencing these problems?</h6>
                                <br /> 
                                <input class="answer11" name="question1" type="checkbox" value="0" /> Few months 
                                <input class="answer11" name="question1" type="checkbox" value="1" /> 1 yr 
                                <input class="answer11" name="question1" type="checkbox" value="2" /> 2-5 yrs 
                                <input class="answer11" name="question1" type="checkbox" value="3" /> &gt;5 yrs
                                <br /> 
                                <br /> <h6> c) Whether the pain is due to a recent injury occurred while playing, at work or at home ?</h6>
                                <br /> 
                                <input name="qu1" type="radio" value="1" />No 
                                <input name="qu1" type="radio" value="-2" />Yes
                                <br /> &nbsp;
                                </div> 
                                </div> 
                                <div class="qheader"> <h6>2) Do you have persistent low back pain or buttock pain for &gt; 3 months, which is obstracting you sleep?</h6>
                                </div> 
                                <input class="answer2 open_options" name="question2" type="checkbox" value="1" /> Low back pain 
                                <input class="answer2 open_options" name="question2" type="checkbox" value="1" /> Buttock Pain 
                                <input class="answer2 open_options" name="question2" type="checkbox" value="2" /> Both 
                                <input class="answer2 close_options" name="question2" type="checkbox" value="1" /> No
                                <br /> 
                                <div id="options"> 
                                <input class="answer2a" name="question2" type="checkbox" value="1" /> Pain after waking up
                                <br /> 
                                <input class="answer2a" name="question2" type="checkbox" value="0" /> After excess of straining &gt; 30 min
                                <br /> 
                                <input class="answer2b" name="question2" type="checkbox" value="1" /> After prolonged sitting
                                <br /> 
                                <input class="answer2b" name="question2" type="checkbox" value="0" /> Dose it impair your daily activities
                                </div> 
                                <div class="qheader"> <h6>a) Do you have early morning stiffness which improves or worsen with activity?</h6>
                                </div> 
                                <div class="qselections"> 
                                <input name="question2" onclick="show5();" type="radio" value="0" />No 
                                <input name="question2" onclick="show6();" type="radio" value="1" />Yes 
                                <div id="div2"> 
                                <input id="answer2aa" name="question2" type="checkbox" value="1" /> stiffness &gt; 30 min
                                <br /> 
                                <input id="answer2aa" name="question2" type="checkbox" value="0" /> stiffness &lt; 30 min
                                <br /> 
                                <input id="answer2ab" name="question2" type="checkbox" value="1" /> improves with activity
                                <br /> 
                                <input id="answer2ab" name="question2" type="checkbox" value="0" /> worsen with activity
                                </div> 
                                </div> 
                                <br /> 
                                <div class="qheader"><h6> 3) Do you have deformity in your hands, feet or back?</h6>
                                </div> 
                                <div class="qselections"> 
                                <input name="question3" onclick="show4();" type="radio" value="3" />Yes 
                                <input name="question3" onclick="show3();" type="radio" value="0" />No 
                                <div class="collapse" id="div3"> 
                                <input class="question3" name="question3" type="checkbox" value="0" /> Deformity in hands
                                <br /> 
                                <input class="question3" name="question3" type="checkbox" value="0" /> Deformity in legs
                                <br /> 
                                <input class="question3" name="question3" type="checkbox" value="0" /> Deformity in back
                                </div> 
                                </div> 
                                <div class="qheader"><h6> a) Are you experiencing any sound (crepitus) on moving the joint?</h6>
                                </div> 
                                <div class="qselections"> 
                                <input name="YorN3a" type="radio" value="2" /> Yes 
                                <input name="YorN3a" type="radio" value="0" /> No
                                </div> 
                                <div class="qheader"><h6> b) Do you experiance excessive fatigue or feel abnormally feverish</h6>
                                </div> 
                                <div class="qselections"> 
                                <input name="YorN3b" type="radio" value="2" /> Yes 
                                <input name="YorN3b" type="radio" value="0" />No
                                </div> 
                                <br /> 
                                <div class="qheader"> <h6>4 a) Do you have any of the following symptoms?</h6>
                                </div> 
                                <input id="answer4a" name="question4" type="checkbox" value="1" /> Generalized body pain
                                <br /> 
                                <input id="answer4a" name="question4" type="checkbox" value="4" /> Difficulty in using upper or lower limb due to muscle weakness
                                <br /> 
                                <input id="answer4a" name="question4" type="checkbox" value="1" /> Red skin rash with raised borders over your face, scalp, neck or behind ears
                                <br /> 
                                <input id="answer4a" name="question4" type="checkbox" value="1" /> Flat facial skin rash over bridge of your nose, across cheeks which is non-itchy and scarring
                                <br /> 
                                <input id="answer4a" name="question4" type="checkbox" value="1" /> Sleep disturbance
                                <br /> 
                                <input id="answer4an" name="question4" type="checkbox" value="1" /> None
                                <br /> 
                                <div class="qheader"><h6> b) Do you have any problems below</h6>
                                </div> 
                                <div class="qselections"> 
                                <input id="answer4b" name="question4" type="checkbox" value="1" /> Unusually / excessively sensitive to sunlight
                                <br /> 
                                <input id="answer4b" name="question4" type="checkbox" value="1" /> Ulcers of mouth, nose or throat
                                <br /> 
                                <input id="answer4b" name="question4" type="checkbox" value="1" /> Having excessive hair loss in last three months
                                <br /> 
                                <input id="answer4b" name="question4" type="checkbox" value="3" /> Does your fingers and toes react to cold, turning blue
                                <br /> 
                                <input id="answer4bn" name="question4" type="checkbox" value="0" /> None
                                </div> 
                                <div class="qheader"><h6> c)Have you experienced?</h6>
                                </div> 
                                <input id="answer4c" name="question4" type="checkbox" value="2" /> Irrational or disturbed thought process
                                <br /> 
                                <input id="answer4c" name="question4" type="checkbox" value="1" /> Seizures
                                <br /> 
                                <input id="answer4c" name="question4" type="checkbox" value="1" /> Personality changes
                                <br /> 
                                <input id="answer4c" name="question4" type="checkbox" value="1" /> Anxiety
                                <br /> 
                                <input id="answer4cn" name="question4" type="checkbox" value="0" /> None
                                <br /> 
                                <div class="qheader"> <h6>d) Do you have inflammation of kidney or abnormal urine analysis showing protein/cellular caste in the urine?</h6>
                                </div> 
                                <input name="Yn4d" type="radio" value="3" />Yes 
                                <input name="Yn4d" type="radio" value="0" />No
                                <br /> 
                                <div class="qheader"> <h6>e) Whether the clinical/ findings show any of the following?</h6>
                                </div> 
                                <input class="4e" name="Yn4e" type="checkbox" value="4" /> Abnormal blood counts (leucopenia, thrombocytopenia, anemia)
                                <br /> 
                                <input class="4e" name="Yn4e" type="checkbox" value="6" /> Blood test positive for ds DNA, ANA, anti-Sm, anticardiolipin antibody, lupus anticoagulant, RO52, SSA or SSB
                                <br /> 
                                <input class="4en" name="Yn4e" type="checkbox" value="0" /> None
                                <br /> 
                                <div class="qheader"> 5) Do you have swelling in the glands around your face or neck?
                                </div> 
                                <div class="qselections"> 
                                <input name="question5" type="radio" value="3" />Yes 
                                <input name="question5" type="radio" value="1" />No
                                <br /> <h6>a) Do you have dental problems (i.e. dental decay, oral disease)</h6>
                                </div> 
                                <div class="qselections"> 
                                <input name="YorN5a" type="radio" value="2" /> Yes 
                                <input name="YorN5a" type="radio" value="0" /> No
                                </div> 
                                <div class="qheader"><h6> b) Have you experienced any of the following in the last three months?</h6>
                                </div> 
                                <input id="answer5" name="question5" type="checkbox" value="2" /> Gritting sensation in the eye
                                <br /> 
                                <input id="answer5" name="question5" type="checkbox" value="1" /> Redness of the eye
                                <br /> 
                                <input id="answer5" name="question5" type="checkbox" value="2" /> Use eye drops for dry eyes
                                <br /> 
                                <input id="answer5n" name="question5" type="checkbox" value="1" /> None
                                <br /> 
                                <div class="qheader"> 
                                <br /> <h6>6) Do you have one or frequent episodes of sudden onset of severe pain in big toe?</h6>
                                </div> 
                                <input name="YorN6a" type="radio" value="3" />Yes 
                                <input name="YorN6a" type="radio" value="0" />No
                                <br /> 
                                <div class="qheader"> <h6>a) Whether the uric acid value is</h6>
                                </div> 
                                <input name="YorN6b" type="radio" value="2" /> &gt;8 
                                <input name="YorN6b" type="radio" value="0" /> &lt;8
                                <br /> 
                                <br /> 
                                <div id="submit" style="text-align: center"> 
                                <input class="btn btn-primary" type="submit" value="Submit" />
                                </div> 
                                <h5> &nbsp;
                                </h5> 
                                <ul> Disclaimer: 
                                <li> This questionnaire is intended for informational purposes only and does not substitute medical advice or consultation.
                                </li> 
                                <li> Always consult your physician or other qualified health provider with any questions you may have regarding a medical condition.
                                </li> 
                                </ul> 
                                </form> 
                            </div>
                        </div>
                    </div>

                    <!-- button -->
                
                   

                    
    
    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css" rel="stylesheet" />
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script></p> 
 <script>/*<![CDATA[*/$(document).ready(function(){$("input.answer1").on("change",function(){$("input.answer1").not(this).prop("checked",false)});$("input.answer1a").on("change",function(){$("input.answer1a").not(this).prop("checked",false)});$("input.answer11").on("change",function(){$("input.answer11").not(this).prop("checked",false)});$("input.answer2").on("change",function(){$("input.answer2").not(this).prop("checked",false)});$("input.answer2a").on("change",function(){$("input.answer2a").not(this).prop("checked",false)});$("input#answer2aa").on("change",function(){$("input#answer2aa").not(this).prop("checked",false)});$("input#answer2ab").on("change",function(){$("input#answer2ab").not(this).prop("checked",false)});$("input#answer4n").on("change",function(){$("input#answer4").not(this).prop("checked",false)});$("input#answer4").on("change",function(){$("input#answer4n").not(this).prop("checked",false)});$("input#answer4an").on("change",function(){$("input#answer4a").not(this).prop("checked",false)});$("input#answer4a").on("change",function(){$("input#answer4an").not(this).prop("checked",false)});$("input#answer4bn").on("change",function(){$("input#answer4b").not(this).prop("checked",false)});$("input#answer4b").on("change",function(){$("input#answer4bn").not(this).prop("checked",false)});$("input#answer4cn").on("change",function(){$("input#answer4c").not(this).prop("checked",false)});$("input#answer4c").on("change",function(){$("input#answer4cn").not(this).prop("checked",false)});$("input.4en").on("change",function(){$("input.4e").not(this).prop("checked",false)});$("input.4e").on("change",function(){$("input.4en").not(this).prop("checked",false)});$("input#answer5n").on("change",function(){$("input#answer5").not(this).prop("checked",false)});$("input#answer5").on("change",function(){$("input#answer5n").not(this).prop("checked",false)});$(function(){$(".close_options").on("click",function(){document.getElementById("options").style.display="none";$(".answer2a, .answer2b").each(function(){this.checked=false})})});$(function(){$(".open_options").on("click",function(){document.getElementById("options").style.display="block"})})});function show1(){document.getElementById("div1").style.display="none";$('input[name="question1"]').each(function(){this.checked=false})}function show2(){document.getElementById("div1").style.display="block"}function show5(){document.getElementById("div2").style.display="none";$("#answer2aa,#answer2ab").each(function(){this.checked=false})}function show6(){document.getElementById("div2").style.display="block"}function show3(){document.getElementById("div3").style.display="none";$(".question3").each(function(){this.checked=false})}function show4(){document.getElementById("div3").style.display="block"}function show7(){document.getElementById("sides").style.display="none";$(".answer1a").each(function(){this.checked=false})}function show8(){document.getElementById("sides").style.display="block"}function validate(g){var f=0;for(var e=0;e<document.form1.question1.length;e++){if(document.form1.question1[e].checked){f+=parseInt(document.form1.question1[e].value)}}f+=parseInt(+(document.form1.Yn.value));f+=parseInt(+(document.form1.qu1.value));var d=0;for(var e=0;e<document.form1.question2.length;e++){if(document.form1.question2[e].checked){d+=parseInt(document.form1.question2[e].value)}}var c=0;for(var e=0;e<document.form1.question3.length;e++){if(document.form1.question3[e].checked){c+=parseInt(document.form1.question3[e].value)}}c+=parseInt(+(document.form1.YorN3a.value));c+=parseInt(+(document.form1.YorN3b.value));var b=0;for(var e=0;e<document.form1.question4.length;e++){if(document.form1.question4[e].checked){b+=parseInt(document.form1.question4[e].value)}}b+=parseInt(+(document.form1.Yn4d.value));b+=parseInt(+(document.form1.Yn4e.value));var a=0;for(var e=0;e<document.form1.question5.length;e++){if(document.form1.question5[e].checked){a+=parseInt(document.form1.question5[e].value)}}a+=parseInt(+(document.form1.YorN6a.value));a+=parseInt(+(document.form1.YorN6b.value));console.log(f);console.log(d);console.log(c);console.log(b);console.log(a);if(f==""){alert("select Answers to question 1")}else{if(d==""){alert("select Answers of question 2")}else{if(b==""){alert("select Answers of quesions number 4")}else{if(a==""){alert("select answers of question 5")}else{if(d>5||f>=6||c>=5||b>=7||a>=6){$.alert({title:"",content:"You require consultation with a rheumatologist.",type:"red",typeAnimated:true,useBootstrap:false,})}else{if(d==3||d==4||f>=4||f==4||c>=3||b>=5||a>=3){$.alert({title:"You may require consultation with a rheumatologist or assessment.",content:"You may probably have a rheumatologic disease to be confirmed with rheumatologist",type:"blue",typeAnimated:true,useBootstrap:false,})}else{$.alert({title:"Consult your family physician for further assessment",content:"You may not have rheumatologic disease kindly contact your physician for further assistance.",type:"green",typeAnimated:true,useBootstrap:false,})}}}}}}return false};/*]]>*/</script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>


    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->

</body>
</html>
