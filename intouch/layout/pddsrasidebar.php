 <div class="sidebar sidebar-left">
           <div class="profile-link"> 
                <a href="#" class="media">
                    <div class="w-auto h-100">
                        <figure class="avatar avatar-40"><img src="img/nopics.jpg" alt=""> </figure>
                    </div>
                    <div class="media-body">
                        <h6 class=" mb-0">CRICR<span class=""></span></h6>
                        <p>Bangalore</p>
                    </div>
                </a>
            </div>
           
            <div class="profile-link text-center">
                <a href="logout.php" class="btn btn-link text-white btn-block">Logout</a>
            </div>
        </div>