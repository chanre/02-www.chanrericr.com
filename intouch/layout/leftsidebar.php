 <div class="sidebar sidebar-left">
      
            <nav class="navbar">
                <ul class="navbar-nav">                    
                    <li class="nav-item">
                        <a href="#" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">star</i> Welcome
                            </div>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="index.php" class="item-link item-content" >
                            <div class="item-title">
                                <i class="material-icons">menu</i> Home
                            </div>
                        </a>
                   
                    </li>
                    
                    <li class="nav-item">
                        <a href="blog.php" class="item-link item-content">
                            <div class="item-title">
                                <i class="material-icons">library_books</i> Blogs
                            </div>
                        </a>
                       
                    </li>
                     <li class="nav-item dropdown">
                        <a href="javascript:void(0)" class="item-link item-content dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="item-title">
                                <i class="material-icons">person</i> Patients
                            </div>
                        </a>
                        <div class="dropdown-menu">
                            <a href="appointment.php" class="sidebar-close dropdown-item">Appointment Booking</a>
                            <a href="symptomchecker.php" class="sidebar-close dropdown-item">Symptom Checker</a>
                            <a href="pddsra.php" class="sidebar-close dropdown-item">PDDSRA</a>
                            <a href="sample_collection.php" class="sidebar-close dropdown-item">Sample Collection</a>
                        </div>
                    </li>


                    <li class="nav-item">
                        <a href="aboutus.php" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">domain</i> About
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="contactus.php" class="sidebar-close">
                            <div class="item-title">
                                <i class="material-icons">add_location</i> Contact Us
                            </div>
                        </a>
                    </li>
                </ul>
            </nav>
            
        </div>