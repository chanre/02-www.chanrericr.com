<?php include('blogconfig.php'); ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">
    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">
    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">
    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">
    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">
    <title>Intouch</title>
</head>
<body class="color-theme-blue push-content-right theme-light">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- sidebar left start -->
       <?php include('layout/leftsidebar.php'); ?>
        <!-- page main start -->
        <div class="page">
            <style type="text/css">
                iframe{
                    width: 100%;
                    min-height: 600px;
                    scroll-behavior: smooth;
                }
            </style>
            <!-- header -->
            <?php include('layout/header.php'); ?>
            <!-- header end -->
            <div class="page-content">
                <div class="content-sticky-footer">    
                   <!--  <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body">
                               <iframe src="https://mychanreclinic.com/#/authentication/signin"  title="Iframe Example"></iframe>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 mb-4">
                        
                               <iframe src="https://mychanreclinic.com/"  title="Iframe Example"></iframe>
                          
                    </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>
    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>
    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>
    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>
    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>
    <!-- page specific script -->
    <script>
        $(window).on('load', function() {
            /* sparklines */
            $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
                type: 'bar',
                height: '25',
                barSpacing: 2,
                barColor: '#a9d7fe',
                negBarColor: '#ef4055',
                zeroColor: '#ffffff'
            });

            /* gauge chart circular progress */
            $('.progress_profile1').circleProgress({
                fill: '#169cf1',
                lineCap: 'butt'
            });
            $('.progress_profile2').circleProgress({
                fill: '#f4465e',
                lineCap: 'butt'
            });
            $('.progress_profile4').circleProgress({
                fill: '#ffc000',
                lineCap: 'butt'
            });
            $('.progress_profile3').circleProgress({
                fill: '#00c473',
                lineCap: 'butt'
            });
            $('.progress_profile5').circleProgress({
                fill: '#ffffff',
                lineCap: 'butt'
            });

            /*Swiper carousel */
            var mySwiper = new Swiper('.swiper-container', {
                slidesPerView: 2,
                spaceBetween: 0,
                autoplay: {
                    delay: 1500,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
            /* tooltip */
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });

    </script>
     <script>
        'use strict'
        $(document).ready(function() {
            function introcarousel() {
                $('#intro').modal('show');
                setTimeout(function() {
                    /*Swiper carousel */
                    var mySwiper = new Swiper('.swiper-container', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        autoplay: {
                            delay: 2000,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        }
                    });
                }, 500);
            }

            introcarousel();

            $('#introbtn').on('click', function() {
                introcarousel();
                /*  $('#intro').modal('show');
                setTimeout(function() {
                    Swiper carousel 
                    var mySwiper = new Swiper('.swiper-container', {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        autoplay: {
                            delay: 2000,
                            disableOnInteraction: false,
                        },
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        }
                    });
                }, 500);*/


            });

        });

    </script>
</body>
</html>
