<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">


    <title>Contact Us</title>
</head>

<body class="color-theme-blue push-content-right theme-light">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- sidebar left start -->
      <?php include ('layout/leftsidebar.php'); ?>
        <!-- sidebar left ends -->

        <!-- sidebar right start -->
       
        <!-- sidebar right ends -->

        <!-- fullscreen menu start -->

       

        <!-- fullscreen menu ends -->

        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
           <?php include ('layout/header.php'); ?>
            <div class="page-content">
                <div class="content-sticky-footer">

                    <div class="row mx-0">
                        <div class="col-12 col-md-6">
                            <div class="card mb-4 box-shadow-large bg-light">
                                 
                                <div class="card-body text-center">
                                    <div align="center"><button class="btn btn-info text-center rounded sq-btn text-white"><i class="material-icons w-25px">mail</i></button></div> <br>

                                    <form class="text-center">
                                <label class="sr-only">Your Name</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="material-icons">person</i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Your name">
                                </div>
                                <br>
                                  <label class="sr-only">Contact</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="material-icons">call</i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Your Contact">
                                </div>
                                <br>
                                <label class="sr-only">Email address</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="material-icons">mail</i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Email address">
                                </div>
                                <br>
                                <label class="sr-only">Message</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="material-icons">chat</i></span>
                                    </div>
                                    <textarea rows="3" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <br>
                                <button class="btn btn-block btn-primary btn-lg rounded text-uppercase px-4">Submit</button>
                            </form>
                                </div>
                               
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-header"> 
                                <div align="center"><button class="btn btn-danger text-center rounded sq-btn text-white"><i class="material-icons w-25px">call</i></button></div> </div>
                            <div class="card-body" >
                                <p class="text-uppercase font-weight-bold text-center text-primary">Contact: +91 - 080 - 42516699</p>
                                <p class="font-weight-bold text-center text-primary">Email:  info@chanrericr.com</p>
                            </div>
                        </div>
                    </div>
                     <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-header"> 
                                <div align="center"><button class="btn btn-success text-center rounded sq-btn text-white"><i class="material-icons w-25px">home</i></button></div> </div>
                            <div class="card-body" >
                                <p class="text-uppercase font-weight-bold text-center text-primary">Address :
No. 65 (414), 20th Main,West of Chord road,1st Block, Rajajinagar, Bangalore-10</p>
                                
                            </div>
                        </div>
                    </div>
             
                  
                    
                </div>
                <div class="footer-wrapper shadow-15">
                    <div class="footer">
                       
                    </div>
                  
                </div>
            </div>
        </div>
        <!-- page main ends -->


        <!-- color chooser menu start -->
     
    </div>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->

</body>
</html>
