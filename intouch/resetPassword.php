<?php 
require 'config.php'; 
if (!isset($_GET['code'])) {
	exit("can't find the page"); 
}

$code = $_GET['code']; 
$getCodequery = mysqli_query($con, "SELECT * FROM resetpasswords WHERE code = '$code'"); 
if (mysqli_num_rows($getCodequery) == 0) {
	exit("can't find the page because not same code"); 
}

// handling the form 
if (isset($_POST['password'])) {
	$pw = $_POST['password']; 
	$pw=password_hash($pw, PASSWORD_DEFAULT); // not the best option but for demo simpilicity
	// $pw = md5($pw); // not the best option but for demo simpilicity
	$row = mysqli_fetch_array($getCodequery); 
	$email = $row['email']; 
	$query = mysqli_query($con, "UPDATE patientdiagram SET AdminPassword = '$pw' WHERE AdminEmailId = '$email'");

	if ($query) {
	 	$query = mysqli_query($con, "DELETE FROM resetpasswords WHERE code ='$code'"); 
	 	exit('Password updated'); 	
 	 }else {
 	 	exit('Something went wrong :('); 	
 	 } 	 
}


?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">


    <title>Forgot Password</title>

</head>
<body class="color-theme-blue">
      <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <div class="wrapper">
        <!-- page main start -->
        <div class="page">
            <div class="page-content h-100">
                <div class="background theme-header"><img src="img/city.jpg" alt=""></div>
                <div class="row mx-0 h-100 justify-content-center">
                    <div class="col-10 col-md-6 col-lg-4 my-3 mx-auto text-center align-self-center">
                        <a href="/"><img src="img/nopics.jpg" alt="" class="login-logo"></a>
                        <br>
                        <br>
                        <h5 class="text-white mb-4">Reset Password</h5>
                        <div class="login-input-content ">
                           
                                  <form method="post">
                                  <h6 align="center" class="text-danger">Please enter your registered email.</h6>
                                  <div class="form-group">
                                     <input type="password" name="password" class="form-control" placeholder="New password">
                                  </div>
                                  <div class="form-group" align="center">
                                    <input type="submit" name="submit" value="Update password">
                                  </div>
                              
                                  
                              </form>
                        </div>
                        <br>
                        <br>
                        <div class="row no-gutters">
                            <div class="col-12 text-center"><a href="pddsra.php" class="text-white mt-3">Already have password? Sign in Now!</a></div>
                        </div>                        
                    </div>
                </div>

                <br>

            </div>

        </div>
        <!-- page main ends -->

    </div>

<script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>
</body>
</html>