<?php 

session_start();
include('config.php');

if(isset($_POST['submit']))
{
     $AdminUserName=$_POST['AdminUserName'];
     $AdminPassword=$_POST['AdminPassword'];
     $AdminEmailId=$_POST['AdminEmailId'];
     $pid=$_POST['pid'];
     $status=0;
     $AdminPassword= password_hash($AdminPassword, PASSWORD_DEFAULT);
    $query=mysqli_query($con,"Insert into patientdiagram (AdminUserName,AdminEmailId,AdminPassword,Is_Active,pid) values('$AdminUserName','$AdminEmailId','$AdminPassword','$status','$pid')");

if($query){
$msg="Successfully Submitted..Wait for approval";
}
else
{
$msg="Couldn't process your request!!! Can you please try again..";
}
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">


    <title>PDDSRA</title>
</head>

<body class="color-theme-blue">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- page main start -->
        <div class="page">
            <div class="page-content h-100">
                <div class="background theme-header"><img src="img/city2.jpg" alt=""></div>
                <div class="row mx-0 h-100 justify-content-center">
                    <div class="col-10 col-md-6 col-lg-4 my-3 mx-auto text-center align-self-center">
                        <a href="index.php"><img src="img/nopics.jpg" alt="" class="login-logo"></a>
                        <br>
                        <h5 class="text-white mb-4">Register</h5>
                        <div class="login-input-content ">
                            <h6><?php echo "$msg"; ?></h6>
                            <form method="POST">
                            <div class="form-group">
                                <input type="text" name="AdminUserName" class="form-control rounded text-center" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control rounded text-center" name="pid" placeholder="Patient Id">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control rounded text-center" name="AdminEmailId" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control rounded text-center" name="AdminPassword" placeholder="Password">
                            </div>
                            
                            <button type="submit" name="submit"  class="btn btn-primary btn btn-block btn-success rounded border-0 z-3">Sign up</button></form>
                        </div>
                        <br>
                        <br>
                        <div class="row no-gutters">
                            <div class="col-6 text-left"><a href="requestPassword.php" class="text-white mt-3">Forgot Password?</a></div>

                        </div>                        
                    </div>
                </div>

                <br>

            </div>

        </div>
        <!-- page main ends -->

    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->

</body>
</html>
