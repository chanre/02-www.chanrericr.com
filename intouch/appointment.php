<?php 

include ('config.php');

if(isset($_POST['submit']))
{
$name = $_POST['name'];
$contact = $_POST['contact'];
$email = $_POST['email'];
$department= $_POST['department'];
$doctor =$_POST['doctor'];
$gender=$_POST['gender'];
$symptoms=$_POST['symptoms'];
$registerno=$_POST['registerno'];
$cdate = $_POST['cdate'];
$ctime = $_POST['ctime'];
$rdoctor =$_POST['rdoctor'];
$status=0;
$query = mysqli_query($con,"insert into test (name,contact,email,department,doctor,gender,symptoms,registerno,cdate,ctime,rdoctor,Is_Active)
 values('$name','$contact','$email','$department','$doctor','$gender','$symptoms','$registerno','$cdate','$ctime','$rdoctor','$status')");
if($query){
echo "<script> alert('Thank you for choosing chanre..We will get back to you in a while!!')</script>";
}
else
{
echo "<script> alert('Request could not proccess.. Please Try again!!')</script>";
}

}



 ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">


    <title>Intouch</title>
</head>

<body class="color-theme-blue push-content-right theme-light">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- sidebar left start -->
       <?php include('layout/leftsidebar.php'); ?>
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
            <!-- header -->
            <?php include('layout/header.php'); ?>
            <!-- header end -->
            <div class="page-content">
                <div class="content-sticky-footer">    
                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body" >
                                <p class="text-uppercase font-weight-bold text-center text-primary">Appointment Booking <br><small>For New Patients</small></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mb-4">
                        <div class="card">
                            <div class="card-body" >
                               <form  method="POST">
                 
                 <span>Are You already registered? <a href="rappointment.php" class="btn btn-primary btn-sm rounded float-right"><i class="material-icons w-25px">touch_app</i></a></span> <br><br>
            <div class="left-agileits-w3layouts same">
                 <div class="form-group">
                      <select class="form-control rounded text-center" name="appoint" required>
                          <option>Select Appointment type</option>
                          <option value="in_person">In Person</option>
                          <option value="video_consultancy">Video Consultancy</option>
                          
                      </select>
                 </div>
                <div class="form-group">
                    <input type="text" name="name" pattern="[a-z A-Z]+ [a-z A-Z]+" title="Enter your first and last name" placeholder="Name" id="name" required="" class="form-control rounded text-center" />
                </div>  
                <div class="form-group">  
                    <input type="text" name="contact" id="contact" pattern="[1-9]{1}[0-9]{9}" title="Enter 10 digit mobile number" placeholder="Mobile no" required="" class="form-control rounded text-center"/>
                </div>
                <div class="form-group">
                    <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" placeholder="Email" required="" class="form-control rounded text-center" />
                </div>
                <div class="form-group">
                    <select id="prov" name="department" class="form-control rounded text-center"></select>
                </div>
                <div class="form-group">
                    <select id="town" name="doctor" class="form-control rounded text-center"></select>
                </div>
                
                <!-- ============================================================================================================ -->
             
                      <div class="form-group">
                      <select class="form-control rounded text-center" name="gender">
                          <option>Select Gender</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                      </select>
                 </div>
                  
                <div class="form-group">
                        <textarea name="symptoms" placeholder="Please Write Symptoms.."  class="form-control rounded text-center" required></textarea>
                </div>
                <div class="form-group">
                        <input type="text" name="rdoctor" id="rdoctor" placeholder="Name of Refered Doctor" class="form-control rounded text-center">
                </div>
                
    
                 
             
                 <!-- ================================================ -->
                
<!-- ======================================================================================================================================================= -->
                 <div class="form-group">
                        <input placeholder="Select Date.."  name="cdate" type="date"  class="form-control rounded text-center" required>
                </div>
               
         
                 <div class="form-group">
                     <input placeholder="Select time.."  type="time" id="timepicker" name="ctime" class="form-control rounded text-center" required> 
                 </div>
                  

                  <div class="form-group" align="center">
                    <input type="submit" name="submit" onclick="CheckValidCaptcha()" value="Submit Request" class="btn btn-primary rounded">   
                 </div>
            </div>
                 
            </form>
                            </div>
                        </div>
                    </div> 
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->
<script type="text/javascript"> 

  var provs = {
          'Select Department':[''],
          'Rheumatology & Immunology': ['Select Doctor','Dr.Chandrashekara .S', 'Dr.Beenish Nazir','Dr. Shruthi Desai','Dr.Dhanashree','Dr. Yogitha'],
        'Allergy & Clinical Immunology':['Select Doctor','Dr. Smitha J N Singh',''],
        'Reproductive immunology & High-risk pregnancy':['Select Doctor','Dr.Chandrashekara .S','Dr. Chaitra S Niranthara'],
        'Paediatric Rheumatology':['Select Doctor','Dr.Chandrashekara .S'],
        'Physiotherapy and Chronic pain management':['Select Doctor','Mr. Sasikumar. M','Mr. Rajkannan. P','Ms.Yogarani'],
        'Ophthalmology':['Select Doctor','Dr. Usharani',''],
        'Radiology':['Select Doctor','Dr. Shivanand N D','Dr.Shivakumar S M'],
        'Cardiology':['Select Doctor','Dr. Anand Kumar M','Dr. Nagesh M B'],
        'Nephrology':['Select Doctor','Dr. Vinod Nagesh'],
        'Pulmonology':['Select Doctor','Dr. Ramesh R','Dr.Arjun A S'],
        'Vascular Surgeon':['Select Doctor','Dr. Chandrashekar A R'],
        'ENT':['Select Doctor','Dr. Tejmurthy B V'],
        'Psychiatrist':['Select Doctor','Dr. Chandrashekar M','Dr. Sushma','Dr.Adarsh B'],
        'Diabetology':['Select Doctor','Dr. Radha Rangarajan',''],
        'Gastroenterology':['Select Doctor','Dr. Abhijith B R',''],
        'General Surgery':['Select Doctor','Dr. Manojith S S','Dr. Nagendra K'],
        'Dental':['Select Doctor','Dr. Dilip Bharadwaj',''],
        'Dermatologist':['Select Doctor','Dr. Sushmitha E S',''],
        'Urology':['Select Doctor','Dr. Nagarajaiah Narayanaswamy',''],
        'Orthopaedics':['Select Doctor','Dr. Balasubramanyam','Dr. Venu Madhav','Dr.C.B Prabhu','Dr. Darshan Kumar  A Jain','Dr. Kodlady Surendar Shetty'],

                // 'Orthopedics':['Dr. Venu Madhav',''],
      },
      // just grab references to the two drop-downs
      prov_select = document.querySelector('#prov'),
      town_select = document.querySelector('#town');

  // populate the provinces drop-down
  setOptions(prov_select, Object.keys(provs));
  // populate the town drop-down
  setOptions(town_select, provs[prov_select.value]);
  
  // attach a change event listener to the provinces drop-down
  prov_select.addEventListener('change', function() {
    // get the towns in the selected province
    setOptions(town_select, provs[prov_select.value]);
  });
    
  function setOptions(dropDown, options) {
    // clear out any existing values
    dropDown.innerHTML = '';
    // insert the new options into the drop-down
    options.forEach(function(value) {
      dropDown.innerHTML += '<option name="' + value + '">' + value + '</option>';
    });
  }  
</script>



                  <script>
    function showDiv(prefix,chooser) 
    {
            for(var i=0;i<chooser.options.length;i++) 
            {
                var div = document.getElementById(prefix+chooser.options[i].value);
                div.style.display = 'none';
            }

            var selectedOption = (chooser.options[chooser.selectedIndex].value);

            if(selectedOption == "1")
            {
                displayDiv(prefix,"1");
            }
            if(selectedOption == "2")
            {
                displayDiv(prefix,"2");
            }
    }

    function displayDiv(prefix,suffix) 
    {
            var div = document.getElementById(prefix+suffix);
            div.style.display = 'block';
    }
</script>
<script type="text/javascript">
  $(document).ready(function() {
  $(document).on("input change paste", "#name","#rdoctor", function() {
    var newVal = $(this).val().replace(/[^a-zA-Z\s]/g, '');
    $(this).val(newVal);
  });
});
   $(document).ready(function() {
  $(document).on("input change paste","#rdoctor", function() {
    var newVal = $(this).val().replace(/[^a-zA-Z\s]/g, '');
    $(this).val(newVal);
  });
});

    $(document).ready(function() {
  $(document).on("input change paste","#contact", function() {
    var newVal = $(this).val().replace(/[^0-9\s]/g, '');
    $(this).val(newVal);
  });
});
</script>

 <script type="text/javascript">
                $(window).on('load', function() {
                    $('#exampleModal').modal('show');
                });
                $(window).on('click', function() {
                    $('#exampleModal').modal('hide');
                });
                </script>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Instructions</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p style="">

                                    1. This application will assist you in submitting the request for appointment.<br>
                                    2. Please select the doctor, department, and your preferred timings and date
                                    for consultation.<br>
                                    3. The appointment date of consultation is provided based on the availability
                                    and consultation timings. Preferred date and time is a guideline to help
                                    the front office and they will try to accommodate as far as possible.<br>
                                    4. Once the appointment request is submitted, ChanRe’s front office team
                                    will contact you within 24 hours, and based on the doctor's availability,
                                    the appointment will be confirmed.<br>
                                    5. While filling the details, please ensure that the mobile number and the
                                    email address are entered correctly.<br><br>

                                    <b>Note: Online video consultation facilities are available only to the registered
                                        patients.</b>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <!-- <button type="button"  class="btn btn-secondary" data-dismiss="#exampleModal">Close</button> -->
                                <button type="button" class="btn btn-primary">Okay</button>
                            </div>
                        </div>
                    </div>
                </div>
</body>
</html>
