
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">


    <title>About Us</title>
</head>

<body class="color-theme-blue push-content-right theme-light">
    <div class="wrapper">
        <!-- sidebar left start -->
        <?php include('layout/leftsidebar.php'); ?>
        <!-- sidebar left ends -->

        <!-- sidebar right start -->
    
        <!-- sidebar right ends -->

        <!-- fullscreen menu start -->

      

        <!-- fullscreen menu ends -->

        <!-- page main start -->
        <div class="page">
            <form class="searchcontrol">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button type="button" class="input-group-text close-search"><i class="material-icons">keyboard_backspace</i></button>
                    </div>
                    <input type="email" class="form-control border-0" placeholder="Search..." aria-label="Username">
                </div>
            </form>
           <?php include('layout/header.php'); ?>
            <div class="page-content">
                <div class="content-sticky-footer">
                      <!-- <div class="w-100">
                        <div class="carosel">
                            <div class="swiper-container swiper-init swipermultiple">
                                <div class="swiper-pagination"></div>
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block shadow-15 bg-white">
                                            <img src="img/product2.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block shadow-15 bg-white">
                                            <img src="img/product3.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="swiper-content-block shadow-15 bg-white">
                                            <img src="img/product4.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  </div> -->
                    <div class="block-title text-center">About us</div>
                    <h2 class="text-center mt-0 mb-4">ChanRe</h2>
                    <div class="row mx-0">
                        <div class="col text-center">
                            <p>CRICR was established on 12th Dec 2002 by Dr. Chandrashekara. S Rheumatologist. A unique one of its kind hospital in India, dedicated for management of patients suffering from Rheumatic Diseases (Musculoskeletal) and other Immunological diseases. It provides under one roof, a complete care to the patients suffering from Arthritis and other Immunological Diseases such as Immune-Deficiency Disorders, Allergic Disorders and Immuno-hematological disorders. This center is a tertiary reference facility.</p>
                        </div>
                        <div class="w-100">
                            <hr class="mb-4">
                        </div>
                        <div class="col">
                            <p>CRICR has created a newly established infrastructure with modern and improved additional facilities with the amenities to accommodate more associated specialties and sub-specialties for the benefit of patients with a vision to deliver the best in the field of immunology & rheumatology. The new facility is fully equipped with modern technologies and it is aimed at providing a comprehensive specialty approach with in-patient, emergency and cutting-edge research facilities.
                            </p>
                        </div>
                   <!--      <div class="col">
                             <div class="card w-100 mb-0">
                                <figure class="background rounded">
                                    <img src="img/city2.jpg" alt="" class="opacity-full">
                                </figure>
                                <div class="card-body text-center pb-1">
                                    <div class="row">
                                        <div class="col pr-0 text-left">
                                            <p class="text-uppercase font-weight-bold text-white">We the best</p>
                                        </div>
                                    </div>
                                    <div class="w-100">
                                        <i class="material-icons icon-3x text-white my-2">business</i>
                                        <div class="text-white  mt-3">That's Beautifully</div>
                                        <button class="btn btn-success rounded btn-sm my-3">Contact us</button>
                                    </div>
                                </div>
                           </div> 
                        </div>-->
                        <div class="w-100">
                            <hr class="mb-4">
                        </div>
                        <div class="col text-center">
                            <p>Specialists & Consultants
The center has added new features in the form of departments like Allergy, Clinical Immunology, Immuno-deficiency, integrated soft tissue and chronic pain management, joint preservation and restoration and has upgraded the existing departments like physiotherapy, orthotics, diet nutrition, obesity and lifestyle counselling, etc. <br>These departments include new consultants and supported by felt need investigational support with the addition of equipment like X-ray, soft tissue ultrasound, Pulmonary Function Test (PFT) and other equipment like EEG machine for Nerve Conduction Studies (NCS), Doppler and Diagnostic Laboratory with Home blood sample collection. <br>Apart from this CRICR has qualified Resident doctor Available 24hours for patient care and Emergency service supported by well-established Receptionist, Nursing staff, Paramedics, Housekeeping personnel.
The institute has strengthened its inpatient facility by the addition of needed emergency services, ICU with ventilator support and minor procedure room, etc.</p>
                        </div>
                    </div>
                    <br>
                </div>
              <!--     <div class="footer-wrapper shadow-15">
                    <div class="footer">
                        <div class="row mx-0">
                            <div class="col">
                                Overux
                            </div>
                            <div class="col-7 text-right">
                                <a href="#" class="social"><img src="img/facebook.png" alt=""></a>
                                <a href="#" class="social"><img src="img/googleplus.png" alt=""></a>
                                <a href="#" class="social"><img src="img/linkedin.png" alt=""></a>
                                <a href="#" class="social"><img src="img/twitter.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="footer dark">
                        <div class="row mx-0">
                            <div class="col  text-center">
                                Copyright @2018, Maxartkiller
                            </div>
                        </div>
                    </div>
              </div> -->
            </div>
        </div>
        <!-- page main ends -->
        <!-- color chooser menu start -->
       
        <!-- color chooser menu ends -->

    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->
    <script>
        $(window).on('load', function() {
            /* sparklines */
            $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
                type: 'bar',
                height: '25',
                barSpacing: 2,
                barColor: '#a9d7fe',
                negBarColor: '#ef4055',
                zeroColor: '#ffffff'
            });

            /*Swiper carousel */
            var mySwiper = new Swiper('.swiper-container', {
                slidesPerView: 1,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
            /* tooltip */
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            });
        });

    </script>
</body>

</html>
