<?php
 session_start();
//Database Configuration File
include('config.php');
//error_reporting(0);


if(isset($_POST['login']))
  {
    $uname=$_POST['username'];
    $password=$_POST['password'];

$sql =mysqli_query($con,"SELECT pid,AdminUserName,AdminEmailId,AdminPassword FROM patientdiagram WHERE (pid='$uname' || AdminEmailId='$uname') and Is_Active=1");
$num=mysqli_fetch_array($sql);
 

if($num>0)
{
$hashpassword=$num['AdminPassword'];
if (password_verify($password, $hashpassword)) {
$_SESSION['login']=$_POST['username'];
    echo "<script type='text/javascript'> document.location = 'pddsra-form.php'; </script>";
  } else {
echo "<script>alert('Wrong Password');</script>";
 
  }
}
else{
echo "<script>alert('User not registered with us');</script>";
  }
 
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no, viewport-fit=cover">

    <link rel="apple-touch-icon" href="img/f7-icon-square.html">
    <link rel="icon" href="img/f7-icon.html">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="vendor/bootstrap-4.1.3/css/bootstrap.min.css">

    <!-- Material design icons CSS -->
    <link rel="stylesheet" href="vendor/materializeicon/material-icons.css">

    <!-- swiper carousel CSS -->
    <link rel="stylesheet" href="vendor/swiper/css/swiper.min.css">

    <!-- app CSS -->
    <link id="theme" rel="stylesheet" href="css/style.css" type="text/css">
    <style type="text/css">
        
        ul li{
            color: white;
            text-align:left;
            line-height: 1em;
        }
        .background{height: 100%;}
    </style>

    <title>PDDSRA</title>
</head>

<body class="color-theme-blue">
    <div class="loader justify-content-center ">
        <div class="maxui-roller align-self-center">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="wrapper">
        <!-- page main start -->
        <div class="page">
            <div class="page-content h-100">
                <div class="background theme-header"><img src="img/city2.jpg" alt=""></div>
                <div class="row mx-0 h-100 justify-content-center">
                    <div class="col-10 col-md-6 col-lg-4 my-3 mx-auto text-center align-self-center">
                        <!-- <img src="img/nopics.jpg" alt="" class="login-logo"> -->
                        <h5 class="login-title">PDDSRA Login</h5><br>
                        <form method="POST">
                        <div class="login-input-content ">
                            <div class="form-group">
                                <input class="form-control rounded text-center" type="text" required="" name="username" placeholder="Enter Patient Id" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input class="form-control rounded text-center" type="password" name="password" required="" placeholder="Enter Password" autocomplete="off">
                            </div>
                            <button class="btn btn-block btn-success rounded border-0 z-3"  type="submit" name="login">Login</button>
                        </div></form><br>
                         <div class="row" >
                            <ul>
                                <li>This application helps the rheumatologist to understand the current RA status and severity. Answer all the questions and provide the exact information.</li>
                                <li>Avoid skipp­ing any questions.</li>
                                <li>After completing the submission, kindly press the submit button and wait till the sending process is completed. </li>
                                <li>If the sending fails, you may have to refill the same after connecting to the network, as it involves data transfer. This need to be done once a week initially and then on once a month.</li>
                            </ul>
                            
                        </div>   
                        <div class="row no-gutters">
                            <div class="col-6 text-left"><a href="requestReset.php" class="text-white mt-3">Forgot Password?</a></div>
                            <div class="col-6 text-right"><a href="pddsra_registration.php" class="text-white text-center mt-3">Sign up</a></div>
                        </div> 

                    </div>
                </div>

                <br>

            </div>

        </div>
        <!-- page main ends -->

    </div>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1.3/js/bootstrap.min.js"></script>

    <!-- Cookie jquery file -->
    <script src="vendor/cookie/jquery.cookie.js"></script>

    <!-- sparklines chart jquery file -->
    <script src="vendor/sparklines/jquery.sparkline.min.js"></script>

    <!-- Circular progress gauge jquery file -->
    <script src="vendor/circle-progress/circle-progress.min.js"></script>

    <!-- Swiper carousel jquery file -->
    <script src="vendor/swiper/js/swiper.min.js"></script>

    <!-- Application main common jquery file -->
    <script src="js/main.js"></script>

    <!-- page specific script -->

</body>
</html>
