<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
  <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
<script data-ad-client="ca-pub-4264858417362171" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
	   <style>
	   table{
		   text-align:center;
		   width:100%;
		   border:2px solid red;
	   }
	   .mytext{
		   font-family:georgia;
		  padding:50px 10px 50px 10px;
		   background: #ccebff;
	   }
	   h2{
		   font-weight:bold;
	   }
	   h3,b{
		   color:red;
	   }
	   </style>
         <div class="container">
		 <div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 mytext">
			<div align="center">
			<h2>Upcoming Scientific Event</h2>
			<h3>ChanRe Update 2020</h3>
			<h3>Virtual Online CME</h3>
			<table border="2px">
			<tr>
			<th>Days</th>
			<th>Theme</th>
			<th>Date</th>
			<th>Time</th>
			</tr>
			<tr>
			<td>Day 1</td>
			<td><b>Reproductive Immunology</b></td>
			<td>19/12/2020</td>
			<td>5:30 PM - 8:00 PM</td>
			</tr>
			<tr>
			<td>Day 2</td>
			<td><b>Infections and Rheumatology</b></td>
			<td>20/12/2020</td>
			<td>11:00 AM - 1:00 PM</td>
			</tr>
			</table>
			<p>
			<h2>Registration Closed</h2>
			<a href="chanreupdateregister.php" class="btn btn-primary" hidden>Click Here to Register</a>
			</p>
			</div>
			<div align="center">
			<h3>Organizing Secretary</h3>
            <h4>Dr. Chandrashekara. S</h4>
           <b>ChanRe Rheumatology & Immunology Center & Research</b>
			</div>
			<div align="center">
		<p style="text-align:center;width:80%;">	#414/65, 20th Main, West of Chord Road,
        1st Block, Rajajinagar, Bengaluru – 560010 Ph: 080 42516699 | Fax: 080 42516600 Email: info@chanrericr.com</p> 
		Website: www.chanrericr.com , www.mychanreclinic.com</div>
			</div>
			<div class="col-md-2"></div>
		 </div>
		 </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->
    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>