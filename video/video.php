<?php 
include('config.php');

?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="../images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="../css/style.css" />
       <link rel="stylesheet" href="../css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css" />
  <style type="text/css">
  .card{
  	width: 100%;
  	border-radius: 16px;
background: #7ac6ff;

             margin: 5px;
  }
  .lightbox-gallery {
    background-image: linear-gradient(#4A148C, #E53935);
    background-repeat: no-repeat;
    color: #000;
    overflow-x: hidden

}
.lightbox-gallery p {
    color: #000;
}
.lightbox-gallery h2 {
    font-weight: bold;
    margin-bottom: 40px;
    padding-top: 40px;
    color: #fff;
}
@media (max-width:767px) {
    .lightbox-gallery h2 {
        margin-bottom: 25px;
        padding-top: 25px;
        font-size: 24px
    }
}
.lightbox-gallery .intro {
    font-size: 16px;
    max-width: 500px;
    margin: 0 auto 40px
}
.lightbox-gallery .intro p {
    margin-bottom: 0
}

.lightbox-gallery .photos {
    padding-bottom: 20px
}
.lightbox-gallery .item {
    padding-bottom: 30px
}  
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  height: 300px;
}
/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}
/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}
/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}
/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 70%;
  border-left: none;
  height: 300px;
}
.carousel-item img{height: 20%;width: 100%;}
  </style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
          <div class="container">
    <h3 align="center"></h3>
  <hr>
  <script>
function getSubCat(val) {
  $.ajax({
  type: "POST",
  url: "category.php",
  data:'catid='+val,
  success: function(data){
    $("#subcategory").html(data);
  }
  });
  }

  function getCont(val) {
  $.ajax({
  type: "POST",
  url: "category.php",
  data:'contid='+val,
  success: function(data){
    $("#content").html(data);
  }
  });
    $("#abc").hide(1000);
  }
  </script> 
  <div class="row">
    <div class="col-md-2 mb-3">
        <form>
           <div class="form-group" >
                        <label>Language</label>
                        <select class="form-control" id="language" name="language" onChange="getSubCat(this.value);">
                          <option>Select Language</option>
                          <?php
                            $ret=mysqli_query($con,"select * from tbl_lang");
                            while($result=mysqli_fetch_array($ret))
                            {    
                            ?>
                            <option value="<?php echo htmlentities($result['id']);?>"><?php echo htmlentities($result['language']);?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <div class="form-group" >
                        <label>Category</label>
                        <select class="form-control" id="subcategory" name="category" onChange="getCont(this.value);">
                          
                        </select>
                      </div>
        </form>
    </div>
    <!-- /.col-md-4 -->
        <div class="col-md-10">
          <div class="lightbox-gallery">
            <div class="container">
              <div class="row" id="content">
                
              </div>
              <div class="row" id="abc">
                <?php $get_vid=mysqli_query($con,"Select * from tbl_video limit 6");
               while($rows=mysqli_fetch_array($get_vid)){ ?>
                <div class="col-md-4" >
                 <div class="card" style="width: 17rem;height: 400px;" >
                    <a href="videofile.php?vid=<?php echo htmlentities($rows['id']);?>"><img class="card-img-top" src="layout/play.jpg" alt="Card image cap"></a>
                    <div class="card-body">
                      <h6 class="card-title"><?php echo htmlentities($rows['title']); ?></h6>
                      <p class="card-text"><?php echo htmlentities($rows['shortdesc']); ?></p>
                     </div>
                     <div class="card-footer">
                      <div class="" align="right">
                        <a href="videofile.php?vid=<?php echo htmlentities($rows['id']);?>" class="btn btn-success btn-block btn-lg btn-round">View</a>
                      </div>
                     </div>
                    </div>
                  </div>
            <?php } ?> 
           
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- /.col-md-8 -->
  </div>
</div>
          <br>
          
 
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->
    <!-- jquery latest version -->
    <script src="../js/jquery.min.js"></script>
    <!-- popper.min js -->
    <script src="../js/popper.min.js"></script>
    <!-- bootstrap.min js -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- ie10 viewport bug-workaround js -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>
    <!-- custom js -->
    <script src="../js/custom.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
</body>
</html>