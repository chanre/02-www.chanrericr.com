<?php include ('config.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Video file</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
 
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="../css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="../css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/../css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="../css/style.css" />
       <link rel="stylesheet" href="../css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css" />
     <style type="text/css">
       body{
        background-color: #e7eaf7;
       }

form{
  width:100%;
  display:inline-block;
  
  padding:10px 30px;
  text-decoration:none;
  margin-top: 20px; 
  font-weight:500;
 
  
  box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}

.qheader{
  font-weight: bold;
  margin: 5px;
  font-size: 16px;
}

    </style>


</head>
<body>
   <?php include ('layout/header.php'); ?>
<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
<?php 
$pid = intval($_GET['vid']); 
$query=mysqli_query($con, "select * from tbl_video where id = $pid ");
while($rows= mysqli_fetch_array($query))
{
 ?>
 <?php $videocode=$rows['videoid'] ?>
    	<div class="intro">
      <h2 class="text-center"><?php echo htmlentities($rows['title']); ?></h2>
    </div>
    
    	 <div id="player"></div>
 <div class=""> 
 <?php $pt=$rows['longdesc']; 
 echo  (substr($pt,0));?>            
 </div>

<?php } ?>
    </div>
    <div class="col-md-2"></div>
  </div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
      // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      var tb = mirGOiSLHYE;
      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '390',
          width: '640',
          videoId: '<?php echo $videocode; ?>',
          playerVars: {
            'playsinline': 1
          },
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
          setTimeout(stopVideo, 6000);
          done = true;
        }
      }
      function stopVideo() {
        player.stopVideo();
      }
    </script>
 
</body>
</html>