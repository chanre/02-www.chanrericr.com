<?php include ('config.php'); 

$did = intval($_GET['del']);
$query=mysqli_query($con,"DELETE FROM tbl_video WHERE id = $did");
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="icon" type="image/png" href="../images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="../css/style.css" />
       <link rel="stylesheet" href="../css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css" />

  <style type="text/css">
      <style>
.vertical-menu {
  width: 200px;
   height: 100%;
   background-color: #eee;
}

.vertical-menu a {

  
  color: black;
  display: block;
  padding: 12px;
  text-decoration: none;
}

.vertical-menu a:hover {
  background-color:  #04AA6D;
}

form{
  width: 80%;
  margin-left: auto;
  margin-right: auto;
}
</style>
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
             <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <script>
function getSubCat(val) {
  $.ajax({
  type: "POST",
  url: "category.php",
  data:'catid='+val,
  success: function(data){
    $("#subcategory").html(data);
  }
  });
  }
  </script> 
             <div class="row">
                 <div class="col-md-3">
                    <?php include ('layout/vertical.php'); ?>
                 </div>
                 <div class="col-md-8">
                    <table border="1">
                      <thead>
                        <tr>
                          <th>Video id</th>
                          <th>Title</th>
                          <th>Speaker</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $query=mysqli_query($con,"select * from tbl_video ");

                        while($rows=mysqli_fetch_array($query))
                        {
                         ?>
                       
                        <tr>
                          <td><?php echo htmlentities($rows['videoid']); ?></td>
                          <td><?php echo htmlentities($rows['title']); ?></td>
                          <td><?php echo htmlentities($rows['sname']); ?></td>
                          <td><a href="edit-video.php?vid=<?php echo htmlentities($rows['id']); ?>">Edit</a>|
                            <a href="view-video.php?del=<?php echo htmlentities($rows['id']); ?>">Delete</a></td>
                         
                        </tr>
                        <?php  } ?>
                      </tbody>
                    </table>
                 </div>
               
             </div>
             </div>
         </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

 
    <!-- jquery latest version -->
    <!-- <script src="../js/jquery.min.js"></script> -->
    
    <!-- popper.min js -->
    <script src="../js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="../js/custom.js"></script>
 <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script>
            jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 240,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
                // Select2
                $(".select2").select2();

                $(".select2-limiting").select2({
                    maximumSelectionLength: 2
                });
            });
        </script>
</body>

</html>