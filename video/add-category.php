<?php include ('config.php');
session_start();

error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:admin.php');
}

else{


 ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="icon" type="image/png" href="../images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="../css/style.css" />
       <link rel="stylesheet" href="../css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="../css/responsive.css" />

  <style type="text/css">
      <style>
.vertical-menu {
  width: 200px;
   height: 100%;
   background-color: #eee;
}

.vertical-menu a {

  
  color: black;
  display: block;
  padding: 12px;
  text-decoration: none;
}

.vertical-menu a:hover {
  background-color:  #04AA6D;
}

form{
  width: 80%;
  margin-left: auto;
  margin-right: auto;
}
</style>
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
           
             <div class="row">
                 <div class="col-md-3">
                    <?php include ('layout/vertical.php'); ?>
                 </div>
                 <div class="col-md-6">
                    <form  id="fupForm" name="form1" method="post" action="category.php">
                     
                      <div class="text-center bg-info text-warning">
                        <h2>Add Category</h2>
                      </div>
                       <div class="form-group" >
                        <label>Language</label>
                        <select class="form-control" name="lang_id" id="lang_id">
                          <option>Please Select language</option>
                          <?php
// Feching active categories
                            $ret=mysqli_query($con,"select id,language from  tbl_lang");
                            while($result=mysqli_fetch_array($ret))
                            {    
                            ?>
                            <option value="<?php echo htmlentities($result['id']);?>"><?php echo htmlentities($result['language']);?></option>
                            <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Category</label>
                        <input type="text" id="category" name="category" class="form-control" placeholder="Please enter the title">
                      </div>
                      <div class="form-group">
                        <div align="center">
                          <button type="submit" id="butsave" name="save" class="btn btn-primary">Add Category</button>
                        </div>
                      </div>
                    </form>
                 </div>
                 <div class="col-md-2">
                   <table class="table table-bordered table-sm" >
                      <thead>
                        <tr>
                          <th>Sl no</th>
                          <th>Category</th>
                          
                        </tr>
                      </thead>
                      <tbody id="table">
                        <?php $rt=mysqli_query($con,"Select * from tbl_cat");
                        while($rows=mysqli_fetch_array($rt))
                        {
                         ?>
                        <tr>
                          <td><?php echo htmlentities($rows['lang_id']); ?></td>
                          <td><?php echo htmlentities($rows['cat_name']); ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                 </div>
               
             </div>
             </div>
         </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="../js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="../js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="../js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="../js/custom.js"></script>

</body>

</html>
<?php } ?>