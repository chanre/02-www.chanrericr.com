
 <header id="top" class="top_navbar">
            <div class="container-fluid">
                <nav class="navbar  navbar-expand-lg navbar-light ">
                    <a class="navbar-brand" href="../index.html"><img src="../images/chanrelogo.png" alt="" width="250" /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon">
                        <i class="fa fa-bars"></i>
                      </span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link active" href="../index.html">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../clinic.php">Clinics</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../doctor.php">Doctors</a>
                            </li>
                              <li class="nav-item">
                                <a class="nav-link" href="../about_us.php">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../services.php">Services</a>
                            </li>                           
                            <li class="nav-item">
                                <a class="nav-link" href="https://chanrericr.com/patient-corner.php">Patient Corner</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="../news.php">News & Events</a>
                            </li>
													
                            <li class="nav-item">
                                <a class="nav-link" href="../contact.php">Contact</a>
                            </li>						
                             <li class="nav-item" style="color:white;text-color">
                                <a class="nav-link btn btn-warning" href="../appointment1.php">Appointment</a>
                            </li>							
                       </ul>                     
                    </div>
                </nav>
            </div>
        </header>
		
		<style>
		p{
			text-align:justify;
		}
		</style>