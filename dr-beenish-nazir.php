<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
   <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
   <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr.Beenish Nazir</h1></div><br>
             <div class="row">
                 <div class="col-md-3">  <img src="images/doctor/beenish.jpg" class="card-img" alt="..." height="230"></div>
                 <div class="col-md-9">

                     <p>
                         <strong>MBBS,DNB,PGDR Dr.Beenish Nazir is a promising Physician and Rheumatologist. She did her MBBS from GMC(Sgr) and worked in many prestigious Institutions in India,UK and Saudi Arabia. She has completed her Post Graduation (DNB) from Shalby hospital Chandigarh under guidance of Dr.R J Dash.
                     </p>
                     <p>
                        She started working at St.Martha's hospital, and did Post Graduate diploma in Rheumatology from John Hopkins Medical center, USA. She also did fellowship in Rheumatology from Guys and Thomas hospital in London under Dr.David D Cruz. She is well versed in managing diverse medical conditions. Her special interest lies in treating Rheumatoid Arthritis,SLE,Vasculitis,various Spondyloarthropathies and Psoriasis.
                     </p>
                     <p>
                        She is an excellent clinician and devotes lot of time in understanding the patient’s disease and advises treatment as per the latest protocol.
                     </p>
                    
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>