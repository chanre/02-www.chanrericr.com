<?php 

session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:admin.php');
}

else{
  if(isset($_POST['submit']))
  {
    $title=$_POST['title'];
    $PostDetails=$_POST['PostDetails'];
    $VideoId=$_POST['VideoId'];
    $speaker=$_POST['speaker'];
    $category=$_POST['category'];
    $query = mysqli_query($con,"Insert into tblposts (title,PostDetails,VideoId,speaker,category)
      values('$title','$PostDetails','$VideoId','$speaker','$category')");
    if($query){echo '<script>alert("Success")</script>';}else{echo '<script>alert("Failed")</script>';}

  }
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style>
th{
	background-color:#53a6d5;
	color:white;
}
form{
border-radius: 0px;
background: linear-gradient(145deg, #d3eafe, #b1c5d5);
box-shadow:  5px 5px 5px #b5c9da,
             -5px -5px 5px #d5edff;

margin-bottom: 20px;
padding: 30px;
}
table{
  border-radius: 0px;
background: linear-gradient(145deg, #d3eafe, #b1c5d5);
box-shadow:  5px 5px 5px #b5c9da,
             -5px -5px 5px #d5edff;
             padding: 10px;
}
tr td{
  padding: 10px;
  text-align: center;
}
tr th{
  text-align: center;
  font-size: 18px;
  font-weight: 600;
  font-family: times;
}
</style>
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <header id="top" class="top_navbar">
            <div class="container">
                <nav class="navbar  navbar-expand-lg navbar-light ">
                    <a class="navbar-brand" href="/admindashboard.php"><img src="images/chanrelogo.png" alt="" width="250" /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon">
                        <i class="fa fa-bars"></i>
                      </span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                          <ul class="navbar-nav"> 
                            <li class="nav-item">
                                <a class="nav-link" href="admindashboard.php">Main Dashboard</a>
                            </li>
                           
                             <li class="nav-item">
                                <a class="nav-link" href="sample-collected.php">Sample Collected</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="pddsraadmin.php">PDDSRA</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="logout.php">Logout</a>
                            </li>
                        </ul>
                  
                    </div>
                </nav>
            </div>
        </header>

        <div class="container">
          <br><br>
            <div class="row">
                
                <div class="col-md-6">
                     <form method="post">
                       <div class="form-group">
                         <label>Video Title</label>
                         <input type="text" name="title" class="form-control" placeholder="Enter the video title">
                       </div>
                       <div class="form-group">
                         <label>Description</label>
                         <textarea name="PostDetails" class="form-control"></textarea> 
                       </div>
                       <div class="form-group">
                         <label>Video Id</label>
                         <input type="text" name="VideoId" class="form-control" placeholder="Enter the video id">
                       </div>
                       <div class="form-group">
                         <label>Speaker</label>
                         <input type="text" name="speaker" class="form-control" placeholder="Enter speaker name">
                       </div>
                       <div class="form-group">
                         <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                       </div>
                     </form>
                </div>
                <div class="col-md-6">
                     <table border="2">
                       <tr>
                         <th>Title</th>
                         <th>Speaker</th>
                         <th>Category</th>
                       </tr>
                       <?php $fetch=mysqli_query($con,'select * from tblposts');
                       while($rows=mysqli_fetch_array($fetch)){
                        ?>
                       <tr>
                         <td><?php echo htmlentities($rows['title']); ?></td>
                         <td><?php echo htmlentities($rows['speaker']); ?></td>
                         <td><?php echo htmlentities($rows['category']); ?></td>
                       </tr>
                     <?php } ?>
                     </table>
                </div>
            </div>
        </div>
         
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>
<?php } ?>