<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
     <style type="text/css">
         p{
                font-family: times;
                font-weight:;
                font-size: 18px;

         }
         .image{
            padding: 10px;
            overflow: hidden;
         }
         .simg img{
            width: 100%;
            margin: auto;
         }
         ul>li a{
            color: blue;
            text-decoration: none;
            font-size: 18px;
         }
         td{
            padding: 10px;
         }
     </style>



</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
    <?php include ('layout/header.php'); ?>
    <div class="container">
        <br>
        <h1 class="Rheumatology" align="center">Research & Development</h1>
        <div class="row">
            
            <div class="col-md-10 offset-1"><br><br>
                <table>
                    <tr><th><h3>R&D Director</h3></th></tr>
                    <tr><td><h5>Dr. S. Chandrashekara (MBBS, MD, DNB, DM)</h5></td></tr>
                    <tr></tr>
                    <tr><th><h3>Additional R&D Director</h3></th></tr>

                    <tr><td><h5>Dr. P. Renuka (MBBS, DCP, DNB)</h5></td></tr>
                </table><br><br>
                <p>
                    Established on 12 December 2002, ChanRe Rheumatology & Immunology Center & Research is a unique kind of hospital in India dedicated for management of patients suffering from rheumatic and other immunological diseases. It is a renowned centre  for excellence in autoimmune disease research and has the honor of being a partner of India AMR  Innovation Hub and a key member in the National Consortium for Research on Autoimmune Diseases that of NATRAD alliance (National Alliance for Translational Research in Autoimmune Diseases) under DBT, Govt. of India.
                </p>
                <p>
                    We have established a research facility in our medical center with special focus in auto-immune diseases in broader area like basic, diagnostic, biomodelling, clinical, translational, and therapeutic, with an emphasis on innovation, advancing technologies, early diagnosis and better therapeutics for autoimmune diseases. We focus on data collection from patient charts, sample collection and processing, retrospective and prospective studies. We have been working on these lines for over 20 years now and recently we have started venturing into research on Healthcare in Artificial Intelligence (AI). To add, we are a non-commercial purely research-based R&D to support scientific advancements and student, faculty training centre for development of research personnel.
                </p>
                <p>
                    Our R&D is presently a team of 21 members and in the future, we envisage to expand our R&D program and enter the mainstream of national research programs strengthening the research outcomes in research on autoimmune diseases, artificial intelligence aiding patient healthcare and better decision making on patient diseases.
                </p>
                <h3>Research Advisory Board</h3>
                <br>
                <table border="2">
<tbody>
<tr class="text-center">
<th>Sl. No.</th>
<th>Name</th>
<th>Affiliation</th>
</tr>
<tr>
<td>1</td>
<td>Dr. Srinivas V. Kaveri</td>
<td>Director, CNRS Office in India,  Embassy of France,
New Delhi
</td>
</tr>
<tr>
<td>2</td>
<td>Dr. K.M. Mahendranath</td>
<td>Senior Consultant Rheumatologist, Samarpan Health Centre,
Arthritis & Rheumatology Clinic,
Bengaluru
</td>
</tr>
<tr>
<td>3</td>
<td>Dr. Ramnath Misra</td>
<td>Professor & Head,
Clinical Immunology, 
Kalinga Institute of Medical Sciences (KIMS),
Bhubaneswar
</td>
</tr>
<tr>
<td>4</td>
<td>Prof. Dr. Jyotirmay Biswas</td>
<td>Director,
Uveitis & Ocular Pathology Department,
Sankara Nethralaya,
Chennai
</td>
</tr>
<tr>
<td>5</td>
<td>Dr. C. Renuka</td>
<td>Additional Director – R&D,
ChanRe Rheumatology & Immunology Center & Research,
Bengaluru
</td>
</tr>
<tr>
<td>6</td>
<td>Prof. Dr. S. Chandrashekara</td>
<td>Director – R&D,
ChanRe Rheumatology & Immunology Center & Research,
Bengaluru
</td>
</tr>
</tbody>
</table>




                <br><br>
                <div align="center">
                    <h3>Our R&D collaborations</h3>
                </div>
                
                <p>
                    <div class="simg">
                        <img src="img/Picture1.png" alt="R&D" >
                    </div>
                    
                
                    <br><br>
                </p>
                <h3>Clinical research</h3>
                <p>We have a full-fledged clinical research facility that works on conducting clinical trials. We have successfully completed several projects in the past, all of which are enlisted in the Clinical Trials Registry of India (CTRI) and have some in the pipeline as well. Few of the organizations with whom we have associated in the past</p>

                <p>
                <br><br>
                    <div class="simg">
                        <img src="img/Picture2.png" alt="R&D" >
                    </div>
                    <br><br>
                </p>
<h3>Ongoing projects</h3>
<p>
1.  Inflammatory bio-markers in Diabetes Mellitus - can they predict microvascular complications <br>
2.  Extraction and analysis of clinically significant features of lower back pain (LBP) from radiography images and IR Thermography images using image processing techniques<br>
3.  Quantification of sialylations and glycosylation of Immunoglobulin G: Can it predict disease activity and stable remission in Rheumatoid Arthritis and SLE?<br>
4.  Epidemiological studies and registry development of rheumatic diseases across India.<br>
5.  Determinants of Predictors for successful withdrawal of DMARDS among patients of Rheumatoid Arthritis in sustained Remission.<br>
6.  Analysis of association of autoantibodies with different Connective Tissue Disorders, their clinical co-relation and their long-term outcome<br>
7.  Risk score system in differentiating infection vs Disease flare in Auto-immune diseases<br>
8.  Clustering of SLE in predicting the outcome and personalizing the treatment.<br></p>
<!-- <h3>Future projects</h3> -->
<p><br><br>
    <h3>Future projects</h3>
<div class="simg">
                        <img src="img/Picture3.png" alt="R&D" >
                    </div><br><br>
</p>
<h3>Selected publications</h3>
<p>
1.  A Pareek, S Chandrashekara, RT Mehta. Hydroxychloroquine retinopathy—less than meets the eye. Eye. 2021. 35(10). 2897-2897. <br>
2.  A Patil, K Chanakya, P Shenoy, S Chandrashekara, at. al.    A Prospective Longitudinal Study Evaluating The Influence of Immunosuppressives and Other Factors On COVID-19 in Autoimmune Rheumatic Diseases. 2021. DOI: https://doi.org/10.21203/rs.3.rs-805748/v1<br>
3.  KP Suresh, SS Patil, BPC Thyagaraju, et. al. Prediction of daily and cumulative cases for COVID-19 infection based on reproductive number (R 0) in Karnataka: a data-driven analytics. Scientific Reports. 2021. 11(1). 1-6. <br>
4.  Chopra A, Shobha V, Chandrashekara S, et. al. Tofacitinib in the treatment of Indian patients with rheumatoid arthritis: A post hoc analysis of efficacy and safety in Phase 3 and long-term extension studies over 7 years. Int J Rheum Dis. 2020. 23(7). 882-897. <br>
5.  S Chandrashekara, P Renuka, KR Anupama. Neutrophil-to-lymphocyte ratio in systemic lupus erythematosus is influenced by steroids and may not completely reflect the disease activity. Internet Journal of Rheumatology and Clinical Immunology. 2020. 8(1):OA1 DOI: 10.15305/ijrci/v8i1/319 <br>
6.  S Chandrashekara, V Shobha, V Rao, et. al. Incidence of infection other than tuberculosis in patients with autoimmune rheumatic diseases treated with bDMARDs: a real-time clinical experience from India. Rheumatology International. 2019. 39(3). 497-507. <br>
7.  G Arungovind, AS Kamalanathan, V Padmanabhan, et. al. Modifications of human plasma apolipoprotein A1 in systemic autoimmune diseases and myocardial infarction: a comparative study. Journal of Proteins and Proteomics. 2019. 10(9). 235-243. <br>
8.  S Chandrashekara, SV Dhote, KR Anupama. The Differential Influence of Immunological Process of Autoimmune Disease on Lipid Metabolism: A Study on RA and SLE. Indian Journal of Clinical Biochemistry. 2019. 34(1). 52-59. <br>
9.  B Sosale, AR Sosale, S Chandrashekara, at. al. Effect of vitamin D supplementation on reduction of cardiometabolic risk in patients with type 2 diabetes mellitus and dyslipidemia. International Journal of Diabetes in Developing Countries. 2018. 38(2). 221-227. <br>
10. CL Deepak, Panchagnula R, J Swetha, Kori D. Internet Journals of Case Reports. 2016.1(1):CS2.
</p>
<br><br>
<h3>List of all publications</h3>
<ul class="offset-1">
    <li><a href="https://scholar.google.co.in/citations?hl=en&user=ECxPLoUAAAAJ&view_op=list_works&sortby=pubdate">List of all publications - I</a></li>
    <li><a href="https://scholar.google.com/citations?hl=en&user=o48cMpwAAAAJ&view_op=list_works&sortby=pubdate">List of all publications - II</a></li>
</ul>
            </div>
            <!-- <div class="col-md-4"> 
                <div class="image"> <img src="images/about.jpg" width="300"></div>
               
            </div>-->
        </div>
    </div>



    <?php include ('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>