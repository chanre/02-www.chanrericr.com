<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
<style type="text/css">
    .heading-title{
        font-family: times;
        font-weight: 700;
        margin: 50px;
        
    }
    .head-row{
        background-color: skyblue;
        margin-bottom: 10px;
    }
    .card{
        max-height: 350px;
        margin-bottom: 30px; 
    }
</style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
            <section>
                <div class="container">
                    <div class="row head-row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6"><h2 align="center" class="heading-title">Welcome to Patients Corner</h2></div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                              <img class="card-img-top" src="img/pg.jpg" alt="Card image cap">
                             
                              <div class="card-footer">
                                 <a href="video/video.php" class="btn btn-success btn-block">Click Here</a>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                              <img class="card-img-top" src="img/sc.jpg" alt="Card image cap">
                             
                              
                              <div class="card-footer">
                                 <a href="symptom-checker-web.php" class="btn btn-success btn-block">Click Here</a>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                              <img class="card-img-top" src="img/ba.jpg" alt="Card image cap">
                              
                              <div class="card-footer">
                                  <a href="https://chanrericr.com/appointment1.php" class="btn btn-success btn-block">Click Here</a>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                              <img class="card-img-top" src="img/ob.jpg" alt="Card image cap">
                              
                              <div class="card-footer">
                                  <a href="#" class="btn btn-success btn-block">Click Here</a>
                              </div>
                            </div>
                        </div>
                       
                    </div>
                </div>

            </section>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>