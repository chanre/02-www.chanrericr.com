<?php 

include ('config.php');

if(isset($_POST['submit']))
{
$name = $_POST['name'];
$contact = $_POST['contact'];
$email = $_POST['email'];
$department= $_POST['department'];
$doctor =$_POST['doctor'];
$gender=$_POST['gender'];
$symptoms=$_POST['symptoms'];
$registerno=$_POST['registerno'];
$cdate = $_POST['cdate'];
$ctime = $_POST['ctime'];
$rdoctor =$_POST['rdoctor'];
$status=0;
$query = mysqli_query($con,"insert into appointment (name,contact,email,department,doctor,gender,symptoms,registerno,cdate,ctime,rdoctor,Is_Active)
 values('$name','$contact','$email','$department','$doctor','$gender','$symptoms','$registerno','$cdate','$ctime','$rdoctor','$status')");
if($query){
echo "<script> alert('Thank you for choosing chanre..We will get back to you in a while!!')</script>";
}
else
{
echo "<script> alert('Request could not proccess.. Please Try again!!')</script>";
}

}



 ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
        h4{
            background-color: #32b58b;
            line-height: 40px;
            color: white;
            font-weight: 800;
            font-family: times;
        }
        .myform{
            background-image: url(images/b1.jpg);
        }
        .myform1{
            background-color: #135c853b;
            color: white;
        }
        .gaps{margin-top: 10px;font-family: times;}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>

       <div class="container-fluid myform">
            <div class="row">
                <div class="col-md-4"></div> <br>
                <div class="col-md-4 myform1"><br>
              
                 <form  method="POST">
            <div class="left-agileits-w3layouts same">
                <div class="gaps">
                    <input type="text" name="name"  placeholder="Enter Name.." required="" class="form-control" />
                </div>  
                <div class="gaps">  
                    <input type="text" name="contact"  placeholder="Enter contact.." required="" class="form-control"/>
                </div>
                <div class="gaps">
                    <input type="email" name="email" placeholder="Enter Your Email .." required="" class="form-control" />
                </div>
                <div class="gaps">
                    <select id="prov" name="department" class="form-control"></select>
                </div>
                <div class="gaps">
                    <select id="town" name="doctor" class="form-control"></select>
                </div>
                 <div class="gaps">
                         <select name="" id="cboOptions" onchange="showDiv('div',this)" class="form-control" >
                              <option value="1">Are You already registered ?</option>
                              <option value="1">No</option>
                              <option value="2">Yes</option>
                         </select>
                </div>
                <!-- ============================================================================================================ -->
                 <div id="div1" style="display:block;">
                      <div class="gaps">
                      <select class="form-control" name="gender">
                          <option>Select Gender</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                      </select>
                 </div>
                  
                <div class="gaps">
                        <textarea name="symptoms" placeholder="Please Write Symptoms.."  class="form-control" required></textarea>
                </div>
                <div class="gaps">
                        <input type="text" name="rdoctor"  title="No numbers or special character allowed." placeholder="Name of Refered Doctor" class="form-control">
                </div>
                
            <br>
                 
                 </div>
                 <!-- ================================================ -->
                 <div id="div2" style="display:none;">
                  <div class="gaps">
                        <input placeholder="Enter Registration.."  type="text" name="registerno" class="form-control">    
                 </div>

                 
                 </div>
<!-- ======================================================================================================================================================= -->
                 <div class="gaps">
                        <input placeholder="Select Date.."  name="cdate" type="date"  class="form-control" required>
                </div>
               
         
                 <div class="gaps">
                      <input placeholder="Select time.."  type="text" id="timepicker" name="ctime" class="timepicker form-control" required>    
                 </div>

                  <div class="gaps" align="center">
                    <input type="submit" name="submit" value="Submit Request" class="btn btn-primary">   
                 </div>
            </div>
                
            </form>
               <br>
    <!-- -============================Registered=============================================== -->
             <script type="text/javascript"> window.onload = function() {
  // provs is an object but you can think of it as a lookup table
  var provs = {
        'Select Department':[''],
              'Rheumatology': ['Dr.Chandrashekara .S','Dr.Beenish Nazir','Dr. Sayid Fahad Nizar Ahamed','Dr. Shruthi Desai','Dr.Dhanashree','Dr. Yogitha'],
        // 'Paediatric': ['Dr.Chandrika S Bhat'],
        'Allergy & Clinical Immunology':['Dr. Smitha J N Singh',''],
        'Reproductive immunology & High-risk pregnancy':['Dr. Nandyala Sundari','Dr. Chaitra S Niranthara'],
        'Paediatric Rheumatology':['',''],
        'Physiotherapy and Chronic pain management':['Mr. Sasikumar. M','Mr. Rajkannan. P'],
        'Joint Preservation & Restoration':['Dr. C. B. Prabhu','Dr. Venu Madhav','Dr. Balasubramanyam'],
        'Clinical Psychology':['Ms. Rajeshwari D C'],
        'Dietetics':['Ms.Shruti Patil',''],
        'Radiology':['Dr. Shivanand N D',''],
        'Vascular Surgeon':['Dr. Chandrashekar A R'],
        'Cardiology':['Dr. Anand Kumar M','Dr. Nagesh M B'],
        'Diabetology':['Dr. Radha Rangarajan',''],
        'Pulmonologist':['Dr. Ramesh R'],
        'Psychiatrist':['Dr. Chandrashekar M','Dr. Sushma'],
        'Ophthalmology':['Dr. Usharani',''],
        'Nephrology':['Dr Sunil R','Dr Sanjay'],
        'General Surgery':['Dr. Manojith S S','Dr. Nagendra K'],
        'Dental':['Dr. Dilip Bharadwaj',''],
        'Dermatologist':['Dr. Sushmitha E S',''],
        'Gastroenterology':['Dr. Abhijith B R',''],
        'ENT':['Dr. Tejmurthy B V'],
        'Urology':['Dr.Sanjay R P','Dr. Nagarajaiah Narayanaswamy',''],
        'Neurosurgery':['Dr. Karthik Malepathi',''],
        'Hand Orthopaedician':['Dr.Darshan Kumar A Jain'],
        'Spine Orthopedician':['Dr.Kodlady Surendar Shetty'],
                // 'Orthopedics':['Dr. Venu Madhav',''],
        

      },
      // just grab references to the two drop-downs
      prov_select = document.querySelector('#prov'),
      town_select = document.querySelector('#town');

  // populate the provinces drop-down
  setOptions(prov_select, Object.keys(provs));
  // populate the town drop-down
  setOptions(town_select, provs[prov_select.value]);
  
  // attach a change event listener to the provinces drop-down
  prov_select.addEventListener('change', function() {
    // get the towns in the selected province
    setOptions(town_select, provs[prov_select.value]);
  });
    
  function setOptions(dropDown, options) {
    // clear out any existing values
    dropDown.innerHTML = '';
    // insert the new options into the drop-down
    options.forEach(function(value) {
      dropDown.innerHTML += '<option name="' + value + '">' + value + '</option>';
    });
  }  
};</script>

<!-- =================================================================================== -->
                </div>

                <div class="col-md-4"></div>
            </div>
        </div>

     
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->
<script type="text/javascript">
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });
        </script>
    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
            <script type="text/javascript">
                $('.timepicker').wickedpicker({twentyFour: false});
            </script>
        <!-- Calendar -->
                <link rel="stylesheet" href="css/jquery-ui.css" />
                <script src="js/jquery-ui.js"></script>
                  <script>
                          $(function() {
                            $( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
                          });
                  </script>
              


                  <script>
    function showDiv(prefix,chooser) 
    {
            for(var i=0;i<chooser.options.length;i++) 
            {
                var div = document.getElementById(prefix+chooser.options[i].value);
                div.style.display = 'none';
            }

            var selectedOption = (chooser.options[chooser.selectedIndex].value);

            if(selectedOption == "1")
            {
                displayDiv(prefix,"1");
            }
            if(selectedOption == "2")
            {
                displayDiv(prefix,"2");
            }
    }

    function displayDiv(prefix,suffix) 
    {
            var div = document.getElementById(prefix+suffix);
            div.style.display = 'block';
    }
</script>

</body>

</html>
