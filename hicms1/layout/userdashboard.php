 <div class="dash-tiles row">
                        <!-- Column 1 of Row 1 -->
                        <div class="col-sm-3">
                            <!-- Total Users Tile -->
                            <div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Manage Users"><i class="fa fa-eye"></i></a>
                                           
                                        </div>
                                    </div>
                                    Complaint
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
                                <div class="dash-tile-text"><?php 
                                    $user=$_SESSION['login'];
                                    $sql="SELECT * FROM tbl_complaint where res_person ='$user' ";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf(" %d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?></div>
                            </div>
                        </div>
                        <!-- END Column 1 of Row 1 -->

                        <!-- Column 2 of Row 1 -->
                        <div class="col-sm-3">
                            <!-- Total Sales Tile -->
                            <div class="dash-tile dash-tile-flower clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="View Ongoing Work"><i class="fa fa-eye"></i></a>
                                            
                                        </div>
                                    </div>
                                    Ongoing Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-tags"></i></div>
                                <div class="dash-tile-text">
                                    <?php 
                                    $user=$_SESSION['login'];
                                    $sql="SELECT * FROM tbl_complaint where res_person ='$user' && Is_Active='Ongoing' ";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf(" %d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <!-- Popularity Tile -->
                            <div class="dash-tile dash-tile-oil clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="What's changed?"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </div>
                                    Completed Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
                                <div class="dash-tile-text">
                                   <?php 
                                    $user=$_SESSION['login'];
                                    $sql="SELECT * FROM tbl_complaint where res_person ='$user' AND Is_Active='Completed' ";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf(" %d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <!-- RSS Subscribers Tile -->
                            <div class="dash-tile dash-tile-balloon clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="View Pending Work"><i class="fa fa-eye"></i></a>
                                    </div>
                                    Cancelled Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-rss"></i></div>
                                <div class="dash-tile-text">
                                    <?php 
                                    $user=$_SESSION['login'];
                                    $sql="SELECT * FROM tbl_complaint where res_person ='$user' && Is_Active='Cancelled' ";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf(" %d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?></div>
                            </div>
                        </div>
                        <!-- END Column 4 of Row 1 -->
                    </div>