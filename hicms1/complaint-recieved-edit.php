<?php
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
// require 'layout/config.php';
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name'];
if(isset($_POST['edit']))
{
$feedback=$_POST['feedback'];
$Is_Active = $_POST['Is_Active'];
$cname=$_POST['cname'];
$rname=$_POST['rname'];
$com_name=$_POST['com_name'];
$sid=intval($_GET['pid']);
$sqr=mysqli_query($con,"UPDATE tbl_complaint SET feedback='$feedback', Is_Active='$Is_Active' WHERE id='$sid'");
$cquery=mysqli_query($con,"select * from tbl_users where user_name='$cname'");
$crows=mysqli_fetch_array($cquery);
 $emailTo=$crows['user_email'];
$rquery=mysqli_query($con,"select * from tbl_users where user_name='$rname'");
$rrows=mysqli_fetch_array($rquery);
$fromEmail=$rrows['user_email'];
    $mail = new PHPMailer(true);
    if (!$sqr) {
       exit('Error'); 
    }
    try {
        $mail->SMTPDebug = 0; 
        $mail->isSMTP();   
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'webdesigner@chanrejournals.com';
        $mail->Password = '*meghalaya2'; 
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom($fromEmail, $rname);
        $mail->addAddress($emailTo, $cname);
        // $mail->addReplyTo('no-reply@example.com', 'No Reply');
        $mail->addReplyTo($fromEmail, $rname);
        $mail->addCC('corporaterelation@chanrericr.com',$fromEmail);
        $mail->isHTML(true);
        $mail->Subject = $com_name;
        $mail->Body    = "$feedback";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );
        $mail->send();
        echo '<script>alert("Message has been sent");window.location.href = "complaint-recieved.php";</script>';
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
    exit(); 
}
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
           <?php include('layout/header.php'); ?>
            <div id="inner-container">
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                   <?php include ('layout/primary-nav.php'); ?>
                </aside>
                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Assign Work</a></li>
                    </ul>
                    <?php $nid=intval($_GET['pid']);
                    $queryy=mysqli_query($con,"SELECT * from tbl_complaint WHERE id='$nid'");
                    while($myrow=mysqli_fetch_array($queryy))
                    {
                     ?>
                 <form  method="POST" class="form-horizontal form-box remove-margin">
                        <h4 class="form-box-header">Edit Complaint</h4>
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Complained By *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="user_name" name="cname" value="<?php echo htmlentities($myrow['user_name']); ?>" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="dept_name" name="dept_name" value="<?php echo htmlentities($myrow['dept_name']); ?>"   class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3" for="user_name">Complaint Title*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="com_name" value="<?php echo htmlentities($myrow['com_name']); ?>"  name="com_name" class="form-control" readonly>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Complaint Descriptions *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="com_desc" class="form-control" readonly><?php echo htmlentities($myrow['com_desc']); ?></textarea>
                                    </div>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Complaint Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        
                                       <input type="text" class="form-control" name="c_dept" value="<?php echo htmlentities($myrow['to_dept']); ?>" readonly>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Complaint to</label>
                                <div class="col-md-4">
                                     <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                    <input type="text" class="form-control" value="<?php echo htmlentities($myrow['res_person']); ?>" name="rname" readonly> 
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Feedback*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="feedback" class="form-control" placeholder="Enter your feedback" ><?php echo htmlentities($myrow['feedback']); ?>.</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Update Status</label>
                                <div class="col-md-4">
                                     <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                    <select class="form-control" name="Is_Active">
                                        <option>Choose an option</option>
                                        <option value="Ongoing" <?php if($myrow['Is_Active'] == 'Ongoing') { ?> selected="selected"<?php } ?>>Ongoing</option>
                                        <option value="Completed" <?php if($myrow['Is_Active'] == 'Completed') { ?> selected="selected"<?php } ?> >Fixed/Completed/Done</option>
                                        <option value="Cancelled" <?php if($myrow['Is_Active'] == 'Cancelled') { ?> selected="selected"<?php } ?>>Cancelled</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                       
                            
                           
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-4">
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" name="edit"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- END Form Content -->
                    </form>
                <?php } ?>
               
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
       
        <!-- Javascript code only for this page -->
       
    </body>
</html>
<?php } ?>