<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name'];
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
           <?php include('layout/header.php'); ?>
            <div id="inner-container">
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                   <?php include ('layout/primary-nav.php'); ?>
                </aside>
                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Assign Work</a></li>
                    </ul>
                 <form  method="POST" action="code.php" class="form-horizontal form-box remove-margin">
                        <div class="form-box-content">
                             <table id="example-datatables2" class="table table-striped table-bordered table-hover">
                                <div class="form-box-header">Work Status
                                    <div  align="right">
                                    <!-- <a href="add-complaint.php" class="btn btn-primary btn-sm"> Add Complaint </a> -->
                                     Export Data: 
                                    <a href="export-7.php" class="btn btn-success btn-sm">7 Days</a>
                                    <a href="export-30.php" class="btn btn-success btn-sm">30 Days</a>
                                    <a href="export-365.php" class="btn btn-success btn-sm">1 Year</a>
                                    
                                  </div>
                                 
                                </div>
                                <thead>
                                    <tr> 
                                        <th>Ticket No</th>
                                        <th>Complaint By</th>
                                        <th>Department</th>
                                        <th>Department admin</th>
                                        <th>Super Admin</th>
                                        <th>Concerned Department</th>
                                        <th>Complaint to</th>
                                        <th>Date</th>
                                        <th>Feedback</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                      $query=mysqli_query($con,"SELECT * from tbl_complaint WHERE dept_name='$dept'");
                                        while ($rows=mysqli_fetch_array($query)) {
                                            $dt = new DateTime($rows['c_date']);
                                            $cd = new DateTime($rows['comp_date']); 
                                         ?>
                                    <tr>
                                        <td><?php echo htmlentities($rows['id']); ?></td>
                                        <td><?php echo htmlentities($rows['user_name']); ?></td>
                                        <td><?php echo htmlentities($rows['dept_name']); ?></td>
                                        <td><?php echo htmlentities($rows['dept_admin']); ?></td>
                                        <td><?php echo htmlentities($rows['super_admin']); ?></td>
                                        <td><?php echo htmlentities($rows['to_dept']); ?></td>
                                        <td><?php echo htmlentities($rows['res_person']); ?></td>
                                        <td><?php echo $dt->format('d-m-Y'); ?></td>
                                        <td><?php echo htmlentities($rows['feedback']); ?></td>
                                        <td><a href="edit-sub-complaint.php?pid=<?php echo htmlentities($rows['id']);?>" class="btn btn-primary btn-sm">Edit</a></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
            </div>
        </div>
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function () {
                /* Initialize Datatables */
                $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
                $('#example-datatables2').dataTable();
                $('#example-datatables3').dataTable();
                $('.dataTables_filter input').attr('placeholder', 'Search');
            });
        </script>
    </body>
</html>
<?php } ?>