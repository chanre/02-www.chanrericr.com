<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name'];
if(isset($_POST['wupdate']))
{
$work_status=$_POST['work_status'];
$dept_admin=$_POST['dept_admin'];
$dept_name=$_POST['dept_name'];
$work_name=$_POST['work_name'];
$work_desc=$_POST['work_desc'];
$pid=intval($_GET['nid']);
$updatequery=mysqli_query($con,"UPDATE tbl_work SET work_status='$work_status',dept_admin='$dept_admin',
dept_name='$dept_name',work_name='$work_name',work_desc='$work_desc'
    WHERE id='$pid'");
 if($updatequery){
// echo "<script> alert('Successfully Added!!')</script>";

echo "<script>alert('You have successfully updated the status');</script>";
echo "<script > document.location ='user-work.php'; </script>";
}
else
{
echo "<script> alert('Request could not proccess.. Please Try again!!')</script>";
// $error="Request could not proccess.. Please Try again!!";
}
}

?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
           <?php include('layout/header.php'); ?>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                   
                    <!-- END Sidebar search -->

                    <!-- Primary Navigation -->
                   <?php include ('layout/primary-nav.php'); ?>
                    <!-- END Primary Navigation -->
                </aside>
                <!-- END Sidebar -->

                <!-- Page Content -->
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Assign Work</a></li>
                    </ul>
                    <!-- END Navigation info -->
         <form  method="POST" class="form-horizontal form-box remove-margin">
                        <!-- Form Header -->
                        <h4 class="form-box-header">Assign Work</h4>
<?php
$pid=intval($_GET['nid']);
 $querys=mysqli_query($con,"SELECT * FROM tbl_work WHERE id='$pid'");
while($rows1=mysqli_fetch_array($querys))
{
  ?>
        
                        <!-- Form Content -->
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Name *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="dept_admin" name="dept_admin" value="<?php echo htmlentities($rows1['dept_admin']); ?>" class="form-control" readonly>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="dept_name" name="dept_name" value="<?php echo htmlentities($rows1['dept_name']); ?>"    class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="user_name">Work Name*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="work_name"  name="work_name" value="<?php echo htmlentities($rows1['work_name']); ?>"  class="form-control">
                                        
                                    </div>
                                    <p id="loaderIcon"> <span id="username-availability-status"></span></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Work Descriptions *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="work_desc"  class="form-control"><?php echo htmlentities($rows1['work_desc']); ?> </textarea>
                                    </div>
                                </div>
                            </div>
                           
                          
                            <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Status</label>
                                <div class="col-md-4">
                                     <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                    <select id="example-select" name="work_status" class="form-control">
                                      <option>Select Work status</option>
                                      <option value="2">Ongoing</option>
                                      <option value="1">Completed</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                            
                       
                            
                           
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-4">
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" name="wupdate"><i class="fa fa-check"></i> update</button>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                        <!-- END Form Content -->
                    </form>
               
               
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
       
        <!-- Javascript code only for this page -->
       
    </body>
</html>
<?php } ?>