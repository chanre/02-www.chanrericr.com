-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2021 at 02:20 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `officedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `resetpasswords`
--

CREATE TABLE `resetpasswords` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dept`
--

CREATE TABLE `tbl_dept` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(50) DEFAULT NULL,
  `dept_date` varchar(25) NOT NULL,
  `dept_admin` varchar(50) DEFAULT NULL,
  `dept_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dept`
--

INSERT INTO `tbl_dept` (`dept_id`, `dept_name`, `dept_date`, `dept_admin`, `dept_update`) VALUES
(1, 'Rheumatology & Autoimmune Diseases', '09-30-2021', NULL, '2021-09-07 05:31:23'),
(2, 'Research Assist', '09-07-2021', NULL, '2021-09-07 05:35:52'),
(3, 'Clinical Research', '09-05-2021', NULL, '2021-09-07 05:36:34'),
(4, 'Accounts', '09-17-2021', NULL, '2021-09-17 05:32:05'),
(5, 'MEDICAL ADMINISTRATIVE OFFICER', '09-17-2021', NULL, '2021-09-17 05:32:29'),
(6, 'Physiotherapy', '09-17-2021', NULL, '2021-09-17 05:32:49'),
(7, 'Fellowship Trainee', '09-17-2021', NULL, '2021-09-17 05:33:07'),
(8, 'Nursing', '09-17-2021', NULL, '2021-09-17 05:33:21'),
(9, 'Information Technology', '09-17-2021', NULL, '2021-09-17 05:33:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feedback`
--

CREATE TABLE `tbl_feedback` (
  `fid` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `f_text` longtext NOT NULL,
  `f_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Is_Active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_feedback`
--

INSERT INTO `tbl_feedback` (`fid`, `user_name`, `dept_name`, `f_text`, `f_time`, `Is_Active`) VALUES
(1, 'admin', 'Research Assist', 'Hello world', '2021-09-16 06:20:41', 0),
(2, 'admin', 'Research Assist', 'Testing', '2021-09-16 06:35:01', 0),
(3, 'test_user', 'Research Assist', 'hello', '2021-09-16 08:56:54', 0),
(4, 'test_user', 'Research Assist', 'Great to see this', '2021-09-16 08:57:34', 0),
(5, 'test_user', 'Research Assist', 'hello its working', '2021-09-16 09:10:40', 0),
(6, 'test_user', 'Research Assist', 'Lets do it.', '2021-09-16 09:23:40', 0),
(7, 'admin', 'Research Assist', 'testing...', '2021-09-16 09:24:56', 0),
(8, 'admin', 'Research Assist', 'testing...', '2021-09-16 09:29:08', 0),
(9, 'test_user', 'Research Assist', 'Okay', '2021-09-16 09:36:25', 0),
(10, 'admin', 'Research Assist', 'send something', '2021-09-16 09:56:43', 0),
(11, 'test_user', 'Research Assist', 'hello', '2021-09-16 10:33:31', 1),
(12, 'Shamim', 'Information Technology', 'Hello', '2021-09-17 09:09:44', 1),
(13, 'Shamim', 'Information Technology', 'I have an issue', '2021-09-17 09:39:24', 1),
(14, 'Shamim', 'Information Technology', 'Resolved', '2021-09-17 09:40:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(12) NOT NULL,
  `name` varchar(70) DEFAULT NULL,
  `user_name` varchar(55) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_contact` varchar(12) DEFAULT NULL,
  `user_email` varchar(70) DEFAULT NULL,
  `user_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_dept` varchar(50) DEFAULT NULL,
  `user_role` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `user_name`, `user_password`, `user_contact`, `user_email`, `user_date`, `user_dept`, `user_role`) VALUES
(7, 'admin', 'admin', '$2y$10$MNwYZ2SmqZ2JpY2oepB1B..DX3RK7Rt0nL.ZmEehfJUH1uB0IR9EG', '9856876212', 'dept2@gmail.com', '2021-09-08 08:45:48', 'Research Assist', '3'),
(9, 'Shamim Akhtar Sheikh', 'Shamim', '$2y$10$YZwmNa6tqpl0.0PMND/fB..WZZd46Rof7Ib0VJyE5Xf6nPwwHSUHO', '9856876212', 'webdesigner@chanrejournals.com', '2021-09-17 07:19:54', 'Information Technology', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work`
--

CREATE TABLE `tbl_work` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `dept_admin` varchar(50) NOT NULL,
  `work_name` varchar(255) NOT NULL,
  `work_desc` longtext NOT NULL,
  `dept_name` varchar(50) NOT NULL,
  `work_assigned` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `work_done` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `work_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_work`
--

INSERT INTO `tbl_work` (`id`, `user_name`, `dept_admin`, `work_name`, `work_desc`, `dept_name`, `work_assigned`, `work_done`, `work_status`) VALUES
(1, 'Shamim', 'admin', 'HICMS', 'Work  ', 'Information Technology', '2021-09-18 07:20:36', '2021-09-18 10:05:46', 2),
(2, 'Shamim', 'admin', 'Rheuma tv', 'create the site ', 'Information Technology', '2021-09-18 12:11:46', '2021-09-18 12:12:35', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `resetpasswords`
--
ALTER TABLE `resetpasswords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dept`
--
ALTER TABLE `tbl_dept`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  ADD PRIMARY KEY (`fid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_work`
--
ALTER TABLE `tbl_work`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `resetpasswords`
--
ALTER TABLE `resetpasswords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_dept`
--
ALTER TABLE `tbl_dept`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_feedback`
--
ALTER TABLE `tbl_feedback`
  MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_work`
--
ALTER TABLE `tbl_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
