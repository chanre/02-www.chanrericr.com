<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
{ 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name'];
?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
           <?php include('layout/header.php'); ?>
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                   <?php include ('layout/primary-nav.php'); ?>
                    <!-- END Primary Navigation -->
                </aside>
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Assign Work</a></li>
                    </ul>
                    <!-- END Navigation info -->
                <script>
                function getSubCat(val) {
                    // let tz=document.getElementById("dept_name").val;
                     var selectedValue = document.getElementById("c_dept").value;
                            // alert(selectedValue);
                  $.ajax({
                  type: "POST",
                  url: "code.php",
                  data:'c_dept='+selectedValue,
                  success: function(data){
                    $("#subcategory").html(data);
                  }
                  });
                  }
                  </script> 
                 <form  method="POST" action="../code.php" class="form-horizontal form-box remove-margin">
                        <!-- Form Header -->
                        <h4 class="form-box-header">Raise A Complaint</h4>

                        <!-- Form Content -->
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Name *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="user_name" name="user_name" value="<?php echo $uname; ?>" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="dept_name" name="dept_name" value="<?php echo $dept; ?>"   class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label class="control-label col-md-3" for="user_name">Complaint Title*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="com_name"  name="com_name" class="form-control">
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Complaint Descriptions *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="com_desc" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Complaint Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        
                                        <select   class="form-control" id="c_dept" name="c_dept" onChange="getSubCat(this.value);">
                                          <option value="">Select department</option>
                                          <?php $qry=mysqli_query($con,"select * from tbl_dept");
                                          while($rows=mysqli_fetch_array($qry))
                                          {
                                           ?>
                                           <option value="<?php echo htmlentities($rows['dept_name']); ?>"><?php echo htmlentities($rows['dept_name']); ?></option>
                                       <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Complaint to</label>
                                <div class="col-md-4">
                                     <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                    <select id="subcategory" name="com_user" class="form-control">
                                        
                                    </select>
                                </div>
                                </div>
                            </div>
                            
                       
                            
                           
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-4">
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" name="complaint"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- END Form Content -->
                    </form>
               
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
       
        <!-- Javascript code only for this page -->
       
    </body>
</html>
<?php } ?>