<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Sign In - Hospital Intra Communication</title>

        <meta name="description" content="Sign in to experience something new.">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body class="login">
        <!-- Login Container -->
        <div id="login-container">
            <div id="login-logo">
                <a href="">
                    <!-- <img src="img/template/uadmin_logo.png" alt="logo"> -->
                </a>
            </div>

            <!-- Login Buttons -->
        
            <!-- END Login Buttons -->

            <!-- Login Form -->
            <form action="code.php" method="post" class="form-horizontal">
                <div class="form-group">
                    <!-- <a href="javascript:void(0)" class="login-back"><i class="fa fa-arrow-left"></i></a> -->
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group">
                          <input type="text" id="login-email" name="user_email" placeholder="Email.." class="form-control">
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group">
                            <input type="password" id="login-password" name="user_password" placeholder="Password.." class="form-control">
                            <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="btn-group btn-group-sm pull-right">
                        <button type="button" id="login-button-pass" class="btn btn-warning" data-toggle="tooltip" title="Forgot pass?"><i class="fa fa-lock"></i></button>
                        <button type="submit" class="btn btn-success" name="login"><i class="fa fa-arrow-right"></i> Login</button>
                    </div>
                    <a href="requestReset.php">Forgot Password?</a>
                </div>
            </form>
            <!-- END Login Form -->
        </div>
        <!-- END Login Container -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Javascript code only for this page -->
        <script>
            $(function () {
                var loginButtons = $('#login-buttons');
                var loginForm = $('#login-form');

                // Reveal login form
                $('#login-btn-email').click(function () {
                    loginButtons.slideUp(600);
                    loginForm.slideDown(450);
                });

                // Hide login form
                $('.login-back').click(function () {
                    loginForm.slideUp(450);
                    loginButtons.slideDown(600);
                });
            });
        </script>
    </body>
</html>