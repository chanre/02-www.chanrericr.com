<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name'];
if(isset($_POST['edit']))
{
    $dept_admin=$_POST['dept_admin'];
    $sid=intval($_GET['pid']);
    $sqr=mysqli_query($con,"UPDATE tbl_complaint SET dept_admin='$dept_admin' WHERE id='$sid'");
    if($sqr)
    {
        echo "<script>alert('Added your note.');</script>";
    }
    else{
        echo "<script>alert('Failed to add');</script>";
    }
}

?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
           <?php include('layout/header.php'); ?>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                   
                    <!-- END Sidebar search -->

                    <!-- Primary Navigation -->
                   <?php include ('layout/primary-nav.php'); ?>
                    <!-- END Primary Navigation -->
                </aside>
                <!-- END Sidebar -->

                <!-- Page Content -->
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Assign Work</a></li>
                    </ul>
                    <!-- END Navigation info -->
                    <?php $nid=intval($_GET['pid']);
                    $queryy=mysqli_query($con,"SELECT * from tbl_complaint WHERE id='$nid'");
                    while($myrow=mysqli_fetch_array($queryy))
                    {
                     ?>
                    
                 <form  method="POST" class="form-horizontal form-box remove-margin">
                        <!-- Form Header -->
                        <h4 class="form-box-header">Edit Complaint</h4>

                        <!-- Form Content -->
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Complained By *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="user_name" name="user_name" value="<?php echo htmlentities($myrow['user_name']); ?>" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="dept_name" name="dept_name" value="<?php echo htmlentities($myrow['dept_name']); ?>"   class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group"> 
                                <label class="control-label col-md-3" for="val_username">Department admin</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <?php /* $deptadmin= mysqli_query($con,"SELECT * FROM tbl_users WHERE  user_dept='$dept' AND user_role=1");
                                        while($rows1=mysqli_fetch_array($deptadmin))
                                        {
                                         ?>
                                       
                               <input type="text" id="dept_admin" name="dept_admin" value="<?php echo htmlentities($rows1['user_name']); ?>"  class="form-control" readonly>
                                    <?php } */?>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Department Admin*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                       
                                        <select name="dept_admin" class="form-control">
                                            <option value="">Choose</option>
                                            <option value="Priority" <?php if($myrow['dept_admin'] == 'Priority') { ?> selected="selected"<?php } ?>>Priority</option>
                                            <option value="Normal" <?php if($myrow['dept_admin'] == 'Normal') { ?> selected="selected"<?php } ?>>Normal</option>
                                            
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Super Admin*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="super_admin" class="form-control"readonly><?php echo htmlentities($myrow['super_admin']); ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="user_name">Complaint Title*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="com_name" value="<?php echo htmlentities($myrow['com_name']); ?>"  name="com_name" class="form-control" readonly>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Complaint Descriptions *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="com_desc" class="form-control" readonly><?php echo htmlentities($myrow['com_desc']); ?></textarea>
                                    </div>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Complaint Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        
                                       <input type="text" class="form-control" name="c_dept" value="<?php echo htmlentities($myrow['to_dept']); ?>" readonly>
                                    </div>
                                </div>
                            </div>
                          
                           <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Complaint to</label>
                                <div class="col-md-4">
                                     <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                    <input type="text" class="form-control" value="<?php echo htmlentities($myrow['res_person']); ?>" name="" readonly>
                                        
                                   
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Feedback*</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <textarea name="feedback" class="form-control" readonly><?php echo htmlentities($myrow['feedback']); ?></textarea>
                                    </div>
                                </div>
                            </div>
                       
                            
                           
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-4">
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" name="edit"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- END Form Content -->
                    </form>
                <?php } ?>
               
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
       
        <!-- Javascript code only for this page -->
       
    </body>
</html>
<?php } ?>