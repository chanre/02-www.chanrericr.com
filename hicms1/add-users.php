<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_name = '$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
           <?php include('layout/header.php'); ?>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                   
                    <!-- END Sidebar search -->

                    <!-- Primary Navigation -->
                   <?php include ('layout/primary-nav.php'); ?>
                    <!-- END Primary Navigation -->
                </aside>
                <!-- END Sidebar -->

                <!-- Page Content -->
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Add Users</a></li>
                    </ul>
                    <!-- END Navigation info -->

                 <form  method="POST" action="code.php" class="form-horizontal form-box remove-margin">
                        <!-- Form Header -->
                        <h4 class="form-box-header">Add Users</h4>

                        <!-- Form Content -->
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Name *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="val_username" name="name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="user_name">Username *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" id="user_name" name="user_name" onkeyup="checkusernameAvailability()" class="form-control">
                                        
                                    </div>
                                    <p id="loaderIcon"> <span id="username-availability-status"></span></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Email *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <input type="text" id="val_email" name="user_email" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_email">Contact *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <input type="number" name="user_contact" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Department *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group fa-fw"></i></span>
                                    <input type="text" name="user_dept" readonly class=" form-control" value="<?php echo $dept; ?>">
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="example-select">Role *</label>
                                <div class="col-md-4">
                                     <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                    <select id="example-select" name="user_role" class="form-control">
                                        <option>Select Role</option>
                                        <option value="0">User</option>
                                    </select>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_password">Password *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-asterisk fa-fw"></i></span>
                                        <input type="password" id="val_password" name="user_password" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                           
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-4">
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" name="subadmin"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- END Form Content -->
                    </form>
               
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
            function checkusernameAvailability() {
                    $("#loaderIcon").show();
                    jQuery.ajax({
                    url: "code.php",
                    data:'user_name='+$("#user_name").val(),
                    type: "POST",
                    success:function(data){
                    $("#username-availability-status").html(data);
                   
                    },
                    error:function (){}
                    });
                    }
        </script>
        <!-- Javascript code only for this page -->
       
    </body>
</html>
<?php } ?>