<?php 

session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:admin.php');
}

else{
if(GET['nid'])
{
	$id=intval($_GET['nid']);
	$query=mysqli_query($con,"delete from  patientdiagram  where id='$id'");
	
}
if($_GET['pid'])
{
	$id=intval($_GET['pid']);
	$query=mysqli_query($con,"Update patientdiagram set Is_Active='1' where id='$id'");

}
?>


<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <header id="top" class="top_navbar">
	   <div class="container">
                <nav class="navbar  navbar-expand-lg navbar-light ">
                    <a class="navbar-brand" href="/admindashboard.php"><img src="images/chanrelogo.png" alt="" width="250" /></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon">
                        <i class="fa fa-bars"></i>
                      </span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                          <ul class="navbar-nav"> 
                            <li class="nav-item">
                                <a class="nav-link" href="admindashboard.php">Main Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="confirmed.php">Confirmed</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="cancelled.php">Cancelled</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="pddsraadmin.php">PDDSRA</a>
                            </li>
							<li class="nav-item">
                                <a class="nav-link" href="logout.php">Logout</a>
                            </li>
                        </ul>
                  
                    </div>
                </nav>
            </div>
      
        </header>
<style>
th{
	background-color:#53a6d5;
		color:white;
}
</style>
       <div class="container-fluid">
            <div class="row">
			<div class="col-md-4"></div>
                <div class="col-md-4">
					
                    <br><br>
					<br>
					<table border="2" align="center" style="padding:20px;">
					<tr>
					<th>Name</th>
					<th>Contact</th>					
					<th>Patient Id</th>
	
					<th>Action</th>
					</tr>
					 <?php $query=mysqli_query($con,"select * from patientdiagram where Is_Active=0 order by id Desc ");
                     while ($rows=mysqli_fetch_array($query)) {
                       ?>
					<tr>
					<td><?php echo htmlentities($rows['AdminUserName']); ?></td>
					<td><?php echo htmlentities($rows['AdminEmailId']); ?></td>
					<td><?php echo htmlentities($rows['pid']); ?></td>
					
					<td><a href="diagramverify.php?pid=<?php echo htmlentities($rows['id'])?>" class="btn btn-success">Approve</a><a href="diagramverify.php?nid=<?php echo htmlentities($rows['id'])?>" class="btn btn-danger">Reject</a></td>
					</tr>
					<?php } ?>
					</table>
					
                </div>
                
                <div class="col-md-2"></div>
            </div>
        </div>
         
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>
<?php } ?>