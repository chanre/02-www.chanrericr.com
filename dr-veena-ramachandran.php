<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr. Veena Ramachandran</h1></div><br>
             <div class="row">
                 <div class="col-md-3"> <img src="images/doctor/veena.jpg" class="card-img" alt="..." height="230"></div>
                 <div class="col-md-9">

                     <p>
                         <strong>MBBS,MD(General Medicine)</strong>
 Dr. Veena Ramachandran is currently a Fellowship Student in Immunology & Rheumatology at the ChanRe Rheumatology & Immunology Center & Research. She has passed her MBBS degree from A. J. Institute of Medical Sciences and Research Center, Mangalore and completed her MD in General Medicine from Vydehi Institute of Medical Sciences and Research Center in Bangalore.
                     </p>
                     <p>
                        Dr. Veena has acquired valuable experience as a Junior Resident at Vydehi Hospital post her graduation. Moreover, after completing her MD, she also worked as a Consultant Physician in Bangalore and Hyderabad for 2 years.
                     </p>
                     <p>
                       Beyond academics & work, she has had the opportunity to present posters, deliver presentations and participate in various state conferences ever since graduation.
                     </p>
                     <p>Her current research project at ChanRe involves assessment of "Temporal profile of patients with Rheumatoid arthritis for more than 5 years- outcomes and treatment compliance".</p>
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>