<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Privacy Policy</title>
   <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
     <style type="text/css">
         p{
                font-family: times;
                font-weight:;
                font-size: 18px;

         }
         .image{
            padding: 10px;
            overflow: hidden;
         }
     </style>



</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
    <?php include ('layout/header.php'); ?>
    <div class="container">
        <br>
        <h1 class="Rheumatology" align="center">Privacy Policy</h1>
        <div class="row">
            
            <div class="col-md-10">

                <p class="about">
                <strong>ChanRe Rheumatology and Immunology Center and Research (CRICR)</strong> has created the following privacy statement in order to demonstrate its policy on privacy, and also to endeavour full compliance with the extant Indian data privacy laws, including, without limitation, the Information Technology Act, 2000 and the Information Technology (Reasonable Security and Procedures and Sensitive Personal Data or Information) Rules, 2011, which govern its collection and/or disclosure of the sensitive personal data or information submitted by its users/customers, if any.
                </p>
                <p class="about">
                CRICR encourages you to read this privacy statement before using this website, https://chanrericr.com/ (“Website”) and Intouch App. By using this Website, you agree to the terms and conditions given in this privacy statement and the Terms & Conditions available on the Website. If you do not agree to the terms and conditions of this statement and the Terms & Conditions, please do not use this Website.
                </p>
        
                <p class="about">
                  The following discloses our information gathering and dissemination practices for the Website. This Website may contain links to other sites. CRICR is not responsible for the privacy practices or the content of such other websites.
                </p>
                <p class="about">
                   We use your IP address to help diagnose problems with our server and/or to administer our website. This gives us an idea of which parts of our site users are visiting. We do not link IP addresses to anything personally identifiable. This means that a user’s session will be tracked, but the user will be anonymous.
                </p>
                <p class="about">
                 You can visit most pages on our site without giving us any information about yourself. However, our website may at various occasions require you to give us contact information like your name, email address and other personal information.
                </p>
                <p class="about">
                 As a general practice, no sensitive personal information or data is collected about or from you without your consent, apart from any information or data voluntarily submitted by you including while making purchases or using services at the Website and applications, in contacting CRICR through the Website, answering surveys and polls, entering contests and similar promotions and singing up for updates. Voluntary provision of information by you, including by the use of our website, will be deemed as consent. 
                </p>
                <p class="about">
                 Your personal information or data may be collected if you wish to create your account on our Website or participate in any other membership program(s) or contests or promotions, through which you may purchase any products or services offered by us, receive discounts, promotions, participate in any contests, etc. Your payment information, such as credit card number and expiration date ("Payment Information"), will be collected by the respective banks and /or third party payment gateway, if you wish to make any purchases at the Website. This privacy statement will not apply to such banks and/or third party payment gateways.
                </p>
                <p class="about">
                 The nature of personal information or data usually received by us, includes:
                </p>
                <ul>
                    <li>Information about your personal details, viz., name, address, telephone/mobile number, e-mail, etc.</li>
                    <li>Information, which you voluntarily disclose to us by e-mail or other means, whether pursuant to a contract or otherwise, including through the Contact Us link on our website,</li>
                    <li>Certain types of information, automatically received and stored whenever you network with us, e.g., through “cookies”, called "pixel tags", "web beacons", "clear GIFs" or similar means, as discussed in detail below</li>
                    <li>All such information that is made available to us by your browser when you visit our website, viz., date and time of the enquiries put forth by you, your browser language, etc.</li>
                </ul>
                <p class="about">This privacy statement does not apply to any non-personal information disclosed by you or collected by us or any personal information or data which is in public domain or which is disclosed pursuant to law.</p>
                <p class="about">We use your contact information only to respond to your queries or send you information about our company and/or any offers or promotions CRICR may be offering and/or its partners. Users may opt-out of receiving future mailings by choosing to request the same to us by return mail. We may use your personal information to make this website more relevant for your needs and create and publish content that may be useful to you. We do not retain any personal information or data received by us longer than it is necessary for achieving the corresponding purpose, unless otherwise required under any applicable laws.</p>
                <p class="about">We occasionally engage other parties to provide limited services to us or on our behalf. To the extent permitted by law, we will only provide those other parties the information they need to deliver the service, and they are prohibited from using that information for any other purpose.</p>
                <p class="about">CRICR may disclose your personal information if required to do so by law, or if required to be disclosed to any Government agencies who are mandated under law to obtain such information for the purpose of verification of identity, or for prevention, detection, investigation including cyber incidents, prosecution and punishment of offences, or in the good-faith belief that such action is necessary to</p>
                <ul>
                    <li>Conform to the edicts of the law or comply with legal process served on CRICR or the site,</li>
                    <li>Protect and defend the rights or property of CRICR and its family of websites, or</li>
                    <li>Act in urgent circumstances to protect the personal safety of CRICR employees, users of CRICR’s products or services, or members of the public</li>
                </ul>
                <p class="about">To the extent required by applicable laws, your consent will be obtained prior to disclosure of any sensitive personal data or information collected from you, which consent may be given by you by implication or specification, including in terms of your use of our website and implied awareness of the terms of this privacy statement governing such use.</p>
                <p class="about">CRICR may use your information for internal business purposes, such as analyzing and managing its businesses. Your information collected from the Website may be combined with other information collected by CRICR via other online or offline sources, with demographic information and other information that is available in the public domain.</p>
                <p class="about">Your information may be stored and processed in India or any other country in which CRICR or its affiliates, subsidiaries or agents maintain facilities, and by using this site, you consent to any such transfer of information outside of your country.</p>
                <p class="about">When you give us personal information, CRICR will not share that information with third parties without your permission, other than for the limited exceptions already listed or if necessary for the performance of a contract with you. It will only be used for the purposes stated above</p>
                <p class="about">In the event that another company acquires all or substantially all of the assets of CRICR’s business through a consolidation, merger, asset purchase or other transaction, we, upon taking consent from you, will have the right to transfer all data (including any sensitive personal information or data) that is in our possession or under our control to such acquiring party and thereafter, the privacy policy of such acquiring party shall govern all such data and any personal information or data that is transferred to the acquiring party.</p>
               
                <p class="about">Web beacons, also known as clear gif technology or action tags, may be used to assist in delivering the cookie on our site. This technology tells us how many visitors clicked on key elements (such as links or graphics) on a web page. We do not use this technology to access your personal information on https://chanrericr.com/, it is a tool we use to compile aggregated statistics about https://chanrericr.com/ Website usage. We may share aggregated site statistics with partner companies but do not allow other companies to place clear gifs on our sites.</p>
                <p class="about">This Website uses technology from its partners for the purpose of collecting and storing these cookies, for marketing and optimization purposes. These cookies do not contain any personal or private details (such as preferences, payment details, contact details, travel trends etc.) and are virus-free. The use of the cookies also targets to deliver advertisements pertaining to your choice and interests, and may also be placed on other websites by our advertising network partners</p>
                <p class="about">Opting Out of Cookies: If you have any concerns about our use of cookies from https://chanrericr.com/, you can take action to prevent cookies being set, including changing your browser settings to block such cookies. Advertising companies also enable you to opt-out of receiving targeted advertising, if you would prefer to do so. In the event you opt-out, you will neither experience a personalized visit nor will you be able to subscribe to the service offerings on the site.</p>
                <p class="about">Further, if you intend to withdraw your consent at any time for usage of any personal information or data provided by you, we will respect your decision and not use such information. However, your intention of withdrawing consent for use of your personal information or data by us should be in writing, and in such a case, we will be under no obligation to provide or continue providing the services for which such information was provided.</p>
                <p class="about">This site has security measures in place to protect the loss, misuse, and/or alteration of information under our control. To protect sensitive personal information or data against accidental or unlawful destruction, loss or modification and against unauthorized disclosure or access, we have implemented various security practices that are updated and audited on a regular basis in accordance with applicable law. The data resides behind a firewall, with access restricted to authorized CRICR personnel. Also, our servers and workstations are protected by antivirus softwares, which are updated regularly to minimize the threats of any virus, malware or Trojan attacks and are located in secure data facilities (with limited access) to further increase security.</p>
                <p class="about">Please be informed that despite these reasonable efforts to protect personal data on our servers, no method of transmission over the Internet is guaranteed to be secure. Therefore, while we strive to protect your personal information or data at all times, we cannot guarantee its absolute security and shall not be liable for any breach of security by an outside party, including where an unauthorized access, modification, destruction or disclosure of your personal information or data is attributable to any acts of an outside party, or any breach of security, virus and data protection protocols at your end or on the computer/mobile system that you use to access our website. It is also important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when you are done using a shared computer</p>
                <p class="about">CRICR will occasionally update this privacy statement.</p>
                <p class="about">For any queries related to this privacy statement, correction of any personal information or data provided by you or for making a complaint about breach of privacy, you may please contact the official of CRICR </p>
            </div>
            <!-- <div class="col-md-4"> 
                <div class="image"> <img src="images/about.jpg" width="300"></div>
               
            </div>-->
        </div>
    </div>



    <?php include ('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>