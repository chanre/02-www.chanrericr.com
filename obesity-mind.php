<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Influence of Diet on Obesity </title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
    <meta name="description"
        content="There is no advisory against vaccinating patients with autoimmune diseases, there is no contraindication to vaccinate patients on immunosuppressive drugs, unless they have severe allergic reaction. " />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />

    <!-- Responsive -->
    <link rel="stylesheet" href="css/responsive.css" />

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-158330329-1');
    </script>
    <style>
    .postImage {

        display: flex;
        justify-content: center;

    }

    .postImage img {
        width: 50%;
        height: 350px;
        margin: 10px;

    }
    </style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>

<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
        <?php include ('layout/header.php'); ?>
        <div class="container">
            <br>
            <div>
                <h4 align="center">Influence of Diet on Obesity</h4>
            </div>

            <br>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <p>Obesity is a complex disease involving an excessive amount of body fat. It can not only impact
                        one’s physical well-being but even mental well-being too. Obesity leads to heart problems,
                        diabetes, high blood pressure, kidney and liver problems, and sleep apnea.. Similarly mental
                        health conditions may lead to a person’s weight gain Both obesity and mental health problems go
                        hand in hand. It is important to follow healthy lifestyle practices to manage weight and mental
                        health issues.</p>
                    <div class="postImage">
                        <img src="img/obm4.jpg" alt="">
                    </div>
                    <h3>Why is obesity always a concern?</h3>
                    <p>There is a long-term relation between obesity and mental health issues. The most probable
                        contributing factors to obesity are genetic, psychological, environmental, social, and cultural
                        influences. Obesity isn’t just about physical appearance It is a medical condition that
                        increases one’s risk of other health problems. Approximately 70-80% of obese patients have
                        mental health issues.
                        Obesity leads to stress, poor self-esteem, and depression.people may become introverts, they
                        stay at home, and their productivity will also decrease.
                    </p>

                    <h3>Fat loss is not a physical challenge ,it's a mental one.</h3>
                    <h2>Obesity is tied to mental health issues</h2>


                    <p>Obesity leads to emotional distress. Being overweight may cause negative feelings about one’s
                        self. Research says that obesity is related to mental disorders and many of the medications used
                        to treat psychiatric illnesses. Stress is an important factor in both depression and obesity.
                        Chronic stress and anxiety, for example, can lead to depression. Similarly stress, depression,
                        or bipolar disorder can make one more likely to turn to food as a coping mechanism that can lead
                        to weight gain and eventually obesity. Obesity increases pain and inflammation in the body which
                        causes stress.</p>
                    <h3>The body achieves what the mind believes</h3>

                    <p>Being obese can lead to social isolation, poor self-esteem, loneliness, and frustration. Because
                        of body shaming. Which may lead to depression in such people. People who are obese tend to
                        experience anxiety over being judged for how they look and aren’t satisfied with their
                        appearance. If one is overweight, he/she may feel frustrated, angry, or upset. Being aware of
                        difficult emotions is the first step in dealing with them. It takes practice to recognise
                        emotions. Sometimes they can be so sudden and powerful that it’s hard to sort out exactly what
                        you’re feeling.</p>

                    <h3>Stress and weight</h3>
                    <div class="postImage">
                        <img src="img/obm5.jpg" alt="">
                    </div>
                    <p>During stress, the hormone cortisol rises. This can turn your overeating into a habit. Because
                        increased levels of the hormone also help cause higher insulin levels, your blood sugar drops
                        and you crave sugary, fatty foods.So instead of a salad or a banana, you’re more likely to reach
                        for cookies or mac and cheese. That’s why they’re called “comfort foods".</p>
                    <h3>Binge eating</h3>
                    <div class="postImage">
                        <img src="img/obm3.jpg" alt="">
                    </div>
                    <p>Binge eating involves regularly eating a lot of food over a short period of time until you're
                        uncomfortably full.Binges are often planned in advance, usually done alone, and may include
                        "special" binge foods. You may feel guilty or ashamed after binge eating.Men and women of any
                        age can get binge eating disorder, but it usually starts in the late teens or early 20s.
                        Number of studies have demonstrated a positive association between obesity and various mental
                        health issues, including depression, eating disorders, anxiety, and substance
                        abuse. Obesity impacts individuals’ quality of life, with many sufferers experiencing increased
                        stigma and discrimination because of their weight.
                    </p>
                    <h3>MIND CAN CONTROL YOUR OBESITY</h3>
                    <div class="postImage">
                        <img src="img/obm1.png" alt="">
                    </div>
                    <p>Willpower is totally overrated! Every time it’s not about willpower, but mind training that is
                        the first step required in any weight loss program. Apart from the body, we need to train our
                        mind to be disciplined and keep pushing ourselves with small motivation. A proactive thought of
                        a wholesome meal plan can change the game and toss the ball in your court.you will find it
                        easier to follow the nutrition protocols once you are in the right frame of mind.</p>
                    <h3>It doesn't matter how slowly you go, as long as you don't stop</h3>
                    <b>Tips for improving your physical and mental health</b>
                    <p>1. Clear your mind <br>
                        2. Believe in what your doing <br>
                        3. Eliminate negative thinking <br>
                        4. Dedication <br>
                        5. Set goals and positive affirmation <br>
                        6. Practice good sleep <br>
                        7. Release stress <br>
                        8. Practice mindfulness <br>
                        9. CBT Cognitive behavior therapy
                    </p>
                    <div class="postImage">
                        <img src="img/obm2.jpg" alt="">
                    </div>
                    <h3>You have the power to shape your life </h3>
                </div>
            </div>

            <br>
        </div>
        <?php include('layout/footer.php'); ?>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>