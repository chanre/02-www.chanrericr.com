<?php
 session_start();
//Database Configuration File
include('config.php');
//error_reporting(0);


if(isset($_POST['login']))
  {
    $uname=$_POST['username'];
    $password=$_POST['password'];

$sql =mysqli_query($con,"SELECT pid,AdminUserName,AdminEmailId,AdminPassword FROM patientdiagram WHERE (pid='$uname' || AdminEmailId='$uname') and Is_Active=1");
$num=mysqli_fetch_array($sql);
 

if($num>0)
{
$hashpassword=$num['AdminPassword'];
if (password_verify($password, $hashpassword)) {
$_SESSION['login']=$_POST['username'];
    echo "<script type='text/javascript'> document.location = 'diagram.php'; </script>";
  } else {
echo "<script>alert('Wrong Password');</script>";
 
  }
}
else{
echo "<script>alert('User not registered with us');</script>";
  }
 
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
       .slider {
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
      background-image: linear-gradient(to right, #ffc107 , #dc3545);
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}
a{	width:100%;

text-align:center;
	display:inline-block;
	position:relative;
	padding:10px 30px;
	text-decoration:none;
	text-transform: uppercase;
	font-weight:500;
	letter-spacing:2px;

	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}



#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}

#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
	font-family:times;
}
img{
	
	width:98%;
}
    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
  <div id="navbar">
  <a class="active" href="/multiapp.php">Chanre</a>
  
</div>

    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       

       <div class="container-fluid">
            <div class="row">
			<div class="col-md-4"></div>
                <div class="col-md-4">
                  
                    <br><br>
					<a href="#" class="btn btn-lg btn-success btn-block">Patient Login</a>
					<form method="Post" style="background-color:#5381d57d;padding:20px;">
					
					<table style="width:100%;">
					<tr>
					<td>User ID</td>
					<td><input class="form-control" type="text" required="" name="username" placeholder="Enter Patient Id" autocomplete="off"></td>
					</tr>
					
					<tr>
					<td> </td>
					</tr>
					
					<tr>
					<td>Password</td>
					<td><input class="form-control" type="password" name="password" required="" placeholder="Enter Password" autocomplete="off"> </td>
					</tr>
					<tr>
					<td> </td>
					</tr>
					<tr>
					<td></td>
					<td><button class="btn btn-lg btn-primary btn-block"  type="submit" name="login">Login</button>  </td>
					</tr>
					<tr>
					<td></td>
					<td></td>
					</tr>

					</table>
					</form><br>
					<div>
					<a href="diagram_user.php" >Register</a>   <br>
					</div><br><div>
					<a href="diagram1.php" >Click Here for Admin Login</a>
					</div>
          <br><div>
          <a href="requestReset.php" >Forgot Password..</a>
          </div>
                </div>
                
                <div class="col-md-2"></div>
            </div>
        </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="jquery.min.js"></script>
<script src="jquery.multiselect.js"></script>

    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>


</body>

</html>
