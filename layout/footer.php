  <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="form-box">
                            
							  <div class="social-icons">
                                <ul>
                                    <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="javascript:;"><i class="fa fa-linkedin"></i></a></li>
                               
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 col-xl-3">
                        <div class="single-widget single-widget-margin-top">
                            <div class="widget-title">
                                <h2>Site Links</h2>
                            </div>
                            <div class="footer">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="doctor.php">Doctor</a></li>
                                    <li><a href="about_us.php">About Us</a></li>
                                    <li><a href="https://www.research-assist.com/">Reasearch</a></li>
                                    <li><a href="symptom-checker-web.php">Symptom Checker</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                    <li><a href="career.php">Career</a></li>
                                     <li><a href="clinic.php">Clinic</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="single-widget">
                            <div class="widget-title">
                                <h2>Quick Links</h2>
                            </div>
                            <div class="footer">
                                <ul>
								    <li><a href="https://office.chanrericr.com/">Doctor Login</a></li>
								    <li><a href="admin.php">Hospital Staff Login</a></li>
                                    <li><a href="https://mychanreclinic.com">My ChanRe Clinic</a></li>
                                    <li><a href="https://www.chanrediagnostic.com/">ChanRe Diagnostic Laboratory</a></li>
                                    <li><a href="https://www.research-assist.com/">Research</a></li>
                                    <li><a href="https://www.chanrejournals.com/">ChanRe Journals</a></li>
                                    <li><a href="https://www.chanrebookshop.com/">ChanRe Book Shop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4 col-xl-3">
                        <div class="single-widget">
                            <div class="widget-title">
                                <h2>Contact</h2>
                            </div>
                            <div class="footer">
                                <p><span>Address :</span><br>No. 414/65, 1st Block, 20th Main, West of Chord Road, Rajajinagar, Bangalore-560010    </p>
                                <p><span>Phone:</span> 08042516699/9632533122
                                </p>
                                <p><span>Email :</span>
                                    <a href=""> info@chanrericr.com</a>
                                </p>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <h6 align="center">Copyright &#169; 2020 <a href="javascript:;">ChanRe Rheumatology and Immunology Center and Research</a>. All Rights Reserved.</h6>
            </div>
        </div>
		<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>