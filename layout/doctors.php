<div class="container">
    <br>
    <div class="row ">

        <div class="col-md-8">
            <h4 class="Rheumatology">Rheumatology & Autoimmune Diseases</h4>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/Chandrashekara.jpg" class="card-img" alt="...">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr.Chandrashekara .S</h5>
                            <p class="card-text">MD, DNB, DM</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-chandrashekhera.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/beenish.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr.Beenish Nazir</h5>
                            <p class="card-text">MBBS,DNB,PGDR</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-beenish-nazir.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!--  <div class="card mb-3" >
            <div class="row no-gutters">
              <div class="col-md-4">
                <img src="images/doctor/fahad.jpeg" class="card-img" alt="..." height="230">
              </div>
              <div class="col-md-5">
                <div class="card-body">
                  <h5 class="card-title">Dr. Sayid Fahad Nizar Ahamed</h5>
                  <p class="card-text">MD (General Medicine)</p>
                </div>
              </div>
              <div class="col-md-3" align="center" style="margin-top: 70px;">
                <a href="dr-sayid-fahad.php" class="btn btn-info btn-lg">View Profile</a>
              </div>
            </div>
          </div> -->
            <!-- <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/shruti2.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Shruthi Desai</h5>
                            <p class="card-text">MBBS, MRCP</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="drshruti.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div> -->
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/dhanashree.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Dhanashree Gavali </h5>
                            <p class="card-text">MBBS, DNB (General Medicine)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-dhanashree.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/yogitha.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Yogitha Chennupati</h5>
                            <p class="card-text">MBBS, MD (General Medicine)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-yogitha.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="image">
                <img src="images/about.jpg" width="300">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <br>
    <div class="row ">
        <div class="col-md-8">
            <div class="card mb-3">
                <h4 class="Rheumatology">Allergy & Clinical Immunology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/smitha.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Smitha J N Singh</h5>
                            <p class="card-text">MD (Physiology)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-smitha-singh.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Reproductive immunology & High risk pregnancy</h4>
                <!-- <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nandyala.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Nandyala Sundari</h5>
                            <p class="card-text">MBBS, DNB, PGDMLE</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-nandyala.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div> -->
                <!-- <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/Chandrashekara.jpg" class="card-img" alt="...">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr.Chandrashekara .S</h5>
                            <p class="card-text">MD, DNB, DM</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-chandrashekhera.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div> -->
            </div>

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/dr-chaitra.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Chaitra S Nirantara</h5>
                            <p class="card-text">MBBS, MS OBG, DNB, PGDMLE</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-chaitra.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <!-- =============================================================== -->

            <!-- =================================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Physiotherapy & Chronic pain Management</h4>
                <!-- <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/sasi.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Mr. Sasikumar. M</h5>
                            <p class="card-text">MPT(Ortho), PG. D.Acu, F.ACC.A, CDNT<br></p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-shasikumar.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div> -->
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/rajkannan.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Rajkannan. P</h5>
                            <p class="card-text">MPT, CDNT, D.(FM), Dip. (Acu)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-rajkannan.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/yogarani.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Ms Yogarani </h5>
                            <p class="card-text">BPT, Dip(Acu) ,DNT</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-yogarani.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!--=============================================================== -->
            <!--=============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Ophthalmology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Usharani</h5>
                            <p class="card-text">MBBS, DOMS (Fellowship in Glaycoma) (Consultant Ophthalmologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!--=============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Radiology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Shivanand N D</h5>
                            <p class="card-text">MBBS</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-shivanand.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr.Shivakumar S M </h5>
                            <p class="card-text">MBBS,DMRD (Consultant Radiologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-shivakumar.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Cardiology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/vikram.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Vikram Kolhari</h5>
                            <p class="card-text">MD, DM</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-vikram.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">

                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nagesh.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Nagesh M B</h5>
                            <p class="card-text">MBBS, DIP CARD </p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-nagesh.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Nephrology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/vinod.jpeg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr Vinod Nagesh </h5>
                            <p class="card-text">MD, DM (Nephrology)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-vinod.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Pulmonology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Ramesh R </h5>
                            <p class="card-text">MBBS, DTCD (Consultant Pulmonologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-ramesh.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr.Arjun A S </h5>
                            <p class="card-text">MBBS,MD (Respiratory Medicine) (Consultant Pulmonologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-ramesh.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Vascular Surgeon</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Chandrashekar A R </h5>
                            <p class="card-text">MS,FIVS,FVES,FIES (Consultant Vascular Surgeon)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>


            <!-- ================================================================ -->
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">ENT</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Tejamurthy B V </h5>
                            <p class="card-text">MBBS MS (Consultant ENT)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>


            <!-- ================================================================ -->
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Psychiatry</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Chandrashekar M </h5>
                            <p class="card-text">MBBS,MD,MS,DNB (Consultant Psychiatrist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Sushma </h5>
                            <p class="card-text">MBBS,MD (Consultant Psychiatrist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div> -->
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/dr-adarsh.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr.Adarsh B </h5>
                            <p class="card-text">MBBS,MD (Consultant Neuro Psychiatrist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-adarsh.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>


            <!-- ================================================================ -->
            <!-- <div class="card mb-3">
                <h4 class="Rheumatology">Diabetology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Radharanga Rajan </h5>
                            <p class="card-text">MBBS,PGDHSC (Consultant Diabetologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div> -->
            <!--=============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Gastroenterology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Abhijith B R</h5>
                            <p class="card-text">MBBS, MD DM (Consultant Gastroenterologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- ================================================================ -->
            <div class="card mb-3">
                <h4 class="Rheumatology">General Surgery</h4>

            </div>
            <div class="card mb-3">

                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Nagendra K</h5>
                            <p class="card-text">MBBS, MS </p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">

                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Manojith S S</h5>
                            <p class="card-text">MBBS, MS (General Surgeon), FSGE (Surgical Gastroenterology)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- ============================================================================================ -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Dental</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/dilip.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Dilip Bharadwaj</h5>
                            <p class="card-text">BDS, (Consultant Dentist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-dilip.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <h4 class="Rheumatology">Dermatologist</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/sushmita.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Sushmita E. S </h5>
                            <p class="card-text">MBBS,MD (Consultant Dermatologist)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="drsushmita.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>


            <div class="card mb-3">
                <h4 class="Rheumatology">Urology</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nagarajaiah.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Nagarajaiah Narayanaswamy</h5>
                            <p class="card-text">MBBS,MS ,MCH (Urology) (Consultant Urologist)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-nagarajaiah.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>








            <!-- ========================================================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Orthopaedics</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Balasubramanyam </h5>
                            <p class="card-text">MBBS,MS (Consultant Orthopaedician)</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/venu.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Venu Madhav</h5>
                            <p class="card-text">MS Ortho, Fellow-Joint Replacement Surgery</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-veenu-madhab.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/prabhu.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. C. B. Prabhu (Knee & Large Joints)</h5>
                            <p class="card-text">D’Ortho, DNB, FAO, AOAA.</p>

                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dr-prabhu.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Darshan Kumar A Jain</h5>
                            <p class="card-text">MBBS,MS Ortho,SNB (Consultant Hand Surgeon)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/nopics.jpg" class="card-img-top" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr. Kodlady Surendar Shetty </h5>
                            <p class="card-text">MBBS D' ORTHO (Consultant Spine Surgeon)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="#" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- =========================================================================================== -->
            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Counsellor</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/dhanalakshmi.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Ms.Dhanalakshmi N</h5>
                            <p class="card-text">M.Sc. Clinical Psychology (Counsellor)</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="dhanalakhsmi.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Dietician</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/ruby.jpeg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Ms. Ruby Malya</h5>
                            <p class="card-text">M.Sc Food Science & Nutrition, FSM & Dietetics  </p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="ruby.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>

            <!-- =============================================================== -->
            <div class="card mb-3">
                <h4 class="Rheumatology">Yoga</h4>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="images/doctor/sandesh.jpg" class="card-img" alt="..." height="230">
                    </div>
                    <div class="col-md-5">
                        <div class="card-body">
                            <h5 class="card-title">Dr Sandesh S Biradar</h5>
                            <p class="card-text">BNYS, PgC.Acu.</p>
                        </div>
                    </div>
                    <div class="col-md-3" align="center" style="margin-top: 70px;">
                        <a href="sandesh.php" class="btn btn-info btn-lg">View Profile</a>
                    </div>
                </div>
            </div>
            <!-- =============================================================== -->
            


        </div>

        <div class="col-md-4">


        </div>

    </div>
</div>