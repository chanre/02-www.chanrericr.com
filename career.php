<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5fa0d74ce019ee7748f033a5/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-158330329-1');
    </script>
    <style type="text/css">
        .career-img img{
            width: 100%;
        }
    </style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>

<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
        <?php include ('layout/header.php'); ?>
        <div class="container">
            <div class="row">

                <div class="col-md-2"></div>
                <!-- <div class="col-md-8">
                    <div class="accordion" id="accordionExample">

                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h1 class="mb-0">
                                    <a class="btn collapsed text-white" type="button" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                        Medical Writer
                                    </a>
                                </h1>
                            </div>
                            <div id="collapseThree" class="collapse show" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-body">
                                            <h4>Roles and Responsibilities</h4>
                                            <p>
                                                Edit different document types for global pharmaceutical clients,
                                                including but not limited to
                                                checks on language and grammar, technical terminology, and accuracy of
                                                data
                                            </p>
                                            <ul class="offset-2" style="text-decoration: none;">

                                                <li>Format documents based on journal/congress guidelines</li>
                                                <li>Ensure that the document adheres to specified style guides</li>
                                                <li>Consistently deliver a high-quality edit, exceeding client
                                                    expectations every time</li>
                                                <li> Contribute to team initiatives like knowledge sharing exercises and
                                                    documentation of</li>
                                                <li>resources</li>
                                                <li>Create content for the website with respect to health care and also
                                                    patient awareness talks description with reference to the topics
                                                    discussed</li>
                                                <li> High level of attention to detail and superior English-language
                                                    skills</li>
                                                <li>Basic knowledge of biostatistics</li>
                                                <li>Literature reviewing and evaluation capabilities</li>
                                                <li>Ability to multi-task and work under tight timelines in a
                                                    high-pressure environment</li>
                                                <li>Check that the content is free from plagiarism and copyright
                                                    infringements.</li>
                                                <li>Deliver all the projects/tasks within the agreed timelines.</li>
                                                <li>Interact effectively with internal teams and external to ensure
                                                    quality and smooth</li>
                                                <li>progression of projects.</li>
                                            </ul>

                                        </div>
                                        <h4>Experience: 2 Years</h4>
                                        <div class="offset-md-1">
                                            <h6>Send your Resume: info@chanrericr.com</h6>
                                            <h6>Contact: +91 9535056289 / 08042516635</h6>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingThre">
                                <h2 class="mb-0">
                                    <a class="btn collapsed text-white" type="button" data-toggle="collapse"
                                        data-target="#collapseThree1" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Digital Marketing Executive
                                    </a>
                                </h2>
                            </div>
                            <div id="collapseThree1" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-body">
                                            <h4>Roles and Responsibilities</h4>

                                            <ul>
                                                <li>Work on creating and executing a strong performance marketing
                                                    strategy & execution plan</li>
                                                <li>Manage the strategy and setup of all FB and Google campaigns</li>
                                                <li>Secure leads and sales using FB and Google Ads</li>
                                                <li>Work on creating a media plan for multiple platforms</li>
                                                <li>Work on examining variations to determine the best performing ads
                                                </li>
                                                <li>Work on regular monitoring and daily reporting of results of all
                                                    campaigns</li>
                                                <li>Measure and optimize our campaigns</li>
                                                <li>Collaborate with different internal teams to have a consistent brand
                                                    voice and message across all paid programs</li>
                                                <li>Stay up-to-date with digital marketing trends and potential new
                                                    channels and strategies to keep us ahead
                                                </li>

                                            </ul>
                                            <h4>Experience: 2 Years</h4>
                                            <div class="offset-md-1">
                                                <h6>Send your Resume: info@chanrericr.com</h6>
                                                <h6>Contact: +91 9535056289 / 08042516635</h6>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="card">
                            <div class="card-header" id="headingThre">
                                <h2 class="mb-0">
                                    <a class="btn collapsed text-white" type="button" data-toggle="collapse"
                                        data-target="#collapseThree3" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Clinical Psychologist /Counsellor
                                    </a>
                                </h2>
                            </div>
                            <div id="collapseThree3" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-body">
                                            <h4>Responsibilities </h4>
                                            <ul>
                                                <li>Provide assessment and recommendations for appropriate treatments
                                                </li>
                                                <li>Diagnose and treat various mental, emotional, and behavioral
                                                    disorders</li>
                                                <li>Conduct case management and other administrative tasks as needed
                                                </li>
                                                <li>Identify opportunities for efficiencies in the work process and
                                                    innovative approaches to completing the scope of work</li>
                                                <li>Interview patients, perform diagnostic tests and give individual,
                                                    family, and group psychotherapy</li>

                                            </ul>
                                            <h4>Skills Required </h4>
                                            <ul>
                                                <li>Interpersonal Skills</li>
                                                <li>Communication Skills </li>
                                                <li>Problem-solving</li>
                                                <li>public speaking, and listening skills</li>
                                                <li>Critical thinking skills</li>
                                                <li>Empathy</li>
                                                <li>Trustworthiness</li>
                                            </ul>
                                            <h4>Education Requirement & Qualification</h4>
                                            <p>Bachelor's degree in psychology or a related field</p>
                                            <h4>Experience: 2 Years</h4>
                                            <div class="offset-md-1">
                                                <h6>Send your Resume: info@chanrericr.com</h6>
                                                <h6>Contact: +91 9535056289 / 08042516635</h6>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>


                        <div class="card">
                            <div class="card-header" id="headingThre">
                                <h2 class="mb-0">
                                    <a class="btn collapsed text-white" type="button" data-toggle="collapse"
                                        data-target="#collapseThree4" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        Dietician Job Description
                                    </a>
                                </h2>
                            </div>
                            <div id="collapseThree4" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordionExample">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card-body">
                                            <h4>Responsibilities </h4>
                                            <ul>
                                                <li>Speaking to patients to learn about their health goals and dietary
                                                    needs, preferences & restrictions
                                                </li>
                                                <li>Developing nutrition plans for patients </li>
                                                <li>Compiling information and tracking client progress towards their
                                                    health goals
                                                </li>
                                                <li>Speaking to groups to promote healthy eating habits and proper
                                                    nutrition</li>


                                            </ul>
                                            <h4>Skills Required </h4>
                                            <ul>
                                                <li>Exceptional communication </li>
                                                <li>problem-solving</li>
                                                <li>public speaking, and listening skills</li>
                                                <li>Compassion and genuine interest in helping others develop better
                                                    eating habits</li>
                                                <li>Critical thinking skills</li>
                                                <li>Empathy</li>
                                                <li>Trustworthiness</li>
                                            </ul>
                                            <h4>Education Requirement & Qualification</h4>
                                            <p>Bachelor's degree in psychology or a related field</p>
                                            <p>More education or experience may be preferred</p>
                                            <h4>Experience: 2 Years</h4>
                                            <div class="offset-md-1">
                                                <h6>Send your Resume: info@chanrericr.com</h6>
                                                <h6>Contact: +91 9535056289 / 08042516635</h6>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>





                    </div>


                </div> -->
                <div class="col-md-8">
                    <div class="career-img">
                        <img src="img/vacancy.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>

    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>