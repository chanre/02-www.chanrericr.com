<!DOCTYPE HTML>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>ChanRe Rheumatology and Immunology Center and Research</title>
  <link rel="shortcut icon" href="images1/Refined/chanre1.png" />

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css" />

  <!-- Font Awesome CSS -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

  <!-- Style CSS -->
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/doctor.css" />
  <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script> -->

  <!-- Global site tag (gtag.js) - Google Ads: 10865901727 -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-10865901727"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'AW-10865901727');
  </script>

  <script>
    window.addEventListener('load', function() {
      if (window.location.pathname.match('/academic.php')) {
        gtag('event', 'conversion', {
          'send_to': 'AW-10865901727/wlXZCIrnlL4DEJ-Bor0o'
        });
        jQuery('a:contains(view Details)').click(function() {
          gtag('event', 'conversion', {
            'send_to': 'AW-10865901727/nQPoCLSW4L0DEJ-Bor0o'
          });
        })
        jQuery('a:contains(Download Application Form)').click(function() {
          gtag('event', 'conversion', {
            'send_to': 'AW-10865901727/Bf7MCKGjlL4DEJ-Bor0o'
          });
        })
      }
    })
  </script>





  <!-- Responsive CSS -->
  <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
    *{
      margin: 0%;
      padding: 0%;
    }
    .card-header {
      background-color: #4f89d0;
      height: 60px;
    }

    .card-body img {
      width: 100%;
    }

    table {
      width: 100%;
      font-weight: bold;
      text-align: center;
      /*margin-top: 10px;*/
      margin-bottom: 30px;
    }
    th{
      border:1px solid black;
    }
    td{
      border: 1px solid black;
    }
    ul {
      list-style: none;

    }

    .text1 {
      text-align: center;
    }

    .earlybird {
      color: red;
      font-weight: bold;
    }

    /*ul li{padding: 5px;}*/
    .title-header {
      padding-bottom: 10px;
      color: white;


    }

    .title-header h2 {
      line-height: 20px;
      font-size: 30px;
    }

    .modal-dialog{
      min-width: 80%;
    }
    th{
      font-size: 15px;
      /*background-color: #4f89d0;*/
      /*color: #fff;*/
      border-color: black;
      
    }
    .accordion, .col-md-12{
      /*background-color: white;*/
    }
	
	.academy>img{
		width: 100%;
	}
  </style>

  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>

<body>
  <!-- Begin wrapper -->
  <div class="wrapper">
    <!-- Begin Top Navbar -->
    <?php include('layout/header.php'); ?>
    <div class="container">
      <br>

      <div class="row">
        <div class="col-md-12">

          <div class="accordion" id="accordionExample">
            <div class="card">
              <div class="card-header" id="headingThree">
                <!-- <div class="title-header">
                  <h1 align="center">Cerification Courses</h1>
                </div> -->

                <a class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                       <div class="title-header">
                  <h2 align="center">Certification Courses</h2>
                </div>
                        </a>

              </div>
              <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-body">

                    <!--   <div align="center">
                        <h3>ChanRe Rheumatology & Immunology Center & Research</h3>
                        <h5>Starting the Certification Courses for the Academic Year 2022-2023</h5>

                        <p align="center" class="text1">In Association with - Sri Siddhartha Academy of Higher Education, Agalakote, Tumkur</p>
                      </div> -->

                      <table >
                        <tr>
                          <th>Title</th>
                          <th>Duration</th>
                          <th>Eligibility</th>
                          <th>Application</th>
                        </tr>
                        <tr>
                          <td>Master Certification Course in Immunology</td>
                          <td>12 Months</td>
                          <td>
                            <ul>
                              <br>
                              <li>M. Sc (Life Science),</li>
                              <li>BE (Biotechnology)</li>
                            </ul>
                          </td>
                          <td><a href="docs/immunology.pdf" class="btn btn-primary">Click Here</a></td>
                        </tr>
                        <tr>
                          <td>Certification Course in Clinical Research</td>
                          <td>12 Months</td>
                          <td>
                            <ul>
                              <br>
                              <li>B. Sc / M. Sc (Life Sciences),</li>
                              <li>BE (Biotechnology),</li>
                              <li>MBBS,BDS</li>
							  <li>B. PHARMA, M. PHARMA</li>
                            </ul>
                          </td>
                          <td><a href="docs/clinres.pdf" class="btn btn-primary">Click Here</a></td>
                        </tr>
                      </table>
                      <!-- <div align="center">
                        <a href="docs/applicationform.pdf" class="btn btn-success btn-round rounded">Download Application Form</a>
                      </div> -->

                      <!-- <div class="container">
                        <div class="row">
                          <div class="col-md-6">
                            <button type="button" class="" data-toggle="modal" data-target="#exampleModal">
                              <img src="img/Picture1.jpg" alt="">
                            </button>

                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <img src="img/Picture1.jpg" alt="">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                          <button type="button" class="" data-toggle="modal" data-target="#exampleModal">
                              <img src="img/Picture2.jpg" alt="">
                            </button>

                           
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <img src="img/Picture2.jpg" alt="">
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div> -->

                      <div class="container">
                        <div class="row">
                          <div class="col-md-12">
                            <img src="img/courses.jpg">
                          </div>
                        </div>
                      </div>

                      <!-- <div class="container">
                        <div class="row">
                          <div class="col-md-6">
                            <table border="2px">
                              <tr>
                                <th>Course</th>
                                <th>Master Certification Course in Immunology</th>
                              </tr>
                              <tr>
                                <td>Field</td>
                                <td>Health and Lifescience</td>
                              </tr>

                              <tr>
                                <td>Course Commencement </td>
                                <td>July - 2022</td>
                              </tr>

                              <tr>
                                <td>Location</td>
                                <td>ChanRe RICR</td>
                              </tr>


                              <tr>
                                <td>Number of students </td>
                                <td>10 per Batch</td>
                              </tr>

                            
                              <tr class="earlybird">
                                <td>Early Bird Registration</td>
                                <td>15th June 2022</td>
                              </tr>
                              <tr>
                                <td>Registration Deadline</td>
                                <td>30th June 2022</td>
                              </tr>
                              <tr>
                                <td rowspan="2">contact</td>
                                <td>Ms. Chandrika <br> Email: secretarialassistant@chanrericr.com</td>
                              </tr>

                              <tr>

                                <td>Mr. Vijendra <br> Email: marketing@chanrejournals.com</td>
                              </tr>
                            </table>
                          </div>
                          <div class="col-md-6">
                            <table border="2px">
                              <tr>
                                <th>Course</th>
                                <th>Certificate Course in Clinical Research</th>
                              </tr>
                              <tr>
                                <td>Field</td>
                                <td>Medicine and Lifescience</td>
                              </tr>

                              <tr>
                                <td>Course Commencement </td>
                                <td>July-2022</td>
                              </tr>

                              <tr>
                                <td>Location</td>
                                <td>ChanRe RICR</td>
                              </tr>


                              <tr>
                                <td>Number of students </td>
                                <td>10 per Batch</td>
                              </tr>
                             
                              <tr class="earlybird">
                                <td>Early Bird Registration</td>
                                <td>15th June 2022</td>
                              </tr>
                              <tr>
                                <td>Registration Deadline</td>
                                <td>30th June 2022</td>
                              </tr>
                              <tr>
                                <td rowspan="2">contact</td>
                                <td>Ms. Chandrika <br> Email: secretarialassistant@chanrericr.com</td>
                              </tr>

                              <tr>

                                <td>Mr. Vijendra <br> Email: marketing@chanrejournals.com</td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                      <h3>Scope of Certification Courses:</h3>
                      <p> <b>Master Certification Course in Immunology:</b> Individual will be trained in Basic Immunology, Applied Immunology including Diagnostic Immunology. They will get hands on experience in some of the laboratory and research techniques like ELISA, Blot, etc. The trained individual will be suitable for placement in Immunology based Industries and Research.</p>
                      <p> <b>Certificate Course in Clinical Research:</b>
                        Individual will be trained in various aspects of Clinical Research with hands on practical internship for 6 months. They will get different career opportunities like Clinical Research Coordinator, Clinical Research Associate, Clinical / Trial Monitor, Clinical Research Scientist, Clinical Safety Analyst, The Clinical Quality Assurance Auditor, Data Manager, etc. and can be employed in Hospitals, Laboratories and Pharmaceutical Companies, etc.
                      </p> -->





                    </div>
                  </div>
                  <!-- <div class="col-md-4" align="right"><img src="images/serv3.png"></div> -->
                </div>

              </div>
            </div>

            <div class="card">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                  <a class="btn" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <div class="title-header">
                  <h2 align="center">Post MD (Gen. Medicine) Fellowship in Rheumatology & immunology</h2>
                   </div>
                    
                  </a>
                </h2>
              </div>

              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="row">
                  <div class="col-md-10">
                    <div class="card-body">
                    <p>Post MD (Gen. Medicine) Fellowship in Rheumatology & immunology</p>
                    </div>
                    <div class="card-footer">
					<div class="academy"><img src="img/academycourse.jpg"></div>
					
                      <a href="img/Fellowship_Application.pdf" class="btn btn-primary btn-round">Application Form</a>
                    </div>
                  </div>

                </div>

              </div>
            </div>

            <div class="card">
              <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                  <a class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                    <div class="title-header">
                  <h2 align="center">Ph.D Programme</h2>
                   </div>
                    
                  </a>
                </h2>
              </div>
              <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="row">
                  <div class="col-md-10">
                    <div class="card-body">
                      Coming Soon..

                    </div>
                  </div>

                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                  <a class="btn collapsed " type="button" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapseThree">
                    <div class="title-header">
                  <h2 align="center">Other Courses</h2>
                   </div>
                    
                  </a>
                </h2>
              </div>
              <div id="collapsefour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="row">
                  <div class="col-md-6">
                    <div class="card-body">
                      Coming Soon..
                    </div>
                  </div>
                  <!-- <div class="col-md-4" align="right"><img src="images/serv4.png"></div> -->
                </div>
              </div>
            </div>



          </div>

        </div>
        <div class="col-md-4">






        </div>
      </div>
    </div>

    <?php include('layout/footer.php'); ?>
    <!-- End Footer Wrapper -->
  </div>
  <!-- End wrapper -->

  <!-- jquery latest version -->
  <script src="js/jquery.min.js"></script>

  <!-- popper.min js -->
  <script src="js/popper.min.js"></script>

  <!-- bootstrap.min js -->
  <script src="js/bootstrap.min.js"></script>

  <!-- ie10 viewport bug-workaround js -->
  <script src="js/ie10-viewport-bug-workaround.js"></script>

  <!-- custom js -->
  <script src="js/custom.js"></script>

</body>

</html>