<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr. Nagarajaiah Narayanaswamy</h1></div><br>
             <div class="row">
                 <div class="col-md-3">  <img src="images/doctor/nagarajaiah.jpg" class="card-img" alt="..." height="230"></div>
                 <div class="col-md-9">

                     <p>  <strong>Expert in the fields of Urology, Andrology & Sexology.</strong>                   
M.B.B.S and master’s in general surgery
Dr. Nagarajaiah Narayanaswamy, after completion of M.B.B.S and master’s in general surgery, pursued his specialization In Urology. In his 20 years of experience, he has performed numerous successful surgeries and treatments in endourology, Reconstructive Urology, Urooncology And Pediatric Urology. He has also treated many cases of Infertility and Erectile Dysfunction. He is credited with the removal of the largest kidney stone and has performed over 20000 surgeries including Lasers, Turps, Urss, Vius, Pcnls, Cancer Surgeries etc.
                     </p>
                     <p>
                        Expertise: Dr. Nagarajaiah Narayanaswamy is an expert in the fields of Urology, Andrology & Sexology. He is a specialist in the following areas:
                     </p>
                     <p>Endourology, Reconstructive Urology, Uro-oncology, Pediatric Urology, Infertility, Erectile Dysfunction Nagarajaiah Narayanaswamy works for Shushruta Urology And Andrology Clinic in Rajajinagar, Bangalore. Sanjeevini Multispeciality Hospital, Mahalakshmi Layout, Bangalore. Panacea Hospital Pvt Ltd - Basaveshwaranagar Nagarbhavi, Bangalore. Padmavathi Hospital Magadi Road, Bangalore. Ramamani Clinic Rajajinagar, Bangalore. Kaade Hospital Rajajinagar, Bangalore. NRV Hospital Vidyanagar, Bangalore. Gurushree Hospital Vijayanagar, Bangalore</p>
                    
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>