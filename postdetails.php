<?php 

session_start();
include ('config2.php');
error_reporting(0);


?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->
<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
       .slider {
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
      background-image: linear-gradient(to right, #ffc107 , #dc3545);
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}

#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}
#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
	font-family:times;
}
img{
	
	width:98%;
}
a{	width:100%;
	display:inline-block;
	position:relative;
	padding:10px 30px;
	text-decoration:none;
	text-transform: uppercase;
	font-weight:500;
	letter-spacing:2px;
	
	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
.card{
	margin-top:30px;
	width:100%;
	display:inline-block;
	position:relative;
	padding:10px 30px;
	text-decoration:none;
	
	font-weight:500;
	font-size:11px;
	letter-spacing:2px;
	border-radius: 40px;
	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
.PostDetails{
  width: 100%;
  overflow: hidden;
}

    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
  <div id="navbar">
  <a class="active" href="/multiapp.php">Chanre</a>
 
</div>

    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       

       <div class="container-fluid">
	   <br><br><br>
            <div class="row">
				
                <div class="col-md-3">
				<?php $dataid = intval($_GET['pid']);
                     $query=mysqli_query($con,"Select * from tblposts where id= $dataid");
          while($rows = mysqli_fetch_array($query))
                       {
                        ?>
				                 <h3><?php echo htmlentities($rows['PostTitle']); ?></h3>
                        <br>
                        <div class="PostDetails">
                          <?php echo ($rows['PostDetails']); ?>
                        </div>
                          
            
							<?php }?>
				
				</div>
				
				<div class="col-md-3">
			
				          
							
				
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-3"></div>
				
            </div>
        </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="jquery.min.js"></script>
<script src="jquery.multiselect.js"></script>
<script>
$('#langOpt').multiselect({
    columns: 1,
    placeholder: 'Select'
});

$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Select',
    search: true
});

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select where you had pain past week',
    search: true,
    selectAll: true
});
$('#langOpt4').multiselect({
    columns: 1,
    placeholder: 'Select where you had swelling past week',
    search: true,
    selectAll: true
});
$('#langOptgroup').multiselect({
    columns: 4,
    placeholder: 'Select',
    search: true,
    selectAll: true
});
</script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
           

</body>

</html>
