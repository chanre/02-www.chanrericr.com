<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr. Nagesh M B</h1></div><br>
             <div class="row">
                 <div class="col-md-3">  <img src="images/doctor/nagesh.jpg" class="card-img" alt="..." height="230"></div>
                 <div class="col-md-9">

                     <p>                     
Dr. Nagesh, a practitioner at Punya Hospital at present, holds an MBBS degree from Ambedkar College as well as a Diploma in Clinical Cardiology from Mahaveer Jain Heart Centre. He specializes in Medical and Surgical treatments for Cardiac Failure, Hypertension, Myocardial Infarction, and Pulmonary Embolism. His treatment procedures include Echo, TMT & Holter Study, Dobutamine Stress Echocardiogram, and other Cardiac emergencies.
                     </p>
                     <p>
                         He is a treasured member of Karnataka Medical College (KMC), Indian Academy of Echocardiography, Cardiological Society of India, Karnataka Chapter (CSI) and Indian Medical Association (IMA). Dr. Nagesh has presented a paper in Belgaum in 2009 on TMT Positive in Normal Individuals. He also consults at Fortis, Bangalore. He has very good understanding of his patients' issues and is very approachable.
                     </p>
                     
                    
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>