<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-158330329-1');
</script>
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Ms.Ruby Malya</h1></div>
             <div class="row">
                 <div class="col-md-3">  <img src="images/doctor/ruby.jpeg" class="card-img" alt="..." height="230"><p></p><br></div><br>
                 <div class="col-md-9">
                 <div align="center"><p><strong>M.Sc Food Science & Nutrition, FSM & Dietetics  </strong></p></div><br>
                     <p>Ms. Ruby Malya is a Dietitian in ChanRe Rheumatology and Immunology Research Centre. She has completed her UG in Nutrition, FSM and Dietetics from Ethiraj college for women and did her PG in Food science and Nutrition from Pondicherry University. She worked as an Dietician (Intern) in two hospitals. 
                     </p>
                     <p>She also worked in one of the research projects in ICAR-NRCB, Trichy. She is a Life time member in IAPEN Indian Association for Parenteral and Enteral Nutrition. She believes in the concept “Let thy food be the medicine and medicine be thy food”. Her area of interest are Gut health and Nutrition, Gut brain connection, Diabetic health and Weight management.<br>
                     </p>
                 </div>
             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->
    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>