<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>COVID vaccine and autoimmune rheumatic disease </title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
	<meta name="description" content="There is no advisory against vaccinating patients with autoimmune diseases, there is no contraindication to vaccinate patients on immunosuppressive drugs, unless they have severe allergic reaction. "/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />

    <!-- Responsive -->
    <link rel="stylesheet" href="css/responsive.css" />

  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-158330329-1');
</script>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
       <div class="container">
        <br>
        <div><h4 align="center">COVID vaccine and autoimmune rheumatic disease </h4></div>
           <div class="row">
		   <div class="col-md-2"></div>
              <div class="col-md-8"><br>
        <h2>Frequently asked questions </h2>
        <h4>1.  Are COVID-19 vaccines safe for autoimmune disease patients?</h4>
			  <p>There is no advisory against vaccinating patients with autoimmune diseases, there is no contraindication to vaccinate patients on immunosuppressive drugs, unless they have severe allergic reaction.</p>
			  
        <h4>2.  What are recent guidelines for vaccine administration in autoimmune rheumatic disease patients?</h4>
			  <p>The Canadian Rheumatology Association provides the following recommendations regarding the COVID-19 vaccine for patients under the care of a rheumatologist:</p>
        <ol>
          <li>Patients >70 years old are considered at high risk for severe illness due COVID-19 and therefore vaccination should be considered, regardless of underlying diagnosis or treatment.</li>
          <li>In those < 70 years of age, vaccination should be considered by assessing all potential risk factors including occupation. Patients on disease modifying anti-rheumatic drugs (DMARDs, synthetic, biologic or small molecules) do not appear to be at higher risk for developing severe illness due COVID-19. </li>
          <li>Vaccination should be considered for patients at higher risk for severe illness with COVID-19, including those on corticosteroids.</li>
          <li>In addition, subjects > 18 years of age are only eligible for the Pfizer BioNTech COVID-19 vaccine, due to limited efficacy data in children < 16 years of age. However, Health Canada’s prioritization permits vaccination of children with rheumatic diseases between 12-15 years of age after thorough case evaluation, if they are at high risk for contracting COVID-19. Moreover, obtaining informed consent is mandatory due to the absence of data in this age group.</li>
          <li>Currently, there is no data recommending the discontinuation of drugs (DMARDs) during COVID-19 vaccination. Studies on influenza vaccination has suggested that withholding 2 doses of methotrexate, following vaccination, improves vaccine response. However, it is not yet proven for the COVID-19 vaccine or other DMARDs. Concerns for potential disease flare should be considered when making such decisions.</li>
        </ol>
			  <br>
			  
        <h4>3. What are the side effects of COVID vaccine?</h4>
			  <p>The COVID 19 vaccine will be safe and effective, but may have minor side effects like fever and pain at the injection site. These effects can happen for any vaccine. There are no major side effects, unless the patients had allergic reaction to previous dose or allergic to the vaccine content</p>

        <h4>4. What are the evidence recommending the effectiveness of COVID vaccine?</h4>
			  <p>Clinical trial have shown the effectiveness in 60 to 90% of the study group. But there is concern over effectiveness of vaccine in patients with unstable disease or those on synthetic and biological DMARDS, because vaccination trial excluded immunosuppressive patients. </p>

        <h4>5. Is COVID 19 vaccine, a live vaccine?</h4>
        <ul>
          <li>PFIZER COVID-19 is not a live vaccine.</li>
          <li>COVAXIN (Indian origin) is a killed vaccine</li>
          <li>COVISHIELD is a live attenuated weakened version of adenovirus containing genetic material of SARS-CoV-2 virus spike protein.</li>
        </ul>
        <h4>6. Are vaccines recommended for people who already contracted the infection or have tested positive for antibodies?</h4>
			  <p>The answer is yes. There are no studies evaluating the duration of immunological memory following natural infection. Though immunity from COVID-19 vaccines is yet to be determined, research shows that vaccine immunity tends to be stronger than natural immunity. </p>

        <h4>7. How many doses of vaccine should be administered? How long does the protection last?</h4>
			  <p>Two doses of vaccine, 28 days apart, need to be taken by an individual to complete the vaccination schedule. At present there is no sufficient data to ensure the duration of immunity, but it is expected to last from months to years.</p>
        <h4>8. Out of the multiple vaccines available, how one  vaccine can be chosen for administration?</h4>
			  <p>The safety and efficacy data from clinical trials of vaccine candidates are examined by the drug regulator of our country before granting the license for the same. Hence, all the COVID-19 vaccines that receive license will have proven safety and efficacy. <br>
 However, it must be ensured that the entire schedule of vaccination is completed by only one type of vaccine, as different COVID-19 vaccines are not interchangeable.
 </p>
 <h4>9. Whether the antibodies develop after receiving first dose, second dose, or much later?</h4>
			  <p>Protective levels of antibodies are generally developed two weeks after receiving the second dose of COVID-19 vaccine.</p>
        <h4>10. Does one need to follow preventive measures such as wearing a mask, hand sanitization, social distancing after receiving the COVID 19 vaccine? </h4>
			  <p>Even after receiving the COVID 19 vaccine, we must continue taking all precautions like use of face cover or masks, hand sanitization and maintain distancing. These behaviours must be followed both at the session site and in general.</p>

        <h4>11. What are the groups to be vaccinated in the first phase?</h4>
        <p>Based on the potential availability of vaccines the Government of India has selected the priority groups who will be vaccinated based on their risk status. 
The first group includes healthcare workers because they are at high risk of contracting the infection and protecting them helps to sustain essential health services. The vaccination of frontline workers will help in reducing the societal and economic impact by reducing COVID-19 mortalities.
 The next group to receive COVID 19 vaccine will be persons over 50 years of age and persons under 50 years with comorbid conditions considering their high mortality risk.
Patients with autoimmune rheumatic disease on corticosteroids are considered to be at high-risk of COVID-19 illness.  
</p>
<p>Source: Ministry of Family Health and Welfare govt of India, Canada rheumatology Association
Autoimmune rheumatic diseases include SLE, rheumatoid arthritis, scleroderma, Sjogren’s syndrome, and primary vasculitis 
</p>
			
			  </div>
           </div>
           <br>

  
           <br>
       </div>
        <?php include('layout/footer.php'); ?>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>