<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Influence of Diet on Obesity </title>
    <link rel="shortcut icon" href="images1/Refined/chanre1.png" />
    <meta name="description"
        content="There is no advisory against vaccinating patients with autoimmune diseases, there is no contraindication to vaccinate patients on immunosuppressive drugs, unless they have severe allergic reaction. " />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />

    <!-- Responsive -->
    <link rel="stylesheet" href="css/responsive.css" />

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-158330329-1');
    </script>
    <style>
    .postImage {

        display: flex;
        justify-content: center;

    }

    .postImage img {
        width: 50%;
        height: 350px;
        margin: 10px;

    }
    </style>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>

<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
        <?php include ('layout/header.php'); ?>
        <div class="container">
            <br>
            <div>
                <h4 align="center">Influence of Diet on Obesity</h4>
            </div>

            <br>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <p>Being obese or overweight is linked to dietary habits, physical activity, lifestyle
                        modifications, and genetics. According to WHO, obesity or being overweight is defined as
                        abnormal or excessive fat accumulation that impairs health. Nearly 13% of the world's adult
                        population is obese. It leads to many comorbidities such as CVD, Type 2 Diabetes, Hypertension,
                        Autoimmune diseases, and Cancer. Poor diet and physical activity play a major role in fat
                        accumulation leading to weight gain.</p>
                    <div class="postImage">
                        <img src="img/img1.png" alt="">
                    </div>
                    <h3>“Amount of calories people eat or drink has an impact on their weight”</h3>
                    <p>An ideal weight is maintained when you consume the same amount of calories as your body burns.
                        When you consume more calories than the body can burn, it leads to weight gain. Then what about
                        the type of calorie you eat? Should we take calories from one specific nutrient like fats,
                        carbohydrates or protein? Is there any specific food? Is there any specific diet? The answers to
                        all these questions have been briefly explained below.</p>
                    <div class="postImage">
                        <img src="img/img2.png" alt="">
                    </div>
                    <p>There is a handful of research on foods and eating patterns for diabetes, CVD, and other disease
                        conditions. Healthy diets consisting of vegetables, greens, and whole grains along with
                        controlling disease conditions help in maintaining a healthy weight. Similarly, foods like
                        carbonated drinks, processed and packed foods, and fast foods increase the incidence of
                        non-communicable diseases along with weight gain. In order to lose weight, calorie restriction
                        should be implemented and the calorie you consume should be from a healthier source for overall
                        benefits without any ill effects.</p>
                    <h3>Macronutrients and weight</h3>
                    <p>Carbohydrates, protein, and fat are the macronutrients that provide calories to our body. The
                        quality of carbohydrates and fat plays a major role in weight control. Consuming more complex
                        carbohydrates in the form of whole grains like millets and unrefined cereals helps in
                        controlling weight. Restricting the intake of deep-fried foods and oily foods is helpful as they
                        are loaded with trans fats and saturated fats, which are the major contributing factors to fat
                        accumulation in the body. Complete restriction of fat is also not recommended, as it may lead to
                        fat-soluble vitamin deficiency. A good source of fat like oily seeds and nuts is recommended,
                        which also gives the feeling of a fuller stomach. Similarly, a good amount of protein should be
                        included in your diet, as they give you the feeling of fuller (increased satiety) and helps in
                        increasing lean body mass. Another advantage is that it takes more energy to metabolise protein
                        compared to other macronutrients. Fish, poultry, nuts and beans are the recommended protein
                        sources instead of processed and red meats.</p>
                    <h2>Influence of special foods on weight</h2>
                    <h3>Fruits and vegetables</h3>
                    <div class="postImage">
                        <img src="img/img3.png" alt="">
                    </div>
                    <p>Fruits and vegetables play a major role in reducing weight, as they are rich sources of dietary
                        fibre. Research shows that dietary fibre helps in losing weight and reducing the incidence of
                        other disease conditions. Dietary fibre increases the feeling of fullness and reduces the
                        appetite, hence you will end up eating little as your cravings are minimised. Another advantage
                        of fibre is that it traps the fats and sugars in blood and eliminates it from the body. When you
                        take a surplus amount of vegetables and fruits, you will be meeting all the daily requirements
                        of vitamins and minerals. This will benefit the overall health of an individual. Try to include
                        seasonal vegetables, greens, and whole fruits (not fruit juices) in your diet.</p>
                    <h3>Nuts and seeds</h3>
                    <div class="postImage">
                        <img src="img/img4.png" alt="">
                    </div>
                    <p>There was a taboo that nuts and seeds promote weight gain, as they are packed with calories. The
                        truth is that it is loaded with proteins and good fats, which help you to keep your stomach
                        fuller and curb your hunger. Good fats like HDL help to decrease bad fats like LDL and
                        Cholesterol level in blood. Almonds, walnuts, and unsalted pistachios can be included as a snack
                        option for your weight loss diet.</p>
                    <h2>Foods to restrict if you want to lose weight</h2>
                    <h3>Carbonated sugar-sweetened beverages</h3>
                    <div class="postImage">
                        <img src="img/img5.png" alt="">
                    </div>
                    <p>Consumption of carbonated beverages leads to weight gain. Evidence shows that carbohydrates in
                        liquid form contribute to less satiety and increased food consumption compared to solid form.
                        This results in gaining weight. They do not contain any other nutrients other than sugars.</p>
                    <h3>Alcohol consumption</h3>
                    <div class="postImage">
                        <img src="img/img6.png" alt="">
                    </div>
                    <p>Evidence shows that regular consumption of alcohol leads to weight gain. This is because it has
                        too many calories, but keeps the satiety level low. This leads to cravings for salty and greasy
                        foods. It also increases hunger, so people end up eating more. Apart from weight reduction,
                        consumption of alcohol leads to other life-threatening diseases. So, it is better if we avoid
                        alcohol intake.</p>
                    <h3>Processed and Fast foods</h3>
                    <div class="postImage">
                        <img src="img/img7.png" alt="">
                    </div>
                    <p>People find it easy to consume processed and fast foods in this busy world. These foods are
                        loaded with salts and sugars to preserve them for a longer time. These salts and sugars have a
                        negative impact on people's health like insulin resistance, swelling, and renal issues.
                        Processed and fast foods are loaded with trans-fat and saturated fatty acids, which build up the
                        fat cells in the body. It is recommended to restrict the consumption of fast foods and processed
                        foods.

                        Following a calorie-deficit diet by including proteins, fiber-rich vegetables, and fruits helps
                        in losing weight. Along with diet, lifestyle modifications like regular physical activity and a
                        stress-free mind play a significant role in weight reduction. Always consult a dietician before
                        starting any weight loss diet, so they can customise the diet plan according to your body's
                        condition.
                    </p>
                </div>
            </div>

            <br>
        </div>
        <?php include('layout/footer.php'); ?>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>

</body>

</html>