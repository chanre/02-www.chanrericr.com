<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$did=$_GET['pid'];
$result = mysqli_query($con, "DELETE FROM tbl_users WHERE id=$did");
?>

<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <div id="page-container">
           <?php include('layout/header.php'); ?>
            <div id="inner-container">
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    
                   <?php include ('layout/primary-nav.php'); ?>
                </aside>
                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-sub-admin.php">View Department</a></li>
                    </ul>
               

                     <div class="form-horizontal form-box remove-margin">
                            
                            <table id="example-datatables2" class="table table-striped table-bordered table-hover">
                                <h4 class="form-box-header">Department Admin</h4>
                                <thead>
                                    <tr>
                                        <th class="text-center">Full Name</th>
                                        <th><i class="fa fa-user"></i>username</th>
                                        <th><i class="fa fa-mobile"></i> Contact</th>
                                        <th><i class="fa fa-envelope"></i> Email</th>
                                        <th><i class="fa fa-group"></i>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php

                                     include('layout/config.php');
                                      $query=mysqli_query($con,"SELECT * from tbl_users where user_role=1");
                                        while ($rows=mysqli_fetch_array($query)) {
                                         ?>
                                    <tr>
                                        <td class="text-center"><?php echo htmlentities($rows['name']) ?></td>
                                        <td><?php echo htmlentities($rows['user_name']) ?></td>
                                        <td><?php echo htmlentities($rows['user_contact']) ?></td>
                                        <td><?php echo htmlentities($rows['user_email']) ?></td>
                                        <td><?php echo htmlentities($rows['user_dept']) ?></td>
                                        <td><a href="view-department-admin.php?pid=<?php echo htmlentities($rows['id']); ?>" class="btn btn-danger btn-sm">Delete</a></td>
                                    </tr>
                                    <?php } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
            </div>
        </div>
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function () {
                /* Initialize Datatables */
                $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
                $('#example-datatables2').dataTable();
                $('#example-datatables3').dataTable();
                $('.dataTables_filter input').attr('placeholder', 'Search');
            });
        </script>
    </body>
</html>
<?php } ?>