<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>

<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">

        <title>Admin Dashboard</title>

        <meta name="description" content="uAdmin is a Professional, Responsive and Flat Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>

    <!-- Add the class .fixed to <body> for a fixed layout on large resolutions (min: 1200px) -->
    <!-- <body class="fixed"> -->
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
           <?php include('layout/header.php'); ?>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                  
                    <!-- END Sidebar search -->

                    <!-- Primary Navigation -->
                   <?php include ('layout/primary-nav.php'); ?>
                    <!-- END Primary Navigation -->

                    <!-- Demo Theme Options -->
                    
                    <!-- END Demo Theme Options -->
                </aside>
                <!-- END Sidebar -->

                <!-- Page Content -->
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="">Dashboard</a></li>
                    </ul>
                    <!-- END Navigation info -->

                    <!-- Nav Dash -->
                    <!-- <ul class="nav-dash">
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Users" class="animation-fadeIn">
                                <i class="fa fa-user"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Comments" class="animation-fadeIn">
                                <i class="fa fa-comments"></i> <span class="badge badge-success">3</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Calendar" class="animation-fadeIn">
                                <i class="fa fa-calendar"></i> <span class="badge badge-inverse">5</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Photos" class="animation-fadeIn">
                                <i class="fa fa-camera-retro"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Projects" class="animation-fadeIn">
                                <i class="fa fa-paperclip"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Tasks" class="animation-fadeIn">
                                <i class="fa fa-tasks"></i> <span class="badge badge-warning">1</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Reader" class="animation-fadeIn">
                                <i class="fa fa-book"></i>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" data-toggle="tooltip" title="Settings" class="animation-fadeIn">
                                <i class="fa fa-cogs"></i>
                            </a>
                        </li>
                    </ul> -->
                    <!-- END Nav Dash -->

                    <!-- Tiles -->
                    <!-- Row 1 -->
                  <?php include('layout/dashboard.php') ?>
                    <!-- END Row 1 -->

                    <!-- Row 2 -->
                   
                    <!-- END Row 3 -->
                    <!-- END Tiles -->
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>

        <!-- User Modal Settings, appears when clicking on 'User Settings' link found on user dropdown menu (header, top right) -->
       
        <!-- END User Modal Settings -->

        <!-- Excanvas for canvas support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Javascript code only for this page -->
      
    </body>
</html>
<?php } ?>