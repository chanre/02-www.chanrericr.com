<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin'||user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];


?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <div id="page-container">
           <?php include('layout/header.php'); ?>
            <div id="inner-container">
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    
                   <?php include ('layout/primary-nav.php'); ?>
                </aside>
                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="view-users.php">View Users</a></li>
                    </ul>
               

                     <div class="form-horizontal form-box remove-margin">
                            
                            <table id="example-datatables2" class="table table-striped table-bordered table-hover">
                                <h4 class="form-box-header">Users Table</h4>
                                <thead>
                                    <tr>
                                        <th class="text-center">Full Name</th>
                                        <th><i class="fa fa-user"></i>username</th>
                                        <th><i class="fa fa-mobile"></i> Contact</th>
                                        <th><i class="fa fa-envelope"></i> Email</th>
                                        <th><i class="fa fa-group"></i>Department</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                      $query=mysqli_query($con,"SELECT * from tbl_users WHERE  user_dept='$dept' AND user_role='0'");
                                        while ($rows=mysqli_fetch_array($query)) {
                                         ?>
                                    <tr>
                                        <td class="text-center"><?php echo htmlentities($rows['name']) ?></td>
                                        <td><?php echo htmlentities($rows['user_name']) ?></td>
                                        <td><?php echo htmlentities($rows['user_contact']) ?></td>
                                        <td><?php echo htmlentities($rows['user_email']) ?></td>
                                        <td><?php echo htmlentities($rows['user_dept']) ?></td>
                                        <td><a href="#" class="btn btn-danger btn-sm">Delete</a></td>
                                    </tr>
                                    <?php } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
            </div>
        </div>
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function () {
                /* Initialize Datatables */
                $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
                $('#example-datatables2').dataTable();
                $('#example-datatables3').dataTable();
                $('.dataTables_filter input').attr('placeholder', 'Search');
            });
        </script>
    </body>
</html>
<?php } ?>