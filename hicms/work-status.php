<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
$sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name'];


?>
<!DOCTYPE html>

<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
           <?php include('layout/header.php'); ?>
            <!-- END Header -->

            <!-- Inner Container -->
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                   
                    <!-- END Sidebar search -->

                    <!-- Primary Navigation -->
                   <?php include ('layout/primary-nav.php'); ?>
                    <!-- END Primary Navigation -->
                </aside>
                <!-- END Sidebar -->

                <!-- Page Content -->
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="admindashboard.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-users.php">Assign Work</a></li>
                    </ul>
                    <!-- END Navigation info -->

                 <form  method="POST" action="code.php" class="form-horizontal form-box remove-margin">
                        <!-- Form Header -->
                        <!-- <h4 class="form-box-header">Assign Work</h4> -->

                        <!-- Form Content -->
                        <div class="form-box-content">
                             <table id="example-datatables2" class="table table-striped table-bordered table-hover">
                                <h4 class="form-box-header">Work Status</h4>
                                <thead>
                                    <tr>
                                        <th class="text-center">Dept Admin</th>
                                        <th>User name</th>
                                        <th>Work</th>
                                        <th>Assigned Date</th>
                                        <th>Complete Date</th>
                                        <th>Department</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                      $query=mysqli_query($con,"SELECT * from tbl_work WHERE  dept_name='$dept'");
                                        while ($rows=mysqli_fetch_array($query)) {
                                            $dt = new DateTime($rows['work_assigned']);
                                            $cd = new DateTime($rows['work_done']); 
                                         ?>
                                    <tr>
                                        <td class="text-center"><?php echo htmlentities($rows['dept_admin']) ?></td>
                                        <td><?php echo htmlentities($rows['user_name']); ?></td>
                                        <td><?php echo htmlentities($rows['work_name']); ?></td>
                                        <td><?php echo $dt->format('d-m-Y'); ?></td>
                                        <td><?php echo $cd->format('d-m-Y'); ?></td>
                                        <td><?php echo htmlentities($rows['dept_name']); ?></td>
                                        <td>
                                            <?php if($rows['work_status']==0){?>
                                               <a href="" class="btn btn-danger">Pending</a>
                                            <?php }else if($rows['work_status']==1){ ?>
                                                <a href="" class="btn btn-success">Completed</a>
                                            <?php }else if($rows['work_status']==2){  ?>
                                                <a href="" class="btn btn-primary round">Ongoing</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- END Form Content -->
                    </form>
               
                </div>
                <!-- END Page Content -->

                <!-- Footer -->
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
                <!-- END Footer -->
            </div>
            <!-- END Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
       
        <!-- Javascript code only for this page -->
        <script>
            $(function () {
                /* Initialize Datatables */
                $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
                $('#example-datatables2').dataTable();
                $('#example-datatables3').dataTable();
                $('.dataTables_filter input').attr('placeholder', 'Search');
            });
        </script>
    </body>
</html>
<?php } ?>