<?php
session_start();
include('layout/config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <title>Add Department Admin - Hospital Intra Communication</title>
        <meta name="description" content="Add Sub-Admin - Hospital Intra Communication">
        <meta name="author" content="Web developer">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
        <div id="page-container">
           <?php include('layout/header.php'); ?>
            <div id="inner-container">
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    
                   <?php include ('layout/primary-nav.php'); ?>
                </aside>
                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="add-sub-admin.php">Add Department</a></li>
                    </ul>
                 <form id="form-validation" method="post" action="code.php" class="form-horizontal form-box remove-margin">
                        <h4 class="form-box-header">Add Department</h4>
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_username">Department Name *</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group fa-fw"></i></span>
                                        <input type="text" id="val_username" name="dept_name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="val_date">Date *</label>
                                <div class="col-md-4">
                                    <div class="input-group date input-datepicker" data-date="09-30-2021" data-date-format="mm-dd-yyyy">
                                        <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                        <input type="text" id="val_date" name="dept_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-4">
                                    <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <br>
                        <div class="form-horizontal form-box remove-margin">
                            
                            <table id="example-datatables2" class="table table-striped table-bordered table-hover">
                                <h4 class="form-box-header">Departments</h4>
                                <thead>
                                    <tr>
                                        <th class="text-center">Department id</th>
                                        <th><i class="fa fa-user"></i> Department Name</th>
                                        <th><i class="fa fa-bolt"></i> Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php

                                     include('layout/config.php');
                                      $query=mysqli_query($con,"SELECT * from tbl_dept");
                                        while ($rows=mysqli_fetch_array($query)) {
                                         ?>
                                    <tr>
                                        <td class="text-center"><?php echo htmlentities($rows['dept_id']) ?></td>
                                        <td><?php echo htmlentities($rows['dept_name']) ?></td>
                                        <td><span class="label label-success">Active</span></td>
                                    </tr>
                                    <?php } ?>
                                   
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
               
                <footer>
                    Copyright &copy; <strong>Hospital Intra Communication</strong>
                </footer>
            </div>
        </div>
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function () {
                /* Initialize Datatables */
                $('#example-datatables').dataTable({columnDefs: [{orderable: false, targets: [0]}]});
                $('#example-datatables2').dataTable();
                $('#example-datatables3').dataTable();
                $('.dataTables_filter input').attr('placeholder', 'Search');
            });
        </script>
    </body>
</html>
<?php } ?>