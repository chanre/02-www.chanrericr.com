<?php $sub_admin=$_SESSION['login'];
$query2=mysqli_query($con,"select * from tbl_users where user_email = '$sub_admin' || user_name='$sub_admin'");
$rows2=mysqli_fetch_array($query2);
$dept=$rows2['user_dept'];
$uname=$rows2['user_name']; ?>

  <div class="dash-tiles row">
                        <!-- Column 1 of Row 1 -->
                        <div class="col-sm-3">
                            <!-- Total Users Tile -->
                            <div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Manage Users"><i class="fa fa-eye"></i></a>
                                           
                                        </div>
                                    </div>
                                    Users
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
                                <div class="dash-tile-text">
                                    <?php 
                                    $sql="SELECT * FROM tbl_users where user_dept='$dept'";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?>
                                </div>
                            </div>
                            <!-- END Total Users Tile -->

                            <!-- Total Profit Tile -->
                            <div class="dash-tile dash-tile-leaf clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <span class="dash-tile-options">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="popover" data-placement="top" data-content="$500 (230 Sales)" title="View Department"><i class="fa fa-eye"></i></a>
                                    </span>
                                    Department
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-money"></i></div>
                                <div class="dash-tile-text">
                                    <?php 
                                    $sql="SELECT * FROM tbl_dept";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?>
                                </div>
                            </div>
                            <!-- END Total Profit Tile -->
                        </div>
                        <!-- END Column 1 of Row 1 -->

                        <!-- Column 2 of Row 1 -->
                        <div class="col-sm-3">
                            <!-- Total Sales Tile -->
                            <div class="dash-tile dash-tile-flower clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="View Ongoing Work"><i class="fa fa-eye"></i></a>
                                            
                                        </div>
                                    </div>
                                    Ongoing Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-tags"></i></div>
                                <div class="dash-tile-text">
                                    <?php 
                                    $sql="SELECT * FROM tbl_work where work_status = 2 AND dept_name='$dept'";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?>
                                </div>
                            </div>
                            <!-- END Total Sales Tile -->

                            <!-- Total Downloads Tile -->
                            <div class="dash-tile dash-tile-fruit clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="View Pending Works"><i class="fa fa-eye"></i></a>
                                    </div>
                                    Pending Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-cloud-download"></i></div>
                                <div class="dash-tile-text"><?php 
                                    $sql="SELECT * FROM tbl_work where work_status = 0 AND dept_name='$dept'";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?></div>
                            </div>
                            <!-- END Total Downloads Tile -->
                        </div>
                        <!-- END Column 2 of Row 1 -->

                        <!-- Column 3 of Row 1 -->
                        <div class="col-sm-3">
                            <!-- Popularity Tile -->
                            <div class="dash-tile dash-tile-oil clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <div class="btn-group">
                                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="What's changed?"><i class="fa fa-eye"></i></a>
                                           
                                        </div>
                                    </div>
                                    Completed Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
                                <div class="dash-tile-text"><?php 
                                    $sql="SELECT * FROM tbl_work where work_status = 1 AND dept_name='$dept'";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?></div>
                            </div>
                            <!-- END Popularity Tile -->

                            <!-- Server Downtime Tile -->
                            <div class="dash-tile dash-tile-dark clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Monthly report"><i class="fa fa-eye"></i></a>
                                    </div>
                                    Complaint Box
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-hdd-o"></i></div>
                                <div class="dash-tile-text"><?php 
                                    $sql="SELECT * FROM tbl_complaint where dept_name='$dept'";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?></div>
                            </div>
                            <!-- END Server Downtime Tile -->
                        </div>
                        <div class="col-sm-3">
                            <!-- RSS Subscribers Tile -->
                            <div class="dash-tile dash-tile-balloon clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="View Pending Work"><i class="fa fa-eye"></i></a>
                                    </div>
                                    Pending Work
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-rss"></i></div>
                                <div class="dash-tile-text">0</div>
                            </div>
                            <!-- END RSS Subscribers Tile -->

                            <!-- Total Tickets Tile -->
                            <div class="dash-tile dash-tile-doll clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    <div class="dash-tile-options">
                                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Open tickets"><i class="fa fa-eye"></i></a>
                                    </div>
                                    Total Tickets
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-wrench"></i></div>
                                <div class="dash-tile-text">
                                    <?php 
                                    $sql="SELECT * FROM tbl_complaint where dept_name='$dept' and res_person='$uname'";
                                            if ($result=mysqli_query($con,$sql))
                                              {
                                              $rowcount=mysqli_num_rows($result);
                                              printf("%d\n",$rowcount);
                                              mysqli_free_result($result);
                                              }
                                    ?>
                                </div>
                            </div>
                            <!-- END Total Tickets Tile -->
                        </div>
                        <!-- END Column 4 of Row 1 -->
                    </div>