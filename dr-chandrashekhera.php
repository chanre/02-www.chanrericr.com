<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Dr.Chandrashekara .S- ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158330329-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158330329-1');
</script>
  <style type="text/css">
      p{
        font-family: times;
        font-size: 18px;
        line-height: 30px;
      }
  </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       <?php include ('layout/header.php'); ?>
         <div class="container">
            <br>
            <div align="center"><h1 style="font-weight: bold;color: royalblue;">Dr. Chandrashekara S.</h1></div><br>
             <div class="row">
                 <div class="col-md-3"> <img src="images/doctor/Chandrashekara.jpg" class="card-img" alt="..."></div>
                 <div class="col-md-9">

                     <p>
                         <strong>MBBS,MD,DM ( Clinical Immunology & Rheumatology) </strong>
Dr. Chandrashekara. S is a renowned rheumatologist and Immunologist with over 20 years of consultant grade experience and expertise. He received his MD from Mysore Medical college, Karnataka and DNB in General Medicine from New Delhi, India. He subsequently pursued DM in Clinical Immunology and Rheumatology from Sanjay Gandhi Postgraduate institute of Medical Sciences, Lucknow.
                     </p>
                     <p>
                        Dr. Chandrashekara started his career as Assistant Professor and Consultant Immunologist in M. S. Ramaiah Medical College, Bangalore and played a key role in establishing the Dept. of Immunology & Rheumatology in the institute. He got promoted to the designation of Professor in June 2006 for his pioneering and active research work in the field of Immunology & Rheumatology. In order to fulfill his vision, he established ChanRe Rheumatology & Immunology Center & Research, a specialized one-stop center providing care and treatment for diverse autoimmune diseases, in the year 2002. He has been awarded with ‘Sir. C. V. Raman Young Scientist State Award in the field of Medical Sciences’, by the Govt. of Karnataka for the year 2013.
                     </p>
                     <p>
                         He has 92 publications in international and national journals, more than 40 papers in Conferences / seminars, 14 reference books in Rheumatology & 9 patients education books and he has to his credit many chapters written in reference books and several articles in newsletters. He has also conducted CMEs and training programs for general practitioners, physicians and orthopaedicians. Several research articles authored and co-authored by him are published in several national and international publications.
                     </p>
                       <p>He is a life member of the Indian Rheumatology Association (IRA) for the past 22 years and currently he is an EC member, for the past 6 years. He is also a member of Association of Physicians of India (API). He is one of the promoters of the Society of Inflammation Research (SIR) and currently holds the post of General Secretary. He is the Managing Trustee of The Immunology & Arthritis Research & Education Trust (IARET).In association with other rheumatologists, he was involved in organizing Arthritis Awareness Walkathon and Insurance Initiative for Rheumatological Diseases with correspondence with State Government and Prime Minister’s office with the recent directions from the PM’s office for the inclusion of Arthritis Conditions in the package list of NHPS. All his energies are directed for the wholesome development of Rheumatology Care, Research and Knowledge Dissemination.</p>
                 </div>

             </div>
         </div>
        <?php include('layout/footer.php'); ?>
        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="js/jquery.min.js"></script>
    
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>


    <script src="js/bootstrap.min.js"></script>

    
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>

</body>

</html>