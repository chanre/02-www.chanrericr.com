<?php 

session_start();
include('config.php');
error_reporting(0);


?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>ChanRe Rheumatology and Immunology Center and Research</title>
    <link rel="icon" type="image/png" href="images/doctor/nopics.jpg" sizes="16x16">
<!-- Custom Theme files -->

<link href="css/wickedpicker.css" rel="stylesheet" type='text/css' media="all" />
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Font Awesome CSS -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Style CSS -->
    <link rel="stylesheet" href="css/style.css" />
       <link rel="stylesheet" href="css/doctor.css" />

    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css" />

    <style type="text/css">
       body{
        background-color: #e7eaf7;
       }
       .slider {
  -webkit-appearance: none;
  width: 100%;
  height: 25px;
      background-image: linear-gradient(to right, #ffc107 , #dc3545);
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider:hover {
  opacity: 1;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  background: #4CAF50;
  cursor: pointer;
}



#navbar {
  overflow: hidden;
  background-color: #333;
  z-index: 1;
}

#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

#navbar a:hover {
  background-color: #ddd;
  color: black;
}

#navbar a.active {
  background-color: #4CAF50;
  color: white;
}

.content {
  padding: 16px;
}

.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

.sticky + .content {
  padding-top: 60px;
}
div{
	font-family:times;
}
img{
	
	width:98%;
}
a{	width:100%;
	display:inline-block;
	position:relative;
	padding:10px 30px;
	text-decoration:none;
	text-transform: uppercase;
	font-weight:500;
	letter-spacing:2px;
	
	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}
.card{
	margin-top:30px;
	width:100%;
	display:inline-block;
	position:relative;
	padding:10px 30px;
	text-decoration:none;
	
	font-weight:500;
	font-size:11px;
	letter-spacing:2px;
	border-radius: 40px;
	box-shadow: -2px -2px 8px rgb(255,255,255,1),-2px -2px 12px rgb(255,255,255,0.5), inset 2px 2px 4px rgb(255,255,255,0.1),2px 2px 4px rgb(0,0,0,0.15);
}

    </style>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
  <div id="navbar">
  <a class="active" href="/multiapp.php">Chanre</a>
 
</div>

    <!-- Begin wrapper -->
    <div class="wrapper">
        <!-- Begin Top Navbar -->
       

       <div class="container-fluid">
	   <br><br><br>
            <div class="row">
				
                <div class="col-md-3">
				
				  <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="images/ana.png" alt="card image"></p>
                                    <h4 class="card-title">Not all ANA positivity is lupus</h4>
                                     <p>In the current era of information being made accessible at fingertips, health awareness has increased tremendously. This has become a double-edged sword. A lot of individuals believe that every headache could be a sign of brain tumour. Similarly, in autoimmune diseases, a positive antinuclear antibody (ANA) test is always speculated as  lupus by patients.</p>
			  
			  <p>ANA test measures the presence of autoantibodies against nuclear antigens. Antigen is a whole or part of protein that is capable of eliciting an immune response. It can originate from an external source ( e.g. bacteria, virus or parasites) or internally within the body. Certain proteins present in our body are sometimes mistakenly recognised by our immune system as potentially dangerous and mounts an immune response to these proteins/antigens. 
			  
			  </p>
	
			 <br>
			  <p>There are certain types of white blood cells (WBC) called B cells, which produce antibodies against such antigens. Such antibodies  produced against self-antigens are called autoantibodies. An ANA test measures the level of these antibodies and it is performed by several methods. The most popular method used is immunofluorescence (IF) and it is very simple, sensitive and specific. It is performed in dilutions and generally reported as 1:40, 1:80, 1:160, and 1:640. It can also identify the pattern of immunofluorescence produced which is reported as speckled, homogenous etc.</p>
			  <p>The production of autoantibodies is occurring regularly in almost all individuals. However,this process is kept under check by the constant surveillance of regulatory immune cells by the body and hence the presence of small amounts of autoantibodies is considered as normal.</p>
			  <p>It should not be the cause of alarm, unless accompanied by symptoms. ANA is positive in almost 5% of normal population. The chance of ANA test being positive increases gradually with advancement of age and it could be upto 37% in healthy individuals who are > 65 years of age. </p>
			  <p>Presence of larger amounts of ANA could be suggestive of an autoimmune disease, not essentially lupus. Other conditions that can have a positive ANA include mixed connective tissue disorder, Sjogren’s syndrome, scleroderma, myositis (polymyositis/dermatomyositis), and undifferentiated connective tissue disorder. It can also be positive in certain organ specific autoimmune conditions like Hashimotos thyroiditis (a thyroid disease), myasthenia gravis (a muscle disease),autoimmune hepatitis etc. Certain medications can also induce production of autoantibodies like sulfadiazine, procainamide, isoniazid, methyldopa, quinidine, minocycline etc. Rarely, certain infections or cancer types can also produce ANA positivity. </p>
			  <p>Presence of ANA does not translate to having an autoimmune disease. It is just one piece of the jigsaw puzzle that your doctor is trying to put in place. The diagnosis of an autoimmune condition is based on many factors including the symptoms, signs observed, and other blood investigations, apart from positive ANA. ANA test certainly helps in ruling out many conditions. </p>
			  <p>Presence of ANA should not be the cause of panic, if the test turns out to be positive. In fact, it is not included in the routine health check-ups offered by many laboratories. Your clinician does not ask for an ANA test, unless there are certain pointers towards an autoimmune condition. This test is mostly used to confirm the possibility of an autoimmune cause.</p>
			  <p>Feel free to discuss with your healthcare provider about any concerns you might have before taking the test.</p>
                                </div>
                            </div>
							
				
				</div>
				
				<div class="col-md-3">
			
				          
							
				
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-3"></div>
				
            </div>
        </div>

        <!-- End Footer Wrapper -->
    </div>
    <!-- End wrapper -->

    <!-- jquery latest version -->
    <script src="jquery.min.js"></script>
<script src="jquery.multiselect.js"></script>
<script>
$('#langOpt').multiselect({
    columns: 1,
    placeholder: 'Select'
});

$('#langOpt2').multiselect({
    columns: 1,
    placeholder: 'Select',
    search: true
});

$('#langOpt3').multiselect({
    columns: 1,
    placeholder: 'Select where you had pain past week',
    search: true,
    selectAll: true
});
$('#langOpt4').multiselect({
    columns: 1,
    placeholder: 'Select where you had swelling past week',
    search: true,
    selectAll: true
});
$('#langOptgroup').multiselect({
    columns: 4,
    placeholder: 'Select',
    search: true,
    selectAll: true
});
</script>
    <!-- popper.min js -->
    <script src="js/popper.min.js"></script>

    <!-- bootstrap.min js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- ie10 viewport bug-workaround js -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <!-- custom js -->
    <script src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/wickedpicker.js"></script>
            <script>
window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}
</script>
<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>

 <script type="text/javascript"> window.onload = function() {
  // provs is an object but you can think of it as a lookup table
  var provs = {
        'Select':[''],
        'Shoulder': ['Right','Left','Both'],
        'Elbow': ['Right','Left','Both'],
        'Wrist':['Right','Left','Both'],
        'Hand':['Right','Left','Both'],
        'Finger':['Right','Left','Both'],
        'Knee':['Right','Left','Both'],
        'Ankle':['Right','Left','Both'],
        'Toes':['Right','Left','Both'],
         
      },
      // just grab references to the two drop-downs
      prov_select = document.querySelector('#prov'),
      town_select = document.querySelector('#town');

  // populate the provinces drop-down
  setOptions(prov_select, Object.keys(provs));
  // populate the town drop-down
  setOptions(town_select, provs[prov_select.value]);
  
  // attach a change event listener to the provinces drop-down
  prov_select.addEventListener('change', function() {
    // get the towns in the selected province
    setOptions(town_select, provs[prov_select.value]);
  });
    
  function setOptions(dropDown, options) {
    // clear out any existing values
    dropDown.innerHTML = '';
    // insert the new options into the drop-down
    options.forEach(function(value) {
      dropDown.innerHTML += '<option name="' + value + '">' + value + '</option>';
    });
  }  
};</script>

</body>

</html>
